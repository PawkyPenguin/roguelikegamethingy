package entities;

public abstract class AttackTimer {
	
	public abstract boolean countDown(double time);
}
