package entities;

import view.SpriteCompendium;

public class Blank extends Foreground {

	public Blank(int x, int y) {
		super(x, y);
		solid = false;
		setWalkingCost();
	}

	@Override
	protected void setWalkingCost() {
		walkingCost = 0;
	}

	@Override
	public void setSprites() {
		setIdleAnimation(SpriteCompendium.getFloorSprites().getAnimation("blank"));
	}

	@Override
	public void removalAction() {
	}
}
