package entities;

import model.Direction;
import model.EnumDirection;

public class Path {
	private EnumDirection dir;
	private Coordinate position;
	
	public Path(){}
	
	public void setStart(Coordinate s){
		position = s;
	}
	
	public void setDirection(EnumDirection d){
		dir = d;
	}
	
	public Coordinate incrementPath(){
		position = Direction.getFieldInDirection(position, dir);
		return position;
	}

}
