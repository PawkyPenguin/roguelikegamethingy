package entities;

import view.SpriteCompendium;

public class FloorTile extends Background {
	public FloorTile(int x, int y) {
		super(x, y);
		solid = false;
		setWalkingCost();
	}

	@Override
	protected void setWalkingCost() {
		walkingCost = 0;
	}

	@Override
	public void setSprites() {
		setIdleAnimation(SpriteCompendium.getFloorSprites().getAnimation("stoneFloor"));
	}
}