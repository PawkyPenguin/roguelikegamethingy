package entities;

public class ActionNoop extends Action {

	public ActionNoop(Living actor, Entity actee) {
		super(actor, actee);
	}

	@Override
	protected boolean tryExecute() {
		return true;
	}

	@Override
	protected boolean execute() {
		return false;
	}

}
