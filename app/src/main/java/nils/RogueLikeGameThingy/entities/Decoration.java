package entities;

import java.awt.image.BufferedImage;

import view.Animation;

public abstract class Decoration {
	protected Animation animation;

	public Decoration() {
		setSprites();
	}

	public Animation getAnimation() {
		return animation;
	}

	protected abstract void setSprites();

	protected void tick(double timeSinceLastFrame) {
		animation.tick(timeSinceLastFrame);
	}

	public BufferedImage getCurrentFrame() {
		return animation.getCurrentFrame();
	}
}
