package entities;

import java.awt.image.BufferedImage;

import item.Item;
import model.EnumEnemyClasses;
import model.HealthBar;
import model.HealthBuffer;
import view.HealthBarView;

public abstract class Attackable extends Foreground implements Interactable {
	boolean dead;
	protected Item drop;
	private HealthBar healthBar;
	protected boolean canBeAttacked; // true by default.
	protected EnumEnemyClasses enemyClass;

	public Attackable() {
		super();
		setType();
	}

	public Attackable(int x, int y) {
		super(x, y);
		setType();
	}

	public EnumEnemyClasses getType() {
		return enemyClass;
	}

	public abstract void setDrop();

	public boolean isVisibleInPenumbraTo(Living liv) {
		return enemyClass == EnumEnemyClasses.NEUTRAL_OBJECT || enemyClass == liv.getType();
	}

	@Override
	protected void setStandardAttributes() {
		super.setStandardAttributes();
		dead = false;
		canBeAttacked = true;
	}

	protected abstract void setType();

	public boolean canBeAttacked() {
		return canBeAttacked;
	}

	protected void initializeHealthBar(int maxHealth) {
		healthBar = new HealthBar(maxHealth);
	}

	public HealthBarView getHealthBarView() {
		return healthBar.getHealthBarView();
	}

	protected abstract BufferedImage[] getHealthBarSprites();

	protected void initializeHealthBarView() {
		new HealthBarView(healthBar, getHealthBarSprites());
	}

	protected abstract void attackThis(Living attacker, int damage);

	protected abstract void attackThisTrue(Living attacker, int damage);

	private void checkForHealthAction() {
		if (healthBar.getHealth() <= 0) {
			die();
		}
	}

	public void damage(int damage) {
		healthBar.damage(damage);
		checkForHealthAction();
	}

	public void heal(int heal) {
		healthBar.heal(heal);
		checkForHealthAction();
	}

	public int getHealth() {
		return healthBar.getHealth();
	}

	public int getMaxHealth() {
		return healthBar.getMaxHealth();
	}

	public void removeHealthBuffer(int i) {
		healthBar.removeBuffer(i);
		checkForHealthAction();
	}

	public int getTotalBufferHealth() {
		return healthBar.getTotalBufferHealth();
	}

	public int getTotalMaxBufferHealth() {
		return healthBar.getTotalMaxBufferHealth();
	}

	public boolean isHealthToppedOff() {
		return healthBar.isHealthToppedOff();
	}

	public void addHealthBuffer(HealthBuffer buffer) {
		healthBar.addBuffer(buffer);
		checkForHealthAction();
	}

	public void addFullHealthBuffer(int maxHealth, int threshold) {
		healthBar.addFullBuffer(maxHealth, threshold);
		checkForHealthAction();
	}

	public void die() {
		floor.die(this);
		dead = true;
		if (drop != null) {
			floor.dropInCircle(drop, x, y);
		}
	}

	protected void setHealth(int health) {
		healthBar.setHealth(health);
	}

	public boolean isHostile(Attackable attackable) {
		return attackable.enemyClass != enemyClass
				&& !(attackable.enemyClass == EnumEnemyClasses.SHOPKEEPER || enemyClass == EnumEnemyClasses.SHOPKEEPER);
	}

	// FIXME Make abstract, implement.
	@Override
	public boolean interact(Living liv) {
		throw new IllegalStateException("method should not be called");
	}

	@Override
	public boolean isInteractable(Living liv) {
		return liv.enemyClass != enemyClass;
	}

	public int getTotalMaxHealth() {
		return healthBar.getTotalMaxHealth();
	}

	public HealthBar getHealthBar() {
		return healthBar;
	}

	public void setHealthBar(HealthBar h) {
		healthBar = h;
	}

	@Override
	public void removalAction() {
	}
}
