package entities;

import java.awt.Point;
import java.util.ArrayList;

import model.Direction;
import model.EnumDirection;

public class Coordinate {
	public int x;
	public int y;

	public Coordinate(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Coordinate(Point p) {
		this.x = (int) p.getX();
		this.y = (int) p.getY();
	}

	public Coordinate(Coordinate coord) {
		x = coord.x;
		y = coord.y;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	@Override
	public String toString() {
		return "x = " + x + " / y = " + y;
	}

	public EnumDirection getDelta(Coordinate coord) {
		int deltaX = coord.x - x;
		int deltaY = coord.y - y;
		return Direction.getDirection(new Coordinate(deltaX, deltaY));
	}

	public static double getDistance(Coordinate coord1, Coordinate coord2) {
		return Math.sqrt(Math.pow((coord1.x - coord2.x), 2) + Math.pow((coord1.y - coord2.y), 2));
	}

	public boolean resembles(Coordinate coord) {
		return x == coord.x && y == coord.y;
	}

	public boolean resembles(int x, int y) {
		return this.x == x && this.y == y;
	}

	public boolean isInList(ArrayList<Coordinate> list) {
		for (int i = 0; i < list.size(); i++) {
			if ((list.get(i)).resembles(this)) {
				return true;
			}
		}
		return false;
	}

	public boolean isInHList(ArrayList<HeuristicCoord> list) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).resembles(this)) {
				return true;
			}
		}
		return false;
	}

	public int getIndexInList(ArrayList<HeuristicCoord> list) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).resembles(this)) {
				return i;
			}
		}
		return -1;
	}

	public Coordinate scale(double scale) {
		return new Coordinate((int) (x * scale), (int) (y * scale));
	}

	public Coordinate[] getSurroundings() {
		Coordinate[] surroundings = new Coordinate[8];
		surroundings[0] = new Coordinate(x, y - 1);
		surroundings[1] = new Coordinate(x - 1, y - 1);
		surroundings[2] = new Coordinate(x - 1, y);
		surroundings[3] = new Coordinate(x - 1, y + 1);
		surroundings[4] = new Coordinate(x, y + 1);
		surroundings[5] = new Coordinate(x + 1, y + 1);
		surroundings[6] = new Coordinate(x + 1, y);
		surroundings[7] = new Coordinate(x + 1, y - 1);
		return surroundings;
	}

	public Coordinate minus(Coordinate coord) {
		return new Coordinate(coord.getX() - x, coord.getY() - y);
	}

	public Coordinate neg() {
		return new Coordinate(-x, -y);
	}
}
