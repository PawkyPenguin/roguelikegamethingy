package entities;

import view.DialogBox;
import view.DungeonView;

public abstract class LivingTalker extends Living {
	protected DialogBox dialogBox;
	protected boolean talking = false;
	protected double talkingSpeed;
	protected final static double standardTalkingSpeed = 0.05;

	public LivingTalker() {
		super();
		setTalkingSpeed();
	}

	public LivingTalker(int x, int y) {
		super(x, y);
		setTalkingSpeed();
	}

	protected void setTalkingSpeed() {
		talkingSpeed = standardTalkingSpeed;
	}

	@Override
	public void tick(double timeSinceLastFrame) {
		super.tick(timeSinceLastFrame);
		if (dialogBox != null) {
			dialogBox.tick(timeSinceLastFrame);
		}
	}

	protected void talk(String s) {
		talking = true;
		dialogBox = new DialogBox();
		dialogBox.setText(s);
		dialogBox.setTimeBetweenLetters(talkingSpeed);
		dialogBox.setXY(x * DungeonView.pixels - dialogBox.getCurrentFrame().getWidth() / 2 + DungeonView.pixels / 2,
				y * DungeonView.pixels + DungeonView.menuHeight + 16);
	}

	protected void untalk() {
		talking = false;
		dialogBox = null;
	}

	public DialogBox getDialogBox() {
		return dialogBox;
	}

	public boolean isTalking() {
		return talking;
	}

	public abstract void talkTo(Living actor);
}
