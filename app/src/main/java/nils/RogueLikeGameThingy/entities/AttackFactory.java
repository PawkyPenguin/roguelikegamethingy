package entities;

import java.util.ArrayList;
import java.util.LinkedList;

import view.Animation;
import view.AnimationCheckpoint;
import view.AttackAnimation;
import model.EnumDirection;

public class AttackFactory {
	static EnumAttackTimer timerType = EnumAttackTimer.STANDARD;

	private static AttackFeature createRawFeature(int damage){
		AttackFeature feature = new AttackFeature();
		feature.setDamage(damage);
		return feature;
	}
	
	private static AttackFeature createTranscendentFeature(int damage){
		AttackFeature feature = createRawFeature(damage);
		feature.setSpectral(0);
		feature.setHitsOwner(false);
		feature.setTranscendent(true);
		feature.setTrueDamage(false);
		return feature;
	}
	
	private static AttackFeature createTrueFeature(int damage){
		AttackFeature feature = createRawFeature(damage);
		feature.setSpectral(0);
		feature.setHitsOwner(false);
		feature.setTranscendent(false);
		feature.setTrueDamage(true);
		return feature;
	}
	
	private static AttackFeature createTruescendentFeature(int damage){
		AttackFeature feature = createRawFeature(damage);
		feature.setSpectral(0);
		feature.setHitsOwner(false);
		feature.setTranscendent(true);
		feature.setTrueDamage(false);
		return feature;
	}
	
	private static AttackFeature createNormalFeature(int damage){
		AttackFeature feature = createRawFeature(damage);
		feature.setSpectral(0);
		feature.setHitsOwner(false);
		feature.setTranscendent(false);
		feature.setTrueDamage(false);
		return feature;
	}
	
	public static Attack createNormalAttack(Living owner, int damage){
		Attack attack = new Attack(owner);
		attack.setFeature(createNormalFeature(damage));
		return attack;
	}
	
	public static Attack createTranscendentAttack(Living owner, int damage){
		Attack attack = new Attack(owner);
		attack.setFeature(createTranscendentFeature(damage));
		return attack;
	}
	
	public static Attack createTrueAttack(Living owner, int damage){
		Attack attack = new Attack(owner);
		attack.setFeature(createTrueFeature(damage));
		return attack;
	}
	
	public static Attack createTruescendentAttack(Living owner, int damage){
		Attack attack = new Attack(owner);
		attack.setFeature(createTruescendentFeature(damage));
		return attack;
	}
	
	public static AttackLogic createStationaryAttack(Attack attack, Animation animation,
			double timeLength, Coordinate coord){
		AttackLogic logic = new AttackLogic();
		AttackPattern pattern = AttackPatternFactory.straightLine(coord, EnumDirection.NULL, 0);
		ArrayList<Coordinate> path = new ArrayList<Coordinate>();
		path.addAll(pattern.getAttackedFields());
		logic.setFloor(attack.getOwner().getFloor());
		logic.setPattern(pattern);
		logic.setAttackType(EnumAttackPattern.ONE_BY_ONE);
		logic.setAttack(attack);
		if(timerType == EnumAttackTimer.FAST){
			logic.setTimer(generateFastTimer());
		}
		else if(timerType == EnumAttackTimer.STANDARD){
			logic.setTimer(generateOneFieldTimer(timeLength));
		}
		setGraphicsStationary(attack, animation, coord, timeLength);
		return logic;
	}
	
	public static AttackLogic createStationaryAttack(Attack attack, Animation animation, Coordinate coord){
		return createStationaryAttack(attack, animation, animation.getAnimationLength(), coord);
	}
	
	public static AttackLogic createTravelAttack(Attack attack, Animation animation, 
			double velocity, Coordinate start, EnumDirection direction, int range){
		AttackLogic logic = new AttackLogic();
		AttackPattern pattern = AttackPatternFactory.straightLine(start, direction, range);
		ArrayList<Coordinate> path = new ArrayList<Coordinate>();
		path.addAll(pattern.getAttackedFields());
		logic.setFloor(attack.getOwner().getFloor());
		logic.setPattern(pattern);
		logic.setAttackType(EnumAttackPattern.ONE_BY_ONE);
		logic.setAttack(attack);
		if(timerType == EnumAttackTimer.FAST){
			logic.setTimer(generateFastTimer());
		}
		else if(timerType == EnumAttackTimer.STANDARD){
			logic.setTimer(generateStandardTimer(velocity, path));
		}
		setGraphicsTravel(attack, animation, path, velocity);
		return logic;
	}
	
	private static void setGraphicsStationary(Attack attack, Animation animation, Coordinate coord,
			double timeLength){
		ArrayList<AnimationCheckpoint> checkpointList = new ArrayList<AnimationCheckpoint>();
		checkpointList.add(new AnimationCheckpoint(coord, 0));
		checkpointList.add(new AnimationCheckpoint(coord, timeLength));
		attack.setAttackAnimation(new AttackAnimation(animation, checkpointList));
	}
	
	private static void setGraphicsStationary(Attack attack, Animation animation, Coordinate coord){
		setGraphicsStationary(attack, animation, coord, animation.getAnimationLength());
	}
	
	private static void setGraphicsTravel(Attack attack, Animation animation, 
			ArrayList<Coordinate> path,	double velocity){
		ArrayList<AnimationCheckpoint> checkpointList = new ArrayList<AnimationCheckpoint>();
		Coordinate currentCoord = path.get(0);
		Coordinate nextCoord;
		checkpointList.add(new AnimationCheckpoint(currentCoord, 0));
		for(int i = 0; i + 1 < path.size(); i++){
			currentCoord = path.get(i);
			nextCoord = path.get(i + 1);
			checkpointList.add(new AnimationCheckpoint(nextCoord, Coordinate.getDistance(currentCoord, nextCoord) / velocity));
		}
		attack.setAttackAnimation(new AttackAnimation(animation, checkpointList));
	}
	
	private static AttackTimer generateFastTimer(){
		return new AttackTimerFast();
	}
	
	private static AttackTimer generateOneFieldTimer(double time){
		LinkedList<Double> times = new LinkedList<Double>();
		AttackTimerStandard timer = new AttackTimerStandard();
		times.add(time);
		timer.setTimeIntervals(times);
		return timer;
	}
	
	private static AttackTimer generateStandardTimer(double velocity, ArrayList<Coordinate> path){
		LinkedList<Double> times = new LinkedList<Double>();
		AttackTimerStandard timer = new AttackTimerStandard();
		Coordinate currentCoord;
		Coordinate nextCoord;
		times.add(0.0);
		for(int i = 0; i + 1 < path.size(); i++){
			currentCoord = path.get(i);
			nextCoord = path.get(i + 1);
			times.add(Coordinate.getDistance(currentCoord, nextCoord) / velocity);
		}
		timer.setTimeIntervals(times);
		return timer;
	}
	
//	public static AttackLogic createInstantAttack(Living owner, boolean trueDamage, boolean transcendent,
//			int damage, ArrayList<BufferedImage> attackSprites, double time){
//		
//	}
}
