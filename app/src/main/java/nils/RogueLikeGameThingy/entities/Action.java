package entities;

public abstract class Action {
	private Living actor;
	private Entity actee;
	private int energyLevel;

	public Action() {

	}

	public Action(Living actor, Entity actee) {
		this.actor = actor;
		this.actee = actee;
	}

	protected void stop() {
		getActor().getActions().remove(this);
	}

	protected abstract boolean execute();

	protected abstract boolean tryExecute();

	protected Entity getActee() {
		return actee;
	}

	protected Living getActor() {
		return actor;
	}

	protected int getEnergyLevel() {
		return energyLevel;
	}

	protected void setEnergyLevel(int e) {
		energyLevel = e;
	}

	protected void setActor(Living a) {
		actor = a;
	}
}
