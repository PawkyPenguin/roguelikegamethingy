package entities.npcs;

import java.awt.image.BufferedImage;

import entities.Living;
import entities.LivingTalker;

import model.Camera;
import model.EnumEnemyClasses;

import view.SpriteCompendium;
public class Adventurer extends LivingTalker {

	public Adventurer (int x, int y) {
		super(x, y);
		solid = true;
		finishedTurn = true;
		initInventory();
		initializeHealthBar(100);
		setHealth(getMaxHealth());
	}

	public void talkTo(Living l){
		talk("Hey, fellow adventurer!");
	}

	@Override
	public void setSprites() {
		setWalkingAnimations(SpriteCompendium.getShopkeeperSprites());
		initializeHealthBarView();
	}

	@Override
	public void newStandardCamera() {
		Camera cam = new Camera();
		cam.subscribeTo(this);
	}

	public void setDrop() {
	}

	@Override
	protected BufferedImage[] getHealthBarSprites() {
		return null;
	}

	public void setType() {
		enemyClass = EnumEnemyClasses.SHOPKEEPER;	
	}

	@Override
	public void tick(double timeSinceLastFrame) {
		super.tick(timeSinceLastFrame);
		doTurn();
	}

	public void doTurn(){
		finishedTurn = true;
	}

	protected void setWalkingCost(){
	}

	protected void setOwnerIndex(){
		ownerIndex = 3;
	}

	@Override
	public boolean interact(Living l){
		talkTo(l);
		return true;
	}
}
