package entities;

import java.io.File;

import javax.imageio.ImageIO;

import entities.Living;
import view.Animation;
import view.SpriteCompendium;

public class StatuseffectFreeze extends Statuseffect{
	
	public StatuseffectFreeze(){
		super();
	}
	
	public StatuseffectFreeze(int level){
		super(level);
	}
	
	@Override
	protected void setType(){
		statusType = EnumStatuseffectType.FROZEN;
	}
	
	@Override
	protected void setStackable(){
		stackable = false;
	}
	
	@Override
	public void addEffect(Living e){
		e.setConductTurn(false);
		e.addStatuseffect(this);
	}
	
	@Override
	public void removeEffect(Living e){
		e.removeThisStatuseffect(this);
		if(!e.hasStatuseffect(statusType)){
			e.setConductTurn(true);
		}
	}
	
	@Override
	protected void setSprite(){
		sprite = SpriteCompendium.getStatuseffectSprites().getAnimation("statuseffectFreeze");
	}
}
