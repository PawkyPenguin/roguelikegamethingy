package entities;

public class StatuseffectBurnFriendly extends StatuseffectBurn{
	
	public StatuseffectBurnFriendly(){
		super();
		setTurnsLasting((int) (Math.random() * 2 + 3));
	}
	
	@Override
	public void takeEffect(Living l){
		if(l == getOwner()){
			return;
		}
		super.takeEffect(l);
	}
	
	@Override
	protected void applyStatuseffect(Living l){
		if(l == getOwner()){
			return;
		}
		StatuseffectBurn eff = new StatuseffectBurnFriendly();
		eff.setLevel(getLevel());
		eff.setOwner(getOwner());
		eff.setTurnsLasting(getLastingTurns());
		l.addStatuseffect(eff);
	}
}
