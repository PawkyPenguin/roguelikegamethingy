package entities;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import item.Item;
import lazyness.Chance;
import model.Camera;
import model.EnumEnemyClasses;
import model.InvFieldCommon;
import model.InventoryField;
import view.SpriteCompendium;

public class Shopkeeper extends LivingTalker {

	public Shopkeeper(int x, int y) {
		super(x, y);
		solid = true;
		finishedTurn = true;
		initInventory();
		getInvHandler().getInventory().setAllowTransacation(true);
		initializeHealthBar(100);
		setHealth(getMaxHealth());
	}

	@Override
	protected void setType() {
		enemyClass = EnumEnemyClasses.SHOPKEEPER;
	}

	@Override
	protected void setOwnerIndex() {
		ownerIndex = 3;
	}

	@Override
	protected void setWalkingCost() {
	}

	public ArrayList<InventoryField> getInvFields() {
		return getInvHandler().getInventory().getInventory();
	}

	@Override
	public void initInventory() {
		super.initInventory();
		Item healthPot;
		Item arrow;
		Item iceArrow;
		Item fireArrow;
		for (int i = 0; i < 20; i++) {
			arrow = Chest.generateNormalArrow(5);
			iceArrow = Chest.generateIceArrow(3);
			healthPot = Chest.generateHealthpot(1);
			fireArrow = Chest.generateFireArrow(3);
			getInvHandler().addInvField(new InvFieldCommon());
			if (Chance.oneHalf()) {
				getInvHandler().setItem(i, arrow);
			} else if (Chance.oneThird()) {
				getInvHandler().setItem(i, iceArrow);
			} else {
				getInvHandler().setItem(i, fireArrow);
			}
		}
	}

	@Override
	public void doTurn() {
		finishedTurn = true;
	}

	@Override
	public void tick(double timeSinceLastFrame) {
		super.tick(timeSinceLastFrame);
		doTurn();
	}

	@Override
	protected BufferedImage[] getHealthBarSprites() {
		return null;
	}

	@Override
	public void setSprites() {
		setWalkingAnimations(SpriteCompendium.getShopkeeperSprites());
		initializeHealthBarView();
	}

	@Override
	public void newStandardCamera() {
		Camera cam = new Camera();
		cam.subscribeTo(this);
	}

	@Override
	public void setDrop() {
	}

	public void greet() {
		if (Chance.oneHundredth()) {
			talk("JUST BUY IT");
		} else {
			talk("Hello traveller.");
		}
	}

	@Override
	public boolean interact(Living liv) {
		if (!((Player) liv).isInteractingWithOtherInventory()) {
			((Player) liv).interactWithOtherInventory(this);
			greet();
			return true;

		} else {
			return false;
		}
	}

	@Override
	public void talkTo(Living liv) {
		greet();
	}
}
