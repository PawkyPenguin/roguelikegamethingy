package entities;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import view.AttackAnimation;

public class Attack {
	/*
	 * TODO: AoE attacks separate handling of animations from actual effect
	 * implement pathing for animations (flying in arc, flying in straight lines
	 * along a given path) make it possible to access attacks easily in source
	 * code & store them somewhere (.txt files and also in RAM) have item
	 * dependent attacks be handled the same as AI triggered attacks, e.g.:
	 * private void launchAttack(EnumAttackType attack, double damage);
	 */

	private AttackFeature feature;
	private boolean finished;
	private AttackAnimation attackAnimation;
	private Living owner;

	public Attack(Living owner) {
		this.owner = owner;
	}

	protected void setFeature(AttackFeature a) {
		feature = a;
	}

	protected Living getOwner() {
		return owner;
	}

	protected void setTranscendent(boolean t) {
		feature.setTranscendent(t);
	}

	protected void setDamage(int damage) {
		feature.setDamage(damage);
	}

	// public static Attack createAttack(Living owner, boolean trueDamage,
	// ArrayList<Attackable> attacked,
	// Animation animation, ArrayList<Coordinate> path, double velocity, int
	// damage){
	// Attack attack = new Attack(owner);
	// attack.setTrueDamage(trueDamage);
	// attack.setGraphics(animation, path, velocity);
	// attack.setDamage(damage);
	// return attack;
	// }

	// protected void setGraphics(Animation animation, ArrayList<Coordinate>
	// path, double velocity){
	// ArrayList<AnimationCheckpoint> checkpointList = new
	// ArrayList<AnimationCheckpoint>();
	// Coordinate currentCoord = path.get(0);
	// Coordinate nextCoord;
	// checkpointList.add(new AnimationCheckpoint(currentCoord, 0));
	// for(int i = 0; i + 1 < path.size(); i++){
	// currentCoord = path.get(i);
	// nextCoord = path.get(i + 1);
	// checkpointList.add(new AnimationCheckpoint(nextCoord,
	// Coordinate.getDistance(currentCoord, nextCoord) / velocity));
	// }
	// attackAnimation = new AttackAnimation(animation, checkpointList);
	// }
	//
	// protected void setGraphics(Animation animation, Coordinate coord){
	// ArrayList<AnimationCheckpoint> checkpointList = new
	// ArrayList<AnimationCheckpoint>();
	// checkpointList.add(new AnimationCheckpoint(coord, 0));
	// checkpointList.add(new AnimationCheckpoint(coord,
	// animation.getAnimationLength()));
	// attackAnimation = new AttackAnimation(animation, checkpointList);
	// }

	public void setAppliedStatusEffects(ArrayList<Statuseffect> statuseffectList) {
		feature.setAppliedStatuseffects(statuseffectList, owner);

	}

	public int getScreenPosX() {
		return attackAnimation.getScreenPosX();
	}

	public int getScreenPosY() {
		return attackAnimation.getScreenPosY();
	}

	protected void tickAnimation(double timeSinceLastFrame) {
		// time += timeSinceLastFrame;
		if (!attackAnimation.hasFinished()) {
			attackAnimation.tick(timeSinceLastFrame);
		}
		// if(pathIndex + 1 >= checkpointList.size() && time >=
		// checkpointList.get(pathIndex).getTime()){
		// if(pathIndex - 1 < damagedObjectsList.size()){
		// Attackable attacked = damagedObjectsList.get(pathIndex - 1);
		// attack(attacked);
		// }
		// attackFinished = true;
		// }
		// else{
		// if(time >= checkpointList.get(pathIndex).getTime()){
		// if(pathIndex > 0){
		// Attackable attacked = damagedObjectsList.get(pathIndex - 1);
		// attack(attacked);
		// }
		// pathIndex++;
		// }
		// }
	}

	public void setTrueDamage(boolean trueDmg) {
		feature.setTrueDamage(trueDmg);
	}

	protected void attack(Attackable attacked) {
		if (!feature.doesHitOwner() && attacked == owner) {
			return;
		}
		if (!finished) {
			if (feature.isTrueDamage()) {
				attacked.attackThisTrue(owner, feature.getDamage());
			} else {
				attacked.attackThis(owner, feature.getDamage());
			}
			feature.decreaseSpectral();
			if (attacked instanceof Living) {
				ArrayList<Statuseffect> appliedStatuseffects = feature.getAppliedStatuseffects();
				for (int i = 0; i < appliedStatuseffects.size(); i++) {
					appliedStatuseffects.get(i).addEffect((Living) attacked);
				}
			}
			if (feature.isAttackExpired()) {
				finished = true;
			}
		}
	}

	protected boolean hasFinished() {
		return finished;
	}

	public BufferedImage getCurrentFrame() {
		return attackAnimation.getCurrentFrame();
	}

	public boolean isTranscendent() {
		return feature.isTranscendent();
	}

	public void setFinished(boolean b) {
		finished = b;
	}

	public void setSpectral(int s) {
		feature.setSpectral(s);
	}

	public void setAttackAnimation(AttackAnimation animation) {
		attackAnimation = animation;
	}
}
