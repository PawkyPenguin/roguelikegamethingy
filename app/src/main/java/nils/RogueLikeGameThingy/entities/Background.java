package entities;

import java.awt.image.BufferedImage;

import model.Floor;

public abstract class Background extends Decoratable {
	BackgroundInteractable interactable;

	public Background(int x, int y) {
		super(x, y);
	}

	@Override
	public int getDepth() {
		return 0;
	}

	@Override
	public void tick(double timeSinceLastFrame) {
		super.tick(timeSinceLastFrame);
		if (interactable != null) {
			interactable.tick(timeSinceLastFrame);
		}
	}

	public BufferedImage getInteractableSprite() {
		return interactable.getCurrentFrame();
	}

	public void setInteractable(BackgroundInteractable interactable) {
		this.interactable = interactable;
	}

	public void removeInteractable() {
		interactable = null;
	}

	public BackgroundInteractable getInteractable() {
		return interactable;
	}

	public boolean hasBackgroundInteractable() {
		return interactable != null;
	}

	public void interact(Living e) {
		if (interactable != null) {
			interactable.interact(e);
		}
	}

	@Override
	public void placeOnFloor(Floor f) {
		f.setObject(this);
	}

	@Override
	public void removalAction() {
	}
}
