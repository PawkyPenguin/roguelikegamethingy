package entities;

import java.util.ArrayList;

import model.Floor;
import view.SpriteCompendium;

public class Ward extends Foreground implements Interactable {
	final int rangeUp = 1;
	final int rangeDown = 1;
	final int rangeLeft = 1;
	final int rangeRight = 1;
	private ArrayList<Coordinate> wardedFields;

	public Ward(int x, int y) {
		super(x, y);
		wardedFields = new ArrayList<Coordinate>();
		setWardedFields();
		opaque = false;
	}

	private void setWardedFields() {
		wardedFields.clear();
		for (int i = x - rangeLeft; i <= x + rangeRight; i++) {
			for (int j = y - rangeUp; j <= y + rangeDown; j++) {
				wardedFields.add(new Coordinate(i, j));
			}
		}
	}

	public boolean isFieldWithinRange(int x, int y) {
		if (x > this.x - rangeLeft && x < this.x + rangeRight && y > this.y - rangeUp && y < this.y + rangeDown) {
			return true;
		} else {
			return false;
		}
	}

	public ArrayList<Coordinate> getWardedFields() {
		return wardedFields;
	}

	@Override
	protected void setWalkingCost() {
	}

	@Override
	public boolean interact(Living liv) {
		return false;
	}

	@Override
	public boolean isInteractable(Living liv) {
		return false;
	}

	@Override
	public void placeOnFloor(Floor f) {
		super.placeOnFloor(f);
		floor.addToWardList(this);
	}

	@Override
	public void setSprites() {
		setIdleAnimation(SpriteCompendium.getMiscSprites().getAnimation("ward"));
	}

	@Override
	public void removalAction() {
		floor.removeFromWardList(this);
	}
}
