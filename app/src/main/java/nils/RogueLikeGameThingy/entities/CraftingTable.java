package entities;

import java.util.ArrayList;

import crafting.Crafter;
import item.Item;
import model.InvFieldButtonCraft;
import model.InvFieldCommon;
import model.InventoryField;
import view.SpriteCompendium;

public class CraftingTable extends InventoryStation {

	public CraftingTable(int x, int y) {
		super(x, y);
		initInventory();
	}

	@Override
	protected void setDimensions() {
		widthOfFields = 5;
		heightOfFields = 5;
	}

	@Override
	public void setCrafter() {
		// TODO
		crafter = new Crafter();
		crafter.addCraftingTableRecipes();
	}

	public void craft() {
		ArrayList<Item> itemsInCraftingGrid = new ArrayList<Item>();
		for (InventoryField invField : invHandler.getInventory().getInventory()) {
			itemsInCraftingGrid.add(invField.getItem());
		}
		Item[][] items = new Item[widthOfFields][heightOfFields];
		for (int i = 0; i < widthOfFields; i++) {
			for (int j = 0; j < heightOfFields; j++) {
				items[i][j] = itemsInCraftingGrid.get(i * heightOfFields + j);
			}
		}
		Item craft = crafter.getCraftedItem(items);
		if (craft != null) {
			invHandler.updateItems();
			invHandler.stashItem(craft);
		}
	}

	@Override
	public int getOwnerIndex() {
		return 3;
	}

	@Override
	public void initInventory() {
		super.initInventory();
		for (int i = 0; i < widthOfFields * heightOfFields; i++) {
			invHandler.addInvField(new InvFieldCommon());
		}
		InvFieldButtonCraft craftButton = new InvFieldButtonCraft();
		craftButton.setInventoryStation(this);
		invHandler.addInvField(craftButton);

	}

	@Override
	public boolean isInteractable(Living liv) {
		return true;
	}

	@Override
	public boolean interact(Living liv) {
		if (!((Player) liv).isInteractingWithOtherInventory()) {
			((Player) liv).interactWithOtherInventory(this);
			return true;
		}
		return false;
	}

	@Override
	public void setSprites() {
		setIdleAnimation(SpriteCompendium.getInventoryStationSprites().getAnimation("craftingTable"));
	}

	@Override
	protected void setWalkingCost() {
	}

}
