package entities;

import java.util.ArrayList;

import item.ArmorPiece;
import item.Item;
import item.ItemActive;
import item.MeleeWeapon;
import item.Ring;
import item.Weapon;
import model.Camera;
import model.Direction;
import model.Floor;
import model.HasInventory;
import model.Inventory;
import model.InventoryHandler;
import model.Knowledge;
import model.Stat;
import model.TypeHandler;
import view.Animation;
import view.AnimationSet;
import view.LivingView;
import view.SpriteCompendium;

//TODO rework attacking procedure, utilise Weapon attack stats, implement performing turn-variable

public abstract class Living extends Attackable implements HasInventory {
	public int value = 0;
	public int gold = 0;
	protected int ownerIndex;

	// TODO use
	protected int maxEneryLevel;
	protected int energyLevel;
	protected Stat stats;
	protected Camera cam;
	protected boolean hasAttackedThisTurn;
	protected boolean finishedTurn;
	protected boolean conductTurn = true;
	private boolean interactingWithOtherInventory = false;
	protected AttackLogic launchedAttack;
	protected ItemActive itemRequestingUse;
	protected ArrayList<Statuseffect> weaponStatusEffectList;
	private Direction direction;
	private ArrayList<Weapon> weaponList;
	private ArrayList<Statuseffect> statuseffectList;
	private ArrayList<Action> actions = new ArrayList<Action>();
	private InventoryHandler invHandler;
	private Knowledge itemKnowledge;

	public Living() {
		super();
	}

	public Living(int x, int y) {
		super(x, y);
	}

	@Override
	public void initInventory() {
		invHandler = new InventoryHandler();
		invHandler.setOwner(this);
	}

	@Override
	protected void setStandardAttributes() {
		super.setStandardAttributes();
		setOwnerIndex();
		itemKnowledge = new Knowledge();
		stats = new Stat();
		stats.setArmor(0);
		stats.setAttack(20);
		setStatuseffectList(new ArrayList<Statuseffect>());
		weaponStatusEffectList = new ArrayList<Statuseffect>();
		setWeaponList(new ArrayList<Weapon>());
		direction = new Direction();
	}

	@Override
	public void setSprites() {
		((LivingView) view).setAttackAnimations(SpriteCompendium.getAttackSprites());
	}

	public ArrayList<Living> getAdjacentLivings() {
		ArrayList<Living> list = new ArrayList<Living>();
		for (Coordinate coord : floor.getAdjacentCoords(getCoordinate())) {
			if (coord != null && floor.getForegroundObject(coord.x, coord.y) instanceof Living) {
				list.add((Living) floor.getForegroundObject(coord.x, coord.y));
			}
		}
		return list;
	}

	@Override
	public void placeOnFloor(Floor f) {
		super.placeOnFloor(f);
		floor.addToAttackOrderList(this);
	}

	protected void setWalkingAnimations(AnimationSet animations) {
		((LivingView) view).setWalkingAnimations(animations);
		view.publisherUpdated(this);
	}

	@Override
	protected void setView() {
		new LivingView(this);
	}

	public Camera getCamera() {
		return cam;
	}

	public abstract void newStandardCamera();

	// XXX: Jesus this is horrible. Owner indices are magic numbers AND no one has an idea when this method is called.
	protected abstract void setOwnerIndex();

	@Override
	public int getOwnerIndex() {
		return ownerIndex;
	}

	@Override
	public void tick(double timeSinceLastFrame) {
		super.tick(timeSinceLastFrame);
		for (Statuseffect eff : getStatuseffectList()) {
			eff.tick(timeSinceLastFrame);
		}
	}

	protected void setDirection(int dx, int dy) {
		direction.setDirection(dx, dy);
		updateView();
	}

	protected void setDirection(Coordinate coord) {
		direction.setDirection(coord.getX(), coord.getY());
		updateView();
	}

	public boolean isPerformingTurn() {
		return (hasFinishedAttacking() && !hasFinishedTurn());
	}

	public ArrayList<Statuseffect> getStatuseffects() {
		return getStatuseffectList();
	}

	public float getAttackStat() {
		return stats.getAttack();
	}

	public float getArmorStat() {
		return stats.getArmor();
	}

	public Direction getDirection() {
		return direction;
	}

	public abstract void doTurn();

	protected void afterTurn() {
		for (int i = 0; i < statuseffectList.size(); i++)
			statuseffectList.get(i).takeEffect(this);
		for (Living l : getAdjacentLivings()) {
			for (Statuseffect eff : l.getStatuseffects()) {
				if (eff instanceof StatuseffectBurn) {
					((StatuseffectBurn) eff).burn(this);
					break;
				}
			}
		}
	}

	public void setConductTurn(boolean conductTurn) {
		this.conductTurn = conductTurn;
	}

	public void setFinishedTurn(boolean finishedTurn) {
		this.finishedTurn = finishedTurn;
	}

	public void setItemRequestingUse(ItemActive item) {
		itemRequestingUse = item;
	}

	protected void moveTo(int x, int y) {
		floor.deleteObject(this.x, this.y, getDepth());
		this.x = x;
		this.y = y;
		floor.setObject(this);
		updateCamera();
	}

	protected void move(int dx, int dy) {
		floor.deleteObject(x, y, getDepth());
		x += dx;
		y += dy;
		floor.setObject(this);
		updateCamera();
	}

	protected void move(Coordinate coord) {
		move(coord.getX(), coord.getY());
	}

	public void setAttack(float attack) {
		stats.setAttack(attack);
	}

	public void setArmor(float armor) {
		stats.setArmor(armor);
	}

	public void raiseStats(Stat plus) {
		stats.addStats(plus);
		if (plus.getHealthBuffer() != null) {
			addHealthBuffer(plus.getHealthBuffer());
		}
	}

	public void lowerStats(Stat minus) {
		stats.substractStats(minus);
		if (minus.getHealthBuffer() != null) {
			removeHealthBuffer(minus.getHealthBuffer().getIndex());
		}
	}

	public void removeItemFromInventory(Item item) {
		invHandler.removeItemFromInventory(item);
	}

	public void removeItemFromInventory(int i) {
		invHandler.removeItemFromInventory(i);
	}

	public int getIndexOfItem(Item item) {
		return invHandler.getIndexOfItem(item);
	}

	public void addStatuseffect(Statuseffect eff) {
		getStatuseffectList().add(eff);
	}

	public void addStatuseffect(ArrayList<Statuseffect> eff) {
		getStatuseffectList().addAll(eff);
	}

	public boolean hasStatuseffect(EnumStatuseffectType type) {
		for (Statuseffect eff : getStatuseffectList()) {
			if (type == eff.statusType) {
				return true;
			}
		}
		return false;
	}

	public boolean hasFinishedTurn() {
		return finishedTurn;
	}

	@Override
	public Inventory getInventory() {
		return invHandler.getInventory();
	}

	public boolean hasStatuseffect(Statuseffect eff) {
		for (int i = 0; i < getStatuseffectList().size(); i++) {
			if (eff == getStatuseffectList().get(i)) {
				return true;
			}
		}
		return false;
	}

	public void removeThisStatuseffect(Statuseffect eff) {
		for (int i = 0; i < getStatuseffectList().size(); i++) {
			if (eff == getStatuseffectList().get(i)) {
				getStatuseffectList().remove(i);
			}
		}
	}

	public void removeAllStatuseffectsOfType(EnumStatuseffectType type) {
		for (int i = 0; i < getStatuseffectList().size(); i++) {
			if (type == getStatuseffectList().get(i).statusType) {
				getStatuseffectList().remove(i);
			}
		}
	}

	public void launchAttack(AttackLogic attack) {
		hasAttackedThisTurn = true;
		launchedAttack = attack;
	}

	public boolean hasAttackedThisTurn() {
		return hasAttackedThisTurn;
	}

	public boolean isFacingBackgroundObject() {
		if (floor.getForegroundObject(x + direction.getX(), y + direction.getY()) == null) {
			return true;
		}
		return !floor.getForegroundObject(x + direction.getX(), y + direction.getY()).isSolid();
	}

	public boolean isFacingAttackableObject() {
		if (floor.getForegroundObject(x + direction.getX(), y + direction.getY()) == null) {
			return false;
		}
		return floor.getForegroundObject(x + direction.getX(), y + direction.getY()) instanceof Attackable
				&& ((Attackable) floor.getForegroundObject(x + direction.getX(), y + direction.getY())).canBeAttacked();
	}

	public AttackLogic getAttackLogic() {
		return launchedAttack;
	}

	public void addToWeaponEffectList(Statuseffect eff) {
		boolean contained = false;
		for (int i = 0; i < weaponStatusEffectList.size(); i++) {
			if (weaponStatusEffectList.get(i).statusType == eff.statusType) {
				weaponStatusEffectList.add(eff.getMax(weaponStatusEffectList.get(i)));
				contained = true;
				break;
			}
		}
		if (!contained) {
			weaponStatusEffectList.add(eff);
		}
	}

	public void updateWeaponEffectList() {
		weaponStatusEffectList.clear();
		for (Weapon weapon : getWeaponList()) {
			weapon.addToWeaponEffectList(this);
		}
	}

	public boolean hasFinishedAttacking() {
		if (launchedAttack == null || launchedAttack.hasFinished()) {
			return true;
		} else {
			return false;
		}
	}

	// CAN NOT BE USED IN THIS CLASS RIGHT NOW SINCE move(a,b) IS DISABLED

	/*
	 * public void moveIfPositionLegal(int dx, int dy){ boolean
	 * canMoveHorizontally = false; boolean canMoveVertically = false;
	 * if(!(dx==0 && dy==0) && isNewPositionLegal(x+dx,y)){ canMoveHorizontally
	 * = true; } if(!(dx==0 && dy==0) && isNewPositionLegal(x,y+dy)){
	 * canMoveVertically = true; } if(isNewPositionLegal(x+dx, y+dy)){
	 * if(canMoveHorizontally){ move(dx, 0); } if(canMoveVertically){ move(0,
	 * dy); } } else if(!canMoveHorizontally && canMoveVertically){ move(0, dy);
	 * } else if(!canMoveVertically && canMoveHorizontally){ move(dx, 0); }
	 * updateCamera(); }
	 */

	// CAN NOT BE USED IN THIS CLASS RIGHT NOW SINCE ENTITY IS UNABLE TO OBTAIN
	// LOOT FROM CHEST

	/*
	 * public void move(int dx, int dy){
	 * 
	 * if(Game.layer[x + dx][y + dy].entityType == EnumEntityType.MONSTER){ new
	 * Background(x,y); x += dx; y += dy; fight((Entity) Game.layer[x][y]);
	 * Game.layer[x][y] = this; } else if(Game.layer[x + dx][y + dy].entityType
	 * == EnumEntityType.CHEST){ Item receivedItem = ((Chest) Game.layer[x +
	 * dx][y + dy]).loot(); if(receivedItem != null){ boolean receivedTheLoot =
	 * false; for(int i = 0; i < inventory.length; i++){ for(int j = 0; j <
	 * inventory[0].length - 2; j++){ if(inventory[i][j] == null &&
	 * !receivedTheLoot){ receivedTheLoot = true; inventory[i][j] =
	 * receivedItem; } } } } } else{ new Background(x, y); x += dx; y += dy;
	 * Game.layer[x][y] = this; } }
	 */

	protected void fight(Attackable enemy) {
		hasAttackedThisTurn = true;
		launchedAttack = createStandardAttack(enemy, direction);
	}

	private AttackLogic createStandardAttack(Attackable enemy, Direction direction) {
		int damage = 0;
		ArrayList<Statuseffect> effects = new ArrayList<Statuseffect>();
		Weapon weapon;
		for (int i = 0; i < getWeaponList().size(); i++) {
			weapon = getWeaponList().get(i);
			damage += weapon.getDamage();
			for (Statuseffect eff : weapon.getInflictedStatusList()) {
				effects.add(eff);
			}
		}
		damage += stats.getAttack();
		Attack attack = AttackFactory.createNormalAttack(this, damage);
		attack.setAppliedStatusEffects(effects);
		Coordinate startCoord = direction.getFieldInDirection(getCoordinate());
		Animation animation = ((LivingView) view).getAttackAnimation("swordStrike");
		AttackLogic attackLogic = AttackFactory.createStationaryAttack(attack, animation, startCoord);
		return attackLogic;
	}

	public void equipEquipment(Item item) {
		if (item == null) {
			return;
		}
		if (TypeHandler.isItemArmor(item)) {
			((ArmorPiece) item).equipTo(this);
		} else if (TypeHandler.isItemWeapon(item)) {
			((MeleeWeapon) item).equipTo(this);
		} else if (TypeHandler.isItemRing(item)) {
			((Ring) item).equipTo(this);
		}
	}

	public void unequipEquipment(Item item) {
		if (item == null) {
			return;
		}
		if (TypeHandler.isItemArmor(item)) {
			((ArmorPiece) item).unequipFrom(this);
		} else if (TypeHandler.isItemWeapon(item)) {
			((MeleeWeapon) item).unequipFrom(this);
		} else if (TypeHandler.isItemRing(item)) {
			((Ring) item).unequipFrom(this);
		}
	}

	protected void updateCamera() {
		if (cam != null) {
			cam.publisherUpdated();
		}
	}

	public void subscribeThis(Camera c) {
		cam = c;
	}

	public void giveGold(int gold) {
		this.gold += gold;
	}

	@Override
	protected void attackThis(Living attacker, int damage) {
		damage((int) (damage * 100 / (100 + stats.getArmor())));
		if (dead && attacker != null) {
			attacker.giveGold(value + gold);
		}
	}

	@Override
	protected void attackThisTrue(Living attacker, int damage) {
		damage(damage);
		if (dead && attacker != null) {
			attacker.giveGold(value + gold);
		}
	}

	public boolean isItemInInventory(Item item) {
		return invHandler.isItemInInventory(item);
	}

	public int getIndexOfItem(ArrayList<Enum> itemType) {
		return invHandler.getIndexOfItem(itemType);
	}

	public boolean stashItem(Item item) {
		return invHandler.stashItem(item);
	}

	public boolean canItemBeStashed(Item item) {
		return invHandler.canItemBeStashed(item);
	}

	public ArrayList<Statuseffect> getStatuseffectList() {
		return statuseffectList;
	}

	public void setStatuseffectList(ArrayList<Statuseffect> statuseffectList) {
		this.statuseffectList = statuseffectList;
	}

	public ArrayList<Weapon> getWeaponList() {
		return weaponList;
	}

	public void setWeaponList(ArrayList<Weapon> weaponList) {
		this.weaponList = weaponList;
	}

	public InventoryHandler getInvHandler() {
		return invHandler;
	}

	public void stopInteractingWithOtherInventory() {
		if (isInteractingWithOtherInventory()) {
			invHandler.stopInteracting();
			interactingWithOtherInventory = false;
		}
	}

	public void interactWithOtherInventory(HasInventory other) {
		invHandler.interactWithOtherInventory(other);
		interactingWithOtherInventory = true;
	}

	public HasInventory getOtherInventory() {
		return invHandler.getOtherInvOwner();
	}

	public boolean isInteractingWithOtherInventory() {
		return interactingWithOtherInventory;
	}

	public Stat getStats() {
		return stats;
	}

	public boolean isDead() {
		return dead;
	}

	public int getGold() {
		return gold;
	}

	public int getValue() {
		return value;
	}

	public Knowledge getKnowledge() {
		return itemKnowledge;
	}

	public void addKnowledge(Item i) {
		itemKnowledge.addKnowledge(i);
		i.newKnowledge(itemKnowledge);
		invHandler.updateKnowledge(itemKnowledge);
	}

	public ArrayList<Action> getActions() {
		return actions;
	}

	public void addAction(Action a) {
		actions.add(a);
	}

	public boolean holdsWeapon(Class c) {
		for (int i = 0; i < weaponList.size(); i++) {
			if (c.isInstance(weaponList.get(i))) {
				return true;
			}
		}
		return false;
	}
}
