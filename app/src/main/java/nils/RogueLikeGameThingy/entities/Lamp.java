package entities;

import model.Floor;
import view.SpriteCompendium;

public class Lamp extends Foreground implements Interactable {

	public Lamp() {
		super();
	}

	public Lamp(int x, int y) {
		super(x, y);
	}

	@Override
	protected void setStandardAttributes() {
		super.setStandardAttributes();
		opaque = false;
	}

	@Override
	public boolean isInteractable(Living liv) {
		return false;
	}

	@Override
	public void placeOnFloor(Floor f) {
		super.placeOnFloor(f);
		floor.addToLampList(this);
	}

	@Override
	public boolean interact(Living liv) {
		return false;
	}

	@Override
	public void setSprites() {
		setIdleAnimation(SpriteCompendium.getMiscSprites().getAnimation("lamp"));
	}

	@Override
	protected void setWalkingCost() {
	}

	@Override
	public void removalAction() {
		floor.removeFromLampList(this);
	}
}
