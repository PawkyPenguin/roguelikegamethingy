package entities;

import item.ArmorPiece;
import item.Bow;
import item.Chestplate;
import item.EnumChestType;
import item.EnumMaterialType;
import item.FireArrow;
import item.HealthPot;
import item.Helmet;
import item.IceArrow;
import item.Item;
import item.Leggins;
import item.NormalArrow;
import item.Pickaxe;
import item.Potion;
import item.Ring;
import item.Spellbook;
import item.Stick;
import item.Sword;
import model.Floor;
import model.HasInventory;
import model.InvFieldCommon;
import model.Inventory;
import model.Stat;
import view.SpriteCompendium;

public class Chest extends Foreground implements HasInventory, Interactable {
	private boolean isLooted;
	private EnumChestType type;
	private Inventory inventory;

	public Chest() {
		super();
		initInventory();
	}

	public Chest(int x, int y) {
		super(x, y);
		initInventory();
	}

	@Override
	protected void setStandardAttributes() {
		super.setStandardAttributes();
		setType();
		isLooted = false;
		opaque = true;
	}

	@Override
	public void placeOnFloor(Floor f) {
		super.placeOnFloor(f);
		if (type == EnumChestType.GOLD) {
			generateRareLoot();
		} else if (type == EnumChestType.NORMAL) {
			generateNormalLoot();
		}
	}

	protected void setType() {
		if (Math.random() < 0.2) {
			type = EnumChestType.GOLD;
		} else {
			type = EnumChestType.NORMAL;
		}
	}

	public static HealthPot generateHealthpot(int amount) {
		HealthPot loot = new HealthPot();
		loot.setAmount(amount);
		return loot;
	}

	public static IceArrow generateIceArrow(int amount) {
		IceArrow loot = new IceArrow();
		loot.setAmount(amount);
		return loot;
	}

	public static FireArrow generateFireArrow(int amount) {
		FireArrow loot = new FireArrow();
		loot.setAmount(amount);
		return loot;
	}

	public static NormalArrow generateNormalArrow(int amount) {
		NormalArrow loot = new NormalArrow();
		loot.setAmount(amount);
		return loot;
	}

	public static Bow generateBow(int level) {
		Bow loot = new Bow();
		loot.setDamage((int) (Math.random() * 15 + level * 15 + 20));
		return loot;
	}

	public static Stick generateStick(int level) {
		Stick loot = new Stick();
		loot.setDamage((int) (Math.random() * 15 + level * 15 + 20));
		return loot;
	}

	public static Ring generateRing(int level, int healthBufferThreshold) {
		Ring loot = new Ring();
		loot.setMaterialType(EnumMaterialType.values()[(int) (Math.random() * EnumMaterialType.values().length)]);
		Stat stats = new Stat();
		stats.setAttack((int) (Math.random() * 10 + level * 10));
		stats.setArmor((int) (Math.random() * 10 + level * 5));
		stats.setHealth((int) (Math.random() * 10 + 50 + level * 15), healthBufferThreshold);
		loot.setStats(stats);
		return loot;
	}

	public static ArmorPiece generateHelmet(int level) {
		Helmet loot = new Helmet();
		loot.setArmor((int) Math.random() * 40 + 30 + 10 * level);
		loot.setMaterialType(EnumMaterialType.BRONZE);
		return loot;
	}

	public static Chestplate generateChest(int level) {
		Chestplate loot = new Chestplate();
		loot.setArmor((int) Math.random() * 20 + 40 + 12 * level);
		loot.setMaterialType(EnumMaterialType.BRONZE);
		return loot;
	}

	public static Leggins generateLeggins(int level) {
		Leggins loot = new Leggins();
		loot.setArmor((int) Math.random() * 30 + 30 + 10 * level);
		loot.setMaterialType(EnumMaterialType.BRONZE);
		return loot;
	}

	public static Sword generateSword(int level) {
		Sword loot = new Sword();
		loot.setAttack((int) (Math.random() * 20) + 40 + 18 * level);
		loot.setMaterialType(EnumMaterialType.BRONZE);
		return loot;
	}

	public static Pickaxe generatePickaxe(int level) {
		Pickaxe loot = new Pickaxe();
		loot.setAttack((int) (Math.random() * 25 + 35 + 17.5 * level));
		loot.setMaterialType(EnumMaterialType.SILVER);
		return loot;
	}

	public static Item generateSpellbook(int i) {
		Spellbook book = new Spellbook();
		return book;
	}

	public void generateRareLoot() {
		double rn = Math.random() * 100;
		int level = floor.getLevel();
		Item loot;
		if (rn < 10) {
			loot = generateLeggins(level);
		} else if (rn < 20) {
			loot = generateHelmet(level);
		} else if (rn < 30) {
			loot = generateChest(level);
		} else if (rn < 60) {
			loot = new HealthPot();
			loot.setAmount((int) (Math.random() * 2 + 1 + 0.2 * level));
		} else if (rn < 75) {
			loot = new Potion(floor.getPotionGen().getRandomPotionAttribute());
		} else if (rn < 85) {
			loot = generateSword(level);
			((Sword) (loot)).addInflictedStatuseffect(new StatuseffectBurnFriendly());
		} else if (rn < 95) {
			loot = new Chestplate();
			((Chestplate) loot).setMaterialType(EnumMaterialType.SILVER);
			((Chestplate) loot).setArmor((int) Math.random() * 30 + 55 + 12 * level);
		} else {
			loot = new Potion(floor.getPotionGen().getRandomPotionAttribute());
		}
		inventory.stashItem(loot);
	}

	public void generateNormalLoot() {
		double rn = Math.random() * 100;
		int level = floor.getLevel();
		Item loot;
		// if(rn < 10){
		// loot = generateLeggins(level);
		// }
		// else if (rn < 20){
		// loot = generateHelmet(level);
		// }
		// else if (rn < 30){
		// loot = generateChest(level);
		// }
		// else if (rn < 60){
		// loot = new HealthPot();
		// loot.setAmount((int) (Math.random() * 2 + 1 + 0.2 * level));
		// }
		// else if (rn < 75){
		// loot = new Potion(floor.getPotionGen().getRandomEffect());
		// }
		// else if (rn < 85){
		// loot = generateSword(level);
		// }
		// else if (rn < 95){
		// loot = new Chestplate();
		// ((Chestplate)loot).setMaterialType(EnumMaterialType.SILVER);
		// ((Chestplate)loot).setArmor((int) Math.random() * 30 + 55 + 12 *
		// level);
		// }
		// else {
		// loot = new Helmet();
		// ((Helmet)loot).setArmor((int) Math.random() * 30 + 50 + 12 * level);
		// ((Helmet)loot).setMaterialType(EnumMaterialType.SILVER);
		// }
		loot = new Potion(floor.getPotionGen().getRandomPotionAttribute());
		inventory.stashItem(loot);
	}

	@Override
	public void initInventory() {
		inventory = new Inventory();
		inventory.setOwner(this);
		inventory.addInvField(new InvFieldCommon());
	}

	private boolean loot(Living liv) {
		if (!isLooted) {
			switch (type) {
			case NORMAL:
				setIdleAnimation(SpriteCompendium.getChestSprites().getAnimation("openWoodenChest"));
				break;
			case GOLD:
				setIdleAnimation(SpriteCompendium.getChestSprites().getAnimation("openGoldenChest"));
				break;
			}
			isLooted = true;
		}
		if (!liv.isInteractingWithOtherInventory() && liv.getInvHandler().getOtherInvOwner() != inventory) {
			liv.interactWithOtherInventory(this);
			return true;
		} else {
			if (liv.stashItem(inventory.getInventory().get(0).getItem())) {
				inventory.getInventory().get(0).setItem(null);
			}
			return false;
		}
	}

	@Override
	protected void setWalkingCost() {
	}

	@Override
	public boolean interact(Living liv) {
		if (isInteractable(liv)) {
			return loot(liv);
		} else {
			return false;
		}
	}

	@Override
	public boolean isInteractable(Living liv) {
		return true;
	}

	@Override
	public void setSprites() {
		if (type == EnumChestType.NORMAL) {
			setIdleAnimation(SpriteCompendium.getChestSprites().getAnimation("woodenChest"));
		} else if (type == EnumChestType.GOLD) {
			setIdleAnimation(SpriteCompendium.getChestSprites().getAnimation("goldenChest"));
		}
	}

	@Override
	public int getOwnerIndex() {
		return Inventory.commonPropertyIndex;
	}

	@Override
	public Inventory getInventory() {
		return inventory;
	}

	@Override
	public void removalAction() {
	}
}
