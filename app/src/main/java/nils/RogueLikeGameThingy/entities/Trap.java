package entities;

import model.TrapEffect;

public abstract class Trap extends BackgroundInteractable {
	protected TrapEffect effect;
	private int uses;
	protected boolean triggered = false;

	public Trap() {
		initializeTrapEffect();
	}

	@Override
	public boolean isInteractable(Living liv) {
		if (liv instanceof Player) {
			return true;
		}
		return false;
	}

	protected abstract void initializeTrapEffect();

	protected void setUses(int i) {
		uses = i;
	}

	protected int getUses() {
		return uses;
	}

	@Override
	public boolean interact(Living liv) {
		triggered = true;

		return effect.applyTo(liv);
	}

	@Override
	protected void setSprites() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setWalkingCost() {
		walkingCost = 0;
	}

}
