package entities;

import java.awt.image.BufferedImage;

public abstract class Decoratable extends Entity {
	Decoration decoration;

	public Decoratable() {
		super();
	}

	public Decoratable(int x, int y) {
		super(x, y);
	}

	public void setDecoration(Decoration deco) {
		decoration = deco;
	}

	public BufferedImage getDecorationSprite() {
		return decoration.getCurrentFrame();
	}

	@Override
	public void tick(double timeSinceLastFrame) {
		super.tick(timeSinceLastFrame);
		if (decoration != null) {
			decoration.tick(timeSinceLastFrame);
		}
	}

	public Decoration getDecoration() {
		return decoration;
	}
}
