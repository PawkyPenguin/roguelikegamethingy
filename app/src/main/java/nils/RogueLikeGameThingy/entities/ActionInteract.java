package entities;

public class ActionInteract extends Action {

	public ActionInteract(Living actor, Entity actee) {
		super(actor, actee);
	}

	@Override
	protected void stop() {
		super.stop();
		if (getActor() instanceof Player) {
			if (getActee() instanceof Shopkeeper) {
				((Shopkeeper) getActor().getInvHandler().getOtherInvOwner()).untalk();
			} else if (getActee() instanceof Chest) {

			}
			((Player) getActor()).stopInteractingWithOtherInventory();
		}
	}

	@Override
	protected boolean tryExecute() {
		if (getActor() instanceof Player) {
			if (getActee() instanceof Attackable && getActor().isHostile((Attackable) getActee())) {
				ActionAttack aa = new ActionAttack(getActor(), getActee());
				return aa.tryExecute();
			} else if (getActee() instanceof Shopkeeper || getActee() instanceof Chest
					|| getActee() instanceof InventoryStation) {
				boolean success = execute();
				if (success) {
					getActor().addAction(this);
				}
				return success;
			} else if (getActee() instanceof Interactable) {
				return execute();
			}
		}
		return false;
	}

	@Override
	protected boolean execute() {
		return ((Interactable) getActee()).interact(getActor());
	}
}
