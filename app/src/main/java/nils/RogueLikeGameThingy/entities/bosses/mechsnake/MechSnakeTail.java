package entities.bosses.mechsnake;

import java.awt.image.BufferedImage;

public class MechSnakeTail extends MechSnakeBody {

	public MechSnakeTail(int x, int y) {
		super(x, y);
	}

	@Override
	public void newStandardCamera() {
	}

	@Override
	protected BufferedImage[] getHealthBarSprites() {
		return null;
	}

	@Override
	public void setDrop() {
	}
}
