package entities.bosses.mechsnake;

import java.util.ArrayList;

import entities.Living;
import model.Camera;

public abstract class MultiPartsBoss {
	protected BodyPart head;
	protected ArrayList<BodyPart> body;
	protected BodyPart tail;

	public MultiPartsBoss() {
		
	}
	
	protected void die(){
		head.die();
		for(BodyPart bodyPart:body)
			bodyPart.die();
		tail.die();
	}
	
	protected abstract void attackThis(Living attack, int damage);
	
	protected abstract void attackThisTrue(Living attacker, int damage);
	
	protected abstract void doTurn();
}
