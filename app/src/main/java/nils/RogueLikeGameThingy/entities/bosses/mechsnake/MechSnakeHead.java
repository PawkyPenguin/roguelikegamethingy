package entities.bosses.mechsnake;

import java.awt.image.BufferedImage;

import model.Floor;
import model.MonsterCamera;
import model.Stat;
import view.SpriteCompendium;

public class MechSnakeHead extends MechSnakeBody {

	public MechSnakeHead(int x, int y) {
		super(x, y);

	}

	@Override
	public void newStandardCamera() {
		cam = new MonsterCamera();
		cam.subscribeTo(this);
	}

	@Override
	public void placeOnFloor(Floor f) {
		super.placeOnFloor(f);
		initializeHealthBar(100 + 20 * floor.getLevel());
		setHealth(getMaxHealth());
		stats = new Stat();
		setArmor(20);
		setAttack(20);
		setWalkingCost();
	}

	@Override
	protected BufferedImage[] getHealthBarSprites() {
		return SpriteCompendium.getMobHealthBarSprites().getCurrentFrames();
	}

	@Override
	public void setDrop() {
	}
}
