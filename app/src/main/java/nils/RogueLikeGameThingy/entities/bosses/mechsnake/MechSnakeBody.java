package entities.bosses.mechsnake;

public abstract class MechSnakeBody extends BodyPart{
	protected MechSnake handler;

	public MechSnakeBody(int x, int y) {
		super(x, y);
		initInventory();
	}
	
	protected void setHandler(MechSnake h){
		handler = h;
	}
}
