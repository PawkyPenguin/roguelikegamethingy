package entities.bosses.mechsnake;

import model.Camera;
import model.EnumEnemyClasses;
import entities.Living;

public abstract class BodyPart extends Living{
	MultiPartsBoss handler;
	
	public BodyPart(int x, int y) {
		super(x, y);
	}

	protected void setHandler(MultiPartsBoss m){
		handler = m;
	}
	
	@Override 
	public void newStandardCamera(){
		cam = new Camera();
		cam.subscribeTo(this);
	}
	
	@Override
	protected void setType() {
		enemyClass = EnumEnemyClasses.MONSTER;
	}

	@Override
	protected void attackThis(Living attacker, int damage) {
		handler.attackThis(attacker, damage);
	}

	@Override
	protected void attackThisTrue(Living attacker, int damage) {
		handler.attackThisTrue(attacker, damage);
	}

	@Override
	protected void setWalkingCost() {
		walkingCost = Integer.MAX_VALUE;
	}
	
	@Override
	public void tick(double timeSinceLastFrame){
		super.tick(timeSinceLastFrame);
		doTurn();
	}
	
	@Override
	public void doTurn(){
		handler.doTurn();
	}
	
	@Override
	protected void setOwnerIndex(){
		ownerIndex = 2;
	}
	
	protected MultiPartsBoss getHandler(){
		return handler;
	}
}
