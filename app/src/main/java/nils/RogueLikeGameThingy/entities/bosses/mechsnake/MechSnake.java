package entities.bosses.mechsnake;

import java.util.ArrayList;

import model.HealthBar;
import model.LightCoord;
import model.Stat;
import entities.Living;


public class MechSnake extends MultiPartsBoss{
	private HealthBar hpBar;
	private Stat stats;
	private ArrayList<BodyPart> wholeBody;
	
	public void setParts(MechSnakeBody h, ArrayList<BodyPart> b, BodyPart t){
		head = h;
		hpBar = head.getHealthBar();
		head.setHandler(this);
		stats = head.getStats();
		head.newStandardCamera();
		body = b;
		for(BodyPart part:body){
			part.setHealthBar(head.getHealthBar());
			part.setHandler(this);
		}
		tail = t;
		tail.setHealthBar(head.getHealthBar());
		tail.setHandler(this);
		wholeBody = new ArrayList<BodyPart>();
		wholeBody.add(head);
		wholeBody.addAll(body);
		wholeBody.add(tail);
	}

	
	
	@Override
	protected void attackThis(Living attacker, int damage) {
		head.damage((int) (damage * 100 / (100 + stats.getArmor())));
		if(head.isDead()){
			attacker.giveGold(head.getValue() + head.getGold());
			die();
		}
	}

	@Override
	protected void attackThisTrue(Living attacker, int damage) {
		hpBar.damage(damage);
		if(head.isDead()){
			attacker.giveGold(head.getValue() + head.getGold());
			die();
		}
	}

	@Override
	protected void doTurn() {
		for(BodyPart part:wholeBody){
			part.setFinishedTurn(true);
		}
		byte[][] lightMap = head.getCamera().getVisibleFields();
		for(int i = 0; i < lightMap.length; i++){
			for(int j = 0; j < lightMap[0].length; j++){
				//TODO
			}
		}
	}
}
