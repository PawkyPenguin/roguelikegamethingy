package entities;

import java.util.ArrayList;
import java.util.LinkedList;

public class AttackPattern {
	LinkedList<Coordinate> attackedFields;
	
	protected void setAttackedFields(LinkedList<Coordinate> a){
		attackedFields = a;
	}
	
	protected LinkedList<Coordinate> getAttackedFields(){
		return attackedFields;
	}
	
	protected boolean hasFinished(){
		return attackedFields.isEmpty();
	}
	
	protected Coordinate nextField(){
		return attackedFields.remove();
	}

	protected ArrayList<Coordinate> allFields(){
		ArrayList<Coordinate> list = new ArrayList<Coordinate>();
		for(int i = 0; i < attackedFields.size(); i++){
			list.add(attackedFields.remove());
		}
		return list;
	}
}
