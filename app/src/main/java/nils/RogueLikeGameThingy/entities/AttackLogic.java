package entities;

import java.util.ArrayList;

import model.Floor;

public class AttackLogic {
	private AttackPatternHandler patternHandler;
	private AttackTimer timer;
	private Attack attack;
	private Floor floor;
	
	public AttackLogic(){
		timer = new AttackTimerStandard();
		patternHandler = new AttackPatternHandler();
		//timer = new AttackTimerFast();
	}
	
	public Attack getAttack(){
		return attack;
	}
	
	public void setPattern(AttackPattern p){
		patternHandler.setPattern(p);
	}
	
	public void setAttack(Attack a){
		attack = a;
	}
	
	public void setTimer(AttackTimer t){
		timer = t;
	}
	
	public void setFloor(Floor f){
		floor = f;
	}
	
	private void attackFields(ArrayList<Coordinate> fields){
		ArrayList<Attackable> attackables = new ArrayList<Attackable>();
		for(int i = 0; i < fields.size(); i++){
			Coordinate field = fields.get(i);
			Foreground foregroundObject = floor.getForegroundObject(field);
			if(foregroundObject instanceof Attackable){
				attack.attack((Attackable) foregroundObject);
			}
			else{
				checkForEndOfAttackOnField(field);
			}
		}
	}
	
	private void checkForEndOfAttackOnField(Coordinate field){
		Foreground foregroundObject = floor.getForegroundObject(field);
		boolean solidObjectFound = foregroundObject != null && foregroundObject.isSolid() 
				&& foregroundObject != attack.getOwner();
		boolean transcendent = attack.isTranscendent();
		boolean attackOutOfBounds = !floor.isCoordinateWithinLayer(field);
		boolean attackFinished = (solidObjectFound && !transcendent) || attackOutOfBounds;
		attack.setFinished(attackFinished);
	}
	
	public void tick(double timeSinceLastFrame){
		if(!hasFinished()){
			boolean nextStep = timer.countDown(timeSinceLastFrame);
			attack.tickAnimation(timeSinceLastFrame);
			if(nextStep){
				ArrayList<Coordinate> fields = patternHandler.attackNextField();
				attackFields(fields);
			}			
		}
	}
	
	public boolean hasFinished(){
		return patternHandler.hasFinished() || attack.hasFinished();
	}

	public void setAttackType(EnumAttackPattern type) {
		patternHandler.setAttackPatternType(type);
	}
}
