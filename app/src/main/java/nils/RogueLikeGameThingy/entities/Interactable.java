package entities;

public interface Interactable {

	public abstract boolean isInteractable(Living liv);
	
	public abstract boolean interact(Living liv);
}
