package entities;

import java.util.ArrayList;

public class AttackPatternHandler {
	AttackPattern pattern;
	EnumAttackPattern attackPatternType;
	
	public void setAttackPatternType(EnumAttackPattern e){
		attackPatternType = e;
	}
	
	public void setPattern(AttackPattern p){
		pattern = p;
	}
	
	public ArrayList<Coordinate> attackNextField(){
		if(attackPatternType == EnumAttackPattern.ONE_BY_ONE){
			ArrayList<Coordinate> list = new ArrayList<Coordinate>();
			list.add(pattern.nextField());
			return list;
		}
		else if(attackPatternType == EnumAttackPattern.SIMULTANEOUS){
			return pattern.allFields();
		}
		throw new IllegalStateException("EnumAttackPattern has unexpected state");
	}
	
	public boolean hasFinished(){
		return pattern.hasFinished();
	}
}
