package entities;

import model.Floor;

public abstract class Foreground extends Decoratable {

	public Foreground() {
		super();
	}

	public Foreground(int x, int y) {
		super(x, y);
	}

	@Override
	public void placeOnFloor(Floor f) {
		f.setObject(this);
	}

	@Override
	public int getDepth() {
		return 1;
	}
}
