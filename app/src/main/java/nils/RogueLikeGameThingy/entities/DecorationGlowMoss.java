package entities;

import view.SpriteCompendium;

public class DecorationGlowMoss extends Decoration {

	@Override
	protected void setSprites() {
		animation = SpriteCompendium.getDecorationSprites().getAnimation("glowMoss");
	}
}
