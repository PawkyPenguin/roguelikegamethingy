package entities;

import java.util.ArrayList;

public class AttackFeature {
	private boolean trueDamage;
	private boolean transcendent;
	private boolean hitsOwner;
	private int spectral;
	private int damage;
	private ArrayList<Statuseffect> appliedStatuseffects = new ArrayList<Statuseffect>();

	protected AttackFeature(){

	}
	
	public int getDamage(){
		return damage;
	}
	
	public void setDamage(int dmg){
		damage = dmg;
	}
	
	public boolean isTrueDamage() {
		return trueDamage;
	}

	public void setTrueDamage(boolean trueDamage) {
		this.trueDamage = trueDamage;
	}

	public boolean isTranscendent() {
		return transcendent;
	}

	public void setTranscendent(boolean transcendent) {
		this.transcendent = transcendent;
	}

	public int getSpectral() {
		return spectral;
	}

	public void setSpectral(int spectral) {
		this.spectral = spectral;
	}

	public boolean doesHitOwner() {
		return hitsOwner;
	}

	public void setHitsOwner(boolean hitsOwner) {
		this.hitsOwner = hitsOwner;
	}

	public ArrayList<Statuseffect> getAppliedStatuseffects() {
		return appliedStatuseffects;
	}

	public void setAppliedStatuseffects(ArrayList<Statuseffect> statuseffectList, Living owner) {
		appliedStatuseffects.clear();
		appliedStatuseffects.addAll(statuseffectList);
		for(Statuseffect eff:appliedStatuseffects)
			eff.setOwner(owner);
	}

	public void decreaseSpectral() {
		spectral--;
	}

	public boolean isAttackExpired() {
		return !transcendent && spectral <= 0;
	}
}