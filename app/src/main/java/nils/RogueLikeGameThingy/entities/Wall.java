package entities;

import item.Pickaxe;
import model.Floor;
import view.Autotiler;
import view.SpriteCompendium;

public class Wall extends Foreground implements Interactable {
	int destructionLevel = 0;
	int toughness;

	public Wall(int x, int y) {
		super(x, y);
		opaque = true;
		toughness = 5;
	}

	public void hitWall() {
		destructionLevel++;
		decoration.getAnimation().nextFrame();
		if (destructionLevel > toughness) {
			breakThis();
		}
	}

	private void breakThis() {
		floor.deleteObject(x, y, getDepth());
		boolean obstacleMap[][] = new boolean[getFloor().getRelevantX()][getFloor().getRelevantY()];
		for (int i = 0; i < getFloor().getRelevantX(); i++) {
			for (int j = 0; j < getFloor().getRelevantY(); j++) {
				obstacleMap[i][j] = getFloor().isFieldOpaque(i, j);
			}
		}

		getFloor().getShadowCaster().setObstacleMap(obstacleMap);
	}

	@Override
	protected void setWalkingCost() {
	}

	@Override
	public void setAttributes(Floor floor) {
		this.floor = floor;
		placeOnFloor(floor);
	}

	@Override
	public void setSprites() {
		Coordinate coord = getCoordinate();
		boolean[] surroundings = new boolean[8];
		Coordinate[] adjacentCoords = coord.getSurroundings();
		for (int i = 0; i < surroundings.length; i++) {
			surroundings[i] = floor.isWall(adjacentCoords[i]) || !floor.isCoordinateWithinLayer(adjacentCoords[i]);
		}
		setIdleAnimation(Autotiler.getAutoTile(SpriteCompendium.getWallSprites(), surroundings));
		decoration = new DecorationCrack();
	}

	@Override
	public void removalAction() {
		Coordinate[] surroundings = getCoordinate().getSurroundings();
		for (Coordinate adjacentCoord : surroundings) {
			Entity entity = floor.getForegroundObject(adjacentCoord);
			if (floor.getForegroundObject(adjacentCoord) instanceof Wall) {
				entity.setSprites();
			}
		}
	}

	@Override
	public boolean isInteractable(Living liv) {
		return true;
	}

	@Override
	public boolean interact(Living liv) {
		if (liv.holdsWeapon(Pickaxe.class)) {
			hitWall();
			liv.updateCamera();
			return true;
		} else {
			return false;
		}
	}
}
