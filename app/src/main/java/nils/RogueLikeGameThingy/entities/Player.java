package entities;

import java.awt.image.BufferedImage;

import controller.InputContainer;
import controller.InventoryController;
import controller.PlayerControl;
import item.Item;
import item.ItemDrop;
import model.Camera;
import model.EnumEnemyClasses;
import model.Floor;
import model.HasInventory;
import view.SpriteCompendium;

public class Player extends Living implements Cloneable {
	private InventoryController invController;
	private boolean isDescending = false;
	private boolean noclip = false;
	private boolean lux = false;
	private boolean godmode = false;
	private InputContainer currentInputs;
	private PlayerControl turnController;

	public Player(int x, int y) {
		super(x, y);
		initializeHealthBar(100);
		setHealth(getMaxHealth());
		setType();
		setWalkingCost();
		canBeAttacked = true;
		stats.setArmor(50);
		stats.setAttack(20);
		turnController = new PlayerControl(this);
		gold = 0;
	}

	@Override
	public void initInventory() {
		super.initInventory();
		invController = new InventoryController(this);
		// getInvHandler().stashItem(Chest.generateHelmet(0));
		// getInvHandler().stashItem(Chest.generateLeggins(0));
		// getInvHandler().stashItem(Chest.generateChest(0));
		getInvHandler().stashItem(Chest.generateHealthpot(6));
		getInvHandler().stashItem(Chest.generateNormalArrow(20));
		getInvHandler().stashItem(Chest.generateIceArrow(6));
		getInvHandler().stashItem(Chest.generateSword(0));
		getInvHandler().stashItem(Chest.generateRing(0, getMaxHealth()));
		getInvHandler().stashItem(Chest.generatePickaxe(0));
		getInvHandler().stashItem(Chest.generateBow(1));
		getInvHandler().stashItem(Chest.generateStick(0));
		getInvHandler().stashItem(Chest.generateStick(0));
		getInvHandler().stashItem(Chest.generateStick(0));
		// getInvHandler().stashItem(Chest.generateBow(0));
		// getInvHandler().stashItem(Chest.generateFireArrow(10));
		// getInvHandler().stashItem(Chest.generateSpellbook(0));
	}

	@Override
	public void setAttributes(Floor floor) {
		super.setAttributes(floor);
		initInventory();
	}

	@Override
	protected void setType() {
		enemyClass = EnumEnemyClasses.PLAYER;
	}

	public void setInputs(InputContainer inputs) {
		this.currentInputs = inputs;
		invController.updateInputs(inputs);
	}

	@Override
	protected void setOwnerIndex() {
		ownerIndex = 1;
	}

	@Override
	protected void setWalkingCost() {
		walkingCost = (int) (getHealth() * stats.getArmor());
	}

	public void setPosition(int x, int y) {
		if (!isDescending) {
			floor.deleteObject(this.x, this.y, getDepth());
		}
		this.x = x;
		this.y = y;
		floor.setObject(this);
		cam.updateLighting();
	}

	public boolean getLux() {
		return lux;
	}

	public Item getPickedUpItem() {
		return invController.getPickedUpItem();
	}

	public InventoryController getInvController() {
		return invController;
	}

	public boolean isHoldingItem() {
		return invController.isHoldingItem();
	}

	public boolean isInventoryVisible() {
		return invController.isVisible;
	}

	private void doActionIfPossible(int dx, int dy) {
		boolean actionAvailableHorizontally = false;
		boolean actionAvailableVertically = false;
		boolean freePosHorizontally = false;
		boolean freePosVertically = false;
		setDirection(dx, dy);
		freePosHorizontally = (dx != 0 && floor.isPosFreeForMoving(x + dx, y));
		freePosVertically = (dy != 0 && floor.isPosFreeForMoving(x, y + dy));
		actionAvailableHorizontally = (dx != 0 && floor.isActionAvailable(x + dx, y, this));
		actionAvailableVertically = (dy != 0 && floor.isActionAvailable(x, y + dy, this));
		if (freePosHorizontally && freePosVertically
				&& (floor.isPosFreeForMoving(x + dx, y + dy) || floor.isActionAvailable(x + dx, y + dy, this))) {
			doAction(dx, dy);
		} else if ((actionAvailableHorizontally || freePosHorizontally) && !(actionAvailableVertically)) {
			doAction(dx, 0);
		} else if (!(actionAvailableHorizontally) && (actionAvailableVertically || freePosVertically)) {
			doAction(0, dy);
		}
	}

	private void doAction(int dx, int dy) {
		if (dx == 0 && dy == 0) {
			return;
		}
		setDirection(dx, dy);
		ActionMove actionMove = new ActionMove(this, x + dx, y + dy);
		boolean success = actionMove.tryExecute();
		if (success) {
			finishedTurn = true;
		}
	}

	@Override
	public void tick(double timeSinceLastFrame) {
		super.tick(timeSinceLastFrame);
		turnController.tick(timeSinceLastFrame, currentInputs);
	}

	protected void doTurn(int dx, int dy) {
		if (conductTurn) {
			if (itemRequestingUse == null) {
				if (noclip) {
					doAction(dx, dy);
				} else {
					doActionIfPossible(dx, dy);
				}
				if (finishedTurn) {
					turnController.refreshMoveCooldown();
					afterTurn();
				}
			} else {
				itemRequestingUse.use(this);
				afterTurn();
				itemRequestingUse = null;
			}
		} else {
			finishedTurn = true;
			afterTurn();
		}
	}

	public boolean getIsDescending() {
		return isDescending;
	}

	public void setIsDescending(boolean descending) {
		isDescending = descending;
	}

	@Override
	public void doTurn() {
		hasAttackedThisTurn = false;
		finishedTurn = false;
		int dx = 0, dy = 0;
		if (currentInputs.isKeyPressed("directionSelect")) {
			int dirx = 0;
			int diry = 0;
			if (currentInputs.isKeyPressed("moveUp")) {
				diry--;
			}
			if (currentInputs.isKeyPressed("moveLeft")) {
				dirx--;
			}
			if (currentInputs.isKeyPressed("moveDown")) {
				diry++;
			}
			if (currentInputs.isKeyPressed("moveRight")) {
				dirx++;
			}
			setDirection(dirx, diry);
		} else {
			if (currentInputs.isKeyPressed("moveUp"))
				dy -= 1;
			if (currentInputs.isKeyPressed("moveLeft"))
				dx -= 1;
			if (currentInputs.isKeyPressed("moveDown"))
				dy += 1;
			if (currentInputs.isKeyPressed("moveRight"))
				dx += 1;
			if (currentInputs.isKeyPressed("descend")
					&& floor.getBackgroundObject(getCoordinate()).getInteractable() instanceof Stairs) {
				isDescending = true;
			}
			if (currentInputs.isKeyPressed("pickUpItem") && floor.getObject(x, y, 2) instanceof ItemDrop) {
				ItemDrop itemDrop = (ItemDrop) floor.getObject(x, y, 2);
				Item itemOnGround = itemDrop.getItem();
				if (getInvHandler().canItemBeStashed(itemOnGround)) {
					getInvHandler().stashItem(itemOnGround);
					itemDrop.pickUp();
				}
				finishedTurn = true;
				afterTurn();
				return;
			}
		}
		if (turnController.isMoveCooldownZero())
			doTurn(dx, dy);
	}

	@Override
	protected BufferedImage[] getHealthBarSprites() {
		return SpriteCompendium.getHealthBarSprites().getStartingFrames();
	}

	@Override
	public void setSprites() {
		super.setSprites();
		setWalkingAnimations(SpriteCompendium.getPlayerSprites());
		initializeHealthBarView();
	}

	public void setLux(boolean b) {
		lux = b;
	}

	@Override
	public void newStandardCamera() {
		cam = new Camera();
		cam.subscribeTo(this);
	}

	public boolean getGodmode() {
		return godmode;
	}

	public void setGodmode(boolean b) {
		godmode = b;
		stats.setAttack(100000000);
	}

	public boolean getNoclip() {
		return noclip;
	}

	public void setNoclip(boolean b) {
		noclip = b;
	}

	/*
	 * XXX Put this method to shopkeeper (or generally: Interactable) class
	 * instead of keeping it here.
	 */
	@Override
	public void stopInteractingWithOtherInventory() {
		if (getInvHandler().getOtherInvOwner() instanceof Shopkeeper) {
			((Shopkeeper) getInvHandler().getOtherInvOwner()).untalk();
		}
		if (isInteractingWithOtherInventory()) {
			invController.stopInteracting();
		}
		super.stopInteractingWithOtherInventory();
	}

	@Override
	public void interactWithOtherInventory(HasInventory other) {
		super.interactWithOtherInventory(other);
		invController.addInteractionInventory(other.getInventory());
	}

	@Override
	public void attackThis(Living attacker, int damage) {
		if (!godmode) {
			super.attackThis(attacker, damage);
		}
	}

	@Override
	public void attackThisTrue(Living attacker, int damage) {
		if (!godmode) {
			super.attackThisTrue(attacker, damage);
		}
	}

	@Override
	public void setDrop() {
	}

	@Override
	public boolean isHostile(Attackable a) {
		if (a instanceof Shopkeeper) {
			return false;
		} else {
			return super.isHostile(a);
		}
	}
}
