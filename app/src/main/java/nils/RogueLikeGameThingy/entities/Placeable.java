package entities;

import model.Floor;

public interface Placeable {
	abstract void placeOnFloor(Floor f);
}
