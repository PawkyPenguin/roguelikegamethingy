package entities;

public class ActionTalk extends Action {

	public ActionTalk(Living actor, Entity actee) {
		super(actor, actee);
	}

	@Override
	protected boolean tryExecute() {
		if (getActee() instanceof LivingTalker) {
			((LivingTalker) getActee()).talkTo(getActor());
			return true;
		} else {
			return false;
		}
	}

	@Override
	protected void stop() {
		super.stop();
		((LivingTalker) getActee()).untalk();
	}

	@Override
	protected boolean execute() {
		// TODO Auto-generated method stub
		return false;
	}

}
