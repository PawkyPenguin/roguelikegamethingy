package entities;

import view.SpriteCompendium;

public class Stairs extends BackgroundInteractable {

	public Stairs() {

	}

	@Override
	public boolean interact(Living liv) {
		if (isInteractable(liv)) {
			((Player) liv).setIsDescending(true);
			liv.stopInteractingWithOtherInventory();
			return true;
		} else {
			return false;
		}
	}

	@Override
	protected void setSprites() {
		setStandardAnimation(SpriteCompendium.getStairsSprites().getAnimation("stoneStairs"));
	}

	@Override
	protected void setWalkingCost() {
		walkingCost = 0;
	}

	@Override
	public boolean isInteractable(Living liv) {
		if (liv instanceof Player) {
			return true;
		} else {
			return false;
		}
	}
}
