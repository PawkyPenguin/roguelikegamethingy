package entities;

import java.awt.image.BufferedImage;

import view.Animation;

public abstract class BackgroundInteractable implements Interactable {
	private Animation animation;
	protected int walkingCost;

	public BackgroundInteractable() {
		setSprites();
		setWalkingCost();
	}

	protected void setStandardAnimation(Animation animation) {
		this.animation = animation;
	}

	public void tick(double timeSinceLastFrame) {
		animation.tick(timeSinceLastFrame);
	}

	public BufferedImage getCurrentFrame() {
		return animation.getCurrentFrame();
	}

	protected abstract void setSprites();

	protected abstract void setWalkingCost();

	protected int getWalkingCost() {
		return walkingCost;
	}
}
