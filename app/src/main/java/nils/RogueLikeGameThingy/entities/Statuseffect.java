package entities;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import entities.Living;
import view.Animation;

public abstract class Statuseffect {
	protected EnumStatuseffectType statusType;
	protected boolean stackable;
	protected Animation sprite;
	private int lvl = 1;
	private Living owner;
	private int turnsLasting = 0;
	
	public Statuseffect(){
		setType();
		setStackable();
		setSprite();
		setTurnsLasting(2 + (int) Math.log(Math.random() * 2));
	}
	
	public Statuseffect(int level){
		setType();
		setStackable();
		setSprite();
		setLevel(level);
		setTurnsLasting(1 + getLevel() + ((int) Math.log(getLevel() * Math.random() * 2)));
	}
	
	public void setOwner(Living l){
		owner = l;
	}
	
	public static ArrayList<Statuseffect> getStatuseffectsOfType(Statuseffect eff, ArrayList<Statuseffect> list){
		ArrayList<Statuseffect> listOfType = new ArrayList<Statuseffect>();
		for(int i = 0; i < list.size(); i++){
			if(list.get(i).getClass().isInstance(eff)){
				listOfType.add(list.get(i));
			}
		}
		return listOfType;
	}
	
	public Statuseffect generateMaxOfType(Statuseffect eff, ArrayList<Statuseffect> list){
		Statuseffect maximum = eff;
		for(Statuseffect currentEff:getStatuseffectsOfType(eff, list)){
			maximum = maximum.generateMax(currentEff);
		}
		return maximum;
	}
	
	public Statuseffect generateMax(Statuseffect eff){
		Statuseffect newEff = null;
		try {
			newEff = eff.getClass().newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		newEff.setLevel(getMaxLevel(eff));
		if(getMaxLevel(eff) == eff.getLevel()){			
			newEff.setOwner(eff.getOwner());
		}
		else{
			newEff.setOwner(getOwner());
		}
		newEff.setTurnsLasting(getMaxTurns(eff));
		return newEff;
	}
	
	public int getMaxLevel(Statuseffect eff){
		return eff.getLevel() < getLevel() ? getLevel() : eff.getLevel();
	}
	
	public int getMaxTurns(Statuseffect eff){
		return eff.getTurnsLasting() < getTurnsLasting() ? getTurnsLasting() : eff.getTurnsLasting();
	}
	
	public int getTurnsLasting(){
		return turnsLasting;
	}
	
	public Living getOwner(){
		return owner;
	}
	
	public void setLevel(int l){
		lvl = l;
	}
	
	public int getLevel(){
		return lvl;
	}
	
	public void setTurnsLasting(int turns){
		turnsLasting = turns;
	}
	
	public int getLastingTurns(){
		return turnsLasting;
	}
	
	public Statuseffect getMax(Statuseffect stat){
		if(statusType == stat.statusType){
			if(turnsLasting > stat.getLastingTurns()){
				return this;
			}
			else{
				return stat;
			}
		}
		else{
			return null;
		}
	}
	
	public void tick(double timeSinceLastFrame){
		sprite.tick(timeSinceLastFrame);
	}
	
	public void takeEffect(Living e) {
		if(getTurnsLasting() <= 0){
			removeEffect(e);
		}
		setTurnsLasting(getTurnsLasting() - 1);
	}
	
	protected abstract void setType();
	
	protected abstract void setStackable();
	
	public abstract void addEffect(Living e);
	
	public abstract void removeEffect(Living e);
	
	protected abstract void setSprite();
	
	protected EnumStatuseffectType getStatusType(){
		return statusType;
	}
	
	public BufferedImage getSprite(){
		return sprite.getCurrentFrame();
	}
	
	public String getPrefixDescriptionOfEffect(){
		switch(statusType){
		case BURNT:
			return "Sets user on fire";
		case FROZEN:
			return "Freezes user";
		case REGENERATING:
			return "Heals user over time";
		case PARALYSED:
			return "Paralyses user";
		case POISONED:
			return "Poisons user";
		default:
			return null;
		}
	}
	
	public String getInfixDescriptionOfEffect(){
		switch(statusType){
		case BURNT:
			return "sets him on fire";
		case FROZEN:
			return "freezes him";
		case REGENERATING:
			return "adds a regeneration effect";
		case PARALYSED:
			return "paralyses him";
		case POISONED:
			return "poisons him";
		default:
			return null;
		}
	}
}
