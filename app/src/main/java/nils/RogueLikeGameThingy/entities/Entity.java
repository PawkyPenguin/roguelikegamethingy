package entities;

import java.awt.image.BufferedImage;

import model.Floor;
import view.Animation;
import view.EntityView;

public abstract class Entity implements Placeable {
	protected boolean solid = true;
	protected boolean opaque = false;
	public int x;
	public int y;
	protected int walkingCost;
	protected EntityView view;
	protected Floor floor;

	public Entity() {
		setStandardAttributes();
	}

	public Entity(int x, int y) {
		this.x = x;
		this.y = y;
		setStandardAttributes();
	}

	protected void setStandardAttributes() {
		opaque = false;
		solid = true;
		walkingCost = 1000;
		setView();
	}

	public void setAttributes(Floor floor) {
		this.floor = floor;
		placeOnFloor(floor);
		setSprites();
	}

	protected void setIdleAnimation(Animation idle) {
		view.setIdleAnimation(idle);
	}

	public Floor getFloor() {
		return floor;
	}

	public abstract void setSprites();

	public abstract void placeOnFloor(Floor floor);

	protected abstract void setWalkingCost();

	protected void setView() {
		new EntityView(this);
	}

	public boolean isOpaque() {
		return opaque;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public abstract int getDepth();

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	protected void updateView() {
		view.publisherUpdated(this);
	}

	public void tick(double timeSinceLastFrame) {
		view.tick(timeSinceLastFrame);
	}

	public int getWalkingCost() {
		return walkingCost;
	}

	public Coordinate getCoordinate() {
		return new Coordinate(x, y);
	}

	public BufferedImage getCurrentFrame() {
		if (view.getCurrentFrame() == null) {
			return null;
		} else {
			return view.getCurrentFrame();
		}
	}

	public BufferedImage getStartingFrame() {
		if (view.getStartingFrame() == null) {
			return null;
		} else {
			return view.getStartingFrame();
		}
	}

	public EntityView getView() {
		return view;
	}

	public boolean isSolid() {
		return solid;
	}

	public void subscribeThis(EntityView view) {
		this.view = view;
	}

	public abstract void removalAction();
}
