package entities;

import java.awt.image.BufferedImage;

import model.EnumEnemyClasses;
import view.SpriteCompendium;

public class Tree extends Attackable {
	public Tree() {
		super();
	}

	public Tree(int x, int y) {
		super(x, y);
	}

	@Override
	protected void setStandardAttributes() {
		super.setStandardAttributes();
		initializeHealthBar(100);
		setHealth(1);
	}

	@Override
	protected void setType() {
		enemyClass = EnumEnemyClasses.NEUTRAL_OBJECT;
	}

	@Override
	protected void setWalkingCost() {
		walkingCost = 1000;
	}

	@Override
	protected void attackThis(Living attacker, int damage) {
		damage(damage);
	}

	@Override
	protected void attackThisTrue(Living attacker, int damage) {
		damage(damage);
	}

	@Override
	protected BufferedImage[] getHealthBarSprites() {
		return null;
	}

	@Override
	public void setSprites() {
		setIdleAnimation(SpriteCompendium.getObstacleSprites().getAnimation("ordinaryTree"));
		initializeHealthBarView();
	}

	@Override
	public void setDrop() {
	}
}
