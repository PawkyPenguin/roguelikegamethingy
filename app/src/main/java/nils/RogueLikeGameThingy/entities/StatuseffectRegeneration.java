package entities;

import view.SpriteCompendium;

public class StatuseffectRegeneration extends Statuseffect{

	@Override
	public void takeEffect(Living e){
		e.heal(20);
		super.takeEffect(e);
	}
	
	@Override
	protected void setType() {
		statusType = EnumStatuseffectType.REGENERATING;
	}

	@Override
	protected void setStackable() {
		stackable = false;
	}

	@Override
	public void addEffect(Living e) {
		e.addStatuseffect(this);
	}

	@Override
	public void removeEffect(Living e) {
		e.removeThisStatuseffect(this);
	}

	@Override
	protected void setSprite() {
		sprite = SpriteCompendium.getStatuseffectSprites().getAnimation("statuseffectRegen");
	}

}
