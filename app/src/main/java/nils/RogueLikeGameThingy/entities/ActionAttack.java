package entities;

import java.util.ArrayList;

public class ActionAttack extends Action {

	public ActionAttack(Living actor, Entity actee) {
		super(actor, actee);
	}

	@Override
	protected boolean execute() {
		ArrayList<Action> actions = getActor().getActions();
		for (int i = 0; i < actions.size(); i++) {
			if (actions.get(i) instanceof ActionTalk) {
				actions.get(i).stop();
			}
		}
		getActor().fight((Attackable) getActee());

		return true;
	}

	@Override
	protected boolean tryExecute() {
		if (getActee() instanceof Attackable) {
			if (getActor().isHostile((Attackable) getActee())) {
				return execute();
			} else {
				ActionInteract ai = new ActionInteract(getActor(), getActee());
				return ai.tryExecute();
			}
		} else if (getActee() instanceof Wall) {
			ActionInteract ai = new ActionInteract(getActor(), getActee());
			return ai.tryExecute();
		} else {
			return false;
		}
	}

}
