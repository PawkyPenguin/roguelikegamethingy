package entities;

import java.util.LinkedList;

public class AttackTimerStandard extends AttackTimer{
	double timeLeft;
	LinkedList<Double> timeIntervals;

	@Override
	public boolean countDown(double time) {
		timeLeft -= time;
		if(timeLeft <= 0){
			timeLeft = timeIntervals.remove();
			return true;
		}
		return false;
	}
	
	public void setTimeIntervals(LinkedList<Double> t){
		timeIntervals = t;
		timeLeft = timeIntervals.peekFirst();
	}
	
}
