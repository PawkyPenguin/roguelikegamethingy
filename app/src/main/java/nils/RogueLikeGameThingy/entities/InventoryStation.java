package entities;

import crafting.Crafter;
import item.Item;
import model.HasInventory;
import model.Inventory;
import model.InventoryHandler;

public abstract class InventoryStation extends Foreground implements HasInventory, Interactable {
	protected InventoryHandler invHandler;
	protected Crafter crafter;
	protected int widthOfFields;
	protected int heightOfFields;

	public InventoryStation(int x, int y) {
		super(x, y);
		setCrafter();
		setDimensions();
		solid = true;
		opaque = false;
	}

	public int getFieldsWidth() {
		return widthOfFields;
	}

	public int getFieldsHeight() {
		return heightOfFields;
	}

	protected abstract void setDimensions();

	public abstract void setCrafter();

	@Override
	public Inventory getInventory() {
		return invHandler.getInventory();
	}

	@Override
	public void initInventory() {
		invHandler = new InventoryHandler();
		invHandler.setOwner(this);
	}

	@Override
	public void removalAction() {
		for (int i = 0; i < invHandler.getInventory().getInventory().size(); i++) {
			Item drop = invHandler.getInventory().getInventory().get(i).getItem();
			if (drop != null) {
				floor.dropInCircle(invHandler.getInventory().getInventory().get(i).getItem(), x, y);
			}
		}
	}

}
