package entities;

import view.SpriteCompendium;

public class DecorationMoss extends Decoration{

	@Override
	protected void setSprites() {
		animation = SpriteCompendium.getDecorationSprites().getAnimation("moss");
	}

}
