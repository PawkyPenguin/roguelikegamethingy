package entities;

import java.util.ArrayList;

public class ActionMove extends Action {
	private int x;
	private int y;

	public ActionMove(Living actor, int x, int y) {
		setActor(actor);
		this.x = x;
		this.y = y;
	}

	@Override
	protected void stop() {
		super.stop();
	}

	@Override
	protected Entity getActee() {
		return getActor().getFloor().getForegroundObject(x, y);
	}

	@Override
	protected boolean execute() {
		getActor().moveTo(x, y);
		ArrayList<Action> actions = getActor().getActions();
		for (int i = 0; i < actions.size(); i++) {
			if (actions.get(i) instanceof ActionInteract || actions.get(i) instanceof ActionTalk) {
				actions.get(i).stop();
			}
		}
		return true;
	}

	@Override
	protected boolean tryExecute() {
		if (getActor().getFloor().isPosFreeForMoving(x, y)) {
			return execute();
		} else if (getActee() instanceof Wall) {
			ActionInteract ac = new ActionInteract(getActor(), getActee());
			return ac.tryExecute();
		} else if (getActee() instanceof Interactable) {
			ActionInteract ac = new ActionInteract(getActor(), getActee());
			return ac.tryExecute();
		} else {
			return false;
		}
	}

}
