package entities;

import java.util.LinkedList;

import model.Direction;
import model.EnumDirection;

public class AttackPatternFactory {
	
	public static AttackPattern straightLine(Coordinate coord, EnumDirection dir, int length){
		LinkedList<Coordinate> straightLine = new LinkedList<Coordinate>();
		Coordinate nextCoord = coord;
		for(int i = 0; i <= length; i++){
			straightLine.add(nextCoord);
			nextCoord = Direction.getFieldInDirection(nextCoord, dir);
		}
		AttackPattern pattern = new AttackPattern();
		pattern.setAttackedFields(straightLine);
		return pattern;
	}
}
