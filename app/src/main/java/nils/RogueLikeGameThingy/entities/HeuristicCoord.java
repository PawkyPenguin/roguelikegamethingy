package entities;

import java.util.ArrayList;

public class HeuristicCoord extends Coordinate {
	int value;
	HeuristicCoord predecessor;
	ArrayList<Coordinate> adjacents;

	public HeuristicCoord(int x, int y, int value) {
		super(x, y);
		adjacents = new ArrayList<Coordinate>();
		for (int i = -1; i < 2; i++) {
			for (int j = -1; j < 2; j++) {
				if (i == 0 && j == 0)
					continue;
				adjacents.add(new Coordinate(x + i, y + j));
			}
		}
		this.value = value;
	}
}
