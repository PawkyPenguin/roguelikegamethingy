package entities;

import java.awt.image.BufferedImage;

import model.EnumEnemyClasses;

public class Smith extends LivingTalker {

	@Override
	public boolean interact(Living l) {
		return true;
	}

	@Override
	public void talkTo(Living actor) {
		talk("Good day, adventurer.");
	}

	@Override
	public void newStandardCamera() {
	}

	@Override
	protected void setOwnerIndex() {
		ownerIndex = 3;
	}

	@Override
	public void doTurn() {
	}

	@Override
	public void setDrop() {
	}

	@Override
	protected void setType() {
		enemyClass = EnumEnemyClasses.SHOPKEEPER;
	}

	@Override
	protected BufferedImage[] getHealthBarSprites() {
		return null;
	}

	@Override
	protected void setWalkingCost() {
	}
}
