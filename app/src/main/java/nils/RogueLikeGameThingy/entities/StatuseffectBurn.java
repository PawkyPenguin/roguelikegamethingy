package entities;


import view.SpriteCompendium;

//FIXME while burning, sipping a health potion does not cause the statuseffect to take effect.

public class StatuseffectBurn extends Statuseffect{
	boolean spread;

	public StatuseffectBurn() {
		super();
	}
	
	public StatuseffectBurn(int level){
		super(level);
	}
	
	public void burn(Living l){
		boolean isMonsterBurningAlready = false;
		for(Statuseffect eff:l.getStatuseffects()){
			if(eff instanceof StatuseffectBurn){
				isMonsterBurningAlready = true;
			}
		}
		if(!isMonsterBurningAlready){
			applyStatuseffect(l);
		}
	}
	
	protected void spread(Living l){
		boolean isMonsterBurningAlready;
		for(Living liv:l.getAdjacentLivings()){
			isMonsterBurningAlready = false;
			for(Statuseffect eff:liv.getStatuseffects()){
				if(eff instanceof StatuseffectBurn){
					isMonsterBurningAlready = true;
				}
			}
			if(!isMonsterBurningAlready)
				applyStatuseffect(liv);
		}
	}

	protected void applyStatuseffect(Living l){
		StatuseffectBurn eff = new StatuseffectBurn();
		eff.setLevel(getLevel());
		eff.setOwner(getOwner());
		eff.setTurnsLasting(getLastingTurns());
		l.addStatuseffect(eff);
	}
	
	@Override
	public void takeEffect(Living e) {
		super.takeEffect(e);
		e.attackThisTrue(getOwner(), e.getMaxHealth() / 10 + 5 * getLevel());
		if(spread){
			spread(e);
		}
	}

	@Override
	protected void setType() {
		statusType = EnumStatuseffectType.BURNT;
	}

	@Override
	protected void setStackable() {
		stackable = true;
	}

	@Override
	public void addEffect(Living e) {
		e.addStatuseffect(this);
	}

	@Override
	public void removeEffect(Living e) {
		e.removeThisStatuseffect(this);
	}

	@Override
	protected void setSprite() {
		sprite = SpriteCompendium.getStatuseffectSprites().getAnimation("statuseffectBurn");
	}
}
