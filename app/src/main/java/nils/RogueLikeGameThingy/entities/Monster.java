package entities;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import lazyness.Chance;
import model.Direction;
import model.EnumEnemyClasses;
import model.EnumMonsterType;
import model.Floor;
import model.LightCoord;
import model.MonsterCamera;
import view.SpriteCompendium;

public class Monster extends Living {
	EnumMonsterType type;

	public Monster(int x, int y, EnumMonsterType monsterType) {
		super(x, y);
		type = monsterType;
	}

	@Override
	public void initInventory() {
		super.initInventory();
	}

	@Override
	protected void setStandardAttributes() {
		super.setStandardAttributes();
		canBeAttacked = true;
	}

	protected void setStats() {
		if (type == EnumMonsterType.GOBLIN) {
			value = 10;
			setArmor((float) (20 + Math.random() * 10 + floor.getLevel() * 10));
			setAttack((float) (10 + Math.random() * 10));
		}
		if (type == EnumMonsterType.SKELETON) {
			value = 20;
			setArmor((float) (10 + Math.random() * 10 + floor.getLevel() * 9.5));
			setAttack((float) (15 + Math.random() * 10));
		}
	}

	@Override
	public void newStandardCamera() {
		cam = new MonsterCamera();
		cam.subscribeTo(this);
	}

	@Override
	protected void setType() {
		enemyClass = EnumEnemyClasses.MONSTER;
	}

	@Override
	protected void setOwnerIndex() {
		ownerIndex = 2;
	}

	@Override
	public void tick(double timeSinceLastFrame) {
		super.tick(timeSinceLastFrame);
	}

	@Override
	protected void setWalkingCost() {
		walkingCost = (int) (getHealth() * stats.getArmor());
	}

	@Override
	public void doTurn() {
		hasAttackedThisTurn = false;
		if (conductTurn) {
			doAITurn();
		}
		afterTurn();
		finishedTurn = true;
	}

	private void doAITurn() {
		for (int i = 0; i < getDirection().perimeterList.size(); i++) {
			if (floor.isCoordinateWithinLayer(x + getDirection().perimeterList.get(i).x,
					y + getDirection().perimeterList.get(i).y)) {
				Entity facedObject = floor.getForegroundObject(x + getDirection().perimeterList.get(i).x,
						y + getDirection().perimeterList.get(i).y);
				if (facedObject instanceof Player) {
					setDirection(getDirection().perimeterList.get(i));
					fight((Living) facedObject);
					break;
				}
			}
		}
		if (!hasAttackedThisTurn) {
			boolean triedToWalkTowardsPlayer = false;
			ArrayList<Coordinate> walkableFields = getAdjacentWalkableFields();
			byte[][] lightMap = cam.getVisibleFields();
			for(int i = 0; i < lightMap.length; i++){
				for(int j = 0; j < lightMap[0].length; j++){
					if(lightMap[i][j] < 2){
						continue;
					}
					Entity sightedObject = floor.getForegroundObject(i, j);
					if (sightedObject instanceof Player) {
						ArrayList<Coordinate> path = getBestPath(i, j);
						if (path != null && !path.isEmpty()) {
							walkTowardsCoordinateIfPossible(path.get(0), walkableFields);
							triedToWalkTowardsPlayer = true;
							break;
						}
					}
				}
			}
			if (!triedToWalkTowardsPlayer) {
				walkToRandomAdjacentWalkable(walkableFields);
			}
		}
	}

	private ArrayList<Coordinate> getBestPath(int x, int y) {
		ArrayList<HeuristicCoord> openList = new ArrayList<HeuristicCoord>();
		ArrayList<HeuristicCoord> closedList = new ArrayList<HeuristicCoord>();
		HeuristicCoord currentCoord;
		Coordinate target = new Coordinate(x, y);
		openList.add(new HeuristicCoord(x, y, 0));
		while (!openList.isEmpty()) {
			// TODO test for effeeshency
			int iSmallestHC = indexOfSmallestHeuristicCoord(openList, target);
			currentCoord = openList.get(iSmallestHC);
			openList.remove(iSmallestHC);
			if (currentCoord.x == this.x && currentCoord.y == this.y) {
				ArrayList<Coordinate> bestPath = new ArrayList<Coordinate>();
				// TODO make this more effeeshient by saving the path
				bestPath.add(new Coordinate(currentCoord.predecessor.x, currentCoord.predecessor.y));
				return bestPath;
			}
			closedList.add(currentCoord);
			addHCSuccessorsToList(currentCoord, openList, closedList);
		}
		return null;
	}

	private void addHCSuccessorsToList(HeuristicCoord coord, ArrayList<HeuristicCoord> list,
			ArrayList<HeuristicCoord> closedList) {
		for (int i = 0; i < coord.adjacents.size(); i++) {
			Coordinate successor = coord.adjacents.get(i);
			if (!floor.isCoordinateWithinLayer(successor.x, successor.y) || successor.isInHList(closedList)) {
				continue;
			}
			int newValue;
			if (floor.getForegroundObject(successor.x, successor.y) instanceof Monster) {
				newValue = coord.value;
			} else {
				newValue = coord.value + floor.getWalkingCost(successor.x, successor.y);
			}
			HeuristicCoord successorH = new HeuristicCoord(successor.x, successor.y, 0);
			successorH.predecessor = coord;
			int indexOfSuccessor = successorH.getIndexInList(list);
			if (indexOfSuccessor != -1 && newValue >= list.get(indexOfSuccessor).value) {
				continue;
			} else if (indexOfSuccessor != -1) {
				list.get(indexOfSuccessor).value = newValue;
			} else {
				successorH.value = newValue;
				list.add(successorH);
			}
		}
	}

	private int indexOfSmallestHeuristicCoord(ArrayList<HeuristicCoord> list, Coordinate target) {
		int minValueIndex = 0;
		int minValue = list.get(0).value;
		double minDistance = Coordinate.getDistance(list.get(0), target);
		for (int i = 1; i < list.size(); i++) {
			if (list.get(i).value < minValue
					|| (list.get(i).value == minValue && Coordinate.getDistance(list.get(i), target) < minDistance)) {
				minValueIndex = i;
				minValue = list.get(i).value;
				minDistance = Coordinate.getDistance(list.get(i), target);
			}
		}
		return minValueIndex;
	}

	private void walkTowardsCoordinateIfPossible(Coordinate coord, ArrayList<Coordinate> walkableFields) {
		Coordinate thisCoord = new Coordinate(x, y);
		Coordinate moveVector;
		if (isFieldInList(moveVector = Direction.getDirectionVector(thisCoord, coord), walkableFields)) {
			move(moveVector);
		} else if (isFieldInList(
				moveVector = Direction.getDirectionVector(thisCoord, new Coordinate(coord.x, thisCoord.y)),
				walkableFields)) {
			move(moveVector);
		} else if (isFieldInList(
				moveVector = Direction.getDirectionVector(thisCoord, new Coordinate(thisCoord.x, coord.y)),
				walkableFields)) {
			move(moveVector);
		} else {
			walkToRandomAdjacentWalkable(walkableFields);
		}
	}

	private boolean isFieldInList(Coordinate coord, ArrayList<Coordinate> checkList) {
		for (int i = 0; i < checkList.size(); i++) {
			if (coord.resembles(checkList.get(i))) {
				return true;
			}
		}
		return false;
	}

	private void walkToRandomAdjacentWalkable(ArrayList<Coordinate> walkableFields) {
		Coordinate chosenCoord = getRandomAdjacentWalkable(walkableFields);
		if (chosenCoord != null) {
			move(chosenCoord);
		} else {
			setDirection(0, 0);
		}
	}

	private Coordinate getRandomAdjacentWalkable(ArrayList<Coordinate> walkableFields) {
		if (walkableFields.size() != 0) {
			return walkableFields.get((int) (Math.random() * walkableFields.size()));
		} else {
			return null;
		}
	}

	private ArrayList<Coordinate> getAdjacentWalkableFields() {
		ArrayList<Coordinate> coords = new ArrayList<Coordinate>();
		for (int i = 0; i < getDirection().perimeterList.size(); i++) {
			Coordinate coord = getDirection().perimeterList.get(i);
			if (floor.isCoordinateWithinLayer(x + coord.x, y + coord.y)) {
				if ((floor.isPosFreeForMoving(x, y + coord.y) || floor.getForegroundObject(x, y + coord.y) == this)
						&& (floor.isPosFreeForMoving(x + coord.x, y)
								|| floor.getForegroundObject(x + coord.x, y) == this)) {
					if (floor.isPosFreeForMoving(x + coord.x, y + coord.y)) {
						coords.add(new Coordinate(coord.x, coord.y));
					}
				}
			}
		}
		return coords;
	}

	@Override
	protected BufferedImage[] getHealthBarSprites() {
		return SpriteCompendium.getMobHealthBarSprites().getCurrentFrames();
	}

	@Override
	public void placeOnFloor(Floor f) {
		super.placeOnFloor(f);
		initializeHealthBar(100 + 20 * floor.getLevel());
		setHealth(getMaxHealth());
		setStats();
		setWalkingCost();
	}

	@Override
	public void setSprites() {
		super.setSprites();
		setWalkingAnimations(SpriteCompendium.getMonsterSprites(type));
		initializeHealthBarView();
	}

	@Override
	public void setDrop() {
		if (type == EnumMonsterType.SKELETON) {
			if (Chance.oneTenth()) {
				drop = Chest.generateBow(floor.getLevel());
			}
		}
		if (type == EnumMonsterType.GOBLIN) {
			if (Chance.oneTwentieth()) {
				drop = Chest.generateSword(floor.getLevel());
			}
			if (Chance.oneTwentieth()) {
				drop = Chest.generateChest(floor.getLevel());
			}
		}
	}
}
