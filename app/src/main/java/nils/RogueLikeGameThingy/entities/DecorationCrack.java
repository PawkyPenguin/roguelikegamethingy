package entities;

import view.SpriteCompendium;

public class DecorationCrack extends Decoration {

	@Override
	protected void setSprites() {
		animation = SpriteCompendium.getDecorationSprites().getAnimation("cracks").clone();
	}

}
