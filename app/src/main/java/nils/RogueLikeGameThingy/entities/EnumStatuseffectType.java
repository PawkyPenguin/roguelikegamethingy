package entities;

public enum EnumStatuseffectType {
	FROZEN, BURNT, PARALYSED, POISONED, REGENERATING;
}
