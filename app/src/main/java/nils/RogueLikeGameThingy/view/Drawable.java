package view;

import java.awt.image.BufferedImage;

public abstract class Drawable {
	protected int x;
	protected int y;

	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getScreenPosX() {
		return x;
	}

	public int getScreenPosY() {
		return y;
	}

	public abstract BufferedImage getCurrentFrame();
}
