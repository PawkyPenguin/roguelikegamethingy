package view;

import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

public class AnimationLoader {
	private static ArrayList<AnimationSet> loadedAnimations = new ArrayList<AnimationSet>();
	private static ArrayList<String> animationNames = new ArrayList<String>();
	
	public static AnimationSet getAnimationSet(String name){
		if(animationNames.contains(name)){
			return loadedAnimations.get(animationNames.indexOf(name));
		}
		else{
			loadAnimation(name);
			return loadedAnimations.get(loadedAnimations.size() - 1);
		}
	}
	
	private static void loadAnimation(String name){
		//TODO transfer to list
		animationNames.add(name);
		if(name == "skeleton"){
			AnimationSet newAnimationSet = new AnimationSet();
			Animation newAnimation = new Animation();
			try {
				newAnimation = new Animation(ImageIO.read(newAnimation.getClass().getClassLoader().getResourceAsStream("gfx/skeleton.gif")));
			} catch (IOException e) {
				e.printStackTrace();
			}
			newAnimationSet.addAnimation(newAnimation, name);
			loadedAnimations.add(newAnimationSet);
		}
		else{
			throw new IllegalStateException("Animation file name not found");
		}
	}
}
