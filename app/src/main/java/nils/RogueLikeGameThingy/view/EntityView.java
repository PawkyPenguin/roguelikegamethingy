package view;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import entities.Entity;

public class EntityView {
	protected Animation currentAnimation;
	private boolean animate;
	private Entity master;
	
	public EntityView(Entity liv){
		subscribeTo(liv);
		master = liv;
	}
	
	public void setIdleAnimation(Animation idle){
		currentAnimation = idle.clone();
		if(currentAnimation.getSprites().size() > 1){
			animate = true;
			master.getFloor().addToAnimationList(this);
		}
	}
	
	public ArrayList<BufferedImage> getSprites(){
		return currentAnimation.getSprites();
	}
	
	public BufferedImage getCurrentFrame(){
		if(currentAnimation == null){
			return null;
		}
		return currentAnimation.getCurrentFrame();
	}
	
	public BufferedImage getStartingFrame(){
		return currentAnimation.getStartingFrame();
	}
	
	private void subscribeTo(Entity pub){
		pub.subscribeThis(this);
	}
	
	public void publisherUpdated(Entity pub){}
	
	public void tick(double timeSinceLastFrame){
		if(animate){
			currentAnimation.tick(timeSinceLastFrame);
			if(currentAnimation.reachedEnd()){
				master.getFloor().removeFromAnimationList(this);
			}
		}
	}
}
