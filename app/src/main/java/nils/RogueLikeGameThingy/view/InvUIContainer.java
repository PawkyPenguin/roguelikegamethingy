package view;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import entities.Coordinate;
import model.EnumInvButtonPosition;
import model.InvFieldButton;
import model.InvFieldButtonCraft;
import model.InventoryField;
import model.MouseBox;

public class InvUIContainer extends ScalableContainer {
	private ArrayList<InvFieldUI> invUI;
	private static final int standardMaxRows = 4;
	private static final int standardMaxColumns = 6;
	private int maxRows;
	private int maxColumns;
	private FlyweightGraphics scaledGraphics;
	private BufferedImage invFieldBlankSprite = SpriteCompendium.getInvUISprites().getAnimation("invFieldBlank")
			.getCurrentFrame();

	public MouseBox getScaledMouseBox(InvFieldUI invFieldUI) {
		MouseBox box = scaledGraphics.lookupBox(invFieldUI.getMouseBox().hashCode());
		if (box == null) {
			box = invFieldUI.getMouseBox().scale(scaling);
			System.out.println("scaling: " + scaling);
			scaledGraphics.putMouseBox(invFieldUI.getMouseBox().hashCode(), box);
		}
		return box;
	}

	public BufferedImage getScaledImage(BufferedImage sprite) {
		BufferedImage img = scaledGraphics.lookupImg(sprite.hashCode());
		if (img == null) {
			img = FrameScaler.scaleImage(sprite, scaling);
			scaledGraphics.putImage(sprite.hashCode(), img);
		}
		return img;
	}

	private static InvFieldUI createNewUI(InventoryField field) {
		InvFieldUI newField = new InvFieldUI();
		newField.setInvField(field);
		return newField;
	}

	private void initFields(ArrayList<InventoryField> inventory) {
		invUI = new ArrayList<InvFieldUI>();
		for (InventoryField field : inventory)
			addRawInvFieldUIToUI(createNewUI(field));
	}

	public void setInvUI(ArrayList<InventoryField> inventory, int maxRows, int maxColumns) {
		scaledGraphics = new FlyweightGraphics();
		this.maxRows = maxRows;
		this.maxColumns = maxColumns;
		initFields(inventory);
		calculateSize();
	}

	public void setInvUI(ArrayList<InventoryField> inventory) {
		scaledGraphics = new FlyweightGraphics();
		maxRows = standardMaxRows;
		maxColumns = standardMaxColumns;
		initFields(inventory);
		calculateSize();
	}

	public void center() {
		scaledGraphics = new FlyweightGraphics();
		int sizeX;
		if (invUI.size() < maxColumns) {
			sizeX = invUI.size() * (paddingX + InvFieldUI.standardWidth) - paddingX;
		} else {
			sizeX = maxColumns * (paddingX + InvFieldUI.standardWidth) - paddingX;
		}
		int sizeY = ((invUI.size() - 1) / maxColumns + 1) * (paddingY + InvFieldUI.standardHeight) - paddingY;
		int offsetX = -sizeX / 2;
		int offsetY = -sizeY;
		Coordinate posOfInvField = new Coordinate(offsetX, offsetY);
		FrameScaler.ensureTabbingOfBox(posOfInvField, sizeX + paddingX, sizeY + paddingY);
		ensurePadding(new Coordinate(x - offsetX, y - offsetY));
		for (int i = 0; i < invUI.size(); i++) {
			InvFieldUI field = invUI.get(i);
			field.setXY(field.getMouseBox().getX() + offsetX, field.getMouseBox().getY() + offsetY);
			scaledGraphics.putMouseBox(field.getMouseBox().hashCode(), field.getMouseBox().scale(scaling));
		}
	}

	public void setInvMatrixDimensions(int maxRows, int maxColumns) {
		this.maxRows = maxRows;
		this.maxColumns = maxColumns;
		if (invUI != null) {
			calculateSize();
		}
	}

	private void calculateSize() {
		int count = 0;
		ArrayList<InvFieldUI> buttons = new ArrayList<InvFieldUI>();
		for (int i = 0; i < invUI.size(); i++) {
			if (invUI.get(i).getInvField() instanceof InvFieldButton && (((InvFieldButton) invUI.get(i).getInvField())
					.getDesiredPosition() != EnumInvButtonPosition.DEFAULT)) {
				buttons.add(invUI.get(i));
				continue;
			}
			InvFieldUI field = invUI.get(i);
			int fieldTileX = count % (maxColumns);
			int fieldTileY = count / maxColumns;
			int fieldX = x + fieldTileX * (paddingX + InvFieldUI.standardWidth);
			int fieldY = y + fieldTileY * (paddingY + InvFieldUI.standardHeight);
			field.setDimensions(fieldX, fieldY, InvFieldUI.standardWidth, InvFieldUI.standardHeight);
			scaledGraphics.putMouseBox(field.getMouseBox().hashCode(), field.getMouseBox().scale(scaling));
			count++;
		}
		if ((count - 1) / maxColumns >= maxRows) {
			throw new IllegalStateException("Size of inventory bigger than maxColumns * maxRows");
		}
		int height = (count - 1) / maxColumns + 1;
		double width = Integer.min(maxColumns, count) - 1;
		for (int i = 0; i < buttons.size(); i++) {
			InvFieldButtonCraft invField = (InvFieldButtonCraft) buttons.get(i).getInvField();
			switch (invField.getDesiredPosition()) {
			case BOT:
				InvFieldUI field = buttons.get(i);
				double fieldTileX = width / 2;
				int fieldTileY = height;
				int fieldX = x + (int) (fieldTileX * (paddingX + InvFieldUI.standardWidth));
				int fieldY = y + fieldTileY * (paddingY + InvFieldUI.standardHeight);
				field.setDimensions(fieldX, fieldY, InvFieldUI.standardWidth, InvFieldUI.standardHeight);
				scaledGraphics.putMouseBox(field.getMouseBox().hashCode(), field.getMouseBox().scale(scaling));
				break;
			default:
				throw new IllegalStateException("TODO");
			}
		}

	}

	@Override
	protected void setPaddingX(int paddingX) {
		super.setPaddingX(paddingX);
		calculateSize();
	}

	@Override
	protected void setPaddingY(int paddingY) {
		super.setPaddingY(paddingY);
		calculateSize();
	}

	@Override
	protected void setPadding(int paddingX, int paddingY) {
		setPaddingX(paddingX);
		setPaddingY(paddingY);
	}

	@Override
	protected void setScaling(double s) {
		super.setScaling(s);
		scaledGraphics = new FlyweightGraphics();
		for (InvFieldUI invFieldUI : invUI) {
			scaledGraphics.putMouseBox(invFieldUI.getMouseBox().hashCode(), invFieldUI.getMouseBox().scale(s));
			scaledGraphics.putImage(invFieldUI.getSprite().hashCode(),
					FrameScaler.scaleImage(invFieldUI.getSprite(), s));
		}
	}

	@Override
	public void setX(int x) {
		setXY(x, y);
	}

	@Override
	public void setY(int y) {
		setXY(x, y);
	}

	@Override
	public void setXY(int x, int y) {
		super.setXY(x, y);
		if (invUI != null) {
			calculateSize();
		}
	}

	@Override
	public void setXY(Coordinate c) {
		super.setXY(c);
		calculateSize();
	}

	public int getInvUISize() {
		return invUI.size();
	}

	public InvFieldUI getInvUIElement(int i) {
		return invUI.get(i);
	}

	public InvFieldUI removeInvFieldUI(int i) {
		return invUI.remove(i);
	}

	public void addInvFieldUIToUI(InvFieldUI ui) {
		invUI.add(ui);
		scaledGraphics.putMouseBox(ui.getMouseBox().hashCode(), ui.getMouseBox().scale(scaling));
	}

	private void addRawInvFieldUIToUI(InvFieldUI ui) {
		invUI.add(ui);
	}

	public boolean removeInvFieldUI(InvFieldUI invFieldUI) {
		return invUI.remove(invFieldUI);
	}

	public ArrayList<MouseBox> getMouseBoxes() {
		ArrayList<MouseBox> boxes = new ArrayList<MouseBox>();
		for (InvFieldUI ui : invUI)
			boxes.add(ui.getMouseBox());
		return boxes;
	}

	@Override
	protected void draw(Graphics g) {
		if (!isVisible()) {
			return;
		}
		for (InvFieldUI field : invUI) {
			MouseBox box = getScaledMouseBox(field);
			if (field.displayItem() && field.getInvField().getItem() != null) {
				g.drawImage(getScaledImage(invFieldBlankSprite), box.getX(), box.getY(), null);
				g.drawImage(getScaledImage(field.getItemSprite()), box.getX(), box.getY(), null);
				if (field.getInvField().getItem().isStackable()) {
					String itemAmount = Integer.toString(field.getInvField().getItemAmount());
					Coordinate stringPos = new Coordinate(30, 37);
					FrameScaler.drawTextScaledPos(g, itemAmount,
							new Coordinate(field.getMouseBox().getX(), field.getMouseBox().getY()), stringPos, true,
							scaling);
				}
			} else {
				g.drawImage(getScaledImage(field.getSprite()), box.getX(), box.getY(), null);
			}
		}
	}
}
