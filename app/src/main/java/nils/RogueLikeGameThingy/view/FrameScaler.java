package view;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import entities.Coordinate;
import model.MouseBox;

public class FrameScaler {
	private double currentScaling;
	private FlyweightGraphics flyweightImages;
	private Font scaledFont;
	private final Font originalFont;
	protected static MouseBox originalTabbingToFrameBorder = new MouseBox(0, 0, 0, 0);
	protected static MouseBox tabbingToFrameBorder;

	protected FrameScaler(Font font) {
		tabbingToFrameBorder = originalTabbingToFrameBorder.clone();
		flyweightImages = new FlyweightGraphics();
		originalFont = font;
	}

	public double getScaling() {
		return currentScaling;
	}

	protected void setScaling(double s) {
		tabbingToFrameBorder = new MouseBox();
		currentScaling = s;
		flyweightImages = new FlyweightGraphics();
		scaledFont = scaleFont(originalFont);
		tabbingToFrameBorder.setX((int) (currentScaling * originalTabbingToFrameBorder.getX()));
		tabbingToFrameBorder.setY((int) (currentScaling * originalTabbingToFrameBorder.getY()));
		tabbingToFrameBorder.setWidth((int) (currentScaling * originalTabbingToFrameBorder.getWidth()));
		tabbingToFrameBorder.setHeight((int) (currentScaling * originalTabbingToFrameBorder.getHeight()));
	}

	private Font scaleFont(Font font) {
		int newFontSize = (int) (font.getSize() * currentScaling);
		return new Font(font.getName(), font.getStyle(), newFontSize);
	}

	protected int scaleScalar(int s) {
		return (int) (s * currentScaling);
	}

	protected static int scaleScalarCeil(int s, double scaling) {
		return (int) Math.ceil(s * scaling);
	}

	protected int scaleScalarCeil(int s) {
		return (int) Math.ceil(s * currentScaling);
	}

	protected Coordinate scaleXY(int x, int y) {
		return new Coordinate((int) (x * currentScaling), (int) (y * currentScaling));
	}

	protected Coordinate scaleXY(Coordinate c) {
		return scaleXY(c.x, c.y);
	}

	protected void drawScaledImageScaledOffset(Graphics g, BufferedImage img, Coordinate pos, Coordinate offset) {
		int x = (int) (pos.getX() + currentScaling * offset.getX());
		int y = (int) (pos.getY() + currentScaling * offset.getY());
		g.drawImage(getScaledImage(img), x, y, null);
	}

	protected void drawScaledImage(Graphics g, BufferedImage img, int screenX, int screenY) {
		int x = (int) (screenX * currentScaling);
		int y = (int) (screenY * currentScaling);
		g.drawImage(getScaledImage(img), x, y, null);
	}

	public static BufferedImage scaleImage(BufferedImage img, double scaling) {
		int width = img.getWidth();
		int height = img.getHeight();
		int targetWidth = scaleScalarCeil(width, scaling);
		int targetHeight = scaleScalarCeil(height, scaling);
		AffineTransform scaleTransform = AffineTransform.getScaleInstance(scaling, scaling);
		AffineTransformOp bilinearScaleOp = new AffineTransformOp(scaleTransform,
				AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
		BufferedImage newFlyweight = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_ARGB);
		bilinearScaleOp.filter(img, newFlyweight);
		return newFlyweight;
	}

	private BufferedImage getScaledImage(BufferedImage img) {
		BufferedImage drawnImg = flyweightImages.lookupImg(img.hashCode());
		if (drawnImg == null) {
			int width = img.getWidth();
			int height = img.getHeight();
			int targetWidth = scaleScalarCeil(width);
			int targetHeight = scaleScalarCeil(height);
			AffineTransform scaleTransform = AffineTransform.getScaleInstance(currentScaling, currentScaling);
			AffineTransformOp bilinearScaleOp = new AffineTransformOp(scaleTransform,
					AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
			BufferedImage newFlyweight = new BufferedImage(targetWidth, targetHeight, BufferedImage.TYPE_INT_ARGB);
			bilinearScaleOp.filter(img, newFlyweight);
			flyweightImages.putImage(img.hashCode(), newFlyweight);
			return newFlyweight;
		}
		return drawnImg;
	}

	protected static void drawImageScaledPosition(Graphics g, BufferedImage img, Coordinate pos, double scaling) {
		g.drawImage(img, (int) (scaling * pos.getX()), (int) (scaling * pos.getY()), null);
	}

	protected static void drawTextScaledOffset(Graphics g, String text, Coordinate pos, Coordinate offset,
			boolean alignText, double scaling) {
		Font oldFont = g.getFont();
		g.setFont(new Font("Arial", Font.BOLD, (int) (16 * scaling)));
		int x = (int) (scaling * offset.getX() + pos.getX());
		int y = (int) (scaling * offset.getY() + pos.getY());
		if (alignText) {
			x -= g.getFontMetrics().stringWidth(text) / 2;
		}
		Coordinate textPos = new Coordinate(x, y);
		ensureTabbingOfBox(textPos, g.getFontMetrics().stringWidth(text), g.getFontMetrics().getHeight());
		g.drawString(text, textPos.getX(), textPos.getY());
		g.setFont(oldFont);
	}

	/*
	 * Used for drawing a string when an item is held - uses inventory
	 */
	protected void drawTextScaledOffset(Graphics g, String text, Coordinate pos, Coordinate offset, boolean alignText) {
		Font oldFont = g.getFont();
		g.setFont(scaledFont);
		int x = (int) (currentScaling * offset.getX() + pos.getX());
		int y = (int) (currentScaling * offset.getY() + pos.getY());
		if (alignText) {
			x -= g.getFontMetrics().stringWidth(text) / 2;
		}
		Coordinate textPos = new Coordinate(x, y);
		ensureTabbingOfBox(textPos, g.getFontMetrics().stringWidth(text), g.getFontMetrics().getHeight());
		g.drawString(text, textPos.getX(), textPos.getY());
		g.setFont(oldFont);
	}

	protected static void drawTextScaledPos(Graphics g, String text, Coordinate pos, Coordinate offset,
			boolean centerText, double scaling) {
		Font oldFont = g.getFont();
		g.setFont(new Font("Arial", Font.BOLD, (int) (16 * scaling)));
		int x = (int) (scaling * (offset.getX() + pos.getX()));
		int y = (int) (scaling * (offset.getY() + pos.getY()));
		if (centerText) {
			x -= g.getFontMetrics().stringWidth(text) / 2;
		}
		Coordinate textPos = new Coordinate(x, y);
		ensureTabbingOfBox(textPos, g.getFontMetrics().stringWidth(text), g.getFontMetrics().getHeight());
		g.drawString(text, textPos.getX(), textPos.getY());
		g.setFont(oldFont);
	}

	protected void drawTextScaledPos(Graphics g, String text, Coordinate pos, Coordinate offset, boolean centerText) {
		Font oldFont = g.getFont();
		g.setFont(scaledFont);
		int x = (int) (currentScaling * (offset.getX() + pos.getX()));
		int y = (int) (currentScaling * (offset.getY() + pos.getY()));
		if (centerText) {
			x -= g.getFontMetrics().stringWidth(text) / 2;
		}
		Coordinate textPos = new Coordinate(x, y);
		ensureTabbingOfBox(textPos, g.getFontMetrics().stringWidth(text), g.getFontMetrics().getHeight());
		g.drawString(text, textPos.getX(), textPos.getY());
		g.setFont(oldFont);
	}

	protected static void ensureTabbingOfBox(Coordinate coord, int boxWidth, int boxHeight) {
		if (coord.getX() < originalTabbingToFrameBorder.getX()) {
			coord.setX(originalTabbingToFrameBorder.getX());
		} else if (coord.getX() + boxWidth > originalTabbingToFrameBorder.getWidth()) {
			coord.setX(originalTabbingToFrameBorder.getWidth() - boxWidth);
		}
		if (coord.getY() < originalTabbingToFrameBorder.getY()) {
			coord.setY(originalTabbingToFrameBorder.getY());
		} else if (coord.getY() + boxHeight > originalTabbingToFrameBorder.getHeight()) {
			coord.setY(originalTabbingToFrameBorder.getHeight() - boxHeight);
		}
	}

	public void setTabbing(int x, int y, int width, int height) {
		originalTabbingToFrameBorder = new MouseBox(x, y, width, height);
	}

	public void drawTextScaledPos(Graphics g, String text, Coordinate pos) {
		Font oldFont = g.getFont();
		g.setFont(scaledFont);
		int x = (int) (currentScaling * pos.getX());
		int y = (int) (currentScaling * pos.getY());
		Coordinate textPos = new Coordinate(x, y);
		ensureTabbingOfBox(textPos, g.getFontMetrics().stringWidth(text), g.getFontMetrics().getHeight());
		g.drawString(text, textPos.getX(), textPos.getY());
		g.setFont(oldFont);
	}

	public boolean isWithinScaledBox(Coordinate mousePos, MouseBox boundingBox) {
		MouseBox scaled = flyweightImages.lookupBox(boundingBox.hashCode());
		if (scaled == null) {
			scaled = boundingBox.scale(currentScaling);
			flyweightImages.putMouseBox(boundingBox.hashCode(), scaled);
		}
		return scaled.isWithinBox(mousePos.getX(), mousePos.getY());
	}
}
