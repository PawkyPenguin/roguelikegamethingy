package view;

import java.util.ArrayList;

public class AttackAnimation extends Animation {
	private ArrayList<AnimationCheckpoint> checkpointList;
	private double time;
	private int pathIndex;
	private boolean hasFinished;
	protected double x;
	protected double y;
	private double velocityX;
	private double velocityY;

	public AttackAnimation(Animation animation, ArrayList<AnimationCheckpoint> checkpointList) {
		super(animation.getAnimationLength(), animation.getSprites());
		setDoesLoop(animation.getLoop());
		hasFinished = false;
		time = 0;
		pathIndex = 0;
		this.checkpointList = checkpointList;
		animationLength = animation.getAnimationLength();
		x = checkpointList.get(pathIndex).getX();
		y = checkpointList.get(pathIndex).getY();
		updateVelocity();
	}

	@Override
	public int getScreenPosX() {
		return DungeonView.toScreenPosX(x);
	}

	@Override
	public int getScreenPosY() {
		return DungeonView.toScreenPosY(y);
	}

	@Override
	public void tick(double timeSinceLastFrame) {
		super.tick(timeSinceLastFrame);
		if (hasFinished) {
			return;
		}
		time += timeSinceLastFrame;
		if (pathIndex + 1 >= checkpointList.size() && time >= checkpointList.get(pathIndex).getTime()) {
			//TODO modify gameloop a bit so that it still animates animations even if a mob isn't attacking anymore!
			hasFinished = true;
			x = checkpointList.get(pathIndex).getX();
			y = checkpointList.get(pathIndex).getY();
		} else {
			if (time >= checkpointList.get(pathIndex).getTime()) {
				updateVelocity();
				time = 0;
				pathIndex++;
			}
			x = (checkpointList.get(pathIndex - 1).getX() + (velocityX * time));
			y = (checkpointList.get(pathIndex - 1).getY() + (velocityY * time));
		}
	}

	private void updateVelocity() {
		velocityX = (checkpointList.get(pathIndex + 1).getX() - checkpointList.get(pathIndex).getX())
				/ checkpointList.get(1).getTime();
		velocityY = (checkpointList.get(pathIndex + 1).getY() - checkpointList.get(pathIndex).getY())
				/ checkpointList.get(1).getTime();
	}

	public boolean hasFinished() {
		return hasFinished;
	}
}
