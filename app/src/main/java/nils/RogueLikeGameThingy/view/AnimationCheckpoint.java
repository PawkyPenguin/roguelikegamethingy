package view;

import entities.Coordinate;

public class AnimationCheckpoint {
	private Coordinate coord;
	private double pointInTime;
	
	public AnimationCheckpoint(Coordinate coord, double d){
		this.coord = coord;
		this.pointInTime = d;
	}
	
	public int getX(){
		return coord.getX();
	}
	
	public int getY(){
		return coord.getY();
	}
	
	public Coordinate getCoord(){
		return coord;
	}
	
	public static double getTravelSpeed(Coordinate coord1, Coordinate coord2, float time){
		return Coordinate.getDistance(coord1, coord2) / time;
	}
	
	public double getTime(){
		return pointInTime;
	}
}
