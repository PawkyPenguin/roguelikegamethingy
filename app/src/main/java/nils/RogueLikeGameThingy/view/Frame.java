package view;

import javax.swing.JFrame;

import entities.Coordinate;
import entities.Player;
import model.Floor;
import model.GameHandler;

public class Frame extends JFrame {
	private FrameScaler scaler;
	private GameView view;
	private DungeonView dungeonView;

	public Frame() {
		super("RogueLikeGameThingy");
		setSize(800, 640);
		setUndecorated(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		setVisible(true);
	}

	public void setViewToDungeon() {
		if (view != null) {
			remove(view);
		}
		view = dungeonView;
		add(view);
		setSize(view.getSize());
		view.requestFocus();
	}

	public void makeNewDungeonView(Floor floor, Player player, MouseHandler mouseHandler, KeyHandler keyHandler) {
		dungeonView = new DungeonView();
		dungeonView.addMouseListener(mouseHandler);
		dungeonView.addKeyListener(keyHandler);
		dungeonView.setFloor(floor);
		dungeonView.addPlayer(player);
	}

	protected FrameScaler getScaler() {
		return scaler;
	}

	public void publisherUpdated(GameHandler handler) {
		view.repaintScreen();
	}
}
