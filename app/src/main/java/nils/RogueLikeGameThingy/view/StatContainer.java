package view;

import java.awt.FontMetrics;
import java.awt.Graphics;

import entities.Living;

public class StatContainer extends ScalableContainer {
	private Living master;

	public StatContainer(Living master) {
		setVisible(true);
		this.master = master;
	}

	//FIXME has no scaling yet
	@Override
	protected void draw(Graphics g) {
		if (!isVisible()) {
			return;
		}
		FontMetrics fontMetrics = g.getFontMetrics();
		String s = "Gold: " + Integer.toString(master.gold);
		int locationX = x + width;
		g.drawString(s, locationX - fontMetrics.stringWidth(s), y + fontMetrics.getHeight() + 3);
		s = "Attack: " + Float.toString(master.getAttackStat());
		int defenseY = y + 170;
		g.drawString(s, locationX - fontMetrics.stringWidth(s), defenseY);
		s = "Defense: " + Float.toString(master.getArmorStat());
		g.drawString(s, locationX - fontMetrics.stringWidth(s), defenseY + fontMetrics.getHeight());
	}

}