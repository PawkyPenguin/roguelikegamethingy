package view;

import entities.Player;

public class PlayerGraphics {
	private Player player;
	private PlayerInterfaceWindow playerInterface;
	private boolean visible;

	public PlayerGraphics(Player player) {
		this.player = player;
		visible = false;
	}

	public PlayerGraphics(Player player, PlayerInterfaceWindow cont) {
		this.player = player;
		playerInterface = cont;
		visible = true;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean v) {
		visible = v;
	}

}
