package view;

import entities.Player;

public class PlayerInterfaceWindowFactory {

	public static PlayerInterfaceWindow generateStandard(Player player) {
		PlayerInterfaceWindow newContainer = new PlayerInterfaceWindow();
		newContainer.setVisibility(true);
		newContainer.setWidth(player.getInvController().getInvInterface().getWidth());
		newContainer.setHeight(player.getInvController().getInvInterface().getHeight());
		newContainer.setHealthBarView(player.getHealthBarView());
		newContainer.addContainer(player.getInvController().getInvInterface());
		newContainer.setStatContainer(new StatContainer(player));
		newContainer.getHealthBarView().setXY(650, 2);
		newContainer.getStatContainer().setXY(600, 33);
		newContainer.setScaling(1.0);
		return newContainer;
	}
}
