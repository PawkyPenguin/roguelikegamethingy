package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import entities.Coordinate;
import model.HealthBar;
import model.HealthBuffer;
import model.MouseBox;

public class HealthBarView extends ScalableContainer {
	private final Color HPColor = Color.WHITE;
	private BufferedImage[] healthSprites = new BufferedImage[3];
	private ArrayList<Integer> healthPartitionPlacements = new ArrayList<Integer>();
	private ArrayList<Integer> healthPartitionLengths = new ArrayList<Integer>();
	private ArrayList<Integer> healthPartitionSegments = new ArrayList<Integer>();
	private HealthBar publisher;
	private int healthSegments;
	private int healthBarLengthInPixels;
	private boolean displayHealthValue = false;
	private MouseBox mouseBoundingBox;
	private FlyweightGraphics scaledGraphics;

	public HealthBarView(HealthBar hp, BufferedImage[] healthSprites) {
		mouseBoundingBox = new MouseBox(-1, -1, 0, 0);
		scaledGraphics = new FlyweightGraphics();
		setSprites(healthSprites);
		subscribeTo(hp);
	}

	@Override
	protected void setScaling(double scaling) {
		super.setScaling(scaling);
		scaledGraphics = new FlyweightGraphics();
		scaledGraphics.putMouseBox(mouseBoundingBox.hashCode(), mouseBoundingBox.scale(scaling));
		for (BufferedImage sprite : healthSprites) {
			scaledGraphics.putImage(sprite.hashCode(), FrameScaler.scaleImage(sprite, scaling));
		}
	}

	public MouseBox getScaledMouseBox(MouseBox b) {
		MouseBox box = scaledGraphics.lookupBox(b.hashCode());
		if (box == null) {
			box = b.scale(scaling);
			scaledGraphics.putMouseBox(b.hashCode(), box);
		}
		return box;
	}

	public BufferedImage getScaledImage(BufferedImage sprite) {
		BufferedImage img = scaledGraphics.lookupImg(sprite.hashCode());
		if (img == null) {
			img = FrameScaler.scaleImage(sprite, scaling);
			scaledGraphics.putImage(sprite.hashCode(), img);
		}
		return img;
	}

	private void setSprites(BufferedImage[] healthSprites) {
		if (healthSprites == null) {
			setVisible(false);
		} else {
			this.healthSprites = healthSprites;
			if (healthSprites[0] == null) {
				healthBarLengthInPixels = 30;
			} else {
				healthBarLengthInPixels = healthSprites[0].getWidth() - 2;
			}
			mouseBoundingBox.setWidth(healthBarLengthInPixels);
			mouseBoundingBox.setHeight(healthSprites[1].getHeight());
			setVisible(true);

		}
	}

	private void updateHealthPartitions() {
		int totalHealth = publisher.getHealth();
		int totalMaxHealth = publisher.getTotalMaxHealth();
		int maxBufferHealth, bufferThreshold;
		int bufferLength, bufferPlacement;
		healthPartitionLengths = new ArrayList<Integer>();
		healthPartitionPlacements = new ArrayList<Integer>();
		healthPartitionSegments = new ArrayList<Integer>();
		for (HealthBuffer buffer : publisher.getBuffers()) {
			if (buffer.isEmpty()) {
				totalMaxHealth -= buffer.getMaxBufferHealth();
			}
		}
		for (HealthBuffer buffer : publisher.getBuffers()) {
			if (buffer.isEmpty()) {
				continue;
			}
			maxBufferHealth = buffer.getMaxBufferHealth();
			bufferThreshold = buffer.getHealthThreshold();
			bufferLength = (healthBarLengthInPixels * maxBufferHealth) / totalMaxHealth;
			bufferPlacement = (healthBarLengthInPixels * bufferThreshold) / totalMaxHealth;
			healthPartitionLengths.add(bufferLength);
			healthPartitionPlacements.add(bufferPlacement);
			healthPartitionSegments.add((bufferLength * buffer.getHealth()) / maxBufferHealth);
		}
		healthSegments = (totalHealth * healthBarLengthInPixels) / totalMaxHealth;
	}

	public ArrayList<Integer> getPartitionPlacements() {
		return healthPartitionPlacements;
	}

	public ArrayList<Integer> getPartitionLengths() {
		return healthPartitionLengths;
	}

	public ArrayList<Integer> getPartitionSegments() {
		return healthPartitionSegments;
	}

	public int getHealthSegments() {
		return healthSegments;
	}

	public int getLengthInPixels() {
		return healthBarLengthInPixels;
	}

	public BufferedImage getEmptySprite() {
		return healthSprites[0];
	}

	public BufferedImage getHealthSegmentSprite() {
		return healthSprites[1];
	}

	public BufferedImage getBufferSegmentSprite() {
		return healthSprites[2];
	}

	public void publisherUpdated(HealthBar pub) {
		updateHealthPartitions();
	}

	private void subscribeTo(HealthBar healthBar) {
		publisher = healthBar;
		healthBar.subscribeThis(this);
		updateHealthPartitions();
	}

	@Override
	protected void setX(int x) {
		super.setX(x);
		scaledGraphics.removeMouseBox(mouseBoundingBox.hashCode());
		mouseBoundingBox.setX(x);
		scaledGraphics.putMouseBox(mouseBoundingBox.hashCode(), mouseBoundingBox.scale(scaling));
	}

	@Override
	protected void setY(int y) {
		super.setY(y);
		scaledGraphics.removeMouseBox(mouseBoundingBox.hashCode());
		mouseBoundingBox.setY(y);
		scaledGraphics.putMouseBox(mouseBoundingBox.hashCode(), mouseBoundingBox.scale(scaling));
	}

	@Override
	public void setXY(int x, int y) {
		setX(x);
		setY(y);
	}

	@Override
	protected void setWidth(int w) {
		super.setWidth(w);
		scaledGraphics.removeMouseBox(mouseBoundingBox.hashCode());
		mouseBoundingBox.setWidth(w);
		scaledGraphics.putMouseBox(mouseBoundingBox.hashCode(), mouseBoundingBox.scale(scaling));
	}

	@Override
	protected void setHeight(int h) {
		super.setHeight(h);
		scaledGraphics.removeMouseBox(mouseBoundingBox.hashCode());
		mouseBoundingBox.setHeight(h);
		scaledGraphics.putMouseBox(mouseBoundingBox.hashCode(), mouseBoundingBox.scale(scaling));
	}

	public MouseBox getBoundingBox() {
		return getScaledMouseBox(mouseBoundingBox);
	}

	public void setDisplayHealthValue(boolean b) {
		displayHealthValue = b;
	}

	@Override
	protected void draw(Graphics g) {
		if (!isVisible()) {
			return;
		}
		if (displayHealthValue) {
			int width = getEmptySprite().getWidth();
			int height = getEmptySprite().getHeight();
			Color oldColor = g.getColor();
			g.setColor(HPColor);
			String s = "HP: " + publisher.getHealth() + " / " + publisher.getTotalMaxHealth();
			Coordinate pos = new Coordinate(getX() + width, getY() + height - 3);
			FrameScaler.drawTextScaledPos(g, s, pos, new Coordinate(0, 0), false, scaling);
			g.setColor(oldColor);
		}

		if (getEmptySprite() != null)
			FrameScaler.drawImageScaledPosition(g, getScaledImage(getEmptySprite()), new Coordinate(x, y), scaling);
		int amountOfSegments;
		if (getPartitionPlacements().size() > 0) {
			amountOfSegments = getPartitionPlacements().get(0);
		} else {
			amountOfSegments = getHealthSegments();
		}
		for (int i = 0; i < amountOfSegments; i++) {
			FrameScaler.drawImageScaledPosition(g, getScaledImage(getHealthSegmentSprite()),
					new Coordinate(x + 1 + i, y + 1), scaling);
		}
		for (int i = 0; i < getPartitionPlacements().size(); i++) {
			for (int j = 0; j < getPartitionSegments().get(i) + 1; j++) {
				FrameScaler.drawImageScaledPosition(g, getScaledImage(getBufferSegmentSprite()),
						new Coordinate(x + getPartitionPlacements().get(i) + j + 1, y + 1), scaling);
			}
		}
	}
}
