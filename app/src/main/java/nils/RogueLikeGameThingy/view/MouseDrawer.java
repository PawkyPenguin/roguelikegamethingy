package view;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import controller.InventoryController;
import entities.Coordinate;
import entities.Player;
import item.HealthPot;
import item.Item;
import model.InventoryField;
import model.MouseBox;

public class MouseDrawer {
	private DungeonView canvas;
	private FrameScaler scaler;

	public MouseDrawer(DungeonView f) {
		canvas = f;
	}

	public void drawMouseStuff(Graphics g, Coordinate mousePos) {
		drawMouseOverInvField(g, mousePos);
	}

	private boolean isMouseOnInventory(Coordinate mousePos) {
		int x = canvas.getWidth() - canvas.getInventorySprite().getWidth();
		int y = canvas.getMenuHeight();
		int width = canvas.getInventorySprite().getWidth();
		int height = canvas.getInventorySprite().getHeight();
		return MouseBox.withinBox(mousePos, x, y, width, height);
	}

	protected final Coordinate getPickupOffsetForItemSprite() {
		return new Coordinate(16, 6);
	}

	protected final Coordinate getImageCenter() {
		return new Coordinate(DungeonView.pixels / 2, DungeonView.pixels / 2);
	}

	private void drawMouseOverInvField(Graphics g, Coordinate mousePos) {
		for (Player player : canvas.getPlayers()) {
			InventoryController invController = player.getInvController();
			if (mousePos != null) {
				if (invController.isHoldingItem()) {
					Item pickedUpItem = invController.getPickedUpItem();
					BufferedImage itemSprite = pickedUpItem.getSprite();
					Coordinate pickupOffset = getPickupOffsetForItemSprite();
					scaler.drawScaledImageScaledOffset(g, itemSprite, mousePos, getImageCenter().neg());
					if (pickedUpItem.isStackable()) {
						String itemAmount = Integer.toString(invController.getPickedUpItem().getAmount());
						scaler.drawTextScaledOffset(g, itemAmount, mousePos, pickupOffset, true);
					}
					String s = pickedUpItem.getDescription();
					checkForPot(g, player, pickedUpItem, mousePos);
					drawStringUnderMouse(g, s, mousePos);
				} else {
					InvFieldUI fieldUI = invController.getFieldUnderMouse(mousePos.x, mousePos.y);
					if (fieldUI != null) {
						InventoryField invField = fieldUI.getInvField();
						if (invField != null) {
							Item itemUnderMouse = invController.getFieldUnderMouse(mousePos.x, mousePos.y).getInvField()
									.getItem();
							if (itemUnderMouse != null) {
								String s = itemUnderMouse.getDescription();
								checkForPot(g, player, itemUnderMouse, mousePos);
								drawStringUnderMouse(g, s, mousePos);
							}
						}
					}
				}
			}
		}
	}

	/*
	 * Determines if the item was a pot. If so, prints how much the player has
	 * of the corresponding resource and also how much the player can have of it
	 * at most.
	 */
	private void checkForPot(Graphics g, Player player, Item item, Coordinate mousePos) {
		if (item instanceof HealthPot) {
			String s = playerHPToString(player);
			scaler.drawTextScaledPos(g, s,
					new Coordinate(mousePos.x - 50, mousePos.y - 30 + g.getFontMetrics().getHeight()));
		}
	}

	private final Coordinate getItemDescriptionOffset() {
		return new Coordinate(-50, -30);
	}

	// convenience
	private String playerHPToString(Player player) {
		return "Current HP: " + player.getHealth() + " / " + player.getTotalMaxHealth();
	}

	private void drawStringUnderMouse(Graphics g, String s, Coordinate mousePos) {
		Coordinate offset = getItemDescriptionOffset();
		offset.y -= s.split("\n").length * g.getFontMetrics().getHeight();
		for (String line : s.split("\n")) {
			offset.y += g.getFontMetrics().getHeight();
			scaler.drawTextScaledOffset(g, line, mousePos, offset, false);
		}
	}

	protected void setScaler(FrameScaler s) {
		scaler = s;
	}
}
