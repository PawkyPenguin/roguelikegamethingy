package view;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Autotiler {

	public static Animation getAutoTile(AnimationSet minitileset, boolean[] surroundings) {
		final int[][] arrayIndicesOfInfluencingFields = { { 0, 1, 2 }, { 0, 7, 6 }, { 4, 3, 2 }, { 4, 5, 6 } };
		Animation[] minitiles = new Animation[4];
		String[] names = { "A", "B", "C", "D" };
		for (int i = 0; i < minitiles.length; i++) {
			int[] influencingFields = arrayIndicesOfInfluencingFields[i];
			if (!surroundings[influencingFields[0]] && !surroundings[influencingFields[2]]) {
				minitiles[i] = minitileset.getAnimation(names[i] + "1");
			} else if (surroundings[influencingFields[0]] && !surroundings[influencingFields[1]]
					&& surroundings[influencingFields[2]]) {
				minitiles[i] = minitileset.getAnimation(names[i] + "0");
			} else if (surroundings[influencingFields[0]] && surroundings[influencingFields[1]]
					&& surroundings[influencingFields[2]]) {
				minitiles[i] = minitileset.getAnimation(names[i] + "2");
			} else if (surroundings[influencingFields[0]] && !surroundings[influencingFields[2]]) {
				minitiles[i] = minitileset.getAnimation(names[i] + "4");
			} else {
				minitiles[i] = minitileset.getAnimation(names[i] + "3");
			}
		}
		BufferedImage[] tiles = new BufferedImage[4];
		for (int i = 0; i < tiles.length; i++) {
			tiles[i] = minitiles[i].getCurrentFrame();
		}
		return generateTargetTile(tiles);
	}

	private static Animation generateTargetTile(BufferedImage[] minitiles) {
		BufferedImage targetTile = null;
		try {
			targetTile = SpriteCompendium.getFile("../../src/main/res/sprites/transparent.png");
		} catch (IOException e) {
		}
		Graphics2D graphics = targetTile.createGraphics();
		int[][] minitileCoords = { { 0, 0 }, { 16, 0 }, { 0, 16 }, { 16, 16 } };
		for (int i = 0; i < minitileCoords.length; i++) {
			graphics.drawImage(minitiles[i], minitileCoords[i][0], minitileCoords[i][1], null);
		}
		return new Animation(targetTile);
	}
}
