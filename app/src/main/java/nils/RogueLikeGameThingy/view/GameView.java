package view;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import entities.Player;
import model.GameHandler;

public abstract class GameView extends Canvas {
	protected FrameScaler scaler;
	protected Graphics2D g;
	protected ArrayList<Player> players;
	protected final Color defaultColor = Color.WHITE;
	protected final Font defaultFont = new Font("Arial", Font.BOLD, 16);
	protected BufferStrategy bufferStrat;
	protected final static int tabbing = 10;
	private boolean gifNyuBufferShtratPls = false;

	public GameView() {
		players = new ArrayList<Player>();
		scaler = new FrameScaler(new Font("Arial", Font.BOLD, 16));
		setVisible(true);
		changeSize(800, 640);
		setScaling(1);
	}

	public void makeStrat() {
		createBufferStrategy(2);
		bufferStrat = getBufferStrategy();
		gifNyuBufferShtratPls = false;
	}

	protected void changeSize(int x, int y) {
		setSize(x, y);
		scaler.setTabbing(tabbing, tabbing, x - tabbing, y - tabbing);
		gifNyuBufferShtratPls = true;
	}

	protected void setScaling(double newScale) {
		scaler.setScaling(newScale);
	}

	public void addPlayer(Player player) {
		players.add(player);
	}

	public void drawScaledImage(Graphics g, BufferedImage img, int screenX, int screenY) {
		scaler.drawScaledImage(g, img, screenX, screenY);
	}

	public void publisherUpdated(GameHandler handler) {
		repaintScreen();
	}

	protected void repaintScreen() {
		long wallClock = System.currentTimeMillis();
		if (gifNyuBufferShtratPls) {
			makeStrat();
		}
		Color c = defaultColor;
		Font f = defaultFont;
		if (g != null) {
			f = g.getFont();
			c = g.getColor();
		}
		g = (Graphics2D) (bufferStrat.getDrawGraphics());
		g.setColor(c);
		g.setFont(f);
		draw(g);
		g.dispose();
		bufferStrat.show();
		// System.out.println(System.currentTimeMillis() - wallClock);
	}

	protected ArrayList<Player> getPlayers() {
		return players;
	}

	protected abstract void draw(Graphics2D g);
}
