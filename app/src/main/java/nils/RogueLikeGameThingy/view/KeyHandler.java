package view;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import controller.InputDecoder;

public class KeyHandler implements KeyListener{
	InputDecoder sub;
	public static int keySize = 512;
	private boolean[] keys = new boolean[keySize];

	public boolean isKeyPressed(int keyCode){
        if((keyCode>=0)&&(keyCode<keys.length)){
        	return keys[keyCode];
        }
        else{
        	return false;       	
        }
    }
	
	@Override
	public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        if((keyCode>=0)&&(keyCode<keys.length)){
        	keys[keyCode] = true;
        }
        updateSubscriber();
    }

	@Override
	public void keyReleased(KeyEvent e) {
		int keyCode = e.getKeyCode();
        if((keyCode>=0)&&(keyCode<keys.length)){
        	keys[keyCode] = false;
        }
        updateSubscriber();
	}

	@Override
	public void keyTyped(KeyEvent e) {}
	
	private void updateSubscriber(){
		sub.publisherUpdated(this);
	}
	
	public void enlistSub(InputDecoder sub){
		this.sub = sub;
	}

	public void unlistSub(InputDecoder sub){
		this.sub = sub;
	}
	
}
