package view;

import java.awt.image.BufferedImage;

import entities.Coordinate;
import model.InvFieldEquipment;
import model.InventoryField;
import model.MouseBox;

public class InvFieldUI {
	public static final int standardWidth = 33;
	public static final int standardHeight = 33;
	private InventoryField invField;
	private MouseBox mouseBox;
	private Animation sprite;
	private boolean displayItem = true;

	public InvFieldUI() {
	}

	public InvFieldUI(int x, int y) {
		setDimensions(x, y, standardWidth, standardHeight);
	}

	public void setDimensions(Coordinate coord, Coordinate dimensions) {
		setDimensions(coord.getX(), coord.getY(), dimensions.getX(), dimensions.getY());
	}

	private void setSprite(Animation sprite) {
		this.sprite = sprite;
	}

	public void setDisplayItem(boolean b) {
		displayItem = b;
	}

	public boolean displayItem() {
		return displayItem;
	}

	public void setInvField(InventoryField field) {
		invField = field;
		switch (field.getInvFieldType()) {
		case TRASH:
			setSprite(SpriteCompendium.getInvUISprites().getAnimation("invFieldTrash"));
			break;
		case AUTOUSE:
			setSprite(SpriteCompendium.getInvUISprites().getAnimation("invFieldAuto"));
			break;
		case STORAGE:
			setSprite(SpriteCompendium.getInvUISprites().getAnimation("invFieldCommon"));
			break;
		case LOCKED:
			setSprite(SpriteCompendium.getInvUISprites().getAnimation("invFieldLocked"));
			break;
		case EQUIPMENT:
			switch (((InvFieldEquipment) field).getEquipType()) {
			case WEAPON:
				setSprite(SpriteCompendium.getInvUISprites().getAnimation("invFieldWeapon"));
				break;
			case HELMET:
				setSprite(SpriteCompendium.getInvUISprites().getAnimation("invFieldHelmet"));
				break;
			case CHEST:
				setSprite(SpriteCompendium.getInvUISprites().getAnimation("invFieldChest"));
				break;
			case LEGGINS:
				setSprite(SpriteCompendium.getInvUISprites().getAnimation("invFieldLeggins"));
				break;
			case RING:
				setSprite(SpriteCompendium.getInvUISprites().getAnimation("invFieldRing"));
				break;
			case SHIELD:
				setSprite(SpriteCompendium.getInvUISprites().getAnimation("invFieldShield"));
				break;
			default:
				break;
			}
			break;
		case BUTTON:
			setSprite(SpriteCompendium.getInvUISprites().getAnimation("invFieldCommon"));
			break;
		default:
			break;
		}
	}

	public InventoryField getInvField() {
		return invField;
	}

	public BufferedImage getItemSprite() {
		return invField.getItem().getSprite();
	}

	public void setDimensions(int x1, int y1, int width, int height) {
		mouseBox = new MouseBox(x1, y1, width, height);
	}

	public MouseBox getMouseBox() {
		return mouseBox;
	}

	public boolean isMouseWithinField(int mouseX, int mouseY) {
		return mouseBox.isWithinBox(mouseX, mouseY);
	}

	public BufferedImage getSprite() {
		return sprite.getCurrentFrame();
	}

	public void setXY(int x, int y) {
		mouseBox.setX(x);
		mouseBox.setY(y);
	}
}
