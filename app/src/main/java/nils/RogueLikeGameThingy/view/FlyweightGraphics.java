package view;

import java.awt.image.BufferedImage;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import model.MouseBox;

public class FlyweightGraphics {
	private Map<Integer, BufferedImage> images = new ConcurrentHashMap<Integer, BufferedImage>();
	private Map<Integer, MouseBox> mouseBoxes = new ConcurrentHashMap<Integer, MouseBox>();

	public BufferedImage lookupImg(int imageId) {
		return images.get(imageId);
	}

	public MouseBox lookupBox(int boxId) {
		return mouseBoxes.get(boxId);
	}

	public void putMouseBox(int boxId, MouseBox mouseBox) {
		mouseBoxes.put(boxId, mouseBox);
	}

	public void putImage(int imageId, BufferedImage newImage) {
		images.put(imageId, newImage);
	}

	public void removeMouseBox(int boxId) {
		mouseBoxes.remove(boxId);
	}

	public void removeImg(int imgId) {
		images.remove(imgId);
	}

	public boolean containsImg(int hashCode) {
		return images.containsKey(hashCode);
	}

	public boolean containsBox(int hashCode) {
		return images.containsKey(hashCode);
	}
}
