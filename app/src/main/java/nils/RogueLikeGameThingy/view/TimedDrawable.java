package view;

public abstract class TimedDrawable extends Drawable {
	private double timer;
	private double timerValue;

	public void tick(double timeSinceLastFrame) {
		timerValue -= timeSinceLastFrame;
		if (timerValue <= 0) {
			timerValue = timer;
			timerAction();
		}
	}

	protected void setTimer(double t) {
		timer = t;
	}

	protected void setTimerValue(double t) {
		timerValue = t;
	}

	protected abstract void timerAction();
}
