package view;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import entities.Attack;
import entities.Attackable;
import entities.Background;
import entities.Coordinate;
import entities.Decoratable;
import entities.Entity;
import entities.Foreground;
import entities.Living;
import entities.LivingTalker;
import entities.Monster;
import entities.Player;
import entities.Statuseffect;
import model.Floor;
import model.Game;

public class DungeonView extends GameView {
	public static final int pixels = 32;
	public static final int menuHeight = pixels;
	private BufferedImage[] lightSprites;
	private BufferedImage inventorySprite = SpriteCompendium.getInvUISprites().getAnimation("inventory")
			.getCurrentFrame();
	private Floor floor;
	private ArrayList<ContainerCluster> invContainers;
	private Coordinate floorBorder;
	private MouseDrawer mouseDrawer;

	public DungeonView() {
		super();
		players = new ArrayList<Player>();
		mouseDrawer = new MouseDrawer(this);
		invContainers = new ArrayList<ContainerCluster>();
		mouseDrawer.setScaler(scaler);
	}

	public static Coordinate convertToUnscaledScreenCoord(Coordinate coord) {
		return new Coordinate(coord.getX() * 32, coord.getY() * 32 + menuHeight);
	}

	public void setFloor(Floor floor) {
		this.floor = floor;
		floorBorder = getSize(floor);
		AnimationSet sprites = SpriteCompendium.getLightSprites();
		lightSprites = new BufferedImage[2];
		lightSprites[0] = sprites.getAnimation("dark").getCurrentFrame();
		lightSprites[1] = sprites.getAnimation("penumbra").getCurrentFrame();
	}

	public int getMenuHeight() {
		return menuHeight;
	}

	public Coordinate getFloorBorder() {
		return floorBorder;
	}

	private Coordinate getSize(Floor floor) {
		return convertToUnscaledScreenCoord(new Coordinate(floor.getRelevantX(), floor.getRelevantY()));
	}

	public static int toScreenPosY(double i) {
		return (int) (i * pixels + menuHeight);
	}

	public static int toScreenPosX(double i) {
		return (int) (i * pixels);
	}

	public static int centerToScreenPosY(int i) {
		return toScreenPosY(i) + pixels / 2;
	}

	public static int centerToScreenPosX(int i) {
		return toScreenPosX(i) + pixels / 2;
	}

	@Override
	protected void repaintScreen() {
		super.repaintScreen();
	}

	@Override
	protected void draw(Graphics2D g) {
		byte[][] lightMap = getLightMap();
		Color c = g.getColor();
		g.setColor(Color.DARK_GRAY);
		g.fillRect(0, 0, getWidth(), getHeight());
		g.setColor(c);
		drawFloorGround(g, lightMap);
		drawFloorElements(g);
		drawUIElements(g);
		drawMouseStuff(g);
	}

	/*
	 * returns lightMap. The int values have the following semantics: 0 -
	 * completely dark 1 - penumbra 2 - lit 3 - foreground object visible in
	 * penumbra
	 */
	private byte[][] getLightMap() {
		int x = floor.getRelevantX();
		int y = floor.getRelevantY();
		byte lightMap[][] = new byte[x][y];
		for (Player player : players) {
			byte[][] coord = player.getCamera().getVisibleFields();
			for (int i = 0; i < x; i++) {
				for (int j = 0; j < y; j++) {
					if (lightMap[i][j] < coord[i][j]) {
						lightMap[i][j] = coord[i][j];
					}
				}
			}
			for (int i = 0; i < x; i++) {
				for (int j = 0; j < y; j++) {
					if (lightMap[i][j] == 1 && (!(floor.getForegroundObject(i, j) instanceof Attackable)
							|| ((Attackable) floor.getForegroundObject(i, j)).isVisibleInPenumbraTo(player))) {
						lightMap[i][j] = 3;
					}
				}
			}
		}
		return lightMap;
	}

	private void drawMouseStuff(Graphics2D g) {
		Point mousePoint = getMousePosition();
		if (mousePoint == null) {
			return;
		}
		Coordinate mouseCoord = new Coordinate(mousePoint);
		mouseDrawer.drawMouseStuff(g, mouseCoord);
	}

	// TODO draw inventory for all players, not just one
	private void drawUIElements(Graphics2D g) {
		scaler.drawScaledImage(g, inventorySprite, Game.inventoryX + floorBorder.getX(), menuHeight);
		for (int i = 0; i < invContainers.size(); i++) {
			if (invContainers.get(i).isVisible()) {
				invContainers.get(i).draw(g);
			}
		}
	}

	private void drawFloorElements(Graphics2D g) {
		for (int i = 0; i < floor.getRelevantX(); i++) {
			for (int j = 0; j < floor.getRelevantY(); j++) {
				Foreground drawnElement = floor.getForegroundObject(i, j);
				if (drawnElement instanceof Monster) {
					drawMobHealthBar((Monster) floor.getForegroundObject(i, j), g);
				}
				if (drawnElement instanceof LivingTalker) {
					DialogBox dialog = ((LivingTalker) drawnElement).getDialogBox();
					if (((LivingTalker) drawnElement).isTalking()) {
						g.drawImage(dialog.getCurrentFrame(), dialog.getScreenPosX(), dialog.getScreenPosY(), null);
					}
				}
				if (drawnElement instanceof Living
						&& !((Living) floor.getForegroundObject(i, j)).hasFinishedAttacking()) {
					Attack a = ((Living) floor.getForegroundObject(i, j)).getAttackLogic().getAttack();
					drawScaledImage(g, a.getCurrentFrame(), a.getScreenPosX(), a.getScreenPosY());
					drawScaledImage(g, a.getCurrentFrame(), a.getScreenPosX(), a.getScreenPosY());
				}
			}
		}
	}

	private void drawFloorGround(Graphics2D g, byte[][] lightMap) {
		for (int i = 0; i < floor.getRelevantX(); i++) {
			for (int j = 0; j < floor.getRelevantY(); j++) {
				switch (lightMap[i][j]) {
				case 0:
					drawScaledImage(g, lightSprites[0], toScreenPosX(i), toScreenPosY(j));
					break;
				case 1:
					drawEntity(floor.getObject(i, j, 0), g);
					drawEntity(floor.getObject(i, j, 2), g);
					drawScaledImage(g, lightSprites[1], toScreenPosX(i), toScreenPosY(j));
					break;
				case 2:
					drawEntity(floor.getObject(i, j, 0), g);
					drawEntity(floor.getObject(i, j, 2), g);
					drawEntity(floor.getObject(i, j, 1), g);
					break;
				case 3:
					drawEntity(floor.getObject(i, j, 0), g);
					drawEntity(floor.getObject(i, j, 2), g);
					drawEntity(floor.getObject(i, j, 1), g);
					drawScaledImage(g, lightSprites[1], toScreenPosX(i), toScreenPosY(j));
					break;
				default:
					throw new IllegalStateException("LightMap has a value other than 1, 2 or 3 [DungeonView]");
				}
			}
		}
	}

	private void drawEntity(Background drawnEntity, Graphics2D g) {
		if (drawnEntity.getInteractable() != null) {
			BufferedImage sprite = drawnEntity.getInteractableSprite();
			drawScaledImage(g, sprite, toScreenPosX(drawnEntity.getX()), toScreenPosY(drawnEntity.getY()));
		}
	}

	private void drawEntity(Decoratable drawnEntity, Graphics2D g) {
		if (drawnEntity.getDecoration() != null) {
			BufferedImage sprite = drawnEntity.getDecorationSprite();
			drawScaledImage(g, sprite, toScreenPosX(drawnEntity.getX()), toScreenPosY(drawnEntity.getY()));
		}
	}

	private void drawEntity(Living drawnEntity, Graphics2D g) {
		ArrayList<Statuseffect> effects = ((Living) drawnEntity).getStatuseffects();
		for (Statuseffect eff : effects) {
			drawScaledImage(g, eff.getSprite(), toScreenPosX(drawnEntity.getX()),
					toScreenPosY(drawnEntity.getY() + 32 - eff.getSprite().getHeight()));
		}
	}

	private void drawEntity(Entity drawnEntity, Graphics2D g) {
		if (drawnEntity != null && drawnEntity.getCurrentFrame() != null) {
			drawScaledImage(g, drawnEntity.getCurrentFrame(), toScreenPosX(drawnEntity.getX()),
					toScreenPosY(drawnEntity.getY()));
			if (drawnEntity instanceof Decoratable) {
				drawEntity((Decoratable) drawnEntity, g);
			}
			if (drawnEntity instanceof Background) {
				drawEntity((Background) drawnEntity, g);
			}
			if (drawnEntity instanceof Living) {
				drawEntity((Living) drawnEntity, g);
			}
		}
	}

	private void drawHealthBarParts(Graphics2D g, HealthBarView view, int x, int y) {
		if (view.getEmptySprite() != null)
			drawScaledImage(g, view.getEmptySprite(), x, y);
		y++;
		x++;
		int amountOfSegments;
		if (view.getPartitionPlacements().size() > 0) {
			amountOfSegments = view.getPartitionPlacements().get(0);
		} else {
			amountOfSegments = view.getHealthSegments();
		}
		for (int i = 0; i < amountOfSegments; i++) {
			drawScaledImage(g, view.getHealthSegmentSprite(), x + i, y);
		}
		for (int i = 0; i < view.getPartitionPlacements().size(); i++) {
			for (int j = 0; j < view.getPartitionSegments().get(i) + 1; j++) {
				drawScaledImage(g, view.getBufferSegmentSprite(), x + view.getPartitionPlacements().get(i) + j, y);
			}
		}
	}

	private void drawMobHealthBar(Monster e, Graphics2D g) {
		for (Player player : players) {
			if (player.getCamera().isFieldVisible(e.x, e.y)) {
				int screenPosX = e.x * 32;
				int screenPosY = e.y * 32 + menuHeight + 25;
				drawHealthBarParts(g, e.getHealthBarView(), screenPosX, screenPosY);
				break;
			}
		}
	}

	public void addPlayer(Player player) {
		super.addPlayer(player);
		PlayerInterfaceWindow newContainer = PlayerInterfaceWindowFactory.generateStandard(player);
		invContainers.add(newContainer);
		reloadContainers(newContainer);
	}

	public void addInvInterface(PlayerInterfaceWindow invInterface) {
		invContainers.add(invInterface);
	}

	private void reloadContainers(ContainerCluster except) {
		Coordinate c = getSize(floor);
		for (ContainerCluster container : invContainers) {
			if (container == except) {
				c.x += except.getWidth();
				continue;
			}
			container.setXY(c.x, container.getY());
			c.x += container.getWidth();
		}
		if (c.x > getWidth()) {
			changeSize(c.x + 20, getHeight());
		}
	}

	public BufferedImage getInventorySprite() {
		return inventorySprite;
	}

	public Floor getFloor() {
		return floor;
	}
}
