package view;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;

public class Animation extends TimedDrawable {
	private ArrayList<BufferedImage> animation = new ArrayList<BufferedImage>();
	protected double animationLength;
	private int currentFrame;
	private boolean doesLoop;
	protected boolean isAnimated = true;

	public Animation() {
		currentFrame = 0;
	}

	// constructor for animated sprites
	public Animation(double animationLength, ArrayList<BufferedImage> animation) {
		currentFrame = 0;
		this.animation = animation;
		super.setTimer(animationLength / animation.size());
		super.setTimerValue(animationLength / animation.size());
		this.animationLength = animationLength;
	}

	public Animation(double animationLength, BufferedImage[] animation) {
		currentFrame = 0;
		this.animation = new ArrayList<BufferedImage>();
		this.animation.addAll(Arrays.asList(animation));
		super.setTimer(animationLength / animation.length);
		super.setTimerValue(animationLength / animation.length);
		;
		this.animationLength = animationLength;
	}

	// constructor for non-animated sprites
	public Animation(BufferedImage image) {
		currentFrame = 0;
		isAnimated = false;
		animation.add(image);
		super.setTimer(0);
		super.setTimerValue(0);
		this.animationLength = 0;
	}

	public void setAnimate(boolean b) {
		isAnimated = b;
	}

	public void setAnimationLength(double length) {
		animationLength = length;
		super.setTimer(animationLength / animation.size());
		super.setTimerValue(animationLength / animation.size());
	}

	public void setDoesLoop(boolean b) {
		doesLoop = b;
	}

	public boolean getLoop() {
		return doesLoop;
	}

	public void tick(double timeSinceLastFrame) {
		if (animation.size() > 1) {
			if (isAnimated) {
				super.tick(timeSinceLastFrame);
			}
		}
	}

	protected void timerAction() {
		nextFrame();
	}

	public void nextFrame() {
		if (currentFrame + 1 < animation.size()) {
			currentFrame++;
		} else if (doesLoop) {
			currentFrame = 0;
		}
	}

	public void addSprite(BufferedImage img) {
		animation.add(img);
		super.setTimer(animationLength / animation.size());
		super.setTimerValue(animationLength / animation.size());
	}

	public void addSprites(ArrayList<BufferedImage> img) {
		animation.addAll(img);
		super.setTimer(animationLength / animation.size());
		super.setTimerValue(animationLength / animation.size());
	}

	public double getAnimationLength() {
		return animationLength;
	}

	public ArrayList<BufferedImage> getSprites() {
		return animation;
	}

	@Override
	public BufferedImage getCurrentFrame() {
		if (animation.get(0) == null) {
			return null;
		}
		return animation.get(currentFrame);
	}

	public BufferedImage getStartingFrame() {
		if (animation.get(0) == null) {
			return null;
		}
		return animation.get(0);
	}

	public boolean reachedEnd() {
		if (!doesLoop && currentFrame == animation.size() - 1) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public Animation clone() {
		Animation clone = new Animation();
		clone.setXY(x, y);
		clone.addSprites(animation);
		clone.setAnimationLength(animationLength);
		clone.setDoesLoop(doesLoop);
		clone.setAnimate(isAnimated);
		return clone;
	}

	public void setFrame(int frameIndex) {
		currentFrame = frameIndex;
	}
}
