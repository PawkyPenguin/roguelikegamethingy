package view;

import java.awt.Graphics2D;

import model.MouseBox;

public class PlayerInterfaceWindow extends ContainerCluster {
	private HealthBarView healthBarView;
	private StatContainer statContainer;

	public void setHealthBarView(HealthBarView h) {
		healthBarView = h;
	}

	public void setStatContainer(StatContainer s) {
		statContainer = s;
	}

	public StatContainer getStatContainer() {
		return statContainer;
	}

	public HealthBarView getHealthBarView() {
		return healthBarView;
	}

	@Override
	protected void setScaling(double scaling) {
		super.setScaling(scaling);
		healthBarView.setScaling(scaling);
		statContainer.setScaling(scaling);
	}

	@Override
	protected void draw(Graphics2D g) {
		super.draw(g);
		healthBarView.draw(g);
		statContainer.draw(g);
	}

	@Override
	protected void setXY(int x, int y) {
		int deltaX = x - getX();
		int deltaY = y - getY();
		for (InvUIContainer container : containers) {
			container.setX(container.getX() + deltaX);
			container.setY(container.getY() + deltaY);
			for (MouseBox mouseBox : container.getMouseBoxes())
				mouseBox.move(deltaX, deltaY);
		}
		healthBarView.getBoundingBox().move(deltaX, deltaY);
		this.x = x;
		this.y = y;
	}
}