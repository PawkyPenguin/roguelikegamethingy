package view;

import java.awt.Graphics2D;
import java.util.ArrayList;

public class ContainerCluster {
	private boolean visible;
	protected int x;
	protected int y;
	private int width;
	private int height;
	protected double scaling;
	protected ArrayList<InvUIContainer> containers = new ArrayList<InvUIContainer>();

	//TODO
	protected void draw(Graphics2D g) {
		for (InvUIContainer container : containers) {
			container.draw(g);
		}
	}

	protected void setScaling(double scaling) {
		for (InvUIContainer container : containers) {
			container.setScaling(scaling);
		}
	}

	protected void addContainer(InvUIContainer c) {
		containers.add(c);
	}

	protected void clearContainerList() {
		containers.clear();
	}

	protected void setVisibility(boolean v) {
		visible = v;
	}

	protected boolean isVisible() {
		return visible;
	}

	protected void setWidth(int w) {
		width = w;
	}

	protected void setHeight(int h) {
		height = h;
	}

	protected void setXY(int x, int y) {
		int deltaX = x - this.x;
		int deltaY = y - this.y;
		for (InvUIContainer container : containers) {
			container.setX(container.getX() + deltaX);
			container.setY(container.getY() + deltaY);
		}
		this.x = x;
		this.y = y;
	}

	protected int getX() {
		return x;
	}

	protected int getY() {
		return y;
	}

	protected int getWidth() {
		return width;
	}

	protected int getHeight() {
		return height;
	}
}
