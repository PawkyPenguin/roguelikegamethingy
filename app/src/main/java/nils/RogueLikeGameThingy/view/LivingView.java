package view;

import entities.Entity;
import entities.Living;

public class LivingView extends EntityView {
	private Animation[] dir;
	private AnimationSet attackAnimations;

	public LivingView(Living liv) {
		super(liv);
	}

	public Animation getAttackAnimation(String name) {
		return attackAnimations.getAnimation(name);
	}

	private void setCurrentAnimation(Living living) {
		switch (living.getDirection().direction) {
		case LEFT:
			currentAnimation = dir[1];
			break;
		case UP:
			currentAnimation = dir[2];
			break;
		case RIGHT:
			currentAnimation = dir[3];
			break;
		case DOWN:
			currentAnimation = dir[4];
			break;
		default:
			currentAnimation = dir[0];
			break;
		}
	}

	public void setWalkingAnimations(AnimationSet animations) {
		dir = new Animation[5];
		dir[0] = animations.getAnimation("null");
		dir[1] = animations.getAnimation("left");
		dir[2] = animations.getAnimation("up");
		dir[3] = animations.getAnimation("right");
		dir[4] = animations.getAnimation("down");
	}

	public void setAttackAnimations(AnimationSet animations) {
		attackAnimations = animations;
	}

	@Override
	public void publisherUpdated(Entity pub) {
		setCurrentAnimation((Living) pub);
	}
}
