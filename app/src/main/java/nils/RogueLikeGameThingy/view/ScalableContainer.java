package view;

import java.awt.Graphics;

import entities.Coordinate;

public abstract class ScalableContainer {
	protected int x;
	protected int y;
	protected int width = 150;
	protected int height = 200;
	private boolean visible = true;
	protected static final int standardPaddingX = 16;
	protected static final int standardPaddingY = 16;
	protected int paddingX = standardPaddingX;
	protected int paddingY = standardPaddingY;
	protected double scaling;

	protected abstract void draw(Graphics g);

	protected void setScaling(double scaling) {
		this.scaling = scaling;
	}

	protected void setVisible(boolean v) {
		visible = v;
	}

	protected void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}

	protected int getX() {
		return x;
	}

	protected int getY() {
		return y;
	}

	protected boolean isVisible() {
		return visible;
	}

	protected int getWidth() {
		return width;
	}

	protected int getHeight() {
		return height;
	}

	protected void ensurePadding(Coordinate c) {
		if (c.getX() < paddingX) {
			c.setX(paddingX);
		} else if (c.getX() > width - paddingX) {
			c.setX(width - paddingX);
		}
		if (c.getY() < paddingY) {
			c.setY(paddingY);
		} else if (c.getY() > height - paddingY) {
			c.setY(height - paddingY);
		}
	}

	protected void setPaddingX(int paddingX) {
		this.paddingX = paddingX;
	}

	protected void setPaddingY(int paddingY) {
		this.paddingY = paddingY;
	}

	protected void setPadding(int paddingX, int paddingY) {
		setPaddingX(paddingX);
		setPaddingY(paddingY);
	}

	protected void setXY(Coordinate c) {
		this.x = c.getX();
		this.y = c.getY();
	}

	protected void setX(int x) {
		this.x = x;
	}

	protected void setY(int y) {
		this.y = y;
	}

	protected void setWidth(int w) {
		width = w;
	}

	protected void setHeight(int h) {
		height = h;
	}
}
