package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class DialogBox extends TimedDrawable {
	private final static int borderWidth = 5;
	private final static int borderHeight = 5;
	private final static Font defaultFont = new Font("Arial", Font.BOLD, 16);
	private Font currentFont = defaultFont;
	private int maxTextWidth = 140;
	private ArrayList<String> text;
	private int textPosition = 0;
	private int textLength;
	private Graphics2D g;
	private BufferedImage sprite;

	public DialogBox() {
		BufferedImage spriteToClone = SpriteCompendium.getDialogSprites().getAnimation("1").getCurrentFrame();
		sprite = new BufferedImage(spriteToClone.getWidth(), spriteToClone.getHeight(), BufferedImage.TYPE_INT_ARGB);
		g = sprite.createGraphics();
		g.drawImage(spriteToClone, 0, 0, null);
		g.setFont(defaultFont);
		g.setColor(Color.WHITE);
	}

	public void setTimeBetweenLetters(double t) {
		super.setTimer(t);
		super.setTimerValue(t);
	}

	// Kill me (don't actually, please).
	public void setText(String s) {
		text = new ArrayList<String>();
		textLength = s.length();
		int mark = 0;
		int width = 0;
		for (int i = 0; i < s.length(); i++) {
			width += g.getFontMetrics().charWidth(s.charAt(i));
			if (width > maxTextWidth) {
				for (; s.charAt(i) != ' '; i--)
					;
				text.add(s.substring(mark, i));
				mark = i + 1;
				width = 0;
			}
		}
		text.add(s.substring(mark));
	}

	public BufferedImage getCurrentFrame() {
		return sprite;
	}

	@Override
	protected void timerAction() {
		if (textPosition < textLength) {
			textPosition++;
			Graphics2D g = sprite.createGraphics();
			g.setFont(currentFont);
			g.setColor(Color.WHITE);
			// g.drawImage(originalSprite, 0, 0, null);
			int strLength = 0;
			for (int line = 0; line < text.size(); line++) {
				strLength += text.get(line).length();
				if (strLength < textPosition) {
					g.drawString(text.get(line), 2 * borderWidth,
							g.getFontMetrics().getHeight() * (line + 1) + borderHeight);
				} else {
					strLength -= text.get(line).length();
					g.drawString(text.get(line).substring(0, textPosition - strLength), 2 * borderWidth,
							g.getFontMetrics().getHeight() * (line + 1) + borderHeight);
					break;
				}
			}

		}
	}
}