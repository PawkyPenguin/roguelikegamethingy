package view;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import controller.InputDecoder;
import controller.MouseClick;

public class MouseHandler implements MouseListener {
	private MouseClick[] mouseClicks = new MouseClick[4];
	private InputDecoder sub;

	public MouseHandler() {
		for (int i = 0; i < mouseClicks.length; i++) {
			mouseClicks[i] = new MouseClick();
		}
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	public MouseClick getMouseClick(int index) {
		return mouseClicks[index];
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		if (arg0.getButton() >= mouseClicks.length) {
			return;
		}
		MouseClick click = mouseClicks[arg0.getButton()];
		click.setButtonClicked(true);
		click.setMouseX(arg0.getX());
		click.setMouseY(arg0.getY());
		updateSubscriber();
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		if (arg0.getButton() >= mouseClicks.length) {
			return;
		}
		MouseClick click = mouseClicks[arg0.getButton()];
		click.setButtonClicked(false);
		click.setMouseX(arg0.getX());
		click.setMouseY(arg0.getY());
		updateSubscriber();
	}

	public void enlistSub(InputDecoder sub) {
		this.sub = sub;
	}

	public void unlistSub(InputDecoder sub) {
		this.sub = null;
	}

	private void updateSubscriber() {
		sub.publisherUpdated(this);
	}
}
