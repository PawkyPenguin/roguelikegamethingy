package view;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.imageio.ImageIO;

import model.EnumMonsterType;

public class SpriteCompendium {
	static AnimationSet monsterGoblinSprites = new AnimationSet();
	static AnimationSet monsterSkeletonSprites = new AnimationSet();
	static AnimationSet shopkeeperSprites = new AnimationSet();
	static AnimationSet playerSprites = new AnimationSet();
	static AnimationSet attackSprites = new AnimationSet();
	static AnimationSet healthBarSprites = new AnimationSet();
	static AnimationSet mobHealthBarSprites = new AnimationSet();
	static AnimationSet bossHealthBarSprites = new AnimationSet();
	static AnimationSet wallSprites = new AnimationSet();
	static AnimationSet chestSprites = new AnimationSet();
	static AnimationSet obstacleSprites = new AnimationSet();
	static AnimationSet stairsSprites = new AnimationSet();
	static AnimationSet floorSprites = new AnimationSet();
	static AnimationSet miscSprites = new AnimationSet();
	static AnimationSet lightSprites = new AnimationSet();
	static AnimationSet testSprites = new AnimationSet();
	static AnimationSet invUISprites = new AnimationSet();
	static AnimationSet statuseffectSprites = new AnimationSet();
	static AnimationSet potionSprites = new AnimationSet();
	static AnimationSet decorationSprites = new AnimationSet();
	static AnimationSet dialogSprites = new AnimationSet();
	static AnimationSet inventoryStationSprites = new AnimationSet();
	static ArrayList<Animation> itemSprites = new ArrayList<Animation>();

	public SpriteCompendium() {
		try {
			loadMonsterSprites();
			loadPlayerSprites();
			loadAttackSprites();
			loadHealthBarSprites();
			loadMobHealthBarSprites();
			loadBossHealthBarSprites();
			loadShopkeeperSprites();
			loadWallSprites();
			loadChestSprites();
			loadObstacleSprites();
			loadStairsSprites();
			loadFloorSprites();
			loadMiscSprites();
			loadLightSprites();
			loadItemSprites();
			loadInvUISprites();
			loadStatuseffectSprites();
			loadPotionSprites();
			loadDecorationSprites();
			loadDialogSprites();
			loadInventoryStationSprites();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected static BufferedImage getFile(String s) throws IOException {
		return ImageIO.read(new File(s));
	}

	private static void loadInventoryStationSprites() throws IOException {
		Animation craftingTable = new Animation(getFile("../../src/main/res/sprites/craftingTable.gif"));
		inventoryStationSprites.addAnimation(craftingTable, "craftingTable");
	}

	private static void loadDialogSprites() throws IOException {
		Animation dlg1 = new Animation(getFile("../../src/main/res/sprites/dialogBox1.png"));
		dialogSprites.addAnimation(dlg1, "1");
	}

	private static void loadDecorationSprites() throws IOException {
		Animation moss = new Animation(getFile("../../src/main/res/sprites/decorationMoss.gif"));
		BufferedImage[] glowMossSprites = { getFile("../../src/main/res/sprites/decorationGlowMoss.gif"),
				getFile("../../src/main/res/sprites/decorationGlowMoss2.gif"), getFile("../../src/main/res/sprites/decorationGlowMoss3.gif"),
				getFile("../../src/main/res/sprites/decorationGlowMoss4.gif"), getFile("../../src/main/res/sprites/decorationGlowMoss5.gif"),
				getFile("../../src/main/res/sprites/decorationGlowMoss6.gif"), getFile("../../src/main/res/sprites/decorationGlowMoss7.gif"),
				getFile("../../src/main/res/sprites/decorationGlowMoss8.gif"), getFile("../../src/main/res/sprites/decorationGlowMoss9.gif"),
				getFile("../../src/main/res/sprites/decorationGlowMoss10.gif"), getFile("../../src/main/res/sprites/decorationGlowMoss11.gif"),
				getFile("../../src/main/res/sprites/decorationGlowMoss12.gif") };
		BufferedImage[] crackSprites = { getFile("../../src/main/res/sprites/crackSprites/cracks0.gif"),
				getFile("../../src/main/res/sprites/crackSprites/cracks1.gif"), getFile("../../src/main/res/sprites/crackSprites/cracks2.gif"),
				getFile("../../src/main/res/sprites/crackSprites/cracks3.gif"), getFile("../../src/main/res/sprites/crackSprites/cracks4.gif"),
				getFile("../../src/main/res/sprites/crackSprites/cracks5.gif") };
		Animation cracks = new Animation(2, crackSprites);
		Animation glowMoss = new Animation(3, glowMossSprites);
		glowMoss.setDoesLoop(true);
		cracks.setDoesLoop(false);
		cracks.setAnimate(false);
		decorationSprites.addAnimation(moss, "moss");
		decorationSprites.addAnimation(glowMoss, "glowMoss");
		decorationSprites.addAnimation(cracks, "cracks");
	}

	private static void loadPotionSprites() throws IOException {
		Animation red = new Animation(getFile("../../src/main/res/sprites/glassPhioleRed.gif"));
		Animation green = new Animation(getFile("../../src/main/res/sprites/glassPhioleGreen.gif"));
		Animation orange = new Animation(getFile("../../src/main/res/sprites/glassPhioleOrange.gif"));
		Animation lime = new Animation(getFile("../../src/main/res/sprites/glassPhioleLime.gif"));
		Animation pink = new Animation(getFile("../../src/main/res/sprites/glassPhiolePink.gif"));
		Animation empty = new Animation(getFile("../../src/main/res/sprites/glassPhioleEmpty.gif"));
		potionSprites.addAnimation(red, "RED");
		potionSprites.addAnimation(green, "GREEN");
		potionSprites.addAnimation(orange, "ORANGE");
		potionSprites.addAnimation(lime, "LIME");
		potionSprites.addAnimation(pink, "PINK");
		potionSprites.addAnimation(empty, "EMPTY");
	}

	private static void loadStatuseffectSprites() throws IOException {
		BufferedImage[] stateffectBurn = { getFile("../../src/main/res/sprites/statuseffectBurnt.gif"),
				getFile("../../src/main/res/sprites/statuseffectBurnt2.gif"), getFile("../../src/main/res/sprites/statuseffectBurn3.gif"),
				getFile("../../src/main/res/sprites/statuseffectBurn4.gif") };
		Animation statuseffectBurn = new Animation(0.8f, stateffectBurn);
		BufferedImage[] statuseffectRegenSprite = { getFile("../../src/main/res/sprites/statuseffectRegeneration.gif"),
				getFile("../../src/main/res/sprites/statuseffectRegeneration2.gif"), getFile("../../src/main/res/sprites/statuseffectRegeneration3.gif"),
				getFile("../../src/main/res/sprites/statuseffectRegeneration4.gif"), getFile("../../src/main/res/sprites/statuseffectRegeneration5.gif"),
				getFile("../../src/main/res/sprites/statuseffectRegeneration6.gif"), getFile("../../src/main/res/sprites/statuseffectRegeneration7.gif") };
		Animation statuseffectRegen = new Animation(1, statuseffectRegenSprite);
		Animation statuseffectFreeze = new Animation(getFile("../../src/main/res/sprites/statuseffectFreeze.gif"));
		statuseffectBurn.setDoesLoop(true);
		statuseffectRegen.setDoesLoop(true);
		statuseffectSprites.addAnimation(statuseffectBurn, "statuseffectBurn");
		statuseffectSprites.addAnimation(statuseffectFreeze, "statuseffectFreeze");
		statuseffectSprites.addAnimation(statuseffectRegen, "statuseffectRegen");
	}

	private static void loadInvUISprites() throws IOException {
		Animation inventorySprite = new Animation(getFile("../../src/main/res/sprites/inventory.gif"));
		Animation invFieldBlankSprite = new Animation(getFile("../../src/main/res/sprites/inventoryFieldBlank.gif"));
		Animation invFieldCommon = new Animation(getFile("../../src/main/res/sprites/inventoryFieldBlank.gif"));
		Animation invFieldTrash = new Animation(getFile("../../src/main/res/sprites/inventoryFieldTrash.gif"));
		Animation invFieldAuto = new Animation(getFile("../../src/main/res/sprites/inventoryFieldArrow.gif"));
		Animation invFieldWeapon = new Animation(getFile("../../src/main/res/sprites/inventoryFieldWeapon.gif"));
		Animation invFieldHelmet = new Animation(getFile("../../src/main/res/sprites/inventoryFieldHelmet.gif"));
		Animation invFieldChest = new Animation(getFile("../../src/main/res/sprites/inventoryFieldChest.gif"));
		Animation invFieldLeggins = new Animation(getFile("../../src/main/res/sprites/inventoryFieldLeggins.gif"));
		Animation invFieldShield = new Animation(getFile("../../src/main/res/sprites/inventoryFieldShield.gif"));
		Animation invFieldRing = new Animation(getFile("../../src/main/res/sprites/inventoryFieldRing.gif"));
		Animation invFieldLocked = new Animation(getFile("../../src/main/res/sprites/inventoryFieldLocked.gif"));
		invUISprites.addAnimation(inventorySprite, "inventory");
		invUISprites.addAnimation(invFieldBlankSprite, "invFieldBlank");
		invUISprites.addAnimation(invFieldCommon, "invFieldCommon");
		invUISprites.addAnimation(invFieldTrash, "invFieldTrash");
		invUISprites.addAnimation(invFieldAuto, "invFieldAuto");
		invUISprites.addAnimation(invFieldWeapon, "invFieldWeapon");
		invUISprites.addAnimation(invFieldHelmet, "invFieldHelmet");
		invUISprites.addAnimation(invFieldChest, "invFieldChest");
		invUISprites.addAnimation(invFieldLeggins, "invFieldLeggins");
		invUISprites.addAnimation(invFieldRing, "invFieldRing");
		invUISprites.addAnimation(invFieldShield, "invFieldShield");
		invUISprites.addAnimation(invFieldLocked, "invFieldLocked");
	}

	private static void loadItemSprites() throws IOException {
		Animation arrow = new Animation(getFile("../../src/main/res/sprites/arrow.gif"));
		Animation arrowUp = new Animation(getFile("../../src/main/res/sprites/flyingNormalArrowUp.gif"));
		Animation arrowLeft = new Animation(getFile("../../src/main/res/sprites/flyingNormalArrowLeft.gif"));
		Animation arrowDown = new Animation(getFile("../../src/main/res/sprites/flyingNormalArrowDown.gif"));
		Animation arrowRight = new Animation(getFile("../../src/main/res/sprites/flyingNormalArrowRight.gif"));
		Animation iceArrow = new Animation(getFile("../../src/main/res/sprites/iceArrow.gif"));
		Animation iceArrowUp = new Animation(getFile("../../src/main/res/sprites/flyingIceArrowUp.gif"));
		Animation iceArrowLeft = new Animation(getFile("../../src/main/res/sprites/flyingIceArrowLeft.gif"));
		Animation iceArrowDown = new Animation(getFile("../../src/main/res/sprites/flyingIceArrowDown.gif"));
		Animation iceArrowRight = new Animation(getFile("../../src/main/res/sprites/flyingIceArrowRight.gif"));
		Animation fireArrow = new Animation(getFile("../../src/main/res/sprites/fireArrow.gif"));
		Animation fireArrowUp = new Animation(getFile("../../src/main/res/sprites/flyingFireArrowUp.gif"));
		Animation fireArrowLeft = new Animation(getFile("../../src/main/res/sprites/flyingFireArrowLeft.gif"));
		Animation fireArrowDown = new Animation(getFile("../../src/main/res/sprites/flyingFireArrowDown.gif"));
		Animation fireArrowRight = new Animation(getFile("../../src/main/res/sprites/flyingFireArrowRight.gif"));
		Animation chestGold = new Animation(getFile("../../src/main/res/sprites/goldplate.gif"));
		Animation chestSilver = new Animation(getFile("../../src/main/res/sprites/silverplate.gif"));
		Animation chestBronze = new Animation(getFile("../../src/main/res/sprites/bronzeplate.gif"));
		Animation helmetGold = new Animation(getFile("../../src/main/res/sprites/goldhelmet.gif"));
		Animation helmetSilver = new Animation(getFile("../../src/main/res/sprites/silverhelmet.gif"));
		Animation helmetBronze = new Animation(getFile("../../src/main/res/sprites/bronzehelmet.gif"));
		Animation legginsGold = new Animation(getFile("../../src/main/res/sprites/goldleggins.gif"));
		Animation legginsSilver = new Animation(getFile("../../src/main/res/sprites/silverleggins.gif"));
		Animation legginsBronze = new Animation(getFile("../../src/main/res/sprites/bronzeleggins.gif"));
		Animation healthPot = new Animation(getFile("../../src/main/res/sprites/glassPhioleRed.gif"));
		Animation attackRing = new Animation(getFile("../../src/main/res/sprites/attackring.gif"));
		Animation defenseRing = new Animation(getFile("../../src/main/res/sprites/defensering.gif"));
		Animation healthRing = new Animation(getFile("../../src/main/res/sprites/healthring.gif"));
		Animation sword1 = new Animation(getFile("../../src/main/res/sprites/sword1.gif"));
		Animation sword2 = new Animation(getFile("../../src/main/res/sprites/sword2.gif"));
		Animation sword3 = new Animation(getFile("../../src/main/res/sprites/sword3.gif"));
		Animation bow = new Animation(getFile("../../src/main/res/sprites/bow2.gif"));
		Animation spellbook = new Animation(getFile("../../src/main/res/sprites/door.gif"));
		Animation pickaxe = new Animation(getFile("../../src/main/res/sprites/pickaxe.gif"));
		Animation stick = new Animation(getFile("../../src/main/res/sprites/stick2.gif"));
		Animation stoneItem = new Animation(getFile("../../src/main/res/sprites/stoneItem.gif"));
		itemSprites.add(arrow);
		itemSprites.add(arrowUp);
		itemSprites.add(arrowLeft);
		itemSprites.add(arrowDown);
		itemSprites.add(arrowRight);
		itemSprites.add(iceArrow);
		itemSprites.add(iceArrowUp);
		itemSprites.add(iceArrowLeft);
		itemSprites.add(iceArrowDown);
		itemSprites.add(iceArrowRight);
		itemSprites.add(chestBronze);
		itemSprites.add(chestSilver);
		itemSprites.add(chestGold);
		itemSprites.add(helmetBronze);
		itemSprites.add(helmetSilver);
		itemSprites.add(helmetGold);
		itemSprites.add(legginsBronze);
		itemSprites.add(legginsSilver);
		itemSprites.add(legginsGold);
		itemSprites.add(sword1);
		itemSprites.add(sword2);
		itemSprites.add(sword3);
		itemSprites.add(attackRing);
		itemSprites.add(defenseRing);
		itemSprites.add(healthRing);
		// bow is 26th element, i.e. itemSprites.get(25) returns bow
		itemSprites.add(bow);
		itemSprites.add(healthPot);
		itemSprites.add(fireArrow);
		itemSprites.add(fireArrowUp);
		itemSprites.add(fireArrowLeft);
		itemSprites.add(fireArrowDown);
		itemSprites.add(fireArrowRight);
		itemSprites.add(spellbook);
		itemSprites.add(pickaxe);
		itemSprites.add(stick);
		itemSprites.add(stoneItem);
	}

	private static void loadWallSprites() throws IOException {
		Animation wall = new Animation(getFile("../../src/main/res/sprites/wall.gif"));
		String datapath = "../../src/main/res/sprites/autotiles1/";
		Animation[][] minitiles = loadMinitiles(datapath);
		for (int i = 0; i < minitiles[0].length; i++) {
			wallSprites.addAnimation(minitiles[0][i], "A" + i);
			wallSprites.addAnimation(minitiles[1][i], "B" + i);
			wallSprites.addAnimation(minitiles[2][i], "C" + i);
			wallSprites.addAnimation(minitiles[3][i], "D" + i);
		}
		wallSprites.addAnimation(wall, "stoneWall");
	}

	private static Animation[][] loadMinitiles(String datapath) throws IOException {
		Animation[][] minitiles = new Animation[4][5];
		for (int i = 0; i < minitiles[0].length; i++) {
			minitiles[0][i] = new Animation(getFile(datapath + "A" + i + ".gif"));
			minitiles[1][i] = new Animation(getFile(datapath + "B" + i + ".gif"));
			minitiles[2][i] = new Animation(getFile(datapath + "C" + i + ".gif"));
			minitiles[3][i] = new Animation(getFile(datapath + "D" + i + ".gif"));
		}
		return minitiles;
	}

	private static void loadChestSprites() throws IOException {
		Animation woodenChest = new Animation(getFile("../../src/main/res/sprites/woodenChest.gif"));
		ArrayList<BufferedImage> woodenChestOpenAnimation = new ArrayList<BufferedImage>();
		woodenChestOpenAnimation.add(getFile("../../src/main/res/sprites/openWoodenChest1.gif"));
		woodenChestOpenAnimation.add(getFile("../../src/main/res/sprites/openWoodenChest2.gif"));
		woodenChestOpenAnimation.add(getFile("../../src/main/res/sprites/openWoodenChest3.gif"));
		Animation openWoodenChest = new Animation(0.2f, woodenChestOpenAnimation);
		Animation goldChest = new Animation(getFile("../../src/main/res/sprites/goldchest.gif"));
		ArrayList<BufferedImage> goldenChestOpenAnimation = new ArrayList<BufferedImage>();
		goldenChestOpenAnimation.add(getFile("../../src/main/res/sprites/openGoldChest1.gif"));
		goldenChestOpenAnimation.add(getFile("../../src/main/res/sprites/openGoldChest2.gif"));
		goldenChestOpenAnimation.add(getFile("../../src/main/res/sprites/openGoldChest3.gif"));
		Animation openGoldChest = new Animation(0.2f, goldenChestOpenAnimation);
		chestSprites.addAnimation(woodenChest, "woodenChest");
		chestSprites.addAnimation(openWoodenChest, "openWoodenChest");
		chestSprites.addAnimation(goldChest, "goldenChest");
		chestSprites.addAnimation(openGoldChest, "openGoldenChest");
	}

	private static void loadObstacleSprites() throws IOException {
		Animation ordinaryTree = new Animation(getFile("../../src/main/res/sprites/tree.gif"));
		obstacleSprites.addAnimation(ordinaryTree, "ordinaryTree");
	}

	private static void loadStairsSprites() throws IOException {
		Animation stoneStairs = new Animation(getFile("../../src/main/res/sprites/stairs.gif"));
		stairsSprites.addAnimation(stoneStairs, "stoneStairs");
	}

	private static void loadLightSprites() throws IOException {
		Animation dark = new Animation(getFile("../../src/main/res/sprites/lightDarkness.gif"));
		Animation penumbra = new Animation(getFile("../../src/main/res/sprites/lightPenumbra.png"));
		lightSprites.addAnimation(dark, "dark");
		lightSprites.addAnimation(penumbra, "penumbra");
	}

	private static void loadFloorSprites() throws IOException {
		ArrayList<BufferedImage> list = new ArrayList<BufferedImage>();
		list.add(getFile("../../src/main/res/sprites/background3_1.gif"));
		list.add(getFile("../../src/main/res/sprites/background3_2.gif"));
		list.add(getFile("../../src/main/res/sprites/background3_3.gif"));
		list.add(getFile("../../src/main/res/sprites/background3_4.gif"));
		list.add(getFile("../../src/main/res/sprites/background3_5.gif"));
		Animation floorTile1 = new Animation(0.7f, list);
		floorTile1.setDoesLoop(true);
		Animation floorTile2 = new Animation(getFile("../../src/main/res/sprites/background4.gif"));
		floorSprites.addAnimation(floorTile2, "stoneFloor");
		floorSprites.addAnimation(floorTile1, "woodenFloor");
	}

	private static void loadMiscSprites() throws IOException {
		Animation ward = new Animation(getFile("../../src/main/res/sprites/Ward.gif"));
		Animation lamp = new Animation(getFile("../../src/main/res/sprites/lamp.gif"));
		miscSprites.addAnimation(ward, "ward");
		miscSprites.addAnimation(lamp, "lamp");
	}

	private static void loadShopkeeperSprites() throws IOException {
		Animation shop = new Animation(getFile("../../src/main/res/sprites/shopkeep.gif"));
		Animation shopLeft = new Animation(getFile("../../src/main/res/sprites/shopkeep.gif"));
		Animation shopUp = new Animation(getFile("../../src/main/res/sprites/shopkeep.gif"));
		Animation shopRight = new Animation(getFile("../../src/main/res/sprites/shopkeep.gif"));
		Animation shopDown = new Animation(getFile("../../src/main/res/sprites/shopkeep.gif"));
		shopkeeperSprites.addAnimation(shop, "null");
		shopkeeperSprites.addAnimation(shopLeft, "left");
		shopkeeperSprites.addAnimation(shopUp, "up");
		shopkeeperSprites.addAnimation(shopRight, "right");
		shopkeeperSprites.addAnimation(shopDown, "down");
	}

	private static void loadMonsterSprites() throws IOException {
		File folder = new File(".");
		String[] listOfFiles = folder.list();
		for(String s : listOfFiles) {
			System.out.println(s);
		}

		Animation goblin = new Animation(getFile("../../src/main/res/sprites/goblin.gif"));
		Animation goblinLeft = new Animation(getFile("../../src/main/res/sprites/goblin.gif"));
		Animation goblinUp = new Animation(getFile("../../src/main/res/sprites/goblin.gif"));
		Animation goblinRight = new Animation(getFile("../../src/main/res/sprites/goblin.gif"));
		Animation goblinDown = new Animation(getFile("../../src/main/res/sprites/goblin.gif"));
		monsterGoblinSprites.addAnimation(goblin, "null");
		monsterGoblinSprites.addAnimation(goblinLeft, "left");
		monsterGoblinSprites.addAnimation(goblinUp, "up");
		monsterGoblinSprites.addAnimation(goblinRight, "right");
		monsterGoblinSprites.addAnimation(goblinDown, "down");
		Animation skeleton = new Animation(getFile("../../src/main/res/sprites/skeleton.gif"));
		Animation skeletonLeft = new Animation(getFile("../../src/main/res/sprites/skeleton.gif"));
		Animation skeletonUp = new Animation(getFile("../../src/main/res/sprites/skeleton.gif"));
		Animation skeletonRight = new Animation(getFile("../../src/main/res/sprites/skeleton.gif"));
		Animation skeletonDown = new Animation(getFile("../../src/main/res/sprites/skeleton.gif"));
		monsterSkeletonSprites.addAnimation(skeleton, "null");
		monsterSkeletonSprites.addAnimation(skeletonLeft, "left");
		monsterSkeletonSprites.addAnimation(skeletonUp, "up");
		monsterSkeletonSprites.addAnimation(skeletonRight, "right");
		monsterSkeletonSprites.addAnimation(skeletonDown, "down");
	}

	private static void loadPlayerSprites() throws IOException {
		Animation animation = new Animation(getFile("../../src/main/res/sprites/hero.gif"));
		Animation animationLeft = new Animation(getFile("../../src/main/res/sprites/heroLeft.gif"));
		Animation animationUp = new Animation(getFile("../../src/main/res/sprites/heroUp.gif"));
		Animation animationRight = new Animation(getFile("../../src/main/res/sprites/heroRight.gif"));
		Animation animationDown = new Animation(getFile("../../src/main/res/sprites/heroDown.gif"));
		playerSprites.addAnimation(animation, "null");
		playerSprites.addAnimation(animationLeft, "left");
		playerSprites.addAnimation(animationUp, "up");
		playerSprites.addAnimation(animationRight, "right");
		playerSprites.addAnimation(animationDown, "down");
	}

	private static void loadAttackSprites() throws IOException {
		BufferedImage[] images = { getFile("../../src/main/res/sprites/swordStrike0.gif"), getFile("../../src/main/res/sprites/swordStrike1.gif"),
				getFile("../../src/main/res/sprites/swordStrike2.gif"), getFile("../../src/main/res/sprites/swordStrike3.gif"),
				getFile("../../src/main/res/sprites/swordStrike4.gif"), getFile("../../src/main/res/sprites/swordStrike5.gif"),
				getFile("../../src/main/res/sprites/swordStrike6.gif"), getFile("../../src/main/res/sprites/swordStrike7.gif"),
				getFile("../../src/main/res/sprites/swordStrike8.gif"), getFile("../../src/main/res/sprites/swordStrike9.gif") };
		ArrayList<BufferedImage> animation = new ArrayList<BufferedImage>();
		animation.addAll(Arrays.asList(images));
		Animation swordStrike = new Animation(0.15f, animation);
		ArrayList<BufferedImage> darkSphereAnimation = new ArrayList<BufferedImage>();

		BufferedImage[] imagesDarkSphere = { getFile("../../src/main/res/sprites/darkSphere.gif"), getFile("../../src/main/res/sprites/darkSphere2.gif"),
				getFile("../../src/main/res/sprites/darkSphere3.gif"), getFile("../../src/main/res/sprites/darkSphere4.gif"),
				getFile("../../src/main/res/sprites/darkSphere5.gif"), getFile("../../src/main/res/sprites/darkSphere6.gif"),
				getFile("../../src/main/res/sprites/darkSphere7.gif"), getFile("../../src/main/res/sprites/darkSphere8.gif"),
				getFile("../../src/main/res/sprites/darkSphere9.gif"), getFile("../../src/main/res/sprites/darkSphere10.gif"),
				getFile("../../src/main/res/sprites/darkSphere11.gif"), getFile("../../src/main/res/sprites/darkSphere12.gif") };
		darkSphereAnimation.addAll(Arrays.asList(imagesDarkSphere));
		Animation darkSphere = new Animation(0.75f, darkSphereAnimation);
		darkSphere.setDoesLoop(true);
		Animation greenSphere = new Animation(getFile("../../src/main/res/sprites/greenSphere.gif"));
		Animation redSphere = new Animation(getFile("../../src/main/res/sprites/redSphere.gif"));
		Animation blueSphere = new Animation(getFile("../../src/main/res/sprites/blueSphere.gif"));
		attackSprites.addAnimation(swordStrike, "swordStrike");
		attackSprites.addAnimation(darkSphere, "darkSphere");
		attackSprites.addAnimation(greenSphere, "greenSphere");
		attackSprites.addAnimation(redSphere, "redSphere");
		attackSprites.addAnimation(blueSphere, "blueSphere");
	}

	private static void loadHealthBarSprites() throws IOException {
		BufferedImage barEmpty = getFile("../../src/main/res/sprites/healthBarEmpty.gif");
		BufferedImage healthSegment = getFile("../../src/main/res/sprites/healthBarSegment.gif");
		BufferedImage healthBuffer = getFile("../../src/main/res/sprites/healthBufferSegment.gif");
		healthBarSprites.addSprite(barEmpty, "emptyBar");
		healthBarSprites.addSprite(healthSegment, "segment");
		healthBarSprites.addSprite(healthBuffer, "bufferSegment");
	}

	private static void loadMobHealthBarSprites() throws IOException {
		BufferedImage barEmpty = null;
		BufferedImage healthSegment = getFile("../../src/main/res/sprites/mobHealthBarSegment.gif");
		BufferedImage healthBuffer = getFile("../../src/main/res/sprites/mobHealthBufferSegment.gif");
		mobHealthBarSprites.addSprite(barEmpty, "emptyBar");
		mobHealthBarSprites.addSprite(healthSegment, "segment");
		mobHealthBarSprites.addSprite(healthBuffer, "bufferSegment");
	}

	private static void loadBossHealthBarSprites() throws IOException {
		BufferedImage barEmpty = getFile("../../src/main/res/sprites/healthBarEmpty.gif");
		BufferedImage healthSegment = getFile("../../src/main/res/sprites/healthBarSegment.gif");
		BufferedImage healthBuffer = getFile("../../src/main/res/sprites/healthBufferSegment.gif");
		bossHealthBarSprites.addSprite(barEmpty, "emptyBar");
		bossHealthBarSprites.addSprite(healthSegment, "segment");
		bossHealthBarSprites.addSprite(healthBuffer, "bufferSegment");
	}

	public static AnimationSet getHealthBarSprites() {
		return healthBarSprites.clone();
	}

	public static AnimationSet getMobHealthBarSprites() {
		return mobHealthBarSprites.clone();
	}

	public static AnimationSet getBossHealthBarSprites() {
		return bossHealthBarSprites.clone();
	}

	public static AnimationSet getAttackSprites() {
		return attackSprites.clone();
	}

	public static AnimationSet getPlayerSprites() {
		return playerSprites.clone();
	}

	public static AnimationSet getMonsterSprites(EnumMonsterType monsterType) {
		switch (monsterType) {
		case GOBLIN:
			return monsterGoblinSprites.clone();
		case SKELETON:
			return monsterSkeletonSprites.clone();
		default:
			return null;
		}
	}

	public static AnimationSet getShopkeeperSprites() {
		return shopkeeperSprites.clone();
	}

	public static AnimationSet getWallSprites() {
		return wallSprites.clone();
	}

	public static AnimationSet getChestSprites() {
		return chestSprites.clone();
	}

	public static AnimationSet getObstacleSprites() {
		return obstacleSprites.clone();
	}

	public static AnimationSet getStairsSprites() {
		return stairsSprites.clone();
	}

	public static AnimationSet getFloorSprites() {
		return floorSprites.clone();
	}

	public static AnimationSet getMiscSprites() {
		return miscSprites.clone();
	}

	public static AnimationSet getLightSprites() {
		return lightSprites.clone();
	}

	public static ArrayList<Animation> getItemSprites() {
		ArrayList<Animation> clonedList = new ArrayList<Animation>();
		for (Animation itemSprite : itemSprites) {
			clonedList.add(itemSprite);
		}
		return clonedList;
	}

	public static AnimationSet getInvUISprites() {
		return invUISprites.clone();
	}

	public static AnimationSet getStatuseffectSprites() {
		return statuseffectSprites.clone();
	}

	public static AnimationSet getPotionSprites() {
		return potionSprites.clone();
	}

	public static AnimationSet getDecorationSprites() {
		return decorationSprites.clone();
	}

	public static AnimationSet getDialogSprites() {
		return dialogSprites.clone();
	}

	public static AnimationSet getInventoryStationSprites() {
		return inventoryStationSprites.clone();
	}
}
