package view;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class AnimationSet {
	private ArrayList<Animation> animationList = new ArrayList<Animation>();
	private ArrayList<String> animationNameList = new ArrayList<String>();
	private Animation currentAnimation;

	public void selectAnimation(String name) {
		boolean found = false;
		for (int i = 0; i < animationNameList.size(); i++) {
			if (animationNameList.get(i).equals(name)) {
				currentAnimation = animationList.get(i);
				found = true;
				break;
			}
		}
		if (!found) {
			throw new IllegalArgumentException("Animation with name " + name + " not found");
		}
	}

	public void setAnimationList(ArrayList<Animation> l) {
		animationList = l;
	}

	public void setAnimationNameList(ArrayList<String> l) {
		animationNameList = l;
	}

	public void setCurrentAnimation(Animation a) {
		currentAnimation = a;
	}

	public void tick(double timeSinceLastFrame) {
		currentAnimation.tick(timeSinceLastFrame);
	}

	public BufferedImage getCurrentFrame() {
		return currentAnimation.getCurrentFrame();
	}

	public Animation getCurrentAnimation() {
		return currentAnimation;
	}

	public Animation getAnimation(String name) {
		for (int i = 0; i < animationList.size(); i++) {
			if (animationNameList.get(i).equals(name)) {
				return animationList.get(i);
			}
		}
		throw new IllegalArgumentException("Animation with name " + name + " not found");
	}

	public Animation[] toAnimationArray() {
		Animation[] aniArray = new Animation[animationList.size()];
		for (int i = 0; i < aniArray.length; i++) {
			aniArray[i] = animationList.get(i);
		}
		return aniArray;
	}

	public BufferedImage[] getCurrentFrames() {
		BufferedImage[] imgArray = new BufferedImage[animationList.size()];
		for (int i = 0; i < imgArray.length; i++) {
			imgArray[i] = animationList.get(i).getCurrentFrame();
		}
		return imgArray;
	}

	public BufferedImage[] getStartingFrames() {
		BufferedImage[] imgArray = new BufferedImage[animationList.size()];
		for (int i = 0; i < imgArray.length; i++) {
			imgArray[i] = animationList.get(i).getStartingFrame();
		}
		return imgArray;
	}

	public void addSprite(BufferedImage sprite, String name) {
		animationList.add(new Animation(sprite));
		animationNameList.add(name);
	}

	public void addAnimation(Animation animation, String name) {
		animationList.add(animation);
		animationNameList.add(name);
	}

	public void addSpriteSet(ArrayList<BufferedImage> sprites, ArrayList<String> names) {
		for (int i = 0; i < sprites.size(); i++) {
			animationList.add(new Animation(sprites.get(i)));
			animationNameList.add(names.get(i));
		}
	}

	public void addAnimationSet(ArrayList<Animation> animations, ArrayList<String> names) {
		for (int i = 0; i < animations.size(); i++) {
			animationList.add(animations.get(i));
			animationNameList.add(names.get(i));
		}
	}

	@Override
	public AnimationSet clone() {
		AnimationSet clone = new AnimationSet();
		ArrayList<Animation> clonedAnimationList = new ArrayList<Animation>();
		for (Animation animation : animationList) {
			clonedAnimationList.add(animation.clone());
		}
		clone.setAnimationList(clonedAnimationList);
		clone.setAnimationNameList(animationNameList);
		if (getCurrentAnimation() != null) {
			clone.setCurrentAnimation(getCurrentAnimation().clone());
		}
		return clone;
	}
}
