package model;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import view.Animation;
import view.DungeonView;

public class TileAnimation extends Animation {
	public TileAnimation(float animationLength, ArrayList<BufferedImage> animation) {
		super(animationLength, animation);
	}

	@Override
	public void setXY(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public int getScreenPosX() {
		return x * 32;
	}

	@Override
	public int getScreenPosY() {
		return y * 32 + DungeonView.menuHeight;
	}
}
