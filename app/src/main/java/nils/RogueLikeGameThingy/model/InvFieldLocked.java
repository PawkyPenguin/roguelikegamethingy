package model;

import item.Item;

public class InvFieldLocked extends InventoryField {

	public InvFieldLocked() {
		super();
	}

	public InvFieldLocked(Item item) {
		super();
		setItem(item);
	}

	@Override
	public boolean isItemSwapPossible(InventoryField inv) {
		return false;
	}

	@Override
	protected void setType() {
		invFieldType = EnumInventoryFieldType.LOCKED;
	}

}
