package model;

import entities.InventoryStation;

public abstract class InvFieldButton extends InventoryField {
	private EnumInvButtonPosition desiredPosition;
	protected InventoryStation station;

	public InvFieldButton() {
		super();
		setDesiredPosition();

	}

	public void setInventoryStation(InventoryStation c) {
		station = c;
	}

	protected void setDesiredPosition(EnumInvButtonPosition p) {
		desiredPosition = p;
	}

	protected InventoryStation getStation() {
		return station;
	}

	protected abstract void setDesiredPosition();

	public EnumInvButtonPosition getDesiredPosition() {
		return desiredPosition;
	}

	protected abstract void buttonPress();

	@Override
	protected void setType() {
		invFieldType = EnumInventoryFieldType.BUTTON;
	}

	/*
	 * Item swap is made possible iff invField == this. This way, the button
	 * action can be called whenever an item swap is made, i.e. the player has
	 * clicked and released the mouse button in the MouseBox of this field.
	 */
	@Override
	public boolean isItemSwapPossible(InventoryField invField) {
		return invField == this;
	}

	/*
	 * swapItems is only called if the player has pressed and released the mouse
	 * within this field's MouseBox.
	 * 
	 * @see model.InventoryField#isItemSwapPossible(model.InventoryField)
	 */
	@Override
	public void swapItems(InventoryField invField) {
		if (invField != this) {
			return;
		}
		buttonPress();
	}
}
