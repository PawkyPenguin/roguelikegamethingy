package model;

import entities.CraftingTable;

public class InvFieldButtonCraft extends InvFieldButton {

	@Override
	protected void setDesiredPosition() {
		setDesiredPosition(EnumInvButtonPosition.BOT);
	}

	@Override
	protected void buttonPress() {
		if (getStation() instanceof CraftingTable) {
			((CraftingTable) getStation()).craft();
		}
	}
}
