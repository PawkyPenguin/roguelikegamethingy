package model;

import item.Item;

public class InvFieldTrash extends InventoryField {

	@Override
	protected void setType() {
		invFieldType = EnumInventoryFieldType.TRASH;
	}

	@Override
	public Item getItemForSwap() {
		return null;
	}

	@Override
	protected boolean isItemSwapPossible(InventoryField field) {
		if (field == this) {
			return false;
		}
		if (getItem() == null) {
			return isItemCompatible(field.getItem());
		}
		return isStackableWith(field.getItemForSwap())
				|| (isItemCompatible(field.getItemForSwap()) && field.isItemCompatible(getItem()));
	}

}
