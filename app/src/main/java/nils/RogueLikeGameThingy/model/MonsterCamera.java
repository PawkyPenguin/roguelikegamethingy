package model;


public class MonsterCamera extends Camera{
	
	@Override
	public void updateLighting(){
		litFields = new byte[publisher.getFloor().getRelevantX()][publisher.getFloor().getRelevantY()];
		lightHandler.updateLight(publisher.getCoordinate());
    	litFields = lightHandler.getLitFields();
    	addLampsToVisionList();
	}
}
