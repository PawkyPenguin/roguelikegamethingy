package model;
// Quite  a stupid name for what this interface does but couldn't think of something better. The idea is that this can be used by any (e.g.) item that the player gets to know about.
// newKnowledge is called from the Knowledge class to notify the subscribers (items) of knowledge changes.
public interface Knowledgeable {
	public void newKnowledge(Knowledge k);
}
