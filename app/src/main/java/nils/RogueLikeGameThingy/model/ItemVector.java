package model;

import item.Item;

public class ItemVector {
	private Item[] items = new Item[2];
	
	public void setItems(Item item1, Item item2){
		items[0] = item1;
		items[1] = item2;
	}
	
	public void setVector(Item item1, Item item2){
		items[0] = item1.clone();
		if(item1.isStackableAndNotNull(item2)){
			items[0].reduceAmount(item2.getAmount());
		}
		else{
			if(item2 != null){
				items[1] = item2.clone();				
			}
		}
	}
	
	public Item[] getDifference(ItemVector other){
		Item[] difference = new Item[4];
		difference[0] = items[0];
		difference[1] = items[1];
		if(items[0] != null && items[0].isStackableAndNotNull(other.getItems()[0])){
			difference[0].reduceAmount(other.getItems()[0].getAmount());
		}
		else{
			difference[2] = other.getItems()[0];
		}
		if(items[1] != null && items[1].isStackableAndNotNull(other.getItems()[1])){
			difference[1].reduceAmount(other.getItems()[1].getAmount());
		}
		else{
			difference[3] = other.getItems()[1];
		}
		return difference;
	}
	
	public Item[] getItems(){
		return items;
	}
}
