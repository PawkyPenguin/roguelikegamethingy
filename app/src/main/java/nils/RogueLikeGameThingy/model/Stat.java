package model;

public class Stat {
	private float armorStat;
	private float attackStat;
	private HealthBuffer healthStat;
	
	public float getArmor(){
		return armorStat;
	}
	
	public float getAttack(){
		return attackStat;
	}
	
	public HealthBuffer getHealthBuffer(){
		return healthStat;
	}
	
	public void setArmor(float armor){
		armorStat = armor;
	}
	
	public void setAttack(float attack){
		attackStat = attack;
	}
	
	public void setHealth(int maxHealth, int threshold){
		healthStat = new HealthBuffer(maxHealth, threshold);
		healthStat.fill();
	}
	
	public void setHealth(HealthBuffer buffer){
		healthStat = buffer;
	}
	
	public void addArmor(float addedArmor){
		armorStat += addedArmor;
	}
	
	public void addAttack(float addedAttack){
		attackStat += addedAttack;
	}
	
	public void addStats(Stat addedStats){
		addArmor(addedStats.getArmor());
		addAttack(addedStats.getAttack());
		
	}
	
	public void substractStats(Stat subtractedStats){
		addArmor(- subtractedStats.getArmor());
		addAttack(- subtractedStats.getAttack());
	}
}
