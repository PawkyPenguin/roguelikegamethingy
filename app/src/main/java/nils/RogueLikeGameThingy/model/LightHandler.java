package model;

import entities.Coordinate;
import shadowcasting.ShadowCaster;

public class LightHandler {
	private byte[][] litCoordinates;
	private byte[][] lightMask;
	private ShadowCaster shadows = new ShadowCaster();

	public LightHandler(ShadowCaster s, Floor f) {
		shadows = s;
		lightMask = new byte[f.getRelevantX()][f.getRelevantY()];
		litCoordinates = new byte[f.getRelevantX()][f.getRelevantY()];
	}

	public void addLamp(int x, int y) {
		byte[][] lightMap = shadows.getLightMap(x, y);
		for (int i = 0; i < lightMap.length; i++) {
			for (int j = 0; j < lightMap[0].length; j++) {
				lightMask[i][j] |= lightMap[i][j];
			}
		}
	}

	public void updateLight(Coordinate playerCoord) {
		long ms = System.currentTimeMillis();
		byte[][] lightMap = shadows.getLightMap(playerCoord.x, playerCoord.y);
		for (int i = 0; i < lightMap.length; i++) {
			for (int j = 0; j < lightMap[0].length; j++) {
				litCoordinates[i][j] = (byte) (lightMask[i][j] | lightMap[i][j]);
			}
		}
		// System.out.println("[LightHandler] Fetched shadow map: " +
		// (System.currentTimeMillis() - ms) + "ms");
	}

	public byte[][] getLitFields() {
		return litCoordinates;
	}
}
