package model;

import entities.Coordinate;

public class MouseBox implements Cloneable {
	private int x;
	private int y;
	int width;
	int height;

	public MouseBox() {
	}

	public MouseBox(int x, int y, int width, int height) {
		this.setX(x);
		this.setY(y);
		this.width = width;
		this.height = height;
	}

	@Override
	public MouseBox clone() {
		MouseBox clone = new MouseBox(x, y, width, height);
		return clone;
	}

	public int getHeight() {
		return height;
	}

	public int getWidth() {
		return width;
	}

	public boolean isWithinBox(int mouseX, int mouseY) {
		if (mouseX > getX() && mouseX < getX() + width && mouseY > getY() && mouseY < getY() + height) {
			return true;
		} else {
			return false;
		}
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public static boolean withinBox(Coordinate mousePos, int x, int y, int width, int height) {
		return mousePos != null && mousePos.x >= x && mousePos.y >= y && mousePos.x <= x + width
				&& mousePos.y <= y + height;
	}

	public MouseBox scale(double scale) {
		MouseBox clone = clone();
		clone.setX((int) (x * scale));
		clone.setY((int) (y * scale));
		clone.setWidth((int) (width * scale));
		clone.setHeight((int) (height * scale));
		return clone;
	}

	public void move(int deltaX, int deltaY) {
		x += deltaX;
		y += deltaY;
	}
}
