package model;

import java.util.ArrayList;

import entities.Living;
import item.Item;

public class Knowledge {
	ArrayList<Item> knownItems;
	Living owner;
	
	public Knowledge(){
		knownItems = new ArrayList<Item>();
	}
	
	public void addKnowledge(Item i){
		if(!hasKnowledgeAbout(i)){
			knownItems.add(i);
		}
	}
	
	public boolean hasKnowledgeAbout(Item i){
		for(Item knownItem:knownItems){
			if(i == knownItem){
				return true;
			}
		}
		return false;
	}
}
