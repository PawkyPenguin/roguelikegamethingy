package model;

import java.util.ArrayList;
import java.util.Arrays;

import item.EnumAmmunitionType;
import item.EnumArmorType;
import item.EnumArrowType;
import item.EnumConsumableType;
import item.EnumEquipableType;
import item.EnumItemType;
import item.EnumMeleeWeaponType;
import item.EnumPotionColor;
import item.EnumWeaponType;
import item.Item;
import item.ItemEquipable;

public class TypeHandler {
	private static final ArrayList<Enum> itemArrowType = new ArrayList<Enum>(
			Arrays.asList(EnumItemType.AMMUNITION, EnumAmmunitionType.ARROW));
	private static final ArrayList<Enum> itemNormalArrowType = new ArrayList<Enum>(
			Arrays.asList(EnumItemType.AMMUNITION, EnumAmmunitionType.ARROW, EnumArrowType.NORMAL));
	private static final ArrayList<Enum> itemIceArrowType = new ArrayList<Enum>(
			Arrays.asList(EnumItemType.AMMUNITION, EnumAmmunitionType.ARROW, EnumArrowType.ICE));
	private static final ArrayList<Enum> itemArmorHelmetType = new ArrayList<Enum>(
			Arrays.asList(EnumItemType.EQUIPABLE, EnumEquipableType.ARMOR, EnumArmorType.HELMET));
	private static final ArrayList<Enum> itemArmorChestType = new ArrayList<Enum>(
			Arrays.asList(EnumItemType.EQUIPABLE, EnumEquipableType.ARMOR, EnumArmorType.CHEST));
	private static final ArrayList<Enum> itemArmorLegginsType = new ArrayList<Enum>(
			Arrays.asList(EnumItemType.EQUIPABLE, EnumEquipableType.ARMOR, EnumArmorType.LEGGINS));
	private static final ArrayList<Enum> itemShieldType = new ArrayList<Enum>(
			Arrays.asList(EnumItemType.EQUIPABLE, EnumEquipableType.SHIELD));
	private static final ArrayList<Enum> itemRingType = new ArrayList<Enum>(
			Arrays.asList(EnumItemType.EQUIPABLE, EnumEquipableType.RING));
	private static final ArrayList<Enum> itemHealthPotType = new ArrayList<Enum>(
			Arrays.asList(EnumItemType.ITEMACTIVE, EnumConsumableType.POTION));
	private static final ArrayList<Enum> itemSwordType = new ArrayList<Enum>(Arrays.asList(EnumItemType.EQUIPABLE,
			EnumEquipableType.WEAPON, EnumWeaponType.MELEE, EnumMeleeWeaponType.SWORD));
	private static final ArrayList<Enum> itemMeleeWeaponType = new ArrayList<Enum>(
			Arrays.asList(EnumItemType.EQUIPABLE, EnumEquipableType.WEAPON, EnumWeaponType.MELEE));
	private static final ArrayList<Enum> itemFireArrowType = new ArrayList<Enum>(
			Arrays.asList(EnumItemType.AMMUNITION, EnumAmmunitionType.ARROW, EnumArrowType.FIRE));
	private static final ArrayList<Enum> itemPickaxeType = new ArrayList<Enum>(Arrays.asList(EnumItemType.EQUIPABLE,
			EnumEquipableType.WEAPON, EnumWeaponType.MELEE, EnumMeleeWeaponType.PICKAXE));

	public static boolean areTypesEqual(ArrayList<Enum> type1, ArrayList<Enum> type2) {
		for (int i = 0; i < type1.size() && i < type2.size(); i++) {
			if (type1.get(i) != type2.get(i)) {
				return false;
			}
		}
		return true;
	}

	public static boolean areTypesExactlyEqual(ArrayList<Enum> type1, ArrayList<Enum> type2) {
		return areTypesEqual(type1, type2) && type1.size() == type2.size();
	}

	public static Enum getEquipableType(ItemEquipable item) {

		for (Enum type : item.itemType) {
			if ((type instanceof EnumArmorType) || (type == EnumEquipableType.RING)
					|| (type == EnumEquipableType.SHIELD) || (type == EnumEquipableType.WEAPON)) {
				return type;
			}
		}
		throw new IllegalArgumentException("ArmorPiece does not have a armor type");
	}

	/*
	 * public static boolean isObjectInteractable(Entity object){ if(object !=
	 * null){ return !object.type.contains(EnumEntityType.UNINTERACTABLE); }
	 * return false; }
	 */

	public static boolean isFieldStorage(InventoryField field) {
		if (field != null) {
			return field.invFieldType == EnumInventoryFieldType.STORAGE;
		}
		return false;
	}

	public static boolean isFieldAutouse(InventoryField field) {
		if (field != null) {
			return field.invFieldType == EnumInventoryFieldType.AUTOUSE;
		}
		return false;
	}

	public static boolean isFieldEquipment(InventoryField field) {
		if (field != null) {
			return field.invFieldType == EnumInventoryFieldType.EQUIPMENT;
		}
		return false;
	}

	public static boolean isFieldTrash(InventoryField field) {
		if (field != null) {
			return field.invFieldType == EnumInventoryFieldType.TRASH;
		}
		return false;
	}

	public static boolean isFieldLocked(InventoryField field) {
		if (field != null) {
			return field.invFieldType == EnumInventoryFieldType.LOCKED;
		}
		return false;
	}

	public static boolean isItemAmmunition(Item item) {
		if (item != null) {
			return item.itemType.contains(EnumItemType.AMMUNITION);
		}
		return false;
	}

	public static boolean isItemEquipable(Item item) {
		if (item != null) {
			return item.itemType.contains(EnumItemType.EQUIPABLE);
		}
		return false;
	}

	public static boolean isItemEquipable(InventoryField field) {
		if (field.getItem() != null) {
			return field.getItem().itemType.contains(EnumItemType.EQUIPABLE);
		}
		return false;
	}

	public static boolean isItemActive(Item item) {
		if (item != null) {
			return item.itemType.contains(EnumItemType.ITEMACTIVE);
		}
		return false;
	}

	public static boolean isItemArmor(Item item) {
		if (item != null) {
			return item.itemType.contains(EnumItemType.EQUIPABLE) && item.itemType.contains(EnumEquipableType.ARMOR);
		}
		return false;
	}

	public static boolean isItemWeapon(Item item) {
		if (item != null) {
			return item.itemType.contains(EnumItemType.EQUIPABLE) && item.itemType.contains(EnumEquipableType.WEAPON);
		}
		return false;
	}

	public static boolean isItemRing(Item item) {
		if (item != null) {
			return item.itemType.contains(EnumItemType.EQUIPABLE) && item.itemType.contains(EnumEquipableType.RING);
		}
		return false;
	}

	public static boolean isItemArtefact(Item item) {
		if (item != null) {
			return item.itemType.contains(EnumItemType.ARTEFACT);
		}
		return false;
	}

	public static boolean isItemArrow(Item item) {
		if (item != null) {
			return areTypesEqual(item.itemType, itemArrowType);
		}
		return false;
	}

	public static boolean isItemIceArrow(Item item) {
		if (item != null) {
			return areTypesEqual(item.itemType, itemIceArrowType);
		}
		return false;
	}

	public static final ArrayList<Enum> getItemArrowType() {
		return (ArrayList<Enum>) itemArrowType.clone();
	}

	public static final ArrayList<Enum> getItemNormalArrowType() {
		return (ArrayList<Enum>) itemNormalArrowType.clone();
	}

	public static final ArrayList<Enum> getItemIceArrowType() {
		return (ArrayList<Enum>) itemIceArrowType.clone();
	}

	public static final ArrayList<Enum> getItemArmorHelmetType() {
		return (ArrayList<Enum>) itemArmorHelmetType.clone();
	}

	public static final ArrayList<Enum> getItemArmorChestType() {
		return (ArrayList<Enum>) itemArmorChestType.clone();
	}

	public static final ArrayList<Enum> getItemArmorLegginsType() {
		return (ArrayList<Enum>) itemArmorLegginsType.clone();
	}

	public static final ArrayList<Enum> getItemShieldType() {
		return (ArrayList<Enum>) itemShieldType.clone();
	}

	public static final ArrayList<Enum> getItemRingType() {
		return (ArrayList<Enum>) itemRingType.clone();
	}

	public static final ArrayList<Enum> getItemHealthPotType() {
		return (ArrayList<Enum>) itemHealthPotType.clone();
	}

	public static final ArrayList<Enum> getItemPotionType(EnumPotionColor p) {
		ArrayList<Enum> pot = (ArrayList<Enum>) itemHealthPotType.clone();
		pot.add(p);
		return pot;

	}

	public static final ArrayList<Enum> getItemSwordType() {
		return (ArrayList<Enum>) itemSwordType.clone();
	}

	public static final ArrayList<Enum> getItemMeleeWeaponType() {
		return (ArrayList<Enum>) itemMeleeWeaponType.clone();
	}

	public static final ArrayList<Enum> getItemFireArrowType() {
		return (ArrayList<Enum>) itemFireArrowType.clone();
	}

	public static ArrayList<Enum> getItemPickaxeType() {
		return (ArrayList<Enum>) itemPickaxeType.clone();
	}
}
