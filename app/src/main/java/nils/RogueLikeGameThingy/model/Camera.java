package model;

import entities.Coordinate;
import entities.Lamp;
import entities.Living;

public class Camera {
	private int x;
	private int y;
	private int publisher_oldx;
	private int publisher_oldy;
	private static final int viewDistance = 7;
	private int immediateRangeUp;
	private int immediateRangeDown;
	private int immediateRangeLeft;
	private int immediateRangeRight;
	protected Living publisher;
	protected byte[][] litFields; //ayy lmao
	protected LightHandler lightHandler;

	public Camera() {
		setVisionRangeToStandard();
	}

	private void setVisionRangeToStandard() {
		immediateRangeLeft = viewDistance;
		immediateRangeRight = viewDistance;
		immediateRangeUp = viewDistance;
		immediateRangeDown = viewDistance;
	}

	public void subscribeTo(Living publisher) {
		publisher.subscribeThis(this);
		x = publisher.x;
		y = publisher.y;
		publisher_oldx = publisher.x;
		publisher_oldy = publisher.y;
		this.publisher = publisher;
		lightHandler = new LightHandler(publisher.getFloor().getShadowCaster(), publisher.getFloor());
		litFields = new byte[publisher.getFloor().getRelevantX()][publisher.getFloor().getRelevantY()];
	}

	public void publisherUpdated() {
		if (publisher != null) {
			x += publisher.x - publisher_oldx;
			y += publisher.y - publisher_oldy;
			publisher_oldx = publisher.x;
			publisher_oldy = publisher.y;
			updateLighting();
		}
	}

	public boolean isFieldVisible(int x, int y) {
		return litFields[x][y] > 1;
	}

	public Coordinate getUpperLeftVisionCorner() {
		return new Coordinate(x - immediateRangeLeft, y - immediateRangeUp);
	}

	public Coordinate getLowerRightVisionCorner() {
		return new Coordinate(x + immediateRangeRight, y + immediateRangeDown);
	}



	private boolean isFieldInImmediateRange(int x, int y) {
		if (x > this.x - immediateRangeLeft && x < this.x + immediateRangeRight && y > this.y - immediateRangeUp
				&& y < this.y + immediateRangeDown) {
			return true;
		} else {
			return false;
		}
	}

	protected void addLampsToVisionList() {
		for (Lamp lamp : publisher.getFloor().getLampList()) {
			lightHandler.addLamp(lamp.getX(), lamp.getY());
		}
	}

	public void updateLighting() {
		lightHandler.updateLight(publisher.getCoordinate());
		byte[][] newLitFields = lightHandler.getLitFields();
		for (int i = 0; i < publisher.getFloor().getRelevantX(); i++) {
			for (int j = 0; j < publisher.getFloor().getRelevantY(); j++) {
				switch (newLitFields[i][j]) {
				case 3:
				case 2:
					litFields[i][j] = 2;
					break;
				case 1:
					if (litFields[i][j] != 2) {
						litFields[i][j] = 1;
					}
					break;
				case 0:
					if (litFields[i][j] != 0) {
						litFields[i][j] = 1;
					}
					break;
				}
			}
		}
		addLampsToVisionList();
	}

	public byte[][] getVisibleFields() {
		return litFields;
	}

	public boolean isFieldInPenumbra(int i, int j) {
		return litFields[i][j] == 1;
	}

	public void flush() {
		litFields = new byte[litFields.length][litFields[0].length];
		updateLighting();
	}

}
