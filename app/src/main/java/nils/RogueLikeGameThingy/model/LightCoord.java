package model;

import java.util.ArrayList;

import entities.Coordinate;

public class LightCoord extends Coordinate{
	private boolean[] receivedLightFrom = new boolean[4];
	private Lightbeam light = new Lightbeam();
	private int distanceToPlayer = Integer.MAX_VALUE;
	private int maxDistance;
	private int lightLevel = 0;
	
	public LightCoord(int x, int y) {
		super(x, y);
	}
	
	public void resetReceivedLightFrom(){
		receivedLightFrom = new boolean[4];
	}
	
	public int getLightLevel(){
		return lightLevel;
	}
	
	public void setDistanceToLightsource(int distance){
		distanceToPlayer = distance;
	}
	
	public void setMaxDistance(int maxDistance){
		this.maxDistance = maxDistance;
	}
	
	public LightCoord(Coordinate coord){
		super(coord);
	}
	
	public void darken(EnumDirection dir){
		light.darken(dir);
	}
	
	public void newLightbeam(){
		light.newLightbeam();
	}
	
	public Lightbeam getLightbeam(){
		return light;
	}
	
	public int getDistanceToLightsource(){
		return distanceToPlayer;
	}
	
	public boolean hasAlreadyReceivedLightFrom(EnumDirection fromDirection){
		switch(fromDirection){
		case LEFT:
			if(receivedLightFrom[0]){
				return true;
			}
			break;
		case UP:
			if(receivedLightFrom[1]){
				return true;
			}
			break;
		case RIGHT:
			if(receivedLightFrom[2]){
				return true;
			}
			break;
		case DOWN:
			if(receivedLightFrom[3]){
				return true;
			}
			break;
		default:
			throw new IllegalArgumentException("direction is not cardinal");
		}
		return false;
	}
	
	public boolean addLightbeam(Lightbeam beam, EnumDirection fromDirection){
		if(distanceToPlayer >= maxDistance){
			return false;
		}
		switch(fromDirection){
		case LEFT:
			if(receivedLightFrom[0]){
				return false;
			}
			receivedLightFrom[0] = true;
			break;
		case UP:
			if(receivedLightFrom[1]){
				return false;
			}
			receivedLightFrom[1] = true;
			break;
		case RIGHT:
			if(receivedLightFrom[2]){
				return false;
			}
			receivedLightFrom[2] = true;
			break;
		case DOWN:
			if(receivedLightFrom[3]){
				return false;
			}
			receivedLightFrom[3] = true;
			break;
		}
		light.addLightbeam(beam);
		lightLevel = 2;
		return true;
	}

	public void setLightLevel(int lightLvl) {
		lightLevel = lightLvl;
	}
}
