package model;

public enum EnumDirection {
	UP, LEFT, DOWN, RIGHT, NULL, UPLEFT, UPRIGHT, DOWNLEFT, DOWNRIGHT;
}
