package model;

import entities.Living;
import entities.Statuseffect;

public abstract class TrapEffect extends Statuseffect {
	public abstract boolean applyTo(Living liv);

}
