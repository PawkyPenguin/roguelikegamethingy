package model;

public class HealthBuffer {
	private int maxBufferHealth;
	private int bufferHealth;
	private int healthThreshold;
	private int index;
	
	public HealthBuffer(int maxHealth, int healthThreshold){
		maxBufferHealth = maxHealth;
		this.healthThreshold = healthThreshold;
	}
	
	public void setIndex(int index){
		this.index = index;
	}
	
	public int getIndex(){
		return index;
	}
	
	public int getHealth(){
		return bufferHealth;
	}
	
	public boolean isFull(){
		return (maxBufferHealth == bufferHealth);
	}
	
	public boolean isEmpty(){
		return (bufferHealth <= 0);
	}
	
	public void fill(){
		bufferHealth = maxBufferHealth;
	}
	
	public void empty(){
		bufferHealth = 0;
	}
	
	public int getHealthThreshold(){
		return healthThreshold;
	}
	
	public int getMaxBufferHealth(){
		return maxBufferHealth;
	}
	
	public void damage(int damage){
		bufferHealth -= damage;
	}
	
	public void heal(int health){
		bufferHealth += health;
		if(bufferHealth > maxBufferHealth){
			bufferHealth = maxBufferHealth;
		}
	}
}
