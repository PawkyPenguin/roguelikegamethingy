package model;

import java.util.ArrayList;

import entities.Coordinate;

public class Direction {
	public EnumDirection direction;
	public ArrayList<Coordinate> perimeterList = new ArrayList<Coordinate>();
	public static final EnumDirection[] cardinalDirections = { EnumDirection.LEFT, EnumDirection.UP,
			EnumDirection.RIGHT, EnumDirection.DOWN };
	public static final int[][] cardinalDirectionDeltas = { { -1, 0 }, { 0, -1 }, { 1, 0 }, { 0, 1 } };

	public Direction() {
		this.direction = EnumDirection.NULL;
		makePerimeterList();
	}

	public Direction(EnumDirection direction) {
		this.direction = direction;
		makePerimeterList();
	}

	private void makePerimeterList() {
		for (int i = -1; i < 2; i++) {
			for (int j = -1; j < 2; j++) {
				perimeterList.add(new Coordinate(i, j));
			}
		}
	}

	public Coordinate getFieldInDirection(Coordinate coord) {
		coord.setX(coord.getX() + getX());
		coord.setY(coord.getY() + getY());
		return coord;
	}

	public static Coordinate getFieldInDirection(Coordinate coord, EnumDirection dir) {
		Coordinate newCoord = new Coordinate(coord.getX() + getX(dir), coord.getY() + getY(dir));
		return newCoord;
	}

	public static Coordinate getFieldInDirection(Coordinate coord, Coordinate direction) {
		return new Coordinate(coord.getX() + (int) Math.signum(direction.getX() - coord.getX()), coord.getY()
				+ (int) Math.signum(direction.getY() - coord.getY()));
	}

	public static Coordinate getDirectionVector(Coordinate coord, Coordinate direction) {
		return new Coordinate((int) Math.signum(direction.getX() - coord.getX()), (int) Math.signum(direction
				.getY() - coord.getY()));
	}

	public static EnumDirection getDirection(Coordinate coord) {
		if (coord.getX() == 0 && coord.getY() == 0) {
			return EnumDirection.NULL;
		} else if (coord.getX() > 0 && coord.getY() > 0) {
			return EnumDirection.DOWNRIGHT;
		} else if (coord.getX() > 0 && coord.getY() < 0) {
			return EnumDirection.UPRIGHT;
		} else if (coord.getX() > 0 && coord.getY() == 0) {
			return EnumDirection.RIGHT;
		} else if (coord.getX() < 0 && coord.getY() == 0) {
			return EnumDirection.LEFT;
		} else if (coord.getX() < 0 && coord.getY() < 0) {
			return EnumDirection.UPLEFT;
		} else if (coord.getX() < 0 && coord.getY() > 0) {
			return EnumDirection.DOWNLEFT;
		} else if (coord.getX() == 0 && coord.getY() > 0) {
			return EnumDirection.DOWN;
		} else {
			return EnumDirection.UP;
		}
	}

	public void setDirection(Coordinate coord) {
		direction = getDirection(coord);
	}

	public int getX() {
		return getX(direction);
	}

	public static int getX(EnumDirection dir) {
		switch (dir) {
		case DOWNLEFT:
			return -1;
		case LEFT:
			return -1;
		case UPLEFT:
			return -1;
		case UPRIGHT:
			return 1;
		case RIGHT:
			return 1;
		case DOWNRIGHT:
			return 1;
		default:
			return 0;
		}
	}

	public void print() {
		switch (direction) {
		case DOWNLEFT:
			System.out.println("downleft [Direction]");
			break;
		case LEFT:
			System.out.println("left [Direction]");
			break;
		case UPLEFT:
			System.out.println("upleft [Direction]");
			break;
		case UPRIGHT:
			System.out.println("upright [Direction]");
			break;
		case RIGHT:
			System.out.println("right [Direction]");
			break;
		case DOWNRIGHT:
			System.out.println("downright [Direction]");
			break;
		case DOWN:
			System.out.println("down [Direction]");
			break;
		case UP:
			System.out.println("up [Direction]");
			break;
		default:
			System.out.println("null [Direction]");
			break;
		}
	}

	public void setDirection(int dx, int dy) {
		switch (dy) {
		case 0:
			switch (dx) {
			case 1:
				direction = EnumDirection.RIGHT;
				break;
			case -1:
				direction = EnumDirection.LEFT;
				break;
			}
			break;
		case 1:
			switch (dx) {
			case 1:
				direction = EnumDirection.DOWNRIGHT;
				break;
			case -1:
				direction = EnumDirection.DOWNLEFT;
				break;
			case 0:
				direction = EnumDirection.DOWN;
			}
			break;
		case -1:
			switch (dx) {
			case 1:
				direction = EnumDirection.UPRIGHT;
				break;
			case -1:
				direction = EnumDirection.UPLEFT;
				break;
			case 0:
				direction = EnumDirection.UP;
				break;
			}
			break;
		}
	}

	public int getY() {
		return getY(direction);
	}

	public static int getY(EnumDirection dir) {
		switch (dir) {
		case UP:
			return -1;
		case UPLEFT:
			return -1;
		case UPRIGHT:
			return -1;
		case DOWN:
			return 1;
		case DOWNLEFT:
			return 1;
		case DOWNRIGHT:
			return 1;
		default:
			return 0;
		}
	}

	public static EnumDirection toDirection(int i) {
		switch (i) {
		case 0:
			return EnumDirection.UP;
		case 1:
			return EnumDirection.LEFT;
		case 2:
			return EnumDirection.DOWN;
		case 3:
			return EnumDirection.RIGHT;
		case 4:
			return EnumDirection.UPLEFT;
		case 5:
			return EnumDirection.DOWNLEFT;
		case 6:
			return EnumDirection.DOWNRIGHT;
		case 7:
			return EnumDirection.UPRIGHT;
		default:
			throw new IllegalArgumentException(i + " is not in range 0..7");
		}
	}
}
