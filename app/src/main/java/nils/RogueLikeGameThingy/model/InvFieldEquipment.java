package model;

import entities.Living;
import item.EnumItemType;
import item.ItemEquipable;

public class InvFieldEquipment extends InventoryField{
	private Living owner;
	private EnumInvFieldEquipType equipType;
	
	public void setOwner(Living liv){
		owner = liv;
	}
	
	public EnumInvFieldEquipType getEquipType(){
		return equipType;
	}
	
	public void setType(EnumInvFieldEquipType type){
		equipType = type;
		switch (type) {
		case WEAPON:
			setContainedItemType(TypeHandler.getItemMeleeWeaponType());
			break;
		case SHIELD:
			setContainedItemType(TypeHandler.getItemShieldType());
			break;
		case RING:
			setContainedItemType(TypeHandler.getItemRingType());
			break;
		case HELMET:
			setContainedItemType(TypeHandler.getItemArmorHelmetType());
			break;
		case CHEST:
			setContainedItemType(TypeHandler.getItemArmorChestType());
			break;
		case LEGGINS:
			setContainedItemType(TypeHandler.getItemArmorLegginsType());
			break;
		default:
			break;
		}
	}
	
	@Override
	protected void setType() {
		invFieldType = EnumInventoryFieldType.EQUIPMENT;
		containedItemType.add(EnumItemType.EQUIPABLE);
	}

	@Override
	protected void doItemRemovalAction(){
		if(getItem() != null){
			((ItemEquipable)getItem()).unequipFrom(owner);
		}
	}
	
	@Override
	protected void doItemAdditionAction(){
		if(getItem() != null){
			((ItemEquipable)getItem()).equipTo(owner);
		}
	}

}
