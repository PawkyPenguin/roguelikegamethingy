package model;

import controller.Controller;
import controller.InputContainer;
import controller.InputDecoder;
import entities.Player;

public class GameHandler {
	Game game;
	Controller subController;
	InputDecoder inputHandler;
	long msLastFrame;

	public GameHandler() {
	}

	public void setInputHandler(InputDecoder in) {
		inputHandler = in;
	}

	public void restartGame() {
		game = new Game();
		game.fillEntityListPreGame();
		game.start();
	}

	public Floor getFloor() {
		return game.getFloor();
	}

	public Player getPlayer() {
		return game.getPlayer();
	}

	public void tick() {
		long msThisFrame = System.currentTimeMillis();
		float timeSinceLastFrame = ((float) (msThisFrame - msLastFrame) / 1000f);
		msLastFrame = msThisFrame;
		inputHandler.requestCheckForInput();
		if (game.running) {
			game.tick(timeSinceLastFrame);
		}
		updateSubscriber();
	}

	public void publisherUpdated(InputContainer inputs) {
		game.transferInputsToPlayer(inputs);
	}

	public void enlistSub(Controller sub) {
		this.subController = sub;
	}

	public void unlistSub(Controller sub) {
		this.subController = null;
	}

	private void updateSubscriber() {
		subController.publisherUpdated(this);
	}

	public void start() {
		if (game.running) {
			return;
		} else {
			game.start();
		}
	}

	public void stop() {
		if (!game.running) {
			return;
		} else {
			game.stop();
		}
	}

	public void exit() {
		System.exit(1);
	}
}
