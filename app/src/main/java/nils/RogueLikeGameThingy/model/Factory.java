package model;

import entities.Attackable;
import entities.Background;
import entities.BackgroundInteractable;
import entities.Coordinate;
import entities.Decoration;
import entities.DecorationGlowMoss;
import entities.DecorationMoss;
import entities.Entity;
import entities.Living;
import lazyness.Chance;
import view.Animation;

public class Factory {
	Floor floor;

	public Factory() {

	}

	public void newCamera(Living liv) {
		liv.newStandardCamera();
	}

	public void setFloor(Floor floor) {
		this.floor = floor;
	}

	public BackgroundInteractable initializeBackgroundInteractable(BackgroundInteractable i, Coordinate coord) {
		floor.getBackgroundObject(coord).setInteractable(i);
		return i;
	}

	// returns for convenience
	public Entity initializeEntity(Entity e) {
		e.setAttributes(floor);
		if (e instanceof Attackable) {
			((Attackable) e).setDrop();
		}
		if (e instanceof Living) {
			((Living) e).newStandardCamera();
		}
		return e;
	}

	private Decoration generateRandomDecoration() {
		if (Chance.oneTwentieth()) {
			return new DecorationMoss();
		} else if (Chance.oneHundredth()) {
			DecorationGlowMoss glowMoss = new DecorationGlowMoss();
			Animation glowMossAnimation = glowMoss.getAnimation();
			glowMossAnimation.setFrame((int) (Math.random() * glowMossAnimation.getSprites().size()));
			return glowMoss;
		} else {
			return null;
		}
	}

	public void decorate(Background backgroundObject) {
		backgroundObject.setDecoration(generateRandomDecoration());
	}
}
