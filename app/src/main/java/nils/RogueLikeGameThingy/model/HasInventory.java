package model;

import java.awt.image.BufferedImage;

import entities.Coordinate;
import view.SpriteCompendium;

public interface HasInventory {
	public BufferedImage inventoryFieldBlankSprite = SpriteCompendium.getInvUISprites().getAnimation("invFieldBlank").getCurrentFrame();
	
	public int getOwnerIndex();
	
	public Inventory getInventory();
	
	public void initInventory();
	
	public int getX();
	
	public int getY();
	
	public Coordinate getCoordinate();
}
