package model;

import java.util.ArrayList;

import entities.Attackable;
import entities.Background;
import entities.Chest;
import entities.Coordinate;
import entities.CraftingTable;
import entities.Entity;
import entities.FloorTile;
import entities.Foreground;
import entities.Interactable;
import entities.Lamp;
import entities.Living;
import entities.Monster;
import entities.Player;
import entities.Shopkeeper;
import entities.Stairs;
import entities.Wall;
import entities.Ward;
import entities.bosses.mechsnake.BodyPart;
import entities.bosses.mechsnake.MechSnake;
import entities.bosses.mechsnake.MechSnakeBodyPart;
import entities.bosses.mechsnake.MechSnakeHead;
import entities.bosses.mechsnake.MechSnakeTail;
import entities.npcs.Adventurer;
import item.Item;
import item.ItemDrop;
import item.PotionGenerator;
import labyrinthGen.FloorGenerator;
import labyrinthGen.SaphsLabyrinth;
import lazyness.Chance;
import shadowcasting.ShadowCaster;
import view.EntityView;

public class Floor {
	private Entity[][][] floor;
	private int level;
	private ArrayList<EntityView> animationList = new ArrayList<EntityView>();
	private ArrayList<Living> attackOrderList = new ArrayList<Living>();
	private ArrayList<Ward> wardList = new ArrayList<Ward>();
	private ArrayList<Lamp> lampList = new ArrayList<Lamp>();
	private ArrayList<Coordinate> wallList = new ArrayList<Coordinate>();
	private int relevantX;
	private int relevantY;
	private final int depth = 3;
	private Player player;
	private Factory factory = new Factory();
	private EnumLayerType type;
	private FloorGenerator labGen;
	private PotionGenerator potionGen;
	private ShadowCaster shadowCaster = new ShadowCaster();

	public Floor() {
		factory.setFloor(this);
		potionGen = new PotionGenerator();
	}

	public void addItemDrop(int x, int y, ItemDrop i) {
		floor[x][y][2] = i;
	}

	public void setType(EnumLayerType type) {
		this.type = type;
	}

	public void addToLampList(Lamp lamp) {
		lampList.add(lamp);
	}

	public void removeFromLampList(Lamp lamp) {
		lampList.remove(lamp);
	}

	public ArrayList<Lamp> getLampList() {
		return lampList;
	}

	public void newCamera(Living liv) {
		factory.newCamera(liv);
	}

	public void setDimensions(int width, int height) {
		floor = new Entity[width][height][depth];
		relevantX = width - 1;
		relevantY = height - 2;
		labGen = new SaphsLabyrinth();
	}

	public Coordinate[] getAdjacentCoords(Coordinate coord) {
		Coordinate[] adjacents = new Coordinate[4];
		int[][] relativePos = Direction.cardinalDirectionDeltas;
		for (int i = 0; i < relativePos.length; i++) {
			Coordinate newAdjacent = new Coordinate(coord.x + relativePos[i][0], coord.y + relativePos[i][1]);
			if (isCoordinateWithinLayer(newAdjacent.x, newAdjacent.y)) {
				adjacents[i] = newAdjacent;
			}
		}
		return adjacents;
	}
	public void initializePlayer(Player player) {
		this.player = player;
		factory.initializeEntity(player);
	}

	private void updateLevelType() {
		if (isEmptyLevel()) {
			type = EnumLayerType.EMPTY;
		} else if (isBossLevel()) {
			type = EnumLayerType.BOSS;
		} else if (isShopLevel()) {
			type = EnumLayerType.SHOP;
		} else {
			type = EnumLayerType.NORMAL;
		}
	}

	private boolean isEmptyLevel() {
		return level == 0;
	}

	private boolean isBossLevel() {
		return level != 0 && level % 15 == 0;
	}

	private boolean isShopLevel() {
		return level != 0 && level % 5 == 0;
	}

	public void nextLevel() {
		level++;
		updateLevelType();
		generateFloor();
		/*
		 * Updating shadow caster
		 */
		boolean obstacleMap[][] = new boolean[relevantX][relevantY];
		for (int i = 0; i < relevantX; i++) {
			for (int j = 0; j < relevantY; j++) {
				obstacleMap[i][j] = isFieldOpaque(i, j);
			}
		}

		shadowCaster.setObstacleMap(obstacleMap);
		Coordinate playerPos = findRandomEmptyPosition();
		player.setPosition(playerPos.getX(), playerPos.getY());
		player.setIsDescending(false);
		newCamera(player);
		player.getCamera().flush();
		// TODO: Debugging only
		if (player.getGodmode()){
			factory.initializeBackgroundInteractable(new Stairs(), player.getCoordinate());
		}
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public int getLevel() {
		return level;
	}

	public void addToWardList(Ward ward) {
		wardList.add(ward);
	}

	public void removeFromWardList(Ward ward) {
		wardList.remove(ward);
	}

	public void addToAttackOrderList(Living living) {
		attackOrderList.add(living);
	}

	public void addToAnimationList(EntityView view) {
		animationList.add(view);
	}

	public int getRelevantX() {
		return relevantX;
	}

	public int getRelevantY() {
		return relevantY;
	}

	public boolean isFieldOpaque(int x, int y) {
		if (!isCoordinateWithinLayer(x, y)) {
			return false;
		} else {
			for (int i = 0; i < depth; i++) {
				if (getObject(x, y, i) != null && getObject(x, y, i).isOpaque()) {
					return true;
				}
			}
			return false;
		}
	}

	public ArrayList<Ward> getWardList() {
		return wardList;
	}

	public boolean isWall(int x, int y) {
		return getForegroundObject(x, y) instanceof Wall;
	}

	public Foreground getForegroundObject(Coordinate coord) {
		return getForegroundObject(coord.x, coord.y);
	}

	public Foreground getForegroundObject(int x, int y) {
		if (isCoordinateWithinLayer(x, y)) {
			return (Foreground) floor[x][y][1];
		} else {
			return null;
		}
	}

	public Background getBackgroundObject(Coordinate coord) {
		return getBackgroundObject(coord.x, coord.y);
	}

	public Background getBackgroundObject(int x, int y) {
		if (isCoordinateWithinLayer(x, y)) {
			return (Background) floor[x][y][0];
		} else {
			return null;
		}
	}

	public Entity getObject(int x, int y, int z) {
		if (isCoordinateWithinLayer(x, y, z)) {
			return floor[x][y][z];
		} else {
			return null;
		}
	}

	public void setObject(Entity e) {
		deleteObject(e.getX(), e.getY(), e.getDepth());
		floor[e.getX()][e.getY()][e.getDepth()] = e;
	}

	public void deleteObject(int x, int y, int z) {
		if (floor[x][y][z] != null) {
			Entity e = floor[x][y][z];
			floor[x][y][z] = null;
			e.removalAction();
		}
	}

	public void die(Attackable a) {
		if (a instanceof Living) {
			removeLivingFromAttackList((Living) a);
		}
		deleteObject(a.getX(), a.getY(), 1);
	}

	public boolean isPosFreeForMoving(int x, int y) {
		if (!isCoordinateWithinLayer(x, y)) {
			return false;
		} else {
			for (int i = 0; i < depth; i++) {
				if (getObject(x, y, i) != null && getObject(x, y, i).isSolid()) {
					return false;
				}
			}
			return true;
		}
	}

	public void tick(double timeSinceLastFrame) {
		boolean allLivingsFinishedTurns = true;
		for (Entity[][] entityLayer : floor) {
			for (Entity[] entityRow : entityLayer) {
				for (Entity entity : entityRow) {
					if (entity != null) {
						entity.tick(timeSinceLastFrame);
					}
				}
			}
		}
		for (int i = 0; i < attackOrderList.size(); i++) {
			if (!attackOrderList.get(i).hasFinishedTurn()) {
				attackOrderList.get(i).doTurn();
				allLivingsFinishedTurns = false;
				break;
			}
			if (!attackOrderList.get(i).hasFinishedAttacking()) {
				attackOrderList.get(i).getAttackLogic().tick(timeSinceLastFrame);
				allLivingsFinishedTurns = false;
				break;
			}
		}
		if (allLivingsFinishedTurns) {
			for (Living liv : attackOrderList) {
				liv.setFinishedTurn(false);
			}

		}
	}

	public boolean isCoordinateWithinLayer(int x, int y) {
		if (x < 0 || x >= relevantX || y < 0 || y >= relevantY) {
			return false;
		} else {
			return true;
		}
	}

	public boolean isCoordinateWithinLayer(int x, int y, int z) {
		if (!isCoordinateWithinLayer(x, y) || z < 0 || z >= depth) {
			return false;
		} else {
			return true;
		}
	}

	public boolean isSolid(int x, int y, int z) {
		if (floor[x][y][z] != null) {
			return floor[x][y][z].isSolid();
		}
		return false;
	}

	public boolean isPositionUnoccupied(int x, int y) {
		if (!isCoordinateWithinLayer(x, y)) {
			return false;
		} else if (getForegroundObject(x, y) == null) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isPosFreeForMoving(Coordinate coord) {
		if (!isCoordinateWithinLayer(coord.x, coord.y)) {
			return false;
		} else {
			for (int i = 0; i < depth; i++) {
				if (floor[coord.x][coord.y][i] != null && floor[coord.x][coord.y][i].isSolid()) {
					return false;
				}
			}
			return true;
		}
	}

	public int getWalkingCost(int x, int y) {
		int totalCost = 0;
		for (int i = 0; i < depth; i++) {
			if (floor[x][y][i] != null) {
				totalCost += floor[x][y][i].getWalkingCost();
			}
		}
		return totalCost;
	}

	public void drop(Item item, int x, int y) {
		factory.initializeEntity(new ItemDrop(item, x, y));
	}

	public boolean dropInCircle(Item drop, int x, int y) {
		Coordinate[] circle = { new Coordinate(0, 0), new Coordinate(0, -1), new Coordinate(0, 1),
				new Coordinate(-1, 0), new Coordinate(1, 0), new Coordinate(-1, -1), new Coordinate(-1, 1),
				new Coordinate(1, -1), new Coordinate(1, 1) };
		for (int i = 0; i < circle.length; i++) {
			if (isPosFreeForMoving(x + circle[i].x, y + circle[i].y)) {
				drop(drop, x + circle[i].x, y + circle[i].y);
				return false;
			}
		}
		return true;
	}

	public void emptyFloor() {
		Player player = (Player) attackOrderList.get(0);
		for (int i = 0; i < floor.length; i++) {
			for (int j = 0; j < floor[0].length; j++) {
				for (int k = 0; k < floor[0][0].length; k++) {
					deleteObject(i, j, k);
				}
				factory.initializeEntity(new FloorTile(i, j));
				if (getBackgroundObject(i, j) != null) {
					factory.decorate(getBackgroundObject(i, j));
				}
			}
		}
		if (player == null) {
			System.out.println("fatal exception, player not found [Floor]");
			System.exit(1);
		}
		resetLists();
		attackOrderList.add(player);
	}

	public static boolean isEntitySolid(Entity e) {
		return e != null && e.isSolid();
	}

	private void resetLists() {
		wallList = new ArrayList<Coordinate>();
		wardList = new ArrayList<Ward>();
		lampList = new ArrayList<Lamp>();
		attackOrderList = new ArrayList<Living>();
	}

	public void generateFloor() {
		emptyFloor();
		switch (type) {
		case EMPTY:
			break;
		case SHOP:
			factory.initializeEntity(new Shopkeeper(10, 10));
			factory.initializeEntity(new CraftingTable(12, 12));
			factory.initializeEntity(new Adventurer(13, 13));
			break;
		case NORMAL:
			generateLabyrinth();
			wallList = findWallPositions();
			int amountOfEmptyPos = findAmountOfEmptyPositions();
			for (int i = 0; i < Math.floor(amountOfEmptyPos / 18 + Math.random() * 8) - 4; i++) {
				Coordinate monsterPos = findRandomEmptyPosition();
				if (Math.random() >= 0.5) {
					factory.initializeEntity(new Monster(monsterPos.x, monsterPos.y, EnumMonsterType.SKELETON));
				} else {
					factory.initializeEntity(new Monster(monsterPos.x, monsterPos.y, EnumMonsterType.GOBLIN));
				}
			}
			for (int i = 0; i < Math.floor(amountOfEmptyPos / 25 + Math.random() * 6) - 4; i++) {
				Coordinate itemPos = findRandomEmptyPosition();
				if (Chance.oneThird()) {
					factory.initializeEntity(new ItemDrop(Chest.generateHealthpot(1), itemPos.x, itemPos.y));
				} else if (Chance.twoThird()) {
					if (Chance.twoThird()) {
						factory.initializeEntity(new ItemDrop(Chest.generateNormalArrow((int) (Math.random() + 1)),
								itemPos.x, itemPos.y));
					} else {
						if (Chance.oneHalf()) {
							factory.initializeEntity(new ItemDrop(Chest.generateFireArrow((int) (Math.random() + 1)),
									itemPos.x, itemPos.y));
						} else {
							factory.initializeEntity(new ItemDrop(Chest.generateIceArrow((int) (Math.random() + 1)),
									itemPos.x, itemPos.y));
						}
					}
				} else {
					factory.initializeEntity(new ItemDrop(Chest.generateHealthpot(2), itemPos.x, itemPos.y));
				}
			}
			int amountOfChestsOnFloor = (int) Math.floor(wallList.size() / 100 + Math.random() * 2);
			replaceSomeWallWallsWith(Chest.class, amountOfChestsOnFloor);
			int amountOfLampsOnFloor = (int) Math.floor(wallList.size() / 40 + Math.random());
			// replaceSomeWallsWith(Lamp.class, amountOfLampsOnFloor);
			break;
		case BOSS:
			MechSnake boss = new MechSnake();
			MechSnakeHead head = (MechSnakeHead) factory.initializeEntity(new MechSnakeHead(10, 10));
			MechSnakeBodyPart body1 = (MechSnakeBodyPart) factory.initializeEntity(new MechSnakeBodyPart(11, 10));
			MechSnakeTail tail = (MechSnakeTail) factory.initializeEntity(new MechSnakeTail(12, 10));
			ArrayList<BodyPart> bodyList = new ArrayList<BodyPart>();
			bodyList.add(body1);
			boss.setParts(head, bodyList, tail);
			break;
		}
		Coordinate stairsPos = findRandomEmptyPosition();
		factory.initializeBackgroundInteractable(new Stairs(), stairsPos);
		refreshWallSprites();
	}

	private void replaceSomeTallWallsWith(Class<? extends Entity> entity, int amountOfEntity) {
		ArrayList<Coordinate> wallList = findWallPositions();
		if (wallList.size() <= 0) {
			return;
		}
		for (int i = 0; i < amountOfEntity; i++) {
			Coordinate randomWall;
			randomWall = wallList.get(generateRandomIndex(wallList));
			Entity newEntity;
			try {
				newEntity = (Entity) entity.newInstance();
				newEntity.setX(randomWall.x);
				newEntity.setY(randomWall.y);
				factory.initializeEntity(newEntity);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

	private void replaceSomeWallWallsWith(Class<? extends Entity> entity, int amountOfEntity) {
		ArrayList<Coordinate> wallList = findWallPositions();
		if (wallList.size() <= 0) {
			return;
		}
		for (int i = 0; i < amountOfEntity; i++) {
			boolean positionValid = false;
			Coordinate randomWall;
			do {
				randomWall = wallList.get(generateRandomIndex(wallList));
				for (Coordinate coord : getAdjacentCoords(randomWall)) {
					if (coord != null && getForegroundObject(coord.x, coord.y) == null) {
						positionValid = true;
					}
				}
			} while (!positionValid);
			Entity newEntity;
			try {
				newEntity = (Entity) entity.newInstance();
				newEntity.setX(randomWall.x);
				newEntity.setY(randomWall.y);
				factory.initializeEntity(newEntity);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

	public void generateLabyrinth() {
		boolean[][] lab = labGen.generateLabyrinth(relevantX, relevantY);
		for (int i = 0; i < lab.length; i++) {
			for (int j = 0; j < lab[0].length; j++) {
				if (lab[i][j]) {
					factory.initializeEntity(new Wall(i, j));
				}
			}
		}
	}

	public void refreshWallSprites() {
		for (int i = 0; i < floor.length; i++) {
			for (int j = 0; j < floor[0].length; j++) {
				if (getForegroundObject(i, j) instanceof Wall) {
					getForegroundObject(i, j).setSprites();
				}
			}
		}
	}

	public void generateOldLabyrinth() {
		final int startX = 0;
		final int startY = 0;
		final int endX = 0;
		final int endY = 0;
		ArrayList<Coordinate> coordList = new ArrayList<Coordinate>();
		do {
			coordList.clear();
			for (int i = startX; i < floor.length - endX; i += 2) {
				for (int j = startY; j < floor[0].length - endY; j += 2) {
					if (canWallBeStartedFromCoord(new Coordinate(i, j))) {
						coordList.add(new Coordinate(i, j));
					}
				}
			}

			if (!coordList.isEmpty()) {
				Coordinate chosenCoord = coordList.get(generateRandomIndex(coordList));
				if ((Math.random() >= 0.5) && (isPosFreeForMoving(chosenCoord.x + 2, chosenCoord.y))) {
					factory.initializeEntity(new Wall(chosenCoord.x, chosenCoord.y));
					factory.initializeEntity(new Wall(chosenCoord.x + 1, chosenCoord.y));
					factory.initializeEntity(new Wall(chosenCoord.x + 2, chosenCoord.y));
				} else if (isPosFreeForMoving(chosenCoord.x, chosenCoord.y + 2)) {
					factory.initializeEntity(new Wall(chosenCoord.x, chosenCoord.y));
					factory.initializeEntity(new Wall(chosenCoord.x, chosenCoord.y + 1));
					factory.initializeEntity(new Wall(chosenCoord.x, chosenCoord.y + 2));
				} else if ((Math.random() >= 0.5) && isPosFreeForMoving(chosenCoord.x - 2, chosenCoord.y)) {
					factory.initializeEntity(new Wall(chosenCoord.x, chosenCoord.y));
					factory.initializeEntity(new Wall(chosenCoord.x - 1, chosenCoord.y));
					factory.initializeEntity(new Wall(chosenCoord.x - 2, chosenCoord.y));
				} else if (isPosFreeForMoving(chosenCoord.x, chosenCoord.y - 2)) {
					factory.initializeEntity(new Wall(chosenCoord.x, chosenCoord.y));
					factory.initializeEntity(new Wall(chosenCoord.x, chosenCoord.y - 1));
					factory.initializeEntity(new Wall(chosenCoord.x, chosenCoord.y - 2));
				}
			}
		} while (!coordList.isEmpty());

		for (int i = 0; i < floor.length; i++) {
			factory.initializeEntity(new Wall(i, floor[0].length - 2));
			factory.initializeEntity(new Wall(i, floor[0].length - 1));
		}
		for (int i = 0; i < floor[0].length; i++) {
			factory.initializeEntity(new Wall(floor.length - 1, i));
		}
	}

	public void removeLivingFromAttackList(Living e) {
		attackOrderList.remove(e);
	}

	public boolean isActionAvailable(int x, int y, Living liv) {
		if (isCoordinateWithinLayer(x, y) && getForegroundObject(x, y) instanceof Interactable
				&& ((Interactable) getForegroundObject(x, y)).isInteractable(liv)) {
			return true;
		} else {
			return false;
		}
	}

	public boolean canWallBeStartedFromCoord(Coordinate coord) {
		if (isPosFreeForMoving(coord.x, coord.y + 2)) {
			return true;
		}
		if (isPosFreeForMoving(coord.x, coord.y - 2)) {
			return true;
		}
		if (isPosFreeForMoving(coord.x + 2, coord.y)) {
			return true;
		}
		if (isPosFreeForMoving(coord.x - 2, coord.y)) {
			return true;
		}
		return false;
	}

	public ArrayList<Coordinate> findEmptyPositions() {
		ArrayList<Coordinate> coordList = new ArrayList<Coordinate>();
		for (int i = 0; i < relevantX; i++) {
			for (int j = 0; j < relevantY; j++) {
				if (isPosFreeForMoving(i, j) && !(getBackgroundObject(i, j).getInteractable() instanceof Stairs)) {
					coordList.add(new Coordinate(i, j));
				}
			}
		}
		return coordList;
	}

	public ArrayList<Coordinate> findUnoccupiedPositions() {
		ArrayList<Coordinate> coordList = new ArrayList<Coordinate>();
		for (int i = 0; i < relevantX; i++) {
			for (int j = 0; j < relevantY; j++) {
				if (isPositionUnoccupied(i, j) || !getForegroundObject(i, j).isSolid()) {
					coordList.add(new Coordinate(i, j));
				}
			}
		}
		return coordList;
	}

	public Coordinate findRandomEmptyPosition() {
		ArrayList<Coordinate> coordList = findEmptyPositions();
		return coordList.get((int) (Math.random() * coordList.size()));
	}

	public int findAmountOfEmptyPositions() {
		return findEmptyPositions().size();
	}

	public ArrayList<Coordinate> findWallPositions() {
		ArrayList<Coordinate> coordList = new ArrayList<Coordinate>();
		for (int i = 0; i < floor.length; i++) {
			for (int j = 0; j < floor[0].length; j++) {
				if (getForegroundObject(i, j) instanceof Wall) {
					coordList.add(new Coordinate(i, j));
				}
			}
		}
		return coordList;
	}

	private static int generateRandomIndex(ArrayList<Coordinate> coordList) {
		return (int) Math.floor(Math.random() * coordList.size());
	}

	public void removeFromAnimationList(EntityView entityView) {
		animationList.remove(entityView);
	}

	public boolean isCoordinateWithinLayer(Coordinate coord) {
		return isCoordinateWithinLayer(coord.x, coord.y);
	}

	public PotionGenerator getPotionGen() {
		return potionGen;
	}

	public boolean isWall(Coordinate coordinate) {
		return isWall(coordinate.getX(), coordinate.getY());
	}

	public ShadowCaster getShadowCaster() {
		return shadowCaster;
	}
}
