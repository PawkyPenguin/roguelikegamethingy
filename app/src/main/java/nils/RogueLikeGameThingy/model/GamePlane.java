package model;

import entities.Background;
import entities.Coordinate;
import entities.Foreground;
import entities.Player;

public class GamePlane {

	private Game game;
	private Floor currentFloor;
	private final int x = 20;
	private final int y = 21;
	private Player player;

	public GamePlane(Game publisher) {
		currentFloor = new Floor();
		game = publisher;
		currentFloor.setDimensions(x, y);
		currentFloor.setLevel(0);
	}

	public Player getPlayer() {
		return player;
	}

	private void descend() {
		currentFloor.nextLevel();
	}

	public Floor getFloor() {
		return currentFloor;
	}

	public void fillEntityListPreGame() {
		player = new Player(0, 0);
		currentFloor.initializePlayer(player);
		descend();
	}

	public int getLevel() {
		return currentFloor.getLevel();
	}

	public Foreground getForegroundObject(int x, int y) {
		return currentFloor.getForegroundObject(x, y);
	}

	public Background getBackgroundObject(int x, int y) {
		return currentFloor.getBackgroundObject(x, y);
	}

	public boolean isPosFreeForMoving(int x, int y) {
		return currentFloor.isPosFreeForMoving(x, y);
	}

	public void tick(double timeSinceLastFrame) {
		if (game.getPlayer().getIsDescending()) {
			descend();
			return;
		}
		currentFloor.tick(timeSinceLastFrame);
	}

	public Coordinate findRandomEmptyPosition() {
		return currentFloor.findRandomEmptyPosition();
	}

	public void newPlayer() {
		player = new Player(0, 0);
		currentFloor.initializePlayer(player);
	}
}
