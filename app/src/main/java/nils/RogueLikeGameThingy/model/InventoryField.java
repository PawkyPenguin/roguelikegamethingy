package model;

import java.util.ArrayList;

import entities.Living;
import item.Item;
import item.ItemActive;

public abstract class InventoryField {
	private Item item;
	protected ArrayList<Enum> containedItemType;
	protected EnumInventoryFieldType invFieldType;

	public InventoryField() {
		containedItemType = new ArrayList<Enum>();
		setType();
	}

	public void setContainedItemType(ArrayList<Enum> type) {
		containedItemType = type;
	}

	public void useItem(Living owner) {
		((ItemActive) item).use(owner);
		if (getItemAmount() <= 0) {
			item = null;
		}
	}

	public void update() {
		if (item != null && item.getAmount() <= 0) {
			setItem(null);
		}
	}

	protected abstract void setType();

	public boolean isEmpty() {
		return item == null;
	}

	public ArrayList<Enum> getContainedItemType() {
		return containedItemType;
	}

	public boolean isItemCompatible(Item item) {
		if (item == null) {
			return true;
		} else {
			return TypeHandler.areTypesEqual(containedItemType, item.itemType);
		}
	}

	public boolean isStackableWith(Item otherItem) {
		return item != null && otherItem != null && item.isStackableWith(otherItem);
	}

	public String getItemDescription() {
		if (item == null) {
			return "";
		} else {
			return item.getDescription();
		}
	}

	public int getItemAmount() {
		if (item == null) {
			return 0;
		} else {
			return item.getAmount();
		}
	}

	public void setItemAmount(int amount) {
		if (amount <= 0) {
			item = null;
		} else {
			item.setAmount(amount);
		}
	}

	public ArrayList<Enum> getTypeOfContainedItem() {
		if (item == null) {
			return new ArrayList<Enum>();
		} else {
			return item.itemType;
		}
	}

	public ArrayList<Enum> getTypeOfContainedItemForSwap() {
		return getTypeOfContainedItem();
	}

	public EnumInventoryFieldType getInvFieldType() {
		return invFieldType;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	protected boolean isItemSwapPossible(InventoryField field) {
		if (field == this) {
			return false;
		}
		if (getItem() == null) {
			return isItemCompatible(field.getItem());
		}
		return isStackableWith(field.getItemForSwap())
				|| (isItemCompatible(field.getItemForSwap()) && field.isItemCompatible(getItemForSwap()));
	}

	public boolean swapItemsIfPossible(InventoryField field) {
		if (isItemSwapPossible(field) && field.isItemSwapPossible(this)) {
			swapItems(field);
			return true;
		} else {
			return false;
		}
	}

	public Item getItem() {
		return item;
	}

	public Item getItemForSwap() {
		return item;
	}

	public void addItemAmount(int amount) {
		setItemAmount(getItemAmount() + amount);
	}

	public void removeItemAmount(int amount) {
		setItemAmount(getItemAmount() - amount);
		if (getItemAmount() <= 0) {
			item = null;
			doItemRemovalAction();
		}
	}

	protected void stackItems(InventoryField field) {
		Item otherItem = field.getItem();
		if (otherItem == null) {
			return;
		}
		int oldAmount = field.getItemAmount();
		field.addItemAmount(getItemAmount());
		if (field.getItemAmount() > field.getItem().getMaxStack()) {
			field.setItemAmount(field.getItem().getMaxStack());
		}
		removeItemAmount(field.getItemAmount() - oldAmount);
	}

	protected void swapItems(InventoryField field) {
		if (isStackableWith(field.getItem())) {
			stackItems(field);
		} else if (getItem() == null) {
			field.doItemRemovalAction();
			setItem(field.getItem());
			field.setItem(null);
			doItemAdditionAction();
		} else if (field.getItem() == null) {
			doItemRemovalAction();
			field.setItem(getItem());
			setItem(null);
			field.doItemAdditionAction();
		} else if (invFieldType == EnumInventoryFieldType.TRASH ^ field.invFieldType == EnumInventoryFieldType.TRASH) {
			if (getItem() != null && field.getItem() != null) {
				doItemRemovalAction();
				field.doItemRemovalAction();
				Item tempItem = getItem();
				setItem(field.getItemForSwap());
				field.setItem(tempItem);
				doItemAdditionAction();
				field.doItemAdditionAction();
			}
		} else {
			doItemRemovalAction();
			field.doItemRemovalAction();
			Item tempItem = getItem();
			setItem(field.getItem());
			field.setItem(tempItem);
			doItemAdditionAction();
			field.doItemAdditionAction();
		}
	}

	protected void doItemRemovalAction() {
	}

	protected void doItemAdditionAction() {
	}

}
