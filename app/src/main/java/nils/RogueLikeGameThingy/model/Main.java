package model;

import view.SpriteCompendium;
import controller.Controller;

public class Main {
	public static final int screenWidth = 1280;
	public static final int screenHeight = 720;

	public static void main(String[] args) {
		SpriteCompendium sprites = new SpriteCompendium();
		Controller c = new Controller();
		c.startGame();
	}
}
