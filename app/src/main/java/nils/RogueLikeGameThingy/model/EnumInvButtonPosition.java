package model;

public enum EnumInvButtonPosition {
	TOPLEFT, TOP, TOPRIGHT, RIGHT, BOTRIGHT, BOT, BOTLEFT, LEFT, DEFAULT
}
