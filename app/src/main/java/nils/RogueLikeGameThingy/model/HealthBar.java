package model;

import java.util.ArrayList;

import view.HealthBarView;

public class HealthBar {
	private int health;
	private int unbufferedHealth;
	private int maxHealth;
	private int totalMaxBufferHealth;
	private ArrayList<HealthBuffer> bufferList;
	private HealthBarView sub;
	
	public HealthBar(int maxHealth){
		this.maxHealth = maxHealth;
		bufferList = new ArrayList<HealthBuffer>();
	}
	
	public HealthBarView getHealthBarView(){
		return sub;
	}
	
	public void addBuffer(HealthBuffer newBuffer){
		int i = 0;
		int newBufferIndex;
		for(; i < bufferList.size() && (newBuffer.getHealthThreshold() - bufferList.get(i).getHealthThreshold()) > 0; i++);
		if(bufferList.size() == 0){
			bufferList.add(newBuffer);
			newBufferIndex = 0;
		}
		else if(newBuffer.getHealthThreshold() - bufferList.get(i).getHealthThreshold() >= 0){
			bufferList.add(newBuffer);
			newBufferIndex = i + 1;
		}
		else{
			bufferList.add(i, newBuffer);
			newBufferIndex = i;
		}
		newBuffer.setIndex(newBufferIndex);
		totalMaxBufferHealth = getNewTotalMaxBufferHealth();
		heal(newBuffer.getHealth());
	}
	
	public ArrayList<HealthBuffer> getBuffers(){
		return bufferList;
	}
	
	public int getHealth(){
		return health;
	}
	
	public void setHealth(int health){
		this.health = health;
		updateHealth();
	}
	
	public int getTotalMaxHealth(){
		return totalMaxBufferHealth + maxHealth;
	}
	
	public int getMaxHealth(){
		return maxHealth;
	}
	
	public void heal(int heal){
		health += heal;
		if(health > totalMaxBufferHealth + maxHealth){
			health = totalMaxBufferHealth + maxHealth;
		}
		updateHealth();
	}
	
	public void damage(int damage){
		health -= damage;
		updateHealth();
	}
	
	public boolean isHealthToppedOff(){
		if(unbufferedHealth < maxHealth){
			return false;
		}
		for(int i = 0; i < bufferList.size(); i++){
			if(bufferList.get(i).getHealth() < bufferList.get(i).getMaxBufferHealth()){
				return false;
			}
		}
		return true;
	}
	
	public void removeBuffer(int i){
		int overflowHealth = bufferList.get(i).getHealth();
		totalMaxBufferHealth -= bufferList.get(i).getMaxBufferHealth();
		bufferList.remove(i);
		heal(overflowHealth);
	}
	
	public int getTotalBufferHealth(){
		int totalHP = 0;
		for(int i = 0; i < bufferList.size(); i++){
			totalHP += bufferList.get(i).getHealth();
		}
		return totalHP;
	}
	
	private int getNewTotalMaxBufferHealth(){
		int totalHP = 0;
		for(int i = 0; i < bufferList.size(); i++){
			totalHP += bufferList.get(i).getMaxBufferHealth();
		}
		return totalHP;
	}
	
	public int getTotalMaxBufferHealth(){
		return totalMaxBufferHealth;
	}
	
	private void updateHealth(){
		int healthToDistribute = health;
		unbufferedHealth = 0;
		for(HealthBuffer buffer:bufferList){
			buffer.empty();
		}
		for(int i = 0; i < bufferList.size() && healthToDistribute > 0; i++){
			if(unbufferedHealth + healthToDistribute > bufferList.get(i).getHealthThreshold()){
				unbufferedHealth = bufferList.get(i).getHealthThreshold();
				healthToDistribute -= bufferList.get(i).getHealthThreshold();
				if(healthToDistribute - bufferList.get(i).getMaxBufferHealth() > 0){
					bufferList.get(i).fill();					
					healthToDistribute -= bufferList.get(i).getMaxBufferHealth();
				}
				else{
					bufferList.get(i).heal(healthToDistribute);
					healthToDistribute = 0;
					break;
				}
			}
			else{
				break;
			}
		}
		unbufferedHealth += healthToDistribute;
		updateSubscribers();
	}
	
	private void updateSubscribers(){
		if(sub != null){
			sub.publisherUpdated(this);
		}
	}
	
	public void subscribeThis(HealthBarView view){
		sub = view;
		updateSubscribers();
	}

	public void addFullBuffer(int maxHealth, int threshold) {
		HealthBuffer newBuffer = new HealthBuffer(maxHealth, threshold);
		newBuffer.fill();
		addBuffer(newBuffer);
	}
}
