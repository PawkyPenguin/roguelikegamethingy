package model;

import java.util.ArrayList;

import entities.Living;
import item.Item;

public class Inventory {
	private ArrayList<InventoryField> inventory = new ArrayList<InventoryField>();
	private ArrayList<InventoryField> invAuto = new ArrayList<InventoryField>();
	private ArrayList<InventoryField> invCommon = new ArrayList<InventoryField>();
	private ArrayList<InventoryField> invEquipment = new ArrayList<InventoryField>();
	private ArrayList<InventoryField> invLocked = new ArrayList<InventoryField>();
	private ArrayList<InventoryField> invTrash = new ArrayList<InventoryField>();
	private ArrayList<Item> transactionListBought = new ArrayList<Item>();
	private ArrayList<Item> transactionListSold = new ArrayList<Item>();
	private int ownerIndex;
	private HasInventory owner;
	public final static int commonPropertyIndex = -1;
	private boolean allowTransaction;

	public void addInvField(InventoryField field) {
		field.doItemAdditionAction();
		inventory.add(field);
		switch (field.invFieldType) {
		case AUTOUSE:
			invAuto.add(field);
			break;
		case STORAGE:
			invCommon.add(field);
			break;
		case EQUIPMENT:
			invEquipment.add(field);
			break;
		case LOCKED:
			invLocked.add(field);
			break;
		case TRASH:
			invTrash.add(field);
			break;
		}
	}

	public void removeInvField(InventoryField field) {
		field.doItemRemovalAction();
		inventory.remove(field);
		switch (field.invFieldType) {
		case AUTOUSE:
			invAuto.remove(field);
			break;
		case STORAGE:
			invCommon.remove(field);
			break;
		case EQUIPMENT:
			invEquipment.remove(field);
			break;
		case LOCKED:
			invLocked.remove(field);
			break;
		case TRASH:
			invTrash.remove(field);
			break;
		}
	}

	public ArrayList<InventoryField> getAutos() {
		return invAuto;
	}

	public ArrayList<InventoryField> getCommons() {
		return invCommon;
	}

	public ArrayList<InventoryField> getEquips() {
		return invEquipment;
	}

	public ArrayList<InventoryField> getLockeds() {
		return invLocked;
	}

	public ArrayList<InventoryField> getTrashes() {
		return invTrash;
	}

	public boolean isAuto(InventoryField invField) {
		return invAuto.contains(invField);
	}

	public boolean isCommon(InventoryField invField) {
		return invCommon.contains(invField);
	}

	public boolean isEquipment(InventoryField invField) {
		return invEquipment.contains(invField);
	}

	public boolean isLocked(InventoryField invField) {
		return invLocked.contains(invField);
	}

	public boolean isTrash(InventoryField invField) {
		return invTrash.contains(invField);
	}

	public void setOwner(HasInventory liv) {
		owner = liv;
	}

	public void setAllowTransacation(boolean allow) {
		allowTransaction = allow;
	}

	public void setItem(int index, Item item) {
		inventory.get(index).setItem(item);
	}

	public void setOwnerIndex(int index) {
		ownerIndex = index;
	}

	public ArrayList<InventoryField> getInventory() {
		return inventory;
	}

	public int getIndexOfItem(Item item) {
		int neededAmount = item.getAmount();
		if (neededAmount == 0) {
			return 0;
		}
		int amountFound;
		for (InventoryField inv : invAuto) {
			if (Item.haveItemsSameType(inv.getItem(), item)) {
				amountFound = inv.getItemAmount();
				if (amountFound >= neededAmount) {
					return inventory.indexOf(inv);
				}
			}
		}
		for (InventoryField inv : invCommon) {
			if (Item.haveItemsSameType(inv.getItem(), item)) {
				amountFound = inv.getItemAmount();
				if (amountFound >= neededAmount) {
					return inventory.indexOf(inv);
				}
			}
		}
		return -1;
	}

	public int getIndexOfItem(ArrayList<Enum> itemType) {
		for (InventoryField inv : invAuto) {
			if (inv.getItem() != null && TypeHandler.areTypesEqual(inv.getItem().itemType, itemType)) {
				return inventory.indexOf(inv);
			}
		}
		for (InventoryField inv : invCommon) {
			if (inv.getItem() != null && TypeHandler.areTypesEqual(inv.getItem().itemType, itemType)) {
				return inventory.indexOf(inv);
			}
		}
		return -1;
	}

	public Item getItem(int index) {
		return inventory.get(index).getItem();
	}

	public boolean doInvHaveSameOwners(Inventory inv) {
		return ownerIndex == inv.ownerIndex;
	}

	public boolean getAllowTransaction() {
		return allowTransaction;
	}

	public static InventoryField findSuitableField(ArrayList<InventoryField> fields, Item item) {
		for (int i = 0; i < fields.size(); i++) {
			if (fields.get(i).isItemCompatible(item)) {
				return fields.get(i);
			}
		}
		return null;
	}

	public static InventoryField findEmptyField(ArrayList<InventoryField> fields) {
		for (InventoryField field : fields) {
			if (field.isEmpty()) {
				return field;
			}
		}
		return null;
	}

	public static InventoryField findSuitableEmptyField(ArrayList<InventoryField> fields, Item item) {
		for (InventoryField field : fields) {
			if (field.isItemCompatible(item) && field.isEmpty()) {
				return field;
			}
		}
		return null;
	}

	public boolean canLivingIndexAccessThis(int livingIndex) {
		if (ownerIndex == livingIndex) {
			return true;
		}
		if (ownerIndex == commonPropertyIndex) {
			return true;
		}
		return false;
	}

	public boolean isTransactionPossible(Inventory inv) {
		if (inv.getAllowTransaction() && allowTransaction) {
			return true;
		} else {
			return false;
		}
	}

	public void removeItemFromInv(Item item) {
		int amountFound = 0;
		int neededAmount = item.getAmount();
		for (int i = 0; i < inventory.size() && neededAmount > 0; i++) {
			Item foundItem = inventory.get(i).getItem();
			if (Item.haveItemsSameType(foundItem, item)) {
				amountFound = foundItem.getAmount();
				inventory.get(i).removeItemAmount(neededAmount);
				neededAmount -= amountFound;
			}
		}
	}

	public void removeItemFromInv(int i) {
		inventory.get(i).setItem(null);
	}

	public boolean canItemBeStashed(Item item) {
		if (item == null) {
			return true;
		}
		for (int i = 0; i < inventory.size(); i++) {
			if (!TypeHandler.isFieldLocked(inventory.get(i)) && item.isStackableWith(inventory.get(i).getItem())) {
				return true;
			}
		}
		return false;
	}

	public boolean isItemInInventory(Item item) {
		int amountFound = 0;
		int neededAmount = item.getAmount();
		if (neededAmount == 0) {
			return true;
		}
		for (int i = 0; i < inventory.size(); i++) {
			if (Item.haveItemsSameType(inventory.get(i).getItem(), item)) {
				amountFound += inventory.get(i).getItemAmount();
				if (amountFound >= neededAmount) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean doesOwnInvField(InventoryField field) {
		for (int i = 0; i < inventory.size(); i++) {
			if (inventory.get(i) == field) {
				return true;
			}
		}
		return false;
	}

	private void doSwappingProcess(InventoryField pickedUpItemField, InventoryField droppedItemField) {
		Living livOwner = null;
		if (owner instanceof Living) {
			livOwner = (Living) owner;
			pickedUpItemField.swapItemsIfPossible(droppedItemField);
			if (pickedUpItemField.invFieldType == EnumInventoryFieldType.EQUIPMENT
					&& droppedItemField.invFieldType != EnumInventoryFieldType.EQUIPMENT) {
				livOwner.equipEquipment(pickedUpItemField.getItem());
				livOwner.unequipEquipment(droppedItemField.getItem());
			}
			if (pickedUpItemField.invFieldType != EnumInventoryFieldType.EQUIPMENT
					&& droppedItemField.invFieldType == EnumInventoryFieldType.EQUIPMENT) {
				livOwner.unequipEquipment(pickedUpItemField.getItem());
				livOwner.equipEquipment(droppedItemField.getItem());
			}
		}
	}

	public boolean isTradingPossible(Inventory inv) {
		return (!haveSameIndices(inv) && allowTransaction && inv.allowTransaction);
	}

	public boolean haveSameIndices(Inventory inv) {
		return ownerIndex == inv.ownerIndex;
	}

	public void tradeItems(InventoryField field1, InventoryField field2) {
		if (owner instanceof Living) {
			Living ownerliv = (Living) owner;
			InventoryField bought;
			InventoryField sold;
			if (doesOwnInvField(field1)) {
				sold = field1;
				bought = field2;
			} else {
				bought = field1;
				sold = field2;
			}
			int goldAfterPurchase = ownerliv.gold + getTradeBalance(sold.getItem(), false)
					+ getTradeBalance(bought.getItem(), true);
			if (bought.isStackableWith(sold.getItem())) {
				if (sold == field1) {
					goldAfterPurchase -= getTradeBalance(bought.getItem(), true);
				} else {
					goldAfterPurchase -= getTradeBalance(sold.getItem(), false);
				}
			}
			if (goldAfterPurchase >= 0) {
				ownerliv.gold = goldAfterPurchase;
				// updateTransactionList(bought, sold);
				Item oldBought = Item.clone(bought.getItem());
				Item oldSold = Item.clone(sold.getItem());
				doSwappingProcess(field1, field2);
				Item newBought = Item.clone(bought.getItem());
				Item newSold = Item.clone(sold.getItem());
				updateTransactionList(oldBought, oldSold, newBought, newSold);
			}
		}
	}

	private int getTradeBalance(Item item, boolean buying) {
		if (item == null) {
			return 0;
		}
		int transactionBalance = 0;
		boolean transactionComplete = false;
		if (buying) {
			for (int i = 0; i < transactionListSold.size(); i++) {
				if (Item.haveItemsSameType(item, transactionListSold.get(i))) {
					if (transactionListSold.get(i).getAmount() - item.getAmount() < 0) {
						transactionBalance = -transactionListSold.get(i).getAmount() * item.getSellValue()
								- item.getBuyValue() * (item.getAmount() - transactionListSold.get(i).getAmount());
					} else {
						transactionBalance = -item.getAmount() * item.getSellValue();
					}
					transactionComplete = true;
					break;
				}
			}
		} else {
			for (int i = 0; i < transactionListBought.size(); i++) {
				if (Item.haveItemsSameType(item, transactionListBought.get(i))) {
					if (transactionListBought.get(i).getAmount() - item.getAmount() < 0) {
						transactionBalance = transactionListBought.get(i).getAmount() * item.getBuyValue()
								+ item.getSellValue() * (item.getAmount() - transactionListBought.get(i).getAmount());
					} else {
						transactionBalance = item.getAmount() * item.getBuyValue();
					}
					transactionComplete = true;
					break;
				}
			}
		}
		if (!transactionComplete) {
			if (buying) {
				transactionBalance = -item.getAmount() * item.getBuyValue();
			} else {
				transactionBalance = item.getAmount() * item.getSellValue();
			}

		}
		return transactionBalance;
	}

	private void updateTransactionList(Item oldBought, Item oldSold, Item newBought, Item newSold) {
		Item boughtItem = Item.getItemDifference(newSold, oldSold);
		if (boughtItem == null && newSold != null) {
			boughtItem = oldBought.clone();
		}
		Item soldItem = Item.getItemDifference(newBought, oldBought);
		if (soldItem == null && newBought != null) {
			soldItem = oldSold.clone();
		}
		boolean boughtItemAddedToList = false;
		for (int i = 0; i < transactionListSold.size(); i++) {
			if (Item.haveItemsSameType(boughtItem, transactionListSold.get(i))) {
				if (transactionListSold.get(i).getAmount() < boughtItem.getAmount()) {
					int tempTransListAmount = transactionListSold.get(i).getAmount();
					transactionListSold.remove(i);
					Item addedItem = boughtItem;
					addedItem.reduceAmount(tempTransListAmount);
					addToTransactionListBought(addedItem);
				} else {
					transactionListSold.get(i).reduceAmount(boughtItem.getAmount());
				}
				boughtItemAddedToList = true;
				break;
			}
		}
		if (!boughtItemAddedToList && boughtItem != null) {
			addToTransactionListBought(boughtItem);
		}

		boolean soldItemAddedToList = false;
		for (int i = 0; i < transactionListBought.size(); i++) {
			if (Item.haveItemsSameType(soldItem, transactionListBought.get(i))) {
				if (transactionListBought.get(i).getAmount() < soldItem.getAmount()) {
					int tempTransListAmount = transactionListBought.get(i).getAmount();
					transactionListBought.remove(i);
					Item addedItem = soldItem;
					addedItem.reduceAmount(tempTransListAmount);
					addToTransactionListSold(addedItem);
				} else {
					transactionListBought.get(i).reduceAmount(soldItem.getAmount());
				}
				soldItemAddedToList = true;
				break;
			}
		}
		if (!soldItemAddedToList && soldItem != null) {
			addToTransactionListSold(soldItem);
		}
	}

	private void addToTransactionListBought(Item item) {
		boolean itemAdded = false;
		for (int i = 0; i < transactionListBought.size(); i++) {
			if (transactionListBought.get(i).haveItemsSameType(item)) {
				transactionListBought.get(i).increaseAmount(item.getAmount());
				itemAdded = true;
			}
		}
		if (!itemAdded) {
			transactionListBought.add(item);
		}
	}

	private void addToTransactionListSold(Item item) {
		boolean itemAdded = false;
		for (int i = 0; i < transactionListSold.size(); i++) {
			if (transactionListSold.get(i).haveItemsSameType(item)) {
				transactionListSold.get(i).increaseAmount(item.getAmount());
				itemAdded = true;
			}
		}
		if (!itemAdded) {
			transactionListSold.add(item);
		}
	}

	private void printTransactionListStatus() {
		System.out.print("Bought List: ");
		for (int i = 0; i < transactionListBought.size(); i++) {
			System.out.print(transactionListBought.get(i) + " amount: " + transactionListBought.get(i).getAmount()
					+ " [InventoryHandler]    ");
		}
		System.out.println();
		System.out.print("Sold List: ");
		for (int i = 0; i < transactionListSold.size(); i++) {
			System.out.print(transactionListSold.get(i) + " amount: " + transactionListSold.get(i).getAmount()
					+ " [InventoryHandler]    ");
		}
		System.out.println();
	}

	public boolean stashItem(Item item) {
		for (int i = 0; i < inventory.size(); i++) {
			if (inventory.get(i) instanceof InvFieldButton || inventory.get(i) instanceof InvFieldLocked) {
				continue;
			}
			if (inventory.get(i).getItem() != null && item != null && inventory.get(i).isItemCompatible(item)
					&& item.isStackableWith(inventory.get(i).getItem())) {
				inventory.get(i).getItem().addAmount(item.getAmount());
				return true;
			}
		}
		for (int i = 0; i < inventory.size(); i++) {
			if (inventory.get(i) instanceof InvFieldButton || inventory.get(i) instanceof InvFieldLocked) {
				continue;
			}
			if (inventory.get(i).getItem() == null && inventory.get(i).isItemCompatible(item)) {
				inventory.get(i).setItem(item);
				return true;
			}
		}
		return false;
	}

	public HasInventory getOwner() {
		return owner;
	}
}
