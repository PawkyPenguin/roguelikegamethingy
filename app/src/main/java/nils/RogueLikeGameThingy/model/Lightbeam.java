package model;

public class Lightbeam {
	private boolean[] lightTravel;
	private int recursiveCalls = 0;
	
	public Lightbeam(){
		lightTravel = new boolean[4];
	}
	
	public boolean isIlluminating(EnumDirection dir){
		return dir == EnumDirection.LEFT && lightTravel[0] || dir == EnumDirection.UP && lightTravel[1] 
				|| dir == EnumDirection.RIGHT && lightTravel[2] || dir == EnumDirection.DOWN && lightTravel[3];
	}
	
	public void darken(EnumDirection dir){
		switch(dir){
		case LEFT:
			lightTravel[0] = false;
			break;
		case UP:
			lightTravel[1] = false;
			break;
		case RIGHT:
			lightTravel[2] = false;
			break;
		case DOWN:
			lightTravel[3] = false;
			break;
		default:
			throw new IllegalArgumentException("Direction is not cardinal direction");
		}
	}
	
	public void newLightbeam(){
		for(int i = 0; i < lightTravel.length; i++){
			lightTravel[i] = true;
		}
	}
	
	public boolean[] getLight(){
		return lightTravel;
	}
	
	public void addLightbeam(Lightbeam beam){
		boolean[] otherLight = beam.getLight();
		for(int i = 0; i < otherLight.length; i++){
			lightTravel[i] = lightTravel[i] || otherLight[i];
		}
		recursiveCalls++;
	}
	
	public int getRecursiveCalls(){
		return recursiveCalls;
	}
	
}
