package model;

import controller.InputContainer;
import entities.Coordinate;
import entities.Player;

public class Game {
	public boolean running = false;
	// inventoryX coord. 0 is right at the border to the floor.
	public static final int inventoryX = 0;
	public static final int inventoryY = 0;
	private GamePlane gamePlane;

	public Game() {
		resetGameState();
	}

	public void resetGameState() {
		gamePlane = new GamePlane(this);
	}

	public Floor getFloor() {
		return gamePlane.getFloor();
	}

	public Player getPlayer() {
		return gamePlane.getPlayer();
	}

	public void start() {
		running = true;
	}

	public void fillEntityListPreGame() {
		gamePlane.fillEntityListPreGame();
	}

	public Coordinate getRandomEmptyPosition() {
		return gamePlane.findRandomEmptyPosition();
	}

	public void transferInputsToPlayer(InputContainer inputs) {
		getPlayer().setInputs(inputs);
	}

	public void stop() {
		running = false;
	}

	public void tick(float timeSinceLastFrame) {
		gamePlane.tick(timeSinceLastFrame);
	}

	public void setPlayer() {
		gamePlane.newPlayer();
	}
}
