package model;

import java.util.ArrayList;

import entities.Living;
import item.EnumArmorType;
import item.EnumEquipableType;
import item.Item;
import item.ItemEquipable;

public class InventoryHandler {
	private Inventory inventory = new Inventory();
	private HasInventory other;
	private HasInventory owner;

	public InventoryHandler() {
	}

	public InventoryHandler(HasInventory master) {
		setOwner(master);
	}

	public void deleteAllItems() {
		for (InventoryField invField : inventory.getInventory()) {
			invField.setItemAmount(0);
		}
	}

	public void updateItems() {
		if (other != null) {
			for (InventoryField field : other.getInventory().getInventory()) {
				field.update();
			}
		}
		for (InventoryField field : owner.getInventory().getInventory()) {
			field.update();
		}
	}

	public void setOwnerIndex(int index) {
		inventory.setOwnerIndex(index);
	}

	public void setOwner(HasInventory liv) {
		inventory.setOwner(liv);
		inventory.setOwnerIndex(liv.getOwnerIndex());
		owner = liv;
	}

	public Inventory getInventory() {
		return inventory;
	}

	public void addInvField(InventoryField field) {
		inventory.addInvField(field);
	}

	public void setItem(int index, Item item) {
		inventory.setItem(index, item);
	}

	public int getIndexOfItem(ArrayList<Enum> itemType) {
		return inventory.getIndexOfItem(itemType);
	}

	public boolean stashItem(Item item) {
		return inventory.stashItem(item);
	}

	public boolean canItemBeStashed(Item item) {
		return inventory.canItemBeStashed(item);
	}

	public void putInAutouseSlot(InventoryField itemField) {
		boolean swapped = false;
		for (int i = 0; i < inventory.getAutos().size(); i++) {
			if (inventory.getAutos().get(i).isEmpty()) {
				swapped = inventory.getAutos().get(i).swapItemsIfPossible(itemField);
				if (swapped) {
					break;
				}
			}
		}
		if (!swapped) {
			for (int i = 0; i < inventory.getAutos().size(); i++) {
				swapped = inventory.getAutos().get(i).swapItemsIfPossible(itemField);
				if (swapped) {
					break;
				}
			}
		}

	}

	public void putInEquipmentSlot(InventoryField itemField) {
		if (itemField.getItem() == null) {
			throw new IllegalArgumentException("item to be equipped is null");
		}
		ArrayList<Enum> requiredTypeOfField = null;
		Enum typeOfItem = TypeHandler.getEquipableType((ItemEquipable) itemField.getItem());
		if (typeOfItem == EnumEquipableType.WEAPON)
			requiredTypeOfField = TypeHandler.getItemMeleeWeaponType();
		if (typeOfItem == EnumArmorType.HELMET)
			requiredTypeOfField = TypeHandler.getItemArmorHelmetType();
		if (typeOfItem == EnumArmorType.CHEST)
			requiredTypeOfField = TypeHandler.getItemArmorChestType();
		if (typeOfItem == EnumArmorType.LEGGINS)
			requiredTypeOfField = TypeHandler.getItemArmorLegginsType();
		if (typeOfItem == EnumEquipableType.RING)
			requiredTypeOfField = TypeHandler.getItemRingType();
		if (typeOfItem == EnumEquipableType.SHIELD)
			requiredTypeOfField = TypeHandler.getItemShieldType();
		putEquipmentInSuitableSlot(itemField, requiredTypeOfField);
	}

	private void putEquipmentInSuitableSlot(InventoryField itemField, ArrayList<Enum> requiredTypeOfField) {
		ArrayList<InventoryField> slots = inventory.getEquips();
		InventoryField foundSlot = findEmptyFieldWithType(slots, requiredTypeOfField);
		if (foundSlot != null) {
			foundSlot.swapItemsIfPossible(itemField);
		} else {
			foundSlot = findFieldWithType(slots, requiredTypeOfField);
			if (foundSlot != null) {
				foundSlot.swapItemsIfPossible(itemField);
			}
		}
	}

	private InventoryField findEmptyField(ArrayList<InventoryField> fields) {
		for (int i = 0; i < fields.size(); i++) {
			if (fields.get(i).isEmpty()) {
				return fields.get(i);
			}
		}
		return null;
	}

	private InventoryField findEmptyFieldWithType(ArrayList<InventoryField> fields, ArrayList<Enum> type) {
		for (int i = 0; i < fields.size(); i++) {
			if (fields.get(i).isEmpty() && TypeHandler.areTypesEqual(fields.get(i).getContainedItemType(), type)) {
				return fields.get(i);
			}
		}
		return null;
	}

	private InventoryField findFieldWithType(ArrayList<InventoryField> fields, ArrayList<Enum> type) {
		for (int i = 0; i < fields.size(); i++) {
			if (TypeHandler.areTypesEqual(fields.get(i).getContainedItemType(), type)) {
				return fields.get(i);
			}
		}
		return null;
	}

	public boolean isItemInInventory(Item item) {
		return inventory.isItemInInventory(item);
	}

	public void addInventory(ArrayList<InventoryField> inventory) {
		if (owner instanceof Living) {
			updateDescriptionKnowledge(inventory, ((Living) owner).getKnowledge());
		}

		for (int i = 0; i < inventory.size(); i++) {
			this.inventory.addInvField(inventory.get(i));
		}
	}

	public void updateDescriptionKnowledge(ArrayList<InventoryField> inventory, Knowledge knowledge) {
		if (owner instanceof Living) {
			for (InventoryField invField : inventory) {
				Item item = invField.getItem();
				if (item != null)
					item.updateDescriptionKnowledge(knowledge);
			}
		}
	}

	public void removeInventory(ArrayList<InventoryField> inventory) {
		for (int i = 0; i < inventory.size(); i++) {
			this.inventory.removeInvField(inventory.get(i));
		}
	}

	public int getIndexOfItem(Item item) {
		return inventory.getIndexOfItem(item);
	}

	public void removeItemFromInventory(int i) {
		inventory.removeItemFromInv(i);
	}

	public void removeItemFromInventory(Item item) {
		inventory.removeItemFromInv(item);
	}

	public HasInventory getOtherInvOwner() {
		return other;
	}

	public void stopInteracting() {
		other = null;
	}

	public void interactWithOtherInventory(HasInventory other) {
		this.other = other;
	}

	public void updateKnowledge(Knowledge k) {
		if (other != null) {
			updateDescriptionKnowledge(other.getInventory().getInventory(), k);
		}
		updateDescriptionKnowledge(inventory.getInventory(), k);
	}

}
