package shadowcasting;

public class Shadowline {
	private double slope;
	private double y;
	
	public Shadowline(double slope, double y){
		this.slope = slope;
		this.y = y;
	}
	
	public void setSlope(double slope){
		this.slope = slope;
	}
	
	public double getY(){
		return y;
	}
	
	public double getSlope(){
		return slope;
	}
}
