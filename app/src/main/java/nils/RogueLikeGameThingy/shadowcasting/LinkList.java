package shadowcasting;

public class LinkList<E> {
	E element;
	LinkList<E> next;
	LinkList<E> previous;
	
	public LinkList(){}
	
	public LinkList(E firstNode){
		element = firstNode;
	}
	
	public void setElement(E element){
		this.element = element;
	}
	
	public E getElement(){
		return element;
	}
	
	public boolean hasNext(){
		return next != null;
	}
	
	public LinkList<E> getNext(){
		return next;
	}
	
	public LinkList<E> getPrevious(){
		return previous;
	}
	
	public void setPrevious(LinkList<E> previous){
		this.previous = previous;
		if(previous != null)
			previous.next = this;
	}
	
	public void setPrevious(E previous){
		this.previous = new LinkList<E>(previous);
		this.previous.next = this;
	}
	
	public void setNext(LinkList<E> next){
		this.next = next;
		if(next != null)
			next.previous = this;
	}
	
	public void setNext(E next){
		this.next = new LinkList<E>(next);
		this.next.previous = this;
	}
}
