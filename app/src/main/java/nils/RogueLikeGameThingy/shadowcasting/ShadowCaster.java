package shadowcasting;

public class ShadowCaster {
	private double sourceDeltaX;
	private double sourceDeltaY;
	private boolean[][] obstacleMap;
	private final int penumbra = 1;
	private final int light = 2;
	// private static double slopes[][][][];
	private static byte[][][][] lightMaps; // Flyweight, test

	/*
	 * Scans from quadrant 1 to quadrant 4, on fixed quadrant, scans from
	 * columns near source to columns further away, on fixed column, scans from
	 * rows near source to rows further away.
	 */

	public ShadowCaster() {
	}

	private void setSource(double sourceX, double sourceY) {
		sourceDeltaX = -(int) sourceX + sourceX;
		sourceDeltaY = -(int) sourceY + sourceY;
	}

	/*
	 * Called by Floor after generation of new layer. Calculates light map for
	 * whole layer and stores it in lightMaps
	 */
	public void setObstacleMap(boolean[][] obstacleMap) {
		long timeNow = System.currentTimeMillis();
		int dimX = obstacleMap.length;
		int dimY = obstacleMap[0].length;
		this.obstacleMap = obstacleMap;
		lightMaps = new byte[dimX][dimY][dimX][dimY];
		for (int i = 0; i < dimX; i++) {
			for (int j = 0; j < dimY; j++) {
				lightMaps[i][j] = shadowCast(i + 0.5, j + 0.5);
			}
		}
		// int obstacleMapHeight = obstacleMap[0].length;
		// for (int i = 1; i < obstacleMap.length; i++) {
		// assert obstacleMap[i] != null && obstacleMap[i].length ==
		// obstacleMapHeight;
		// }
		// boolean newObstacleMap = false;
		//
		// //FIXME MY EYES
		// if (lastObstacleMap != null) {
		// for (int i = 0; i < lastObstacleMap.length; i++) {
		// for (int j = 0; j < lastObstacleMap[0].length; j++) {
		// if (obstacleMap[i][j] != lastObstacleMap[i][j]) {
		// newObstacleMap = true;
		// break;
		// }
		// }
		// }
		// } else {
		// newObstacleMap = true;
		// }
		// if (lightMaps == null || newObstacleMap) {
		// System.out.println("new map");
		// lightMaps = new
		// byte[obstacleMap.length][obstacleMap[0].length][obstacleMap.length][obstacleMap[0].length];
		// for (byte[][][] i : lightMaps) {
		// for (byte j = 0; j < i.length; j++) {
		// i[j][0][0] = -1;
		// }
		// }
		// }
		// if (slopes == null) {
		// slopes = new
		// double[obstacleMap.length][obstacleMap[0].length][obstacleMap.length][obstacleMap[0].length];
		// for (double[][][] s1 : slopes)
		// for (double[][] s2 : s1)
		// for (double[] s3 : s2)
		// for (int i = 0; i < s3.length; i++)
		// s3[i] = Double.NaN;
		// }
		// "MY EYES" until here can be removed safely, this
		// isnewObstacleMapnewObstacleMap just flyweight stuff.

		this.obstacleMap = obstacleMap;
		// System.out.println("[ShadowCaster] Setting map took: " +
		// (System.currentTimeMillis() - timeNow) + "ms");
	}

	public byte[][] getLightMap(int sourceX, int sourceY) {
		// return shadowCast(sourceX + 0.5, sourceY + 0.5);
		return lightMaps[sourceX][sourceY];
	}

	private byte[][] shadowCast(double sourceX, double sourceY) {
		long startns = System.nanoTime();
		int sourceTileX = (int) sourceX;
		int sourceTileY = (int) sourceY;
		setSource(sourceX, sourceY);
		boolean quadrants[][][] = new boolean[4][][];
		fromWholeToQuadrants(quadrants, obstacleMap, sourceTileX, sourceTileY);
		byte[][] shadowMap = new byte[obstacleMap.length][obstacleMap[0].length];
		LinkList<Shadowline>[] s = new LinkList[4];
		LinkList<Shadowline>[] e = new LinkList[4];
		for (int i = 0; i < 4; i++) { // for each quadrant
			s[i] = new LinkList<Shadowline>(new Shadowline(0, -2));
			e[i] = new LinkList<Shadowline>(new Shadowline(Double.POSITIVE_INFINITY, -1));
		}
		byte[][][] quadrantShadows = new byte[4][][];
		for (int i = 0; i < 4; i++) {
			if (quadrants[i].length != 0) {
				quadrantShadows[i] = castShadowsInQuadrant(quadrants[i], s[i], e[i]);
			} else {
				quadrantShadows[i] = new byte[0][];
			}
		}
		quadrantsToWhole(quadrantShadows, shadowMap, sourceTileX, sourceTileY);
		lightMaps[(int) sourceX][(int) sourceY] = shadowMap;
		// System.out.println("[ShadowCaster] Shadowcasting took: " +
		// (System.nanoTime() - startns) + "ns");
		return shadowMap;
	}

	private void quadrantsToWhole(byte[][][] quadrants, byte[][] whole, int sourceTileX, int sourceTileY) {
		for (int i = 0; i < quadrants[0].length; i++) {
			for (int j = 0; j < quadrants[0][0].length; j++) {
				whole[i + sourceTileX][sourceTileY - j] = quadrants[0][i][j];
			}
		}
		for (int i = 0; i < quadrants[1].length; i++) {
			for (int j = 0; j < quadrants[1][0].length; j++) {
				whole[sourceTileX - i][sourceTileY - j] = quadrants[1][i][j];
			}
		}
		for (int i = 0; i < quadrants[2].length; i++) {
			for (int j = 0; j < quadrants[2][0].length; j++) {
				whole[sourceTileX - i][j + sourceTileY] = quadrants[2][i][j];
			}
		}
		for (int i = 0; i < quadrants[3].length; i++) {
			for (int j = 0; j < quadrants[3][0].length; j++) {
				whole[i + sourceTileX][j + sourceTileY] = quadrants[3][i][j];
			}
		}
	}

	public byte[][] castShadowsInQuadrant(boolean[][] obstacleMap, LinkList<Shadowline> s, LinkList<Shadowline> e) {
		int startX;
		int endX, endPenumbraX;
		boolean drawPenumbraStart, drawPenumbraEnd;
		if (obstacleMap.length == 0 || obstacleMap[0].length == 0) {
			return new byte[0][];
		}
		LinkList<Shadowline> firstS = s;
		LinkList<Shadowline> firstE = e;
		byte[][] shadowMap = new byte[obstacleMap.length][obstacleMap[0].length];
		int shadowsCast = 0;
		for (int j = 0; j < obstacleMap[0].length; j++) {
			e = firstE;
			for (; firstS.getPrevious() != null; firstS = firstS.getPrevious())
				;
			for (s = firstS; s != null; s = s.getNext()) {
				startX = (int) ((j - e.getElement().getY()) / e.getElement().getSlope() + 0.00000001);
				endX = (int) ((j + 1 - s.getElement().getY()) / s.getElement().getSlope() - 0.0000001);
				double newStartSlope = 0;
				double newEndSlope = 0;
				boolean onSolid = false;
				drawPenumbraStart = false;
				drawPenumbraEnd = false;
				for (int i = startX; i <= endX && i < obstacleMap.length; i++) {
					if (i == endX - 1) {
						drawPenumbraEnd = true;
					}
					drawPenumbraStart = i == startX && j <= e.getElement().getY();
					shadowMap[i][j] = light;
					if (obstacleMap[i][j]) {
						if (!onSolid) {
							newStartSlope = getSlopeFromSource(i, j + 1);
							newEndSlope = getSlopeFromSource(i + 1, j);
							boolean[] returnValues = addShadowlines(newStartSlope, newEndSlope, e, s);
							boolean newSeperateShadows = returnValues[0];
							if (newSeperateShadows) {
								e = e.getNext();
							}
							boolean shadowStartMerge = returnValues[1];
							if (shadowStartMerge) {
								endX = (int) ((j + 1 - s.getElement().getY()) / s.getElement().getSlope() - 0.0000001);
							}
							onSolid = true;
						}
						if (i + 1 == obstacleMap.length || i + 1 > endX || onSolid) {
							onSolid = false;
							newEndSlope = getSlopeFromSource(i + 1, j);
							boolean[] returnValues = addShadowlines(newStartSlope, newEndSlope, e, s);
							boolean newSeperateShadows = returnValues[0];
							if (newSeperateShadows) {
								e = e.getNext();
							}
							boolean shadowStartMerge = returnValues[1];
							if (shadowStartMerge) {
								endX = (int) ((j + 1 - s.getElement().getY()) / s.getElement().getSlope() - 0.0000001);
							}
						}
					}
					shadowsCast++;
				}
				e = e.getNext();
			}
		}
		// System.out.println("[ShadowCaster] Shadows cast: " + shadowsCast);
		return shadowMap;
	}

	private boolean mergeStartShadows(double newStartSlope, double newEndSlope, LinkList<Shadowline> s) {
		if (newEndSlope < s.getElement().getSlope()) {
			s.setElement(new Shadowline(newStartSlope, sourceDeltaY - newStartSlope * sourceDeltaX));
			return true;
		}
		return false;
	}

	private boolean mergeEndShadows(double newStartSlope, double newEndSlope, LinkList<Shadowline> e) {
		if (newStartSlope > e.getElement().getSlope() || newStartSlope < 0) {
			e.setElement(new Shadowline(newEndSlope, sourceDeltaY - newEndSlope * sourceDeltaX));
			return !(newStartSlope < 0);
		}
		return false;
	}

	private boolean[] addShadowlines(double newStartSlope, double newEndSlope, LinkList<Shadowline> e,
			LinkList<Shadowline> s) {
		boolean setNewLines = false;
		boolean[] returnValues = new boolean[3];
		returnValues[1] = mergeStartShadows(newStartSlope, newEndSlope, s);
		returnValues[2] = mergeEndShadows(newStartSlope, newEndSlope, e);
		setNewLines = returnValues[1] || returnValues[2];
		if (!setNewLines) {
			LinkList<Shadowline> temp = s.getPrevious();
			s.setPrevious(new Shadowline(newStartSlope, sourceDeltaY - newStartSlope * sourceDeltaX));
			s.getPrevious().setPrevious(temp);
			temp = e.getNext();
			e.setNext(new Shadowline(newEndSlope, sourceDeltaY - newEndSlope * sourceDeltaX));
			e.getNext().setNext(temp);
			e = e.getNext();
		}
		returnValues[0] = !setNewLines;
		return returnValues;
	}

	public static double getSlope(double x1, double y1, double x2, double y2) {
		return (y2 - y1) / (x2 - x1);
	}

	private double getSlopeFromSource(double x2, double y2) {
		// double slope = slopes[(int) sourceDeltaX][(int) sourceDeltaY][(int)
		// x2][(int) y2];
		// if (slope != slope) {
		double slope = (y2 - sourceDeltaY) / (x2 - sourceDeltaX);
		// slopes[(int) sourceDeltaX][(int) sourceDeltaY][(int) x2][(int) y2] =
		// slope;
		return slope;
		// }
		// return slope;
	}

	// FIXME comment formating
	private static void fromWholeToQuadrants(boolean quadrants[][][], boolean[][] obstacleMap, int sourceTileX,
			int sourceTileY) {
		/**
		 * Used for initializing the four quadrants to divide the
		 * {@value #obstacleMap}.
		 * 
		 * @param x
		 *            The sourceTileX ordinate of the eye
		 * @param y
		 *            The sourceTileY ordinate of the eye
		 * @param obstacleMap
		 *            The obstacleMap contains the grid on which shadowcasting
		 *            is performed on. true indicates an opaque object, false
		 *            indicates a transparent one.
		 */
		quadrants[0] = new boolean[obstacleMap.length - sourceTileX][sourceTileY + 1];
		for (int i = sourceTileX; i < obstacleMap.length; i++) {
			for (int j = 0; j <= sourceTileY; j++) {
				quadrants[0][i - sourceTileX][sourceTileY - j] = obstacleMap[i][j];
			}
		}
		quadrants[1] = new boolean[sourceTileX + 1][sourceTileY + 1];
		for (int i = 0; i <= sourceTileX; i++) {
			for (int j = 0; j <= sourceTileY; j++) {
				quadrants[1][sourceTileX - i][sourceTileY - j] = obstacleMap[i][j];
			}
		}
		quadrants[2] = new boolean[sourceTileX + 1][obstacleMap[0].length - sourceTileY];
		for (int i = 0; i <= sourceTileX; i++) {
			for (int j = sourceTileY; j < obstacleMap[0].length; j++) {
				quadrants[2][sourceTileX - i][j - sourceTileY] = obstacleMap[i][j];
			}
		}
		quadrants[3] = new boolean[obstacleMap.length - sourceTileX][obstacleMap[0].length - sourceTileY];
		for (int i = sourceTileX; i < obstacleMap.length; i++) {
			for (int j = sourceTileY; j < obstacleMap[0].length; j++) {
				quadrants[3][i - sourceTileX][j - sourceTileY] = obstacleMap[i][j];
			}
		}
	}
}
