package plants;

import entities.Background;
import entities.BackgroundInteractable;
import entities.Living;

import item.Item;

import model.Floor;

public abstract class Plant extends BackgroundInteractable {
	int age = 0;
	boolean pluckable = false;
	boolean fragile = false;
	Floor floor;
	Background master;
	Item loot;

	public Plant(Background b, Floor f) {
		master = b;
		floor = f;
	}

	protected void grow() {
		age++;
		growAction();
	}

	protected abstract void growAction();

	protected void pluck(Living l) {
		if (pluckable) {
			l.stashItem(loot);
			master.removeInteractable();
			doPluckAction();
		}
	}

	private void destroy() {
		master.removeInteractable();
		doDestroyedAction();
	}

	protected abstract void doDestroyedAction();

	protected abstract void doPluckAction();

	protected void stepOn(Living l) {
		if (fragile) {
			destroy();
		}
	}
}
