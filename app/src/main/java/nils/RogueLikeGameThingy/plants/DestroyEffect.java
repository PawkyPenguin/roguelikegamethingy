package plants;

import entities.Living;

public abstract class DestroyEffect{
	
	public abstract boolean trigger(Living liv);
	
}	
