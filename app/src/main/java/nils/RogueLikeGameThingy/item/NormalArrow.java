package item;

import java.util.ArrayList;

import model.TypeHandler;
import view.SpriteCompendium;
import entities.Statuseffect;

public class NormalArrow extends Arrow {

	public NormalArrow() {
		super();
		addAnimation(SpriteCompendium.getItemSprites().get(0), "arrow");
		setAnimation("arrow");
		addAnimation(SpriteCompendium.getItemSprites().get(1), "up");
		addAnimation(SpriteCompendium.getItemSprites().get(2), "left");
		addAnimation(SpriteCompendium.getItemSprites().get(3), "down");
		addAnimation(SpriteCompendium.getItemSprites().get(4), "right");
	}

	@Override
	protected void setDescription() {
		setDescription("An ordinary arrow");
	}

	@Override
	protected void setStatusType() {
		inflictedStatusList = new ArrayList<Statuseffect>();
	}

	@Override
	protected void setType() {
		itemType = TypeHandler.getItemNormalArrowType();
	}

	@Override
	protected void setBuySellValue() {
		setBuySellValue(5, 2);
	}
}
