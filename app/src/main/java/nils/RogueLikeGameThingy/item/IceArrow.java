package item;

import entities.StatuseffectFreeze;
import model.TypeHandler;
import view.SpriteCompendium;

public class IceArrow extends Arrow {

	public IceArrow() {
		super();
		addAnimation(SpriteCompendium.getItemSprites().get(5), "arrow");
		setAnimation("arrow");
		addAnimation(SpriteCompendium.getItemSprites().get(6), "up");
		addAnimation(SpriteCompendium.getItemSprites().get(7), "left");
		addAnimation(SpriteCompendium.getItemSprites().get(8), "down");
		addAnimation(SpriteCompendium.getItemSprites().get(9), "right");
	}

	@Override
	protected void setStatusType() {
		setInflictedStatuseffect(new StatuseffectFreeze());
	}

	@Override
	protected void setType() {
		itemType = TypeHandler.getItemIceArrowType();
	}

	@Override
	protected void setDescription() {
		setDescription("An icy arrow that can freeze enemies");
	}

	@Override
	protected void setBuySellValue() {
		setBuySellValue(10, 5);
	}
}
