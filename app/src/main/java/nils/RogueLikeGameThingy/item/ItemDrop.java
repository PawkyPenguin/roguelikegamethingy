package item;

import entities.Entity;
import model.Floor;

public class ItemDrop extends Entity {
	private Item item;

	public ItemDrop(Item item, int x, int y) {
		super(x, y);
		solid = false;
		this.item = item;
		setSprites();
	}

	@Override
	public int getDepth() {
		return 2;
	}

	@Override
	public void setSprites() {
		setIdleAnimation(item.getAnimation());
	}

	@Override
	public void placeOnFloor(Floor floor) {
		floor.setObject(this);
	}

	@Override
	protected void setWalkingCost() {
		walkingCost = 0;
	}

	@Override
	public void removalAction() {
	}

	public Item getItem() {
		return item;
	}

	public void pickUp() {
		floor.deleteObject(x, y, getDepth());
	}
}
