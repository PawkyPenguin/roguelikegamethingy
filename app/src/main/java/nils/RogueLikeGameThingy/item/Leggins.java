package item;

import java.io.IOException;

import javax.imageio.ImageIO;

import model.TypeHandler;
import view.Animation;
import view.SpriteCompendium;

public class Leggins extends ArmorPiece{

	@Override
	protected void setBuySellValue() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setType() {
		itemType = TypeHandler.getItemArmorLegginsType();
	}
	
	@Override
	protected void loadSprite(EnumMaterialType materialType){
		switch(materialType){
		case GOLD:
			addAnimation(SpriteCompendium.getItemSprites().get(18), "gold");
			setAnimation("gold");
			break;
		case SILVER:
			addAnimation(SpriteCompendium.getItemSprites().get(17), "silver");
			setAnimation("silver");
			break;
		case BRONZE:
			addAnimation(SpriteCompendium.getItemSprites().get(16), "bronze");
			setAnimation("bronze");
			break;
		}
	}
	
}
