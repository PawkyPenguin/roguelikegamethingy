package item;

import entities.Living;
import entities.Statuseffect;
import model.Stat;

public abstract class Weapon extends ItemStatinflicter {
	private int damage;

	public Weapon() {
		super();
		setStatusType();
	}

	@Override
	public void equipTo(Living liv) {
		liv.getWeaponList().add(this);
		liv.raiseStats(raisedStats);
		addToWeaponEffectList(liv);
		//		liv.getStatuseffectList().addAll(inflictedStatusList);
	}

	public void addToWeaponEffectList(Living liv) {
		for (Statuseffect eff : inflictedStatusList) {
			liv.addToWeaponEffectList(eff);
		}
	}

	@Override
	public void unequipFrom(Living liv) {
		liv.getWeaponList().remove(this);
		liv.updateWeaponEffectList();
		liv.lowerStats(raisedStats);
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public void setAttack(int attack) {
		if (raisedStats == null) {
			raisedStats = new Stat();
		}
		raisedStats.setAttack(attack);
		setDescription();
	}
}
