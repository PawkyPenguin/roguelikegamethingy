package item;

import model.TypeHandler;

public class Pickaxe extends MeleeWeapon {

	public Pickaxe() {
		weaponType = EnumMeleeWeaponType.PICKAXE;
	}

	@Override
	protected void setType() {
		itemType = TypeHandler.getItemPickaxeType();
	}

	@Override
	public void setDescription() {
		super.setDescription();
		addDescription("\nCan break walls!");
	}

}
