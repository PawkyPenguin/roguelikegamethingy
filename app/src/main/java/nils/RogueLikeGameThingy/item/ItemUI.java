package item;

import java.awt.image.BufferedImage;

import view.Animation;
import view.AnimationSet;

public class ItemUI {
	private AnimationSet animationSet;
	private String description;

	public ItemUI() {
		animationSet = new AnimationSet();
	}

	public void addAnimation(Animation animation, String name) {
		animationSet.addAnimation(animation, name);
	}

	public void setAnimation(String name) {
		animationSet.selectAnimation(name);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public BufferedImage getCurrentSprite() {
		return animationSet.getCurrentFrame();
	}

	public Animation getAnimation() {
		return animationSet.getCurrentAnimation();
	}

	public Animation getAnimation(String name) {
		return animationSet.getAnimation(name);
	}

	public void addDescription(String description) {
		this.description += description;
	}
}
