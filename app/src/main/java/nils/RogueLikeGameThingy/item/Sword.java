package item;

import model.TypeHandler;

public class Sword extends MeleeWeapon {
	public Sword() {
		weaponType = EnumMeleeWeaponType.SWORD;
	}

	@Override
	protected void setType() {
		itemType = TypeHandler.getItemSwordType();
	}
}
