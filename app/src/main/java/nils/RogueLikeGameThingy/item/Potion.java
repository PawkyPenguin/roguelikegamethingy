package item;

import java.util.ArrayList;

import entities.Living;
import entities.Statuseffect;
import item.potions.PotionAttribute;
import model.TypeHandler;
import model.Knowledge;
import view.SpriteCompendium;

public class Potion extends ItemActive {
	PotionAttribute potionAttribute;

	public Potion(PotionAttribute potionAttribute){
		super();
		setStackable(true);
		setMaxStack(99);
		setDescription();
		this.potionAttribute = potionAttribute;
		String animationName = getAnimationName();
		itemType.add(potionAttribute.getColor());
		addAnimation(SpriteCompendium.getPotionSprites().getAnimation(animationName), animationName);
		setAnimation(animationName);
	}

	public Potion(ArrayList<Statuseffect> effects, EnumPotionColor color) {
		super();
		setStackable(true);
		setMaxStack(99);
		potionAttribute = new PotionAttribute();
		potionAttribute.setEffects(effects);
		potionAttribute.setColor(color);
		String animationName = getAnimationName();
		itemType.add(potionAttribute.getColor());
		addAnimation(SpriteCompendium.getPotionSprites().getAnimation(animationName), animationName);
		setAnimation(animationName);
	}

	@Override
	public void setDescription() {
		setDescription(potionAttribute.generateDescription());
	}

	@Override
	public boolean use(Living e) {
		if (!e.hasFinishedTurn()) {
			reduceAmount(1);
			for (Statuseffect eff : potionAttribute.getAppliedEffects()) {
				eff.addEffect(e);
			}
			e.setFinishedTurn(true);
			e.addKnowledge(this);
			return true;
		}
		return false;
	}

	@Override
	protected void setBuySellValue() {
		setBuySellValue(20, 10);
	}

	@Override
	protected void setType() {
		itemType = TypeHandler.getItemHealthPotType();
	}

	public void setEffects(ArrayList<Statuseffect> effs) {
		for (Statuseffect eff : effs) {
			potionAttribute.addEffect(eff);
		}
	}

	public String getAnimationName() {
		return potionAttribute.getColor().toString();
	}

	@Override
	public void newKnowledge(Knowledge k) {
		if (k.hasKnowledgeAbout(this)) {
			setDescription(potionAttribute.effectsToDescriptionString());
		}

	}

}
