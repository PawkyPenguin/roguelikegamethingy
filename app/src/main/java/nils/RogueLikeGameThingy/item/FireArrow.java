package item;

import entities.StatuseffectBurn;
import entities.StatuseffectBurnFriendly;
import model.TypeHandler;
import view.SpriteCompendium;
	
public class FireArrow extends Arrow{
	public FireArrow(){
		super();
		addAnimation(SpriteCompendium.getItemSprites().get(27), "arrow");
		setAnimation("arrow");
		addAnimation(SpriteCompendium.getItemSprites().get(28), "up");
		addAnimation(SpriteCompendium.getItemSprites().get(29), "left");
		addAnimation(SpriteCompendium.getItemSprites().get(30), "down");
		addAnimation(SpriteCompendium.getItemSprites().get(31), "right");
	}

	@Override
	protected void setStatusType() {
		setInflictedStatuseffect(new StatuseffectBurnFriendly());
	}

	@Override
	protected void setDescription() {
		setDescription("A burning arrow for charring enemies.");
	}

	@Override
	protected void setBuySellValue() {
		setBuySellValue(10, 5);
	}

	@Override
	protected void setType() {
		itemType = TypeHandler.getItemFireArrowType();
	}
}