package item;

import java.util.ArrayList;

import entities.Statuseffect;
import view.SpriteCompendium;

public abstract class MeleeWeapon extends Weapon {
	EnumMeleeWeaponType weaponType;
	EnumMaterialType materialType;

	public MeleeWeapon() {
		super();
		setStackable(false);
	}

	public void setMaterialType(EnumMaterialType materialType) {
		this.materialType = materialType;
		loadSprite(materialType);
	}

	public void loadSprite(EnumMaterialType materialType) {
		switch (weaponType) {
		case SWORD:
			switch (materialType) {
			case GOLD:
				addAnimation(SpriteCompendium.getItemSprites().get(19), "sword1");
				setAnimation("sword1");
				break;
			case SILVER:
				addAnimation(SpriteCompendium.getItemSprites().get(20), "sword2");
				setAnimation("sword2");
				break;
			case BRONZE:
				addAnimation(SpriteCompendium.getItemSprites().get(21), "sword3");
				setAnimation("sword3");
				break;
			}
			break;
		case PICKAXE:
			switch (materialType) {
			case SILVER:
				addAnimation(SpriteCompendium.getItemSprites().get(33), "pickaxe");
				setAnimation("pickaxe");
				break;
			}
		}
	}

	@Override
	protected void setStatusType() {
		inflictedStatusList = new ArrayList<Statuseffect>();
	}

	@Override
	protected void setDescription() {
		if (raisedStats != null) {
			setDescription("Increases attack stat by " + raisedStats.getAttack());
		} else {
			setDescription("A broken weapon");
		}
	}

	@Override
	protected void setBuySellValue() {
		if (materialType == EnumMaterialType.BRONZE) {
			setBuySellValue(0, (int) (10 + raisedStats.getAttack() * 0.75));
		} else if (materialType == EnumMaterialType.SILVER) {
			setBuySellValue(0, (int) (30 + raisedStats.getAttack() * 0.75));
		}
	}
}
