package item;

import model.TypeHandler;
import view.SpriteCompendium;

public class Helmet extends ArmorPiece {

	@Override
	protected void setBuySellValue() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setType() {
		itemType = TypeHandler.getItemArmorHelmetType();
	}

	@Override
	protected void loadSprite(EnumMaterialType materialType) {
		switch (materialType) {
		case GOLD:
			addAnimation(SpriteCompendium.getItemSprites().get(15), "gold");
			setAnimation("gold");
			break;
		case SILVER:
			addAnimation(SpriteCompendium.getItemSprites().get(14), "silver");
			setAnimation("silver");
			break;
		case BRONZE:
			addAnimation(SpriteCompendium.getItemSprites().get(13), "bronze");
			setAnimation("bronze");
			break;
		}

	}

}
