package item.potions;
import java.util.ArrayList;
import entities.Statuseffect;

import item.EnumPotionColor;

public class PotionAttribute {
	private	EnumPotionColor color;
	private ArrayList<Statuseffect> appliedEffects;

	public PotionAttribute(){
		appliedEffects = new ArrayList<Statuseffect>();
	}

	public PotionAttribute(ArrayList<Statuseffect> a) {
		appliedEffects = a;
	}

	public void setColor(EnumPotionColor color) {
		this.color = color;
	}

	public EnumPotionColor getColor() {
		return color;
	}

	public void addEffect(Statuseffect effect) {
		appliedEffects.add(effect);
	}

	public void setEffects(ArrayList<Statuseffect> effects) {
		appliedEffects = effects;
	}

	public ArrayList<Statuseffect> getAppliedEffects() {
		return appliedEffects;
	}

	public String effectsToDescriptionString() {
		if (appliedEffects.size() == 0) {
			return "";
		}
		String description = "";
		description += appliedEffects.get(0).getPrefixDescriptionOfEffect();
		for (int i = 1; i + 1 < appliedEffects.size(); i++) {
			description += ", ";
			description += appliedEffects.get(i).getInfixDescriptionOfEffect();
		}
		if (appliedEffects.size() > 1) {
			description += " and ";
			description += appliedEffects.get(appliedEffects.size() - 1).getInfixDescriptionOfEffect();
		}
		description += ".";
		return description;
	}

	public static String generateDescription() {
                 String[] possibleDescriptions = { "The potion glows ominously.", "Who knows what this potion does?",
                                 "The vial is filled with a strange liquid.", "A rather peculiar fluid bubbles inside."    ,
                                 "When shaken, its content seems to shift colors.", "You can see wafts of mist inside the bottle" };
                 return possibleDescriptions[(int) (Math.random() * possibleDescriptions.length)];
	}
}
