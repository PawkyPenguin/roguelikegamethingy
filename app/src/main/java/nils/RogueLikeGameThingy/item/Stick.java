package item;

import model.TypeHandler;
import view.SpriteCompendium;

public class Stick extends MeleeWeapon {
	public Stick() {
		super();
		setStackable(false);
		setAttack(2);
		setAnimation(SpriteCompendium.getItemSprites().get(34), "stick");
	}

	@Override
	protected void setBuySellValue() {
		setBuySellValue(1, 1);
	}

	@Override
	protected void setDescription() {
		String[] levelsOfAwesomeness = { "mighty", "incredible", "marvellous", "awesome", "terrific", "frightening",
				"strong", "pointy", "mind-blowing" };
		int awesomeAdjective = (int) (Math.random() * levelsOfAwesomeness.length);
		setDescription("A particularly " + levelsOfAwesomeness[awesomeAdjective] + " stick");
	}

	@Override
	protected void setType() {
		itemType = TypeHandler.getItemMeleeWeaponType();
	}
}
