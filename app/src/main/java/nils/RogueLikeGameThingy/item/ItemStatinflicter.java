package item;

import java.util.ArrayList;

import entities.Statuseffect;

public abstract class ItemStatinflicter extends ItemEquipable {
	protected ArrayList<Statuseffect> inflictedStatusList = new ArrayList<Statuseffect>();

	public ItemStatinflicter() {
		setStatusType();
	}

	protected abstract void setStatusType();

	public ArrayList<Statuseffect> getInflictedStatusList() {
		ArrayList<Statuseffect> cloned = new ArrayList<Statuseffect>();
		for (int i = 0; i < inflictedStatusList.size(); i++) {
			Class<? extends Statuseffect> type = (Class<? extends Statuseffect>) inflictedStatusList.get(i)
					.getClass();
			Statuseffect clone = null;
			try {
				clone = type.newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			((Statuseffect) clone).setLevel(inflictedStatusList.get(i).getLevel());
			cloned.add(clone);
		}
		return cloned;
	}

	public void setInflictedStatuseffect(ArrayList<Statuseffect> status) {
		inflictedStatusList.clear();
		if (status != null) {
			inflictedStatusList.addAll(status);
		}
	}

	public void setInflictedStatuseffect(Statuseffect status) {
		inflictedStatusList.clear();
		if (status != null) {
			inflictedStatusList.add(status);
		}
	}

	public void addInflictedStatuseffect(Statuseffect status) {
		if (status != null) {
			inflictedStatusList.add(status);
		}
	}
}
