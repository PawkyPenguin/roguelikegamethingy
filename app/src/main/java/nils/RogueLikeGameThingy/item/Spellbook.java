package item;

import view.Animation;
import view.SpriteCompendium;
import model.EnumDirection;
import entities.Attack;
import entities.AttackFactory;
import entities.AttackLogic;
import entities.Coordinate;
import entities.Living;

public class Spellbook extends ItemActive{

	public Spellbook(){
		addAnimation(SpriteCompendium.getItemSprites().get(32), "spellbook");
		setAnimation("spellbook");
	}
	
	@Override
	public boolean use(Living e) {
		//XXX
		if(e.hasAttackedThisTurn()){
			return false;
		}
		Attack attack = AttackFactory.createTranscendentAttack(e, 30);
		Coordinate coord = e.getCoordinate();
		EnumDirection dir = e.getDirection().direction;
		Animation animation = SpriteCompendium.getAttackSprites().getAnimation("darkSphere");
		AttackLogic logic = AttackFactory.createTravelAttack(attack, animation, 8, coord, dir, 20);
		//XXX e.setFinishedTurn should be moved to Living
		e.setFinishedTurn(true);
		e.launchAttack(logic);
		return true;
	}

	@Override
	protected void setBuySellValue() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void setDescription() {
		setDescription("It is said to contain dark magic.");
	}

	@Override
	protected void setType() {
		itemType.add(EnumItemType.ITEMACTIVE);
	}

}
