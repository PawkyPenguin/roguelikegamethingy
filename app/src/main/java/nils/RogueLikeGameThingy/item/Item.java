package item;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import model.Knowledge;
import model.TypeHandler;
import view.Animation;
import model.Knowledgeable;

public abstract class Item implements Cloneable, Knowledgeable {
	private int buyValue;
	private int sellValue;
	private int amount;
	private boolean stackable;
	private int maxStack;
	private ItemUI ui;
	public ArrayList<Enum> itemType = new ArrayList<Enum>();

	public Item() {
		amount = 1;
		ui = new ItemUI();
		setType();
		setDescription();
		setBuySellValue();
	}

	@Override
	public String toString() {
		return getDescription();
	}

	protected abstract void setBuySellValue();

	public Item(int amount) {
		this.amount = amount;
		ui = new ItemUI();
		setType();
		setDescription();
	}

	public static Item clone(Item prototype) {
		if (prototype == null) {
			return null;
		} else {
			return prototype.clone();
		}
	}

	// TODO might have to adjust sell and buy values of the clone too
	@Override
	public Item clone() {
		Item clone = null;
		try {
			clone = (Item) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			System.exit(1);
		}
		clone.itemType = new ArrayList<Enum>();
		clone.itemType.addAll(itemType);
		clone.setBuySellValue(buyValue, sellValue);
		clone.setAmount(amount);
		return clone;
	}

	public static Item getItemDifference(Item item1, Item item2) {
		if (item1 != null) {
			Item diff = item1.clone();
			if (Item.haveItemsSameType(item1, item2)) {
				diff.reduceAmount(item2.getAmount());
				return diff;
			}
		}
		return null;
	}

	public void setStackable(boolean stackable) {
		this.stackable = stackable;
	}

	public void setAnimation(String name) {
		ui.setAnimation(name);
	}

	public void setAnimation(Animation animation, String name) {
		ui.addAnimation(animation, name);
		ui.setAnimation(name);
	}

	public void addAnimation(Animation animation, String name) {
		ui.addAnimation(animation, name);
	}

	public Animation getAnimation(String name) {
		return ui.getAnimation(name);
	}

	protected abstract void setDescription();

	public void setDescription(String description) {
		ui.setDescription(description);
	}

	public void addDescription(String description) {
		ui.addDescription(description);
	}

	public String getDescription() {
		return ui.getDescription();
	}

	public BufferedImage getSprite() {
		return ui.getCurrentSprite();
	}

	public boolean isStackable() {
		return stackable;
	}

	public void setMaxStack(int max) {
		maxStack = max;
	}

	public int getBuyValue() {
		return buyValue;
	}

	public int getSellValue() {
		return sellValue;
	}

	public void setBuySellValue(int buy, int sell) {
		buyValue = buy;
		sellValue = sell;
	}

	public int getMaxStack() {
		return maxStack;
	}

	public static boolean haveItemsSameType(Item item1, Item item2) {
		if (item1 == null && item2 == null) {
			return true;
		} else if (item1 == null || item2 == null) {
			return false;
		}
		return TypeHandler.areTypesExactlyEqual(item1.itemType, item2.itemType);
	}

	public boolean isStackableAndNotNull(Item item) {
		if (item == null) {
			return false;
		} else {
			return (haveItemsSameType(this, item) && item.isStackable() && isStackable() && amount < maxStack);
		}
	}

	public boolean isStackableWith(Item item) {
		if (item == null
				|| (haveItemsSameType(this, item) && item.isStackable() && isStackable() && amount < maxStack)) {
			return true;
		}
		return false;
	}

	protected abstract void setType();

	public boolean haveItemsSameType(Item item) {
		return haveItemsSameType(this, item);
	}

	public void addAmount(int amount) {
		this.amount += amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}

	public void reduceAmount(int amount) {
		this.amount -= amount;
	}

	public void increaseAmount(int amount) {
		this.amount += amount;
	}

	public void updateDescriptionKnowledge(Knowledge knowledge) {
	}

	public Animation getAnimation() {
		return ui.getAnimation();
	}

	public void newKnowledge(Knowledge k) {
	}
}
