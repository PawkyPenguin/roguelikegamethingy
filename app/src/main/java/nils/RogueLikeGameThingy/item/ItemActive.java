package item;

import entities.Living;

public abstract class ItemActive extends Item{
	
	abstract public boolean use(Living e);
}
