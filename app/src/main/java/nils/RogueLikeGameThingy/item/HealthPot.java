package item;

import entities.Living;
import model.TypeHandler;
import view.Animation;
import view.SpriteCompendium;

//TODO probably should have the class be together with some things like mana pot or similar
public class HealthPot extends ItemActive{	
	public int amountOfHealing;
	
	public HealthPot(){
		setMaxStack(99);
		setAmountOfHealing(30);
		setBuySellValue();
		setStackable(true);
		addAnimation(SpriteCompendium.getItemSprites().get(26), "healthpot");
		setAnimation("healthpot");
	}
	
	public void setAmountOfHealing(int healing){
		amountOfHealing = healing;
		setDescription();
	}
	
	@Override
	public boolean use(Living e) {
		if(!e.isHealthToppedOff() && !e.hasFinishedTurn()){
			reduceAmount(1);
			e.heal(amountOfHealing);
			e.setFinishedTurn(true);
			return true;
		}
		else{
			return false;
		}
	}

	@Override
	protected void setType() {
		itemType = TypeHandler.getItemHealthPotType();
	}

	@Override
	protected void setDescription() {
		setDescription("Heals " + amountOfHealing + " health points");
	}

	@Override
	protected void setBuySellValue() {
		setBuySellValue(20, 10);
	}
	
}
