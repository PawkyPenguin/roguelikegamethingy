package item;

import entities.Attack;
import entities.AttackFactory;
import entities.AttackLogic;
import entities.Living;
import entities.Player;
import model.Direction;
import model.EnumDirection;
import model.TypeHandler;
import view.Animation;
import view.SpriteCompendium;

public class Bow extends ItemActive {

	boolean isLoaded;
	int damage = 30;
	final private int arrowSpeed = 25;

	public Bow() {
		addAnimation(SpriteCompendium.getItemSprites().get(25), "bow");
		setAnimation("bow");
	}

	public void setDamage(int damage) {
		this.damage = damage;
		setDescription();
		setBuySellValue();
	}

	@Override
	protected void setDescription() {
		setDescription("Shoots arrows dealing " + damage + " damage");
	}

	@Override
	public boolean use(Living owner) {
		// XXX
		if (owner.hasAttackedThisTurn()) {
			return false;
		}
		int indexOfArrow = ((Player) owner).getIndexOfItem(TypeHandler.getItemArrowType());
		if (indexOfArrow != -1 && (owner.isFacingAttackableObject() || owner.isFacingBackgroundObject())
				&& owner.getDirection().direction != EnumDirection.NULL) {
			Arrow usedArrow = (Arrow) ((Player) owner).getInventory().getItem(indexOfArrow).clone();
			usedArrow.setAmount(1);
			((Player) owner).removeItemFromInventory(usedArrow);
			// XXX Bow should not have that much control over player.
			// Move this statement to player.move instead.
			owner.setFinishedTurn(true);
			shootArrow(usedArrow, owner, owner.getDirection());
			return true;
		} else {
			return false;
		}
	}

	public void shootArrow(Arrow usedArrow, Living owner, Direction direction) {
		Animation attackSprites = usedArrow.getArrowAnimation(owner.getDirection().direction);
		Attack arrowAttack = AttackFactory.createNormalAttack(owner, damage);
		arrowAttack.setAppliedStatusEffects(usedArrow.getInflictedStatusList());
		AttackLogic attackLogic = AttackFactory.createTravelAttack(arrowAttack, attackSprites, arrowSpeed,
				owner.getCoordinate(), direction.direction, 20);
		owner.launchAttack(attackLogic);
	}

	@Override
	protected void setType() {
		itemType.add(EnumItemType.ITEMACTIVE);
	}

	@Override
	protected void setBuySellValue() {
		setBuySellValue(30, damage);
	}

}
