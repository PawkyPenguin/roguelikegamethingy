package item;

import java.util.ArrayList;

import entities.Statuseffect;
import entities.StatuseffectBurn;
import entities.StatuseffectFreeze;
import entities.StatuseffectRegeneration;
import item.potions.PotionAttribute;
import lazyness.Chance;

// Pre-generate potions. Currently, the generation is done once, i.e. at the beginning of the first floor, potions colorlist and effectlist is filled. This is because the player should know which potions belong to which effect.
public class PotionGenerator {
	// This seems completely stupid, there's most likely a better way but it's 2am and I'm too lazy to google.
	ArrayList<EnumPotionColor> colors = new ArrayList<EnumPotionColor>();
	ArrayList<ArrayList<Statuseffect>> availableEffects = new ArrayList<ArrayList<Statuseffect>>();
	
	public PotionGenerator(){
		colors.add(EnumPotionColor.PINK);
		colors.add(EnumPotionColor.GREEN);
		colors.add(EnumPotionColor.LIME);
		colors.add(EnumPotionColor.ORANGE);
		colors.add(EnumPotionColor.RED);
		for(int i = 0; i < colors.size(); i++){
			availableEffects.add(generatePotionEffect());
		}
	}

	public PotionAttribute getRandomPotionAttribute() {
		PotionAttribute potionAttribute = new PotionAttribute();
		int i = (int) (Math.random() * colors.size());
		potionAttribute.setColor(colors.get(i));
		potionAttribute.setEffects(availableEffects.get(i));
		return potionAttribute;
	}
	
	public ArrayList<Statuseffect> generatePotionEffect(){
		ArrayList<Statuseffect> appliedEffects = new ArrayList<Statuseffect>();
		if(Chance.oneFourth()){
			appliedEffects.add(new StatuseffectFreeze());
		}
		else if(Chance.oneFourth()){
			appliedEffects.add(new StatuseffectBurn());
		}
		else if(Chance.oneFourth()){
			appliedEffects.add(new StatuseffectBurn());
			appliedEffects.add(new StatuseffectFreeze());
		}
		else{
			appliedEffects.add(new StatuseffectRegeneration());
		}
		return appliedEffects;
	}
	
}
