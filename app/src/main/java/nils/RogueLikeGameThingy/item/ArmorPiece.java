package item;

import model.Stat;

public abstract class ArmorPiece extends ItemEquipable{
	EnumMaterialType materialType;
	
	public ArmorPiece(){
		super();
		setStackable(false);
	}
	
	public void setArmor(float armor){
		if(raisedStats == null){
			raisedStats = new Stat();
		}
		raisedStats.setArmor(armor);
		setDescription();
	}
	
	@Override
	protected void setDescription() {
		if(raisedStats != null && raisedStats.getArmor() > 0){
			setDescription("Raises armor stat by " + raisedStats.getArmor());
		}
		else{
			setDescription("A broken piece of armor.");
		}
	}
	
	public void setMaterialType(EnumMaterialType materialType){
		this.materialType = materialType;
		loadSprite(materialType);
	}
	
	protected abstract void loadSprite(EnumMaterialType materialType);
		/*try{
			switch(armorType){
			case SHIELD:
				switch(materialType){
				case GOLD:
					setAnimation(new Animation(ImageIO.read(getClass().getClassLoader().getResourceAsStream("gfx/goldshield.gif"))));
					break;
				case SILVER:
					setAnimation(new Animation(ImageIO.read(getClass().getClassLoader().getResourceAsStream("gfx/silvershield.gif"))));
					break;
				case BRONZE:
					setAnimation(new Animation(ImageIO.read(getClass().getClassLoader().getResourceAsStream("gfx/bronzeshield.gif"))));
					break;
				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}*/
}
