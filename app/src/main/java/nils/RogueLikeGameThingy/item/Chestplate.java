package item;

import model.TypeHandler;
import view.SpriteCompendium;

public class Chestplate extends ArmorPiece {

	@Override
	protected void setBuySellValue() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void setType() {
		itemType = TypeHandler.getItemArmorChestType();
	}

	@Override
	protected void loadSprite(EnumMaterialType materialType) {
		switch (materialType) {
		case GOLD:
			addAnimation(SpriteCompendium.getItemSprites().get(12), "gold");
			setAnimation("gold");
			break;
		case SILVER:
			addAnimation(SpriteCompendium.getItemSprites().get(11), "silver");
			setAnimation("silver");
			break;
		case BRONZE:
			addAnimation(SpriteCompendium.getItemSprites().get(10), "bronze");
			setAnimation("bronze");
			break;
		}
	}

}
