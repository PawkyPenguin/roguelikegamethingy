 package item;

import view.Animation;
import model.EnumDirection;

public abstract class Arrow extends Ammunition{
	
	public Arrow() {
		setStackable(true);
		setMaxStack(99);
	}

	public Animation getArrowAnimation(EnumDirection direction) {
		switch(direction){
		case UP:
			return getAnimation("up");
		case LEFT:
			return getAnimation("left");
		case DOWN:
			return getAnimation("down");
		case RIGHT:
			return getAnimation("right");
		default:
			throw new IllegalArgumentException("direction not valid");
		}
	}
}
