package item;

import model.Stat;
import entities.Living;

public abstract class ItemEquipable extends Item {
	protected Stat raisedStats;

	public void setStats(Stat stats) {
		raisedStats = stats;
		setDescription();
	}

	public void equipTo(Living liv) {
		if (raisedStats != null) {
			liv.raiseStats(raisedStats);
		}
	}

	public void unequipFrom(Living liv) {
		if (raisedStats != null) {
			liv.lowerStats(raisedStats);
		}
	}

}
