package item;

import java.util.ArrayList;

import model.Stat;
import model.TypeHandler;
import view.SpriteCompendium;

public class Ring extends ItemEquipable {
	EnumMaterialType materialType;
	int bufferIndex;

	public Ring() {
		setStackable(false);
	}

	@Override
	public void setStats(Stat stats) {
		raisedStats = stats;
		setDescription();
	}

	public void setMaterialType(EnumMaterialType materialType) {
		this.materialType = materialType;
		loadSprite(materialType);
	}

	private void loadSprite(EnumMaterialType materialType) {
		switch (materialType) {
		case GOLD:
			addAnimation(SpriteCompendium.getItemSprites().get(22), "attack");
			setAnimation("attack");
			break;
		case SILVER:
			addAnimation(SpriteCompendium.getItemSprites().get(23), "defense");
			setAnimation("defense");
			break;
		case BRONZE:
			addAnimation(SpriteCompendium.getItemSprites().get(24), "health");
			setAnimation("health");
			break;
		}
	}

	@Override
	protected void setDescription() {
		String description;
		if (raisedStats != null) {
			description = "Increases ";
			ArrayList<String> descriptionParts = new ArrayList<String>();
			if (raisedStats.getAttack() != 0) {
				descriptionParts.add("attack stat by " + raisedStats.getAttack());
			}
			if (raisedStats.getArmor() != 0) {
				descriptionParts.add("defense stat by " + raisedStats.getArmor());
			}
			if (raisedStats.getHealthBuffer() != null) {
				descriptionParts.add("max health by " + raisedStats.getHealthBuffer().getMaxBufferHealth());
			}
			for (int i = 0; i < descriptionParts.size(); i++) {
				description += descriptionParts.get(i);
				if ((i + 2) < descriptionParts.size()) {
					description += ", ";
				} else if ((i + 1) < descriptionParts.size()) {
					description += " and ";
				}
			}
		} else {
			description = "A broken ring";
		}
		setDescription(description);
	}

	@Override
	protected void setBuySellValue() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void setType() {
		itemType = TypeHandler.getItemRingType();
	}

}
