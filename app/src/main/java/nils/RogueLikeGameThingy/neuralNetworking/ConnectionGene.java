package neuralNetworking;

public class ConnectionGene {
	private int innovationNumber;
	private NeuralNode in;
	private NeuralNode out;
	private boolean isEnabled;
	private double weight;
	
	protected ConnectionGene(int innovationNr, NeuralNode in, NeuralNode out, double weight){
		isEnabled = true;
		this.in = in;
		this.out = out;
		innovationNumber = innovationNr;
		this.weight = weight;
	}
	
	protected int getInnovation(){
		return innovationNumber;
	}
}
