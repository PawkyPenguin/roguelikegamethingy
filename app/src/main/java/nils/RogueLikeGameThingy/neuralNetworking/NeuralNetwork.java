package neuralNetworking;

import java.util.ArrayList;

public class NeuralNetwork {
	private ArrayList<NeuralNode> inputs;
	private ArrayList<NeuralNode> outputs;
	private ArrayList<ConnectionGene> genes;
	private int maxInnovationNr;
	private int fitness;
	
	public NeuralNetwork(){
		
	}
	
//	public static NeuralNetwork crossover(NeuralNetwork network1, NeuralNetwork network2){
//		NeuralNetwork newNetwork = new NeuralNetwork();
//		int maxInnovation = Integer.max(network1.getMaxInnovationNr(), network2.getMaxInnovationNr());
//		int minInnovation = Integer.min(network1.getMaxInnovationNr(), network2.getMaxInnovationNr());
//		ArrayList<ConnectionGene> disjointGenes1 = new ArrayList<ConnectionGene>();
//		ArrayList<ConnectionGene> disjointGenes2 = new ArrayList<ConnectionGene>();
//		ArrayList<ConnectionGene> matchedGenes = new ArrayList<ConnectionGene>();
//		ArrayList<ConnectionGene> excessGenes = new ArrayList<ConnectionGene>();
//		int i = 0;
//		int j = 0;
//		while(i < network1.getGenes().size() && j < network2.getGenes().size()){
//			int innovation1 = network1.getGenes().get(i).getInnovation();
//			int innovation2 = network2.getGenes().get(j).getInnovation();
//			while(innovation1 < innovation2 && i < network1.getGenes().size()){
//				disjointGenes1.add(network1.getGenes().get(i));
//				i++;
//				innovation1 = network1.getGenes().get(i).getInnovation();
//			}
//			while(innovation2 < innovation1 && j < network2.getGenes().size()){
//				disjointGenes2.add(network2.getGenes().get(j));
//				j++;
//				innovation2 = network2.getGenes().get(j).getInnovation();
//			}
//			if(innovation1 == innovation2){
//				if(network1.getGenes().get(i).getInnovation() != network2.getGenes().get(j).getInnovation()){
//					throw new IllegalStateException("Genes should match!");
//				}
//				matchedGenes.add(network1.getGenes().get(i));
//			}
//		}
//	}
	
	public void setMaxInnovationNr(int nr){
		maxInnovationNr = nr;
	}
	
	public int getMaxInnovationNr(){
		return maxInnovationNr;
	}
	
	public void setGenes(ArrayList<ConnectionGene> genes){
		this.genes = genes;
	}
	
	public int getFitness(){
		return fitness;
	}
	
	public ArrayList<ConnectionGene> getGenes(){
		return genes;
	}
}
