package crafting;

import java.util.ArrayList;

import entities.Coordinate;
import item.Item;

public class CraftingRule {
	Item craft;
	ArrayList<ArrayList<Itempos>> rules = new ArrayList<ArrayList<Itempos>>();

	protected void addRule(ArrayList<Itempos> rule) {
		rules.add(rule);
	}

	protected Item getCraft() {
		return craft.clone();
	}

	protected void setCraft(Item i) {
		craft = i.clone();
	}

	protected void addRule(Item[][] rule) {
		ArrayList<Itempos> newRule = new ArrayList<Itempos>();
		for (int i = 0; i < rule.length; i++) {
			for (int j = 0; j < rule[0].length; j++) {
				if (rule[i][j] != null) {
					Coordinate c = new Coordinate(i, j);
					Item item = rule[i][j].clone();
					newRule.add(new Itempos(item, c));
				}
			}
		}
		rules.add(newRule);
	}

	protected boolean tryCrafting(Item[][] items) {
		boolean[][] used = new boolean[items.length][items[0].length];
		int[][] aggregator = new int[items.length][items[0].length];
		/*
		 * Iterates through all crafting rules. Each rule is a shapeless
		 * crafting recipe itself.
		 */
		for (ArrayList<Itempos> rule : rules) {
			if (rule.isEmpty()) {
				return true;
			}
			Coordinate deltaVec = null;
			/*
			 * Iterates through all items in input matrix and checks whether one
			 * of them confirm to rule. If so, we have found a candidate and
			 * break out of the loop.
			 */
			int anchor = -1;
			itemMatcher: while (true) {
				used = new boolean[items.length][items[0].length];
				boolean foundMatchForRule = false;
				matchTest: for (Itempos pos : rule) {
					for (int i = 0; i < items.length; i++) {
						for (int j = 0; j < items[0].length; j++) {
							if (doItemsMatch(items[i][j], pos.getItem()) && (i * items[0].length + j) > anchor) {
								anchor = i * items[0].length + j;
								deltaVec = new Coordinate(i - pos.getCoord().getX(), j - pos.getCoord().getY());
								foundMatchForRule = true;
								break matchTest;
							}
						}
					}
				}
				if (!foundMatchForRule) {
					return false;
				} else {
					for (Itempos pos : rule) {
						if (pos.getCoord().getX() + deltaVec.getX() >= items.length
								|| pos.getCoord().getY() + deltaVec.getY() >= items[0].length) {
							continue itemMatcher;
						} else if (!doItemsMatch(
								items[pos.getCoord().getX() + deltaVec.getX()][pos.getCoord().getY() + deltaVec.getY()],
								pos.getItem())
								|| used[pos.getCoord().getX() + deltaVec.getX()][pos.getCoord().getY()
										+ deltaVec.getY()]) {
							continue itemMatcher;
						} else {
							used[pos.getCoord().getX() + deltaVec.getX()][pos.getCoord().getY()
									+ deltaVec.getY()] = true;
							aggregator[pos.getCoord().getX() + deltaVec.getX()][pos.getCoord().getY()
									+ deltaVec.getY()] += pos.getItem().getAmount();
						}
					}
					break itemMatcher;
				}
			}
		}
		for (int i = 0; i < used.length; i++) {
			for (int j = 0; j < used[0].length; j++) {
				if (used[i][j]) {
					items[i][j].reduceAmount(aggregator[i][j]);
				}
			}
		}
		return true;
	}

	private boolean doItemsMatch(Item item, Item rule) {
		if (rule == null) {
			return true;
		} else if (item != null && rule.getClass().isInstance(item) && item.getAmount() >= rule.getAmount()) {
			return true;
		} else {
			return false;
		}
	}
}
