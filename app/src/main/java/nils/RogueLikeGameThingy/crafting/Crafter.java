package crafting;

import java.util.ArrayList;

import entities.Chest;
import item.Item;
import item.Stick;

public class Crafter {
	private ArrayList<CraftingRule> craftingRecipes = new ArrayList<CraftingRule>();

	public Crafter() {
	}

	public void addCraftingTableRecipes() {
		CraftingRule pickaxeRule = new CraftingRule();
		Item[][] pickaxeRecipe = { { new Stick(), null }, { new Stick(), new Stick() } };
		pickaxeRule.addRule(pickaxeRecipe);
		pickaxeRule.setCraft(Chest.generatePickaxe(0));
		craftingRecipes.add(pickaxeRule);

		CraftingRule stickRule = new CraftingRule();
		Item[][] stickRecipe = { {} };
		stickRule.addRule(stickRecipe);
		stickRule.setCraft(Chest.generateStick(0));
		craftingRecipes.add(stickRule);
	}

	public Item getCraftedItem(Item[][] items) {
		for (int i = 0; i < craftingRecipes.size(); i++) {
			if (craftingRecipes.get(i).tryCrafting(items)) {
				return craftingRecipes.get(i).getCraft();
			}
		}
		return null;
	}
}
