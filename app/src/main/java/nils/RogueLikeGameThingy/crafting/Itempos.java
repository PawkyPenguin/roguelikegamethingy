package crafting;

import entities.Coordinate;
import item.Item;

public class Itempos {
	private Item item;
	private Coordinate coord;

	public Itempos(Item i, Coordinate c) {
		item = i;
		coord = c;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Coordinate getCoord() {
		return coord;
	}

	public void setCoord(Coordinate coord) {
		this.coord = coord;
	}
}
