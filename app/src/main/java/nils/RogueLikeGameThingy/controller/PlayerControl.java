package controller;

import entities.Player;

public class PlayerControl {
	private final double godmodeCd = 0.75;
	private final double noclipCd = 0.75;
	private final double luxToggleCd = 0.75;
	private final double movingCd = 0.20;
	private double movingCooldown;
	private double godModeToggleCooldown = 0;
	private double noclipToggleCooldown = 0;
	private double luxToggleCooldown = 0;
	private double speedupLvl = 1;
	private Player player;
	
	public PlayerControl(Player player){
		this.player = player;
	}
	
	public void tick(double timeSinceLastFrame, InputContainer inputs){
		if(movingCooldown > 0){
			movingCooldown -= timeSinceLastFrame;
		}
		if(godModeToggleCooldown > 0){
			godModeToggleCooldown -= timeSinceLastFrame;
		}
		if(noclipToggleCooldown > 0){
			noclipToggleCooldown -= timeSinceLastFrame;
		}
		if(luxToggleCooldown > 0){
			luxToggleCooldown -= timeSinceLastFrame;
		}
		if(inputs.isKeyPressed("godmode")){
        	if(godModeToggleCooldown <= 0){
        		player.setGodmode(!player.getGodmode());
        		godModeToggleCooldown = godmodeCd;
        	}
        }
        if(inputs.isKeyPressed("noclip")){
        	if(noclipToggleCooldown <= 0){
        		player.setNoclip(!player.getNoclip());
        		noclipToggleCooldown = noclipCd;
        	}
        }
        if(inputs.isKeyPressed("lux")){
        	if(luxToggleCooldown <= 0){
        		player.setLux(!player.getLux());
        		luxToggleCooldown = luxToggleCd;
        	}
        }
        if(inputs.isKeyPressed("speedup")){
        	speedupLvl = 1.8;
        }
        else{
        	speedupLvl = 1.0;
        }
	}
	
	public void refreshMoveCooldown(){
		movingCooldown = movingCd / speedupLvl;
	}
	
	public boolean isMoveCooldownZero(){
		return movingCooldown <= 0;
	}
}
