package controller;

import java.util.ArrayList;

public class InputContainer {

	public final ArrayList<Boolean> keyPressed = new ArrayList<Boolean>();
	public final ArrayList<String> keyNameList = new ArrayList<String>();
	public final ArrayList<String> mouseNameList = new ArrayList<String>();
	public final ArrayList<MouseClick> mouseInputs = new ArrayList<MouseClick>();
	
	public boolean isKeyPressed(String keyName){
		return keyPressed.get(keyNameList.indexOf(keyName));
	}
	
	public boolean isMousePressed(String mouseName){
		return mouseInputs.get(mouseNameList.indexOf(mouseName)).isButtonClicked();
	}
	
	public ArrayList<MouseClick> getMouseClicks(){
		return mouseInputs;
	}
	
	public MouseClick getMouseClick(String mouseName){
		return mouseInputs.get(mouseNameList.indexOf(mouseName));
	}
}
