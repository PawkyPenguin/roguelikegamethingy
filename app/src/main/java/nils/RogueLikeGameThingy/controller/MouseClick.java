package controller;

public class MouseClick {
	private int mouseX;
	private int mouseY;
	private boolean buttonClicked;

	public int getX() {
		return mouseX;
	}

	public int getY() {
		return mouseY;
	}

	public void setButtonClicked(boolean clicked) {
		buttonClicked = clicked;
	}

	public boolean isButtonClicked() {
		return buttonClicked;
	}

	public void setMouseX(int x) {
		mouseX = x;
	}

	public void setMouseY(int y) {
		mouseY = y;
	}
}
