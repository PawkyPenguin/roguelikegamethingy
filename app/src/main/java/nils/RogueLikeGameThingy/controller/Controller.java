package controller;

import model.Floor;
import model.GameHandler;
import view.Frame;
import view.KeyHandler;
import view.MouseHandler;

public class Controller {
	Frame frame;
	private GameHandler model;
	private InputDecoder inputHandler;
	private MouseHandler mouseHandler;
	private KeyHandler keyHandler;
	private long gameloopStartedAt;
	private boolean doGameLoop = true;

	public Controller() {
		frame = new Frame();
		mouseHandler = new MouseHandler();
		keyHandler = new KeyHandler();
		inputHandler = new InputDecoder(keyHandler, mouseHandler);
	}

	public void startGame() {
		model = new GameHandler();
		model.setInputHandler(inputHandler);
		inputHandler.enlistSub(model);
		model.enlistSub(this);
		restartGame();
		inputHandler.forceSubUpdate();
		startGameloop();
	}

	public void restartGame() {
		model.restartGame();
		Floor floor = model.getFloor();
		reinitializeFrame(floor);
	}

	private void startGameloop() {
		gameloopStartedAt = System.currentTimeMillis();
		while (doGameLoop) {
			model.tick();
		}
	}

	private void reinitializeFrame(Floor floor) {
		frame.dispose();
		frame = new Frame();
		frame.makeNewDungeonView(floor, model.getPlayer(), mouseHandler, keyHandler);
		frame.setViewToDungeon();
	}

	public void publisherUpdated(GameHandler pub) {
		frame.publisherUpdated(pub);
	}
}
