package controller;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import model.GameHandler;
import view.KeyHandler;
import view.MouseHandler;

public class InputDecoder {
	KeyHandler keyHandler;
	MouseHandler mouseHandler;
	GameHandler sub;
	boolean newKeyInputs;
	boolean newMouseInputs;
	private final ArrayList<Integer> mouseEventList = new ArrayList<Integer>();
	private final ArrayList<Integer> keyEventList = new ArrayList<Integer>();
	private InputContainer inputs;

	public InputDecoder(KeyHandler key, MouseHandler mouse) {
		inputs = new InputContainer();
		keyHandler = key;
		mouseHandler = mouse;
		mouseHandler.enlistSub(this);
		keyHandler.enlistSub(this);
		setKeyControls();
		setMouseControls();
	}

	private void setMouseControls() {
		inputs.mouseNameList.add("mouseLeft");
		inputs.mouseNameList.add("mouseRight");
		inputs.mouseInputs.add(new MouseClick());
		inputs.mouseInputs.add(new MouseClick());
		mouseEventList.add(MouseEvent.BUTTON1);
		mouseEventList.add(MouseEvent.BUTTON3);
	}

	private void setKeyControls() {
		keyEventList.add(KeyEvent.VK_W);
		keyEventList.add(KeyEvent.VK_A);
		keyEventList.add(KeyEvent.VK_S);
		keyEventList.add(KeyEvent.VK_D);
		keyEventList.add(KeyEvent.VK_UP);
		keyEventList.add(KeyEvent.VK_LEFT);
		keyEventList.add(KeyEvent.VK_DOWN);
		keyEventList.add(KeyEvent.VK_RIGHT);
		keyEventList.add(KeyEvent.VK_L);
		keyEventList.add(KeyEvent.VK_G);
		keyEventList.add(KeyEvent.VK_ALT);
		keyEventList.add(KeyEvent.VK_F);
		keyEventList.add(KeyEvent.VK_N);
		keyEventList.add(KeyEvent.VK_SPACE);
		keyEventList.add(KeyEvent.VK_F);
		final ArrayList<String> keyNameList = inputs.keyNameList;
		keyNameList.add("moveUp");
		keyNameList.add("moveLeft");
		keyNameList.add("moveDown");
		keyNameList.add("moveRight");
		keyNameList.add("lookUp");
		keyNameList.add("lookLeft");
		keyNameList.add("lookDown");
		keyNameList.add("lookRight");
		keyNameList.add("lux");
		keyNameList.add("godmode");
		keyNameList.add("directionSelect");
		keyNameList.add("descend");
		keyNameList.add("noclip");
		keyNameList.add("speedup");
		keyNameList.add("pickUpItem");
		final ArrayList<Boolean> keyPressed = inputs.keyPressed;
		for (int i = 0; i < keyNameList.size(); i++) {
			keyPressed.add(false);
		}
	}

	public void requestCheckForInput() {
		if (newKeyInputs) {
			decodeKeyInputs();
			newKeyInputs = false;
		}
		if (newMouseInputs) {
			decodeMouseInputs();
			newMouseInputs = false;
		}
	}

	public void forceSubUpdate() {
		sub.publisherUpdated(inputs);
	}

	private void updateSubscriber() {
		sub.publisherUpdated(inputs);
	}

	public void publisherUpdated(KeyHandler pub) {
		newKeyInputs = true;
	}

	public void publisherUpdated(MouseHandler pub) {
		newMouseInputs = true;
	}

	private void decodeKeyInputs() {
		if (keyHandler.isKeyPressed(KeyEvent.VK_ESCAPE)) {
			sub.exit();
		}
		final ArrayList<Boolean> keyPressed = inputs.keyPressed;
		for (int i = 0; i < keyEventList.size(); i++) {
			if (keyHandler.isKeyPressed(keyEventList.get(i))) {
				keyPressed.set(i, true);
			} else {
				keyPressed.set(i, false);
			}
		}
		updateSubscriber();
	}

	private void decodeMouseInputs() {
		ArrayList<MouseClick> mouseClicks = inputs.mouseInputs;
		for (int i = 0; i < mouseEventList.size(); i++) {
			mouseClicks.set(i, mouseHandler.getMouseClick(mouseEventList.get(i)));
		}
		updateSubscriber();
	}

	public void enlistSub(GameHandler sub) {
		this.sub = sub;
	}

	public void unlistSub(GameHandler sub) {
		this.sub = null;
	}
}
