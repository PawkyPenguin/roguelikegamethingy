package controller;

import java.util.ArrayList;

import entities.Chest;
import entities.Coordinate;
import entities.CraftingTable;
import entities.Player;
import entities.Shopkeeper;
import item.Item;
import model.EnumInvFieldEquipType;
import model.EnumInventoryFieldType;
import model.Game;
import model.HasInventory;
import model.InvFieldAutouse;
import model.InvFieldCommon;
import model.InvFieldEquipment;
import model.InvFieldTrash;
import model.Inventory;
import model.InventoryField;
import model.InventoryHandler;
import model.TypeHandler;
import view.DungeonView;
import view.InvFieldUI;
import view.InvUIContainer;

public class InventoryController {
	public boolean isVisible = true;
	private boolean mouseLeftPressedLastTick;
	private boolean mouseRightPressedLastTick;
	private boolean holdingItem = false;
	private Item pickedUpItem;
	private InvFieldUI pickedUpItemField;
	private InvFieldUI droppedItemField;
	private InputContainer currentInputs;
	private Inventory inventory;
	private InvUIContainer invInterface;
	private InvUIContainer otherInvUIContainer;
	private InventoryHandler handler;
	private Player master;

	public InventoryController(Player master) {
		this.master = master;
		otherInvUIContainer = new InvUIContainer();
		handler = master.getInvHandler();
		inventory = handler.getInventory();
		inventory.setAllowTransacation(true);
		initializeInventoryMouseInterface();
	}

	private void addInvField(InventoryField field, int x, int y) {
		inventory.addInvField(field);
		if (field.getInvFieldType() == EnumInventoryFieldType.EQUIPMENT) {
			((InvFieldEquipment) field).setOwner(master);
		}
		InvFieldUI newFieldUI = new InvFieldUI(x, y);
		newFieldUI.setInvField(field);
		invInterface.addInvFieldUIToUI(newFieldUI);
	}

	private void initializeInventoryMouseInterface() {
		ArrayList<InventoryField> invFields = new ArrayList<InventoryField>();
		InventoryField newField;
		final int invCoordX = Game.inventoryX + 608;
		final int invCoordY = Game.inventoryY + DungeonView.menuHeight;
		invInterface = new InvUIContainer();
		invInterface.setXY(invCoordX + 14, invCoordY + 380);
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 4; j++) {
				InventoryField field = new InvFieldCommon();
				invFields.add(field);
				addInvField(field);
			}
		}
		invInterface.setInvUI(invFields, 4, 3);
		newField = new InvFieldAutouse();
		newField.setContainedItemType(TypeHandler.getItemArrowType());
		addInvField(newField, invCoordX + 63, invCoordY + 233);
		newField = new InvFieldTrash();
		addInvField(newField, invCoordX + 112, invCoordY + 331);
		newField = new InvFieldTrash();
		addInvField(newField, invCoordX + 63, invCoordY + 331);
		newField = new InvFieldTrash();
		addInvField(newField, invCoordX + 14, invCoordY + 331);
		newField = new InvFieldEquipment();
		((InvFieldEquipment) newField).setType(EnumInvFieldEquipType.RING);
		addInvField(newField, invCoordX + 20, invCoordY + 28);
		newField = new InvFieldEquipment();
		((InvFieldEquipment) newField).setType(EnumInvFieldEquipType.HELMET);
		addInvField(newField, invCoordX + 63, invCoordY + 28);
		newField = new InvFieldEquipment();
		((InvFieldEquipment) newField).setType(EnumInvFieldEquipType.WEAPON);
		addInvField(newField, invCoordX + 20, invCoordY + 71);
		newField = new InvFieldEquipment();
		((InvFieldEquipment) newField).setType(EnumInvFieldEquipType.CHEST);
		addInvField(newField, invCoordX + 63, invCoordY + 71);
		newField = new InvFieldEquipment();
		((InvFieldEquipment) newField).setType(EnumInvFieldEquipType.SHIELD);
		addInvField(newField, invCoordX + 106, invCoordY + 71);
		newField = new InvFieldEquipment();
		((InvFieldEquipment) newField).setType(EnumInvFieldEquipType.LEGGINS);
		addInvField(newField, invCoordX + 63, invCoordY + 114);
	}

	private void addInvField(InventoryField field) {
		inventory.addInvField(field);
		if (field.getInvFieldType() == EnumInventoryFieldType.EQUIPMENT) {
			((InvFieldEquipment) field).setOwner(master);
		}
	}

	public void updateInputs(InputContainer inputs) {
		currentInputs = inputs;
		doClicks();
		mouseLeftPressedLastTick = currentInputs.isMousePressed("mouseLeft");
		mouseRightPressedLastTick = currentInputs.isMousePressed("mouseRight");
	}

	public void setInventory(Inventory inv) {
		inventory = inv;
	}

	public boolean isHoldingItem() {
		return holdingItem && pickedUpItem != null;
	}

	public InventoryField getPickedUpItemField() {
		return pickedUpItemField.getInvField();
	}

	public Item getPickedUpItem() {
		return pickedUpItem;
	}

	public InvUIContainer getInvInterface() {
		return invInterface;
	}

	private void doClicks() {
		if (master.isPerformingTurn()) {
			if (!mouseLeftPressedLastTick && currentInputs.isMousePressed("mouseLeft")) {
				pressLeftClick();
			} else if (mouseLeftPressedLastTick && !currentInputs.isMousePressed("mouseLeft")) {
				releaseLeftClick();
			}
			if (!mouseRightPressedLastTick && currentInputs.isMousePressed("mouseRight")) {
				pressRightClick();
			} else if (mouseRightPressedLastTick && !currentInputs.isMousePressed("mouseRight")) {
				releaseRightClick();
			}
		}
	}

	public InvFieldUI getFieldUnderMouse(int mouseX, int mouseY) {
		for (int i = 0; i < invInterface.getInvUISize(); i++) {
			InvFieldUI field = invInterface.getInvUIElement(i);
			if (invInterface.getScaledMouseBox(field).isWithinBox(mouseX, mouseY))
				return field;
		}
		return null;
	}

	private void pressLeftClick() {
		int mouseX = currentInputs.getMouseClick("mouseLeft").getX();
		int mouseY = currentInputs.getMouseClick("mouseLeft").getY();
		InvFieldUI fieldUnderMouse = getFieldUnderMouse(mouseX, mouseY);
		if (fieldUnderMouse != null) {
			holdingItem = true;
			pickedUpItem = fieldUnderMouse.getInvField().getItem();
			pickedUpItemField = fieldUnderMouse;
			pickedUpItemField.setDisplayItem(false);
		}
	}

	private void releaseLeftClick() {
		int mouseX = currentInputs.getMouseClick("mouseLeft").getX();
		int mouseY = currentInputs.getMouseClick("mouseLeft").getY();
		InvFieldUI fieldUnderMouse = getFieldUnderMouse(mouseX, mouseY);
		if (holdingItem) {
			if (fieldUnderMouse != null) {
				if (master.isInteractingWithOtherInventory()
						&& inventory.isTradingPossible(handler.getOtherInvOwner().getInventory())) {
					if (inventory.doesOwnInvField(fieldUnderMouse.getInvField())
							^ inventory.doesOwnInvField(pickedUpItemField.getInvField())) {
						inventory.tradeItems(pickedUpItemField.getInvField(), fieldUnderMouse.getInvField());
					} else {
						pickedUpItemField.getInvField().swapItemsIfPossible(fieldUnderMouse.getInvField());
					}
				} else {
					pickedUpItemField.getInvField().swapItemsIfPossible(fieldUnderMouse.getInvField());
				}
			}
			holdingItem = false;
			pickedUpItemField.setDisplayItem(true);
			pickedUpItemField = null;
		}
	}

	private void pressRightClick() {
		int mouseX = currentInputs.getMouseClick("mouseRight").getX();
		int mouseY = currentInputs.getMouseClick("mouseRight").getY();
		InvFieldUI uiFieldUnderMouse = getFieldUnderMouse(mouseX, mouseY);
		if (uiFieldUnderMouse != null) {
			InventoryField fieldUnderMouse = getFieldUnderMouse(mouseX, mouseY).getInvField();
			if (fieldUnderMouse != null) {
				Item itemUnderMouse = fieldUnderMouse.getItem();
				if (TypeHandler.isItemActive(itemUnderMouse) && inventory.doesOwnInvField(fieldUnderMouse)) {
					fieldUnderMouse.useItem(master);
				} else if (!inventory.isEquipment(fieldUnderMouse)) {
					if (TypeHandler.isItemEquipable(fieldUnderMouse)) {
						doAutoEquip(fieldUnderMouse);
					} else if (TypeHandler.isItemAmmunition(fieldUnderMouse.getItem())) {
						putInAutouseSlot(fieldUnderMouse);
					}
				} else {
					InventoryField emptyField = Inventory.findEmptyField(inventory.getCommons());
					emptyField.swapItemsIfPossible(fieldUnderMouse);
				}
			}
		}
	}

	private void doAutoEquip(InventoryField field) {
		handler.putInEquipmentSlot(field);
	}

	private void putInAutouseSlot(InventoryField field) {
		handler.putInAutouseSlot(field);
	}

	private void releaseRightClick() {

	}

	private void addInventory(InvUIContainer invUI) {
		for (int i = 0; i < invUI.getInvUISize(); i++) {
			invInterface.addInvFieldUIToUI(invUI.getInvUIElement(i));
		}
		ArrayList<InventoryField> updateKnowledgeList = new ArrayList<InventoryField>();
		for (int i = 0; i < invUI.getInvUISize(); i++) {
			updateKnowledgeList.add(invUI.getInvUIElement(i).getInvField());
		}
		// TODO: Check if line below is necessary
		//handler.updateDescriptionKnowledge(updateKnowledgeList);
	}

	public InventoryHandler getInvHandler() {
		return handler;
	}

	public void stopInteracting() {
		handler.stopInteracting();
		removeInventory(otherInvUIContainer);
	}

	private void removeInventory(InvUIContainer invUI) {
		for (int i = 0; i < invUI.getInvUISize(); i++) {
			invInterface.removeInvFieldUI(invUI.getInvUIElement(i));
		}
	}

	private static Coordinate fetchStandardCoord(Coordinate entityCoord) {
		int stX = DungeonView.centerToScreenPosX(entityCoord.getX());
		int stY = DungeonView.toScreenPosY(entityCoord.getY());
		return new Coordinate(stX, stY);
	}

	public void addInteractionInventory(Inventory inv) {
		HasInventory invOwner = handler.getOtherInvOwner();
		if (inv.getOwner() instanceof Shopkeeper) {
			otherInvUIContainer.setInvUI(inv.getInventory());
			otherInvUIContainer.setXY(fetchStandardCoord(handler.getOtherInvOwner().getCoordinate()));
		} else if (inv.getOwner() instanceof Chest) {
			otherInvUIContainer.setInvUI(inv.getInventory());
			otherInvUIContainer.setXY(DungeonView.toScreenPosX(invOwner.getX() + 0.5),
					DungeonView.toScreenPosY(invOwner.getY()));
		} else if (inv.getOwner() instanceof CraftingTable) {
			otherInvUIContainer.setInvUI(inv.getInventory(), ((CraftingTable) inv.getOwner()).getFieldsWidth(),
					((CraftingTable) inv.getOwner()).getFieldsHeight());
			otherInvUIContainer.setXY(DungeonView.toScreenPosX(invOwner.getX() + 0.5),
					DungeonView.toScreenPosY(invOwner.getY() + 3.5));
		}
		otherInvUIContainer.center();
		addInventory(otherInvUIContainer);
	}
}
