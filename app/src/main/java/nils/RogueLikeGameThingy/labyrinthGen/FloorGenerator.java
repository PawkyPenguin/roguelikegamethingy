package labyrinthGen;

public interface FloorGenerator {
	public boolean[][] generateLabyrinth(int width, int height);
}
