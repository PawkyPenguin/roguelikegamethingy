package labyrinthGen;

import entities.Coordinate;

public class MazeTile {
	Coordinate coord;
	UnionFindNode node;

	public final Coordinate getCoord() {
		return coord;
	}

	public final void setCoord(Coordinate coord) {
		this.coord = coord;
	}

	public final UnionFindNode getNode() {
		return node;
	}

	public final void setNode(UnionFindNode node) {
		this.node = node;
	}
}
