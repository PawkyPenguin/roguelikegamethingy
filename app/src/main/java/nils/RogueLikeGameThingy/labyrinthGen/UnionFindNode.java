package labyrinthGen;


public class UnionFindNode {
	UnionFindNode parent;
	int size;
	int id;
	boolean connectionFlag;

	public UnionFindNode() {
		size = 1;
	}

	protected void setId(int id) {
		this.id = id;
	}

	protected int getId() {
		return id;
	}

	protected int getSize() {
		return size;
	}

	protected boolean getConnectionFlag() {
		return connectionFlag;
	}

	protected void setConnectionFlag(boolean flag) {
		connectionFlag = flag;
	}

	protected void setSize(int size) {
		this.size = size;
	}

	protected UnionFindNode getParent() {
		return parent;
	}

	protected void setParent(UnionFindNode parent) {
		this.parent = parent;
	}
}
