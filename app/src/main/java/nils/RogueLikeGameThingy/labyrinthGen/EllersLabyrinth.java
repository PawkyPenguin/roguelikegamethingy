package labyrinthGen;

import java.util.ArrayList;

public class EllersLabyrinth implements FloorGenerator {
	/*
	 * This algorithm generates a labyrinth row by row, starting at the top.
	 * Calling "generateEllerLabyrinth" executes the algorithm.
	 */

	/*
	 * Generates the labyrinth. Eller's algorithm starts at the first row and
	 * divides it into separate cells by filling the row with walls and then
	 * removes half of them in a checkerboard-pattern manner. Then, it randomly
	 * removes additional walls to interconnect the separate floor tiles. It
	 * keeps track of which floor tiles are interconnected by storing them in
	 * 'sets'.
	 */
	@Override
	public boolean[][] generateLabyrinth(int width, int height) {
		boolean[][] labyrinth = new boolean[width][height];
		UnionFind sets = new UnionFind(width / 2 + 1);
		initLabyrinth(labyrinth);
		for (int i = 0; i < sets.getLength(); i++)
			sets.makeSet(i);
		for (int j = 0; j < height / 2; j++) {
			sets = generateRow(labyrinth, sets, j);
		}
		for (int i = 1; i < width / 2 + 1; i++) {
			UnionFindNode lastNode = sets.find(i - 1);
			UnionFindNode thisNode = sets.find(i);
			if (lastNode != thisNode) {
				labyrinth[i * 2 - 1][height - 1] = false;
				sets.union(lastNode, thisNode);
			}
		}
		return labyrinth;
	}

	private UnionFind generateRow(boolean[][] labyrinth, UnionFind sets, int row) {
		int width = labyrinth.length / 2 + 1;
		UnionFind nextSets = new UnionFind(sets.getLength());
		/*
		 * Union sets on current row
		 */
		for (int i = 1; i < width; i++) {
			if (Math.random() < 1.0 / 1.5) {
				UnionFindNode lastNode = sets.find(i - 1);
				UnionFindNode thisNode = sets.find(i);
				if (lastNode != thisNode) {
					labyrinth[i * 2 - 1][row * 2] = false;
					sets.union(lastNode, thisNode);
					if (sets.find(i - 1) != sets.find(i)) {
						throw new IllegalStateException();
					}
				}
			}
		}
		UnionFindNode[] newNodeCollection = new UnionFindNode[width];
		ArrayList<Integer>[] setRepresentants = new ArrayList[width];
		for (int i = 0; i < width; i++)
			setRepresentants[i] = new ArrayList<Integer>();
		/*
		 * Make connections to next row
		 */
		for (int i = 0; i < width; i++) {
			UnionFindNode thisNode = sets.find(i);
			setRepresentants[thisNode.getId()].add(i);
			if (Math.random() < 1.0 / 2) {
				if (labyrinth[i * 2].length > row * 2 + 1) {
					labyrinth[i * 2][row * 2 + 1] = false;
					if (newNodeCollection[thisNode.getId()] == null) {
						UnionFindNode newNode = nextSets.makeSet(i);
						newNodeCollection[thisNode.getId()] = newNode;
					} else {
						nextSets.union(newNodeCollection[thisNode.getId()], nextSets.makeSet(i));
					}
					thisNode.setConnectionFlag(true);
				}
			}
		}
		for (int i = 0; i < sets.nodes.length; i++) {
			UnionFindNode thisNode = sets.find(i);
			if (newNodeCollection[thisNode.getId()] == null) {
				int rndIndex = (int) (Math.random() * setRepresentants[thisNode.getId()].size());
				int connectionToNextRow = setRepresentants[thisNode.getId()].get(rndIndex);
				labyrinth[connectionToNextRow * 2][row * 2 + 1] = false;
				UnionFindNode newNode = nextSets.makeSet(setRepresentants[thisNode.getId()].get(rndIndex));
				newNodeCollection[thisNode.getId()] = newNode;
			}
		}
		for (int i = 0; i < nextSets.getLength(); i++) {
			if (nextSets.find(i) == null) {
				nextSets.makeSet(i);
			}
		}
		return nextSets;
	}

	private void initLabyrinth(boolean[][] labyrinth) {
		for (int i = 1; i < labyrinth.length; i += 2) {
			for (int j = 0; j < labyrinth[0].length; j++) {
				labyrinth[i][j] = true;
			}
		}
		for (int j = 1; j < labyrinth[0].length; j += 2) {
			for (int i = 0; i < labyrinth.length; i++) {
				labyrinth[i][j] = true;
			}
		}
	}
}