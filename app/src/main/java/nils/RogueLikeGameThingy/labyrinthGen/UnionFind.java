package labyrinthGen;

public class UnionFind {
	UnionFindNode[] nodes;
	int length;
	int disjointSets;

	public UnionFind(int numOfElements) {
		nodes = new UnionFindNode[numOfElements];
		length = numOfElements;
		disjointSets = 0;
	}

	protected int getLength() {
		return length;
	}

	protected int getSize(int i) {
		return nodes[i].getSize();
	}

	protected UnionFindNode makeSet(int i) {
		UnionFindNode newNode = new UnionFindNode();
		nodes[i] = newNode;
		newNode.setId(i);
		disjointSets++;
		return newNode;
	}

	protected int getDisjointSets() {
		return disjointSets;
	}

	protected void setConnectionFlag(boolean flag, UnionFindNode node) {
		if (node.getParent() != null) {
			setConnectionFlag(flag, node.getParent());
		} else {
			node.setConnectionFlag(flag);
		}
	}

	protected UnionFindNode find(int i) {
		if (nodes[i] != null) {
			return find(nodes[i]);
		} else {
			return null;
		}
	}

	public UnionFindNode find(UnionFindNode node) {
		if (node.getParent() == null) {
			return node;
		} else {
			return find(node.getParent());
		}
	}

	protected void union(UnionFindNode e, UnionFindNode f) {
		if (e.getSize() < f.getSize()) {
			UnionFindNode temp = e;
			e = f;
			f = temp;
		}
		f.setParent(e);
		e.setSize(e.getSize() + f.getSize());
		disjointSets--;
	}
}
