package labyrinthGen;

import entities.Coordinate;

public class Room {
	Coordinate position;
	Coordinate dimensions;

	public Room(Coordinate pos, Coordinate dim) {
		position = pos;
		dimensions = dim;
	}

	public Coordinate getPosition() {
		return position;
	}

	public void setPosition(Coordinate pos) {
		position = pos;
	}
}
