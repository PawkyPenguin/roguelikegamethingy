package labyrinthGen;

import java.util.ArrayList;
import java.util.LinkedList;

import entities.Coordinate;
import lazyness.Chance;
import model.Direction;
import model.EnumDirection;

public class SaphsLabyrinth implements FloorGenerator {
	private final int numberOfTriesForRooms = 20;
	private final int minRoomWidth = 2;
	private final int maxRoomWidth = 4;
	private final int minRoomHeight = 2;
	private final int maxRoomHeight = 4;
	private final int numberOfRemovedDeadends = 500;
	private final int takeRedundantConnectorChance = 1000;

	@Override
	public boolean[][] generateLabyrinth(int width, int height) {
		boolean[][] wallMap = new boolean[width][height];
		for (int i = 0; i < wallMap.length; i++) {
			for (int j = 0; j < wallMap[0].length; j++) {
				wallMap[i][j] = true;
			}
		}
		UnionFindNode[][] sets = new UnionFindNode[width / 2 + 1][height / 2 + 1];
		UnionFind2 setMaker = new UnionFind2();
		makeRooms(setMaker, sets, wallMap);
		fillWithMaze(setMaker, sets, wallMap);
		makeConnections(setMaker, sets, wallMap);
		makeLabyrinthSparse(sets, wallMap);
		return wallMap;
	}

	// Debugging:
	private ArrayList<Coordinate> findConnectorsDebug(UnionFind2 setMaker, UnionFindNode[][] sets,
			boolean[][] wallMap) {
		ArrayList<Coordinate>[][] connectors = findConnectors(setMaker, sets, wallMap);
		ArrayList<Coordinate> connectorList = new ArrayList<Coordinate>();
		for (int i = 0; i < connectors.length; i++) {
			for (int j = 0; j < connectors[0].length; j++) {
				for (Coordinate coord : connectors[i][j]) {
					connectorList.add(coord);
				}
			}
		}
		return connectorList;
	}

	private ArrayList<Coordinate>[][] findConnectors(UnionFind2 setMaker, UnionFindNode[][] sets, boolean[][] wallMap) {
		int amountOfSets = setMaker.getCurrentId();
		ArrayList<Coordinate>[][] connectors = new ArrayList[amountOfSets][amountOfSets];
		for (int i = 0; i < connectors.length; i++) {
			for (int j = 0; j < connectors[0].length; j++) {
				connectors[i][j] = new ArrayList<Coordinate>();
			}
		}
		for (int i = 0; i < sets.length; i++) {
			for (int j = 0; j < sets[0].length; j++) {
				for (int dirId = 0; dirId < 4; dirId++) {
					EnumDirection direction = Direction.toDirection(dirId);
					Coordinate adjacent = Direction.getFieldInDirection(new Coordinate(i, j), direction);
					if (adjacent.x >= 0 && adjacent.x < sets.length && adjacent.y >= 0 && adjacent.y < sets[0].length) {
						UnionFindNode node1 = sets[i][j];
						UnionFindNode node2 = sets[adjacent.x][adjacent.y];
						Coordinate wallCoordinate = new Coordinate(i + adjacent.x, j + adjacent.y);
						if (node1 != null && node2 != null && setMaker.find(node1) != setMaker.find(node2)) {
							connectors[node1.getId()][node2.getId()].add(wallCoordinate);
							connectors[node2.getId()][node1.getId()].add(wallCoordinate);
						}
					}
				}
			}
		}
		return connectors;
	}

	// returns a randomly shuffled array of the sequence 0,..,elements-1
	private int[] makeRandomArray(int elements) {
		int[] rndSequence = new int[elements];
		for (int i = 0; i < elements; i++) {
			int j = (int) (Math.random() * (i + 1));
			rndSequence[i] = rndSequence[j];
			rndSequence[j] = i;
		}
		return rndSequence;
	}

	private void makeConnections(UnionFind2 setMaker, UnionFindNode[][] sets, boolean[][] wallMap) {
		ArrayList<Coordinate>[][] connectorCandidates = findConnectors(setMaker, sets, wallMap);
		ArrayList<UnionFindNode> originalSets = setMaker.getSets();
		ArrayList<UnionFindNode> processedSets = new ArrayList<UnionFindNode>();
		ArrayList<Coordinate> definitiveConnectors = new ArrayList<Coordinate>();
		int[] randomOrderOfSets = makeRandomArray(connectorCandidates.length);
		UnionFindNode currentlyProcessedSet = originalSets.get((int) (Math.random() * originalSets.size()));
		processedSets.add(currentlyProcessedSet);
		while (processedSets.size() < originalSets.size()) {
			for (int rndIndex : randomOrderOfSets) {
				UnionFindNode otherSet = originalSets.get(rndIndex);
				boolean areSetsUnified = setMaker.find(otherSet) == setMaker.find(currentlyProcessedSet);
				if (!connectorCandidates[currentlyProcessedSet.getId()][rndIndex].isEmpty() && !areSetsUnified) {
					processedSets.add(originalSets.get(rndIndex));
					setMaker.union(currentlyProcessedSet, otherSet);
					ArrayList<Coordinate> possibleConnectors = connectorCandidates[currentlyProcessedSet
							.getId()][rndIndex];
					int rndConnectorIndex = (int) (Math.random() * possibleConnectors.size());
					definitiveConnectors.add(possibleConnectors.get(rndConnectorIndex));
					addRedundancy(definitiveConnectors, possibleConnectors);
					break;
				}
			}
			currentlyProcessedSet = processedSets.get((int) (Math.random() * processedSets.size()));
		}
		for (Coordinate connector : definitiveConnectors) {
			wallMap[connector.x][connector.y] = false;
		}
	}

	private void addRedundancy(ArrayList<Coordinate> definitiveConnectors, ArrayList<Coordinate> possibleConnectors) {
		for (Coordinate connectorCandidate : possibleConnectors) {
			if (Chance.oneIn(takeRedundantConnectorChance)) {
				definitiveConnectors.add(connectorCandidate);
			}
		}
	}

	private void makeLabyrinthSparse(UnionFindNode[][] sets, boolean[][] wallMap) {
		LinkedList<Coordinate> deadEnds = new LinkedList<Coordinate>();
		int removedDeadends = 0;
		for (int i = 0; i < wallMap.length; i++) {
			for (int j = 0; j < wallMap[0].length; j++) {
				if (!wallMap[i][j])
					checkForDeadEnd(wallMap, new Coordinate(i, j), deadEnds);
			}
		}
		while (!deadEnds.isEmpty() && removedDeadends < numberOfRemovedDeadends) {
			Coordinate deadEnd = deadEnds.removeLast();
			/*
			 * this check is necessary because due to integer division and
			 * rounding down, it otherwise sets the set sets[deadEnd.x /
			 * 2][deadEnd.y / 2] (haha) to 'null' too early and possible
			 * connectors between sets are not identified correctly.
			 */
			if (deadEnd.x % 2 == 0 && deadEnd.y % 2 == 0) {
				sets[deadEnd.x / 2][deadEnd.y / 2] = null;
			}
			wallMap[deadEnd.x][deadEnd.y] = true;
			Coordinate adjacent = null;
			for (int i = 0; i < 4; i++) {
				EnumDirection dir = Direction.toDirection(i);
				Coordinate candidate = Direction.getFieldInDirection(deadEnd, dir);
				try {
					if (!wallMap[candidate.x][candidate.y]) {
						adjacent = candidate;
						break;
					}
				} catch (ArrayIndexOutOfBoundsException e) {
				}
			}
			if (adjacent != null) {
				checkForDeadEnd(wallMap, adjacent, deadEnds);
			}
			removedDeadends++;
		}
	}

	private void checkForDeadEnd(boolean[][] wallMap, Coordinate coord, LinkedList<Coordinate> deadEnds) {
		int adjacentWalls = 0;
		for (int dirId = 0; dirId < 4; dirId++) {
			EnumDirection dir = Direction.toDirection(dirId);
			Coordinate adjacentCoord = Direction.getFieldInDirection(coord, dir);
			try {
				if (wallMap[adjacentCoord.x][adjacentCoord.y]) {
					adjacentWalls++;
				}
			} catch (ArrayIndexOutOfBoundsException e) {
				adjacentWalls++;
			}
		}
		if (adjacentWalls == 3) {
			deadEnds.push(coord);
		}
	}

	private void fillWithMaze(UnionFind2 unionFinder, UnionFindNode[][] sets, boolean[][] connectionMap) {
		for (int i = 0; i < sets.length; i++) {
			for (int j = 0; j < sets[0].length; j++) {
				MazeTile tile = new MazeTile();
				tile.setCoord(new Coordinate(i, j));
				tile.setNode(sets[i][j]);
				processField(unionFinder, tile, sets, connectionMap);
			}
		}
	}

	private void processField(UnionFind2 setMaker, MazeTile tile, UnionFindNode[][] sets, boolean[][] wallMap) {
		if (tile.getNode() != null) {
			return;
		}
		UnionFindNode set = setMaker.makeSet();
		ArrayList<Coordinate> unprocessedFieldQueue = new ArrayList<Coordinate>();
		ArrayList<Coordinate> adjacents = new ArrayList<Coordinate>();
		unprocessedFieldQueue.add(tile.getCoord());
		sets[tile.getCoord().x][tile.getCoord().y] = set;
		do {
			adjacents.clear();
			Coordinate processedCoord = unprocessedFieldQueue.get(unprocessedFieldQueue.size() - 1);
			for (int directionId = 0; directionId < 4; directionId++) {
				EnumDirection direction = Direction.toDirection(directionId);
				Coordinate candidate = Direction.getFieldInDirection(processedCoord, direction);
				try {
					if (sets[candidate.x][candidate.y] == null) {
						adjacents.add(candidate);
					}
				} catch (ArrayIndexOutOfBoundsException e) {
				}
			}
			if (!adjacents.isEmpty()) {
				int randomIndex = (int) (Math.random() * adjacents.size());
				Coordinate newCoord = adjacents.get(randomIndex);
				// EnumDirection directionOfNewCoord =
				// Direction.toDirection(randomIndex);
				// int deltaX = Direction.getX(directionOfNewCoord);
				// int deltaY = Direction.getY(directionOfNewCoord);
				addToQueue(unprocessedFieldQueue, newCoord);
				sets[newCoord.x][newCoord.y] = set;
				wallMap[processedCoord.x * 2][processedCoord.y * 2] = false;
				wallMap[processedCoord.x + newCoord.x][processedCoord.y + newCoord.y] = false;
			} else {
				Coordinate removedCoord = unprocessedFieldQueue.remove(unprocessedFieldQueue.size() - 1);
				wallMap[removedCoord.x * 2][removedCoord.y * 2] = false;
			}
		} while (!unprocessedFieldQueue.isEmpty());
	}

	/*
	 * This method can be adjusted in order to change the maze creation
	 * procedure, and by that, it's "feel" or structure.
	 */
	private void addToQueue(ArrayList<Coordinate> queue, Coordinate coord) {
		queue.add(coord);
	}

	private void makeRooms(UnionFind2 setMaker, UnionFindNode[][] sets, boolean[][] wallMap) {
		for (int i = 0; i < numberOfTriesForRooms; i++) {
			Coordinate size = makeRandomRoomSize();
			Coordinate location = makeRandomRoomLocation(size, sets.length, sets[0].length);
			if (roomFits(sets, location, size)) {
				placeRoomOnFloor(setMaker, sets, location, size, wallMap);
			}
		}
	}

	private void placeRoomOnFloor(UnionFind2 setMaker, UnionFindNode[][] sets, Coordinate location, Coordinate size,
			boolean[][] wallMap) {
		UnionFindNode roomSet = setMaker.makeSet();
		for (int i = location.x; i < location.x + size.x; i++) {
			for (int j = location.y; j < location.y + size.y; j++) {
				sets[i][j] = roomSet;
			}
		}
		for (int i = location.x * 2; i < (location.x + size.x) * 2 - 1; i++) {
			for (int j = location.y * 2; j < (location.y + size.y) * 2 - 1; j++) {
				wallMap[i][j] = false;
			}
		}
	}

	private boolean roomFits(UnionFindNode[][] sets, Coordinate location, Coordinate size) {
		for (int i = location.x; i < location.x + size.x; i++) {
			for (int j = location.y; j < location.y + size.y; j++) {
				if (sets[i][j] != null) {
					return false;
				}
			}
		}
		return true;
	}

	private Coordinate makeRandomRoomLocation(Coordinate size, int widthOfDungeon, int heightOfDungeon) {
		int x = ((int) (Math.random() * (widthOfDungeon - size.x + 1)));
		int y = ((int) (Math.random() * (heightOfDungeon - size.y + 1)));
		return new Coordinate(x, y);
	}

	private Coordinate makeRandomRoomSize() {
		int width = ((int) (Math.random() * (maxRoomWidth - minRoomWidth + 1) + minRoomWidth));
		int height = ((int) (Math.random() * (maxRoomHeight - minRoomHeight + 1) + minRoomHeight));
		return new Coordinate(width, height);
	}
}
