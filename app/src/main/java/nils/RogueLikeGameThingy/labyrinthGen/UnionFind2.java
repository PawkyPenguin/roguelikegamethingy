package labyrinthGen;

import java.util.ArrayList;

public class UnionFind2 {
	ArrayList<UnionFindNode> sets;
	private int currentId = 0;

	protected UnionFind2() {
		sets = new ArrayList<UnionFindNode>();
	}

	protected UnionFindNode makeSet() {
		UnionFindNode newNode = new UnionFindNode();
		newNode.setId(currentId);
		sets.add(newNode);
		currentId++;
		return newNode;
	}

	protected ArrayList<UnionFindNode> getSets() {
		return sets;
	}

	// Union-by-size: returns new parent;
	protected UnionFindNode union(UnionFindNode a, UnionFindNode b) {
		if (a.getSize() < b.getSize()) {
			UnionFindNode temp = a;
			a = b;
			b = temp;
		}
		a.setSize(a.getSize() + b.getSize());
		b.setParent(a);
		return a;
	}

	protected UnionFindNode find(UnionFindNode f) {
		if (f.getParent() != null) {
			return find(f.getParent());
		} else {
			return f;
		}
	}

	protected int getCurrentId() {
		return currentId;
	}
}
