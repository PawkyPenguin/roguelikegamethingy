package lazyness;

public class Chance {
	public static boolean oneThird() {
		return Math.random() < 1.0 / 3;
	}

	public static boolean twoThird() {
		return Math.random() < 2.0 / 3;
	}

	public static boolean oneFourth() {
		return Math.random() < 1.0 / 4;
	}

	public static boolean oneHalf() {
		return Math.random() < 1.0 / 2;
	}

	public static boolean threeFourth() {
		return Math.random() < 3.0 / 4;
	}

	public static boolean oneFifth() {
		return Math.random() < 1.0 / 5;
	}

	public static boolean twoFifth() {
		return Math.random() < 2.0 / 5;
	}

	public static boolean threeFifth() {
		return Math.random() < 3.0 / 5;
	}

	public static boolean fourthFifth() {
		return Math.random() < 4.0 / 5;
	}

	public static boolean oneTwentieth() {
		return Math.random() < 1.0 / 20;
	}

	public static boolean oneHundredth() {
		return Math.random() < 1.0 / 100;
	}

	public static boolean oneIn(int chance) {
		return Math.random() < 1.0 / chance;
	}

	public static boolean oneTenth() {
		return Math.random() < 1.0 / 10;
	}
}
